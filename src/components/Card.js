import React,{useState} from 'react';
import {Row,Col,Button} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMinus, faExpandArrowsAlt, faPencilAlt, faRedoAlt,faPlus,faCompressArrowsAlt } from '@fortawesome/free-solid-svg-icons';
import PropTypes from '../utils/propTypes';
import $ from 'jquery';
import FullScreen from 'react-full-screen';
const Card = ({
    title,
    children,
    tag:Tag,
    undo,
    edit,
    ...restProps
})=>{
    const [toggled,setToggled] = useState(false);
    const [fullscreen,setFullScreen] = useState(false);
    let toggle = (event) => {
        if(toggled)
        {
            $(event.target).parents('.cardentry').eq(0).find('.cardbody').eq(0).slideDown();
            setToggled(false);
        }
        else
        {
            $(event.target).parents('.cardentry').eq(0).find('.cardbody').eq(0).slideUp();
            setToggled(true);
        }
    }

    let expand = () => {
        let enable  = !fullscreen;
        setFullScreen(enable);
    }
    
    
    return (
        <FullScreen enabled={fullscreen} onChange={enable=>{setFullScreen(enable)}}>
            <div className="cardentry" style={fullscreen?{margin:20,height:"100%"}:{}}>
                <div className="cardtitle">
                    <Row>
                        <div style={{marginLeft:15}}>
                            <Button color="default" onClick={toggle}>
                                {!toggled && (<FontAwesomeIcon icon={faMinus}></FontAwesomeIcon>)}
                                {toggled && (<FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>)}
                            </Button>
                            <span className="title" style={{marginLeft:10}}>{title}</span>        
                        </div>
                        <Col>
                            <Tag {...restProps}></Tag>
                        </Col>
                        <div style={{marginRight:10}}>
                            <Button color="default" onClick={()=>{if(edit){edit()}}}><FontAwesomeIcon icon={faPencilAlt}></FontAwesomeIcon></Button>
                            <Button color="default" style={{marginLeft:10}} onClick={()=>{if(undo){undo()}}}><FontAwesomeIcon icon={faRedoAlt}></FontAwesomeIcon></Button>
                            <Button color="default" style={{marginLeft:10}} onClick={expand}><FontAwesomeIcon icon={!fullscreen?faExpandArrowsAlt:faCompressArrowsAlt}></FontAwesomeIcon></Button>
                        </div>
                    </Row>
                </div>
                <div className="cardbody">
                    {children}
                </div>
            </div>
        </FullScreen>
    )
}

Card.prototype = {
    title:PropTypes.string,
    tag:PropTypes.component
}



Card.defaultProps = {
    title:'',
    tag:'div'
}

export default Card;
