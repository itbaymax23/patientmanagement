import React from 'react';
import {Row,Col,Input,Button} from 'reactstrap';
import {Scrollbars} from 'react-custom-scrollbars';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperclip } from '@fortawesome/free-solid-svg-icons';
class ChatComponent extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            chats:[{
                name:"jhon",
                body:"Hello \n I am fine and You",
                date:new Date(2019,7,3,11,30,0)
            },
            {
                body:"I am also good \n Thank you",
                date:new Date(2019,7,3,11,31,0)
            }],
            message:""
        }

        this.handleChange.bind(this);
    }

    getbody(param){
        let paramarray = param.split('\n');
        let element = [];
        for(let item in paramarray)
        {
            element.push(<p>{paramarray[item]}</p>);
        }
        return element;
    }
    
    getDate(date)
    {
        let str = "";
        let am = "AM";
        let hour = date.getHours();
        if(hour > 12)
        {
            hour = hour - 12;
            am = "PM";
        }
        let min = date.getMinutes();

        return hour + ":" + min + ":" + am;
    }

   handleChange = (event) => 
   {
       let value = event.target.value;
        this.setState(prevState=>{
            return {
                message:value
            }
        })
   }


   send = () => 
   {
       let chats = this.state.chats;
       if(!chats[chats.length - 1].name)
       {
            chats[chats.length - 1].body += "\n" + this.state.message;
            chats[chats.length - 1].date = new Date();
       }
       else
       {
           let chat = {
               body:this.state.message,
               date:new Date()
           }

           chats.push(chat);
       }

        this.setState(prevState=>{
            return {
                message:"",
                chats:chats
            }
        });
   }

   keypress = (event) => {
        if(event.key == 'Enter')
        {
            this.send();
        }
   }

    render()
    {
        return (
            <Row>
                <Col lg={12} className="chat">
                    <Scrollbars style={{height:300,width:"100%"}}>
                        {
                            this.state.chats.map((row,index)=>{
                                let classname = "chatcontainer_rtl"; let title = "You";
                                if(row.name)
                                {
                                    classname = "chatcontainer"; title = row.name;
                                }

                                return (
                                    <div className={classname}>
                                        <div className="chatelement">
                                            <div className="title_element">{title}</div>
                                            <div className="title_body">
                                                {
                                                    this.getbody(row.body)
                                                }
                                            </div>
                                            <div className="title_date">
                                                {
                                                    this.getDate(row.date)
                                                }
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </Scrollbars>
                </Col>
                <Col className="chatinput">
                    <Row>
                        <Col>
                            <Input placeholder="Type your meessage here" style={{borderRadius:30}} onChange={this.handleChange} value={this.state.message} onKeyDown={this.keypress}></Input>
                        </Col>
                        <div style={{marginRight:10}}>
                            <Button color="primary" style={{borderRadius:30}}><FontAwesomeIcon icon={faPaperclip}></FontAwesomeIcon></Button>
                            <Button color="primary" style={{marginLeft:10}} onClick={this.send}>Send</Button>
                        </div>
                    </Row>
                </Col>
            </Row>
        )
    }
}

export default ChatComponent;