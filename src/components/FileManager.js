import React from 'react';
import {Row,Col,Button,ButtonGroup,Input,InputGroup,InputGroupAddon,Nav,NavItem} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faCopy,faClock,faTrashAlt} from '@fortawesome/free-regular-svg-icons';
import {faWeixin} from '@fortawesome/free-brands-svg-icons';
import {faPlus, faExpandArrowsAlt,faImage, faList, faThList, faFolder} from '@fortawesome/free-solid-svg-icons';
import medium from '../assets/img/icon/medium.png';
import small from '../assets/img/icon/small.png';
import detail from '../assets/img/icon/detail.png';
import content from '../assets/img/icon/content.png';
import note from '../assets/img/icon/note.png';
import {Scrollbars} from 'react-custom-scrollbars';
import FileComponent from '../components/FileComponent';
import ChatComponent from '../components/ChatComponent';
// import FileViewer from 'react-file-viewer';
// import {CustomErrorComponent} from 'custom-error';
class FileManager extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            selected:0,
            document:[
                {title:"X-RAY",doc:"x-ray.png"},
                {title:"Lab Report",doc:"lab.png"},
                {title:"PDF",doc:"1.pdf"},
                {title:"MS-Word",doc:"1.pdf"},
                {title:"MRI FILM",doc:"mri_film.png"},
                {title:"MRI FILM REPORT",doc:"mri_film_report.png"},
                {title:"CT SCAN FILM",doc:"ct_scan_film.png"},
                {title:"CT Scan Report",doc:"CT_SCAN.png"}
            ],
            selectedfile:false,
            error:false
        }
    }

    handleClick(index)
    {
        this.setState({
            selected:index
        });
    }

    componentclick = (row) =>{
      this.setState(prevState => {
        let file = row.doc;
        let type = file.split('.');
        type = type[type.length - 1];
        let selectedfile = row;
        selectedfile.type = type;
        return {
          selectedfile:selectedfile,
          error:false
        }
      })
    }

    onError(e)
    {
      this.setState(prevState=>{
        return {
          selectedfile:false,
          error:"Selected File is not exist"
        }
      })
    }

    render()
    {
        let self = this;
        let string_filter = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        return (
            <Row>
            <Col lg={9}>
              <Row className="icons" style={{margin:0}}>
                <div style={{marginLeft:10}}>
                  <Button color="default"><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                  <Button color="default"><FontAwesomeIcon icon={faTrashAlt}></FontAwesomeIcon></Button>
                  <Button color="default"><FontAwesomeIcon icon={faCopy}></FontAwesomeIcon></Button>
                  <Button color="default"><FontAwesomeIcon icon={faClock}></FontAwesomeIcon></Button>
                  <Button color="default-custom"><FontAwesomeIcon icon={faExpandArrowsAlt}></FontAwesomeIcon><span>Extra Large Icons</span></Button>
                  <Button color="default-custom"><FontAwesomeIcon icon={faImage}></FontAwesomeIcon><span>Large Icons</span></Button>
                  <Button color="default-custom"><img src={medium}></img><span>Medium Icons</span></Button>
                  <Button color="default-custom"><img src={small}></img><span>Small Icons</span></Button>
                  <Button color="default-custom"><FontAwesomeIcon icon={faList}></FontAwesomeIcon><span>List</span></Button>
                  <Button color="default-custom"><img src={detail}></img><span>Details</span></Button>
                  <Button color="default-custom"><FontAwesomeIcon icon={faThList}></FontAwesomeIcon><span>Tiles</span></Button>
                  <Button color="default-custom"><img src={content}></img><span>Content</span></Button>
                  <ButtonGroup>
                    <Button color="primary"><img src={note}></img></Button>
                    <Button color="primary">Users To Do</Button>
                  </ButtonGroup>
                </div>
                <Col>
                  <InputGroup>
                    <Input placeholder="Search Here ..."></Input>
                    <InputGroupAddon>
                      <Button color="primary">Go</Button>
                    </InputGroupAddon>
                  </InputGroup>
                </Col>
              </Row>
              <Row className="text_filter" style={{marginTop:10}}>
                <Col>
                  <ButtonGroup style={{width:"100%"}}>
                    {
                      string_filter.map((row,index)=>{
                        return (
                          <Button color="default">{row}</Button>
                        )
                      })
                    }
                  </ButtonGroup>
                </Col>
              </Row>
              <Row style={{marginTop:20}}>
                <Nav className="sidebar" vertical>
                    {
                        
                        this.props.tabledata.map((row,index)=>{
                            let classname = index == self.state.selected?"active":"";
                            return (<NavItem className={classname} onClick={()=>this.handleClick(index)}><FontAwesomeIcon icon={faFolder} style={{marginRight:10,color:"#f6cc1e"}}></FontAwesomeIcon>{row.name}</NavItem>)
                        })
                    }
                </Nav>
                <Col>
                    <Row className="title">
                       <p>Smith, John(5.12.65) 51-y/o WM, (706)678-4758, (706)401-2478<FontAwesomeIcon icon={faFolder} style={{marginLeft:10,color:"#f6cc1e"}}></FontAwesomeIcon></p>
                       <Col>
                            <Row style={{alignItems:"center"}}>
                                <span style={{marginLeft:"auto"}}>Sort by:</span>
                                <Col lg={4}>
                                    <select className="form-control">
                                        <option value="date">Date</option>
                                    </select>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    
                    <Row style={{marginTop:20}}>
                        <Col lg={12}>
                            <Scrollbars style={{width:"100%",height:600}}>
                              <div className="filemanager_content">
                                {
                                    this.state.document.map((row,index)=>{
                                        return (
                                            <FileComponent name={row.title} filename={row.doc} clickcomponent={()=>{this.componentclick(row)}}></FileComponent>
                                        )                                        
                                    })
                                }
                              </div>
                            </Scrollbars>
                        </Col>
                        {/* <Col lg={12} style={{marginTop:20,marginRight:15}}>
                            <Row style={{margin:0,paddingLeft:15}}>
                              <Col lg={12} className="title" style={{textAlign:"center",fontSize:25,marginLeft:0}}>
                                <FontAwesomeIcon icon={faWeixin} style={{marginRight:10}}></FontAwesomeIcon>
                                Chat
                              </Col>
                              <Col>
                                <ChatComponent/>
                              </Col>
                            </Row>
                        </Col> */}
                    </Row>
                </Col>
              </Row>
            </Col>
            <Col className="fileviewer">
              <Row style={{alignItems:"center",height:"100%"}}>
                  {
                    !this.state.selectedfile &&  !this.state.error && (  
                        <p>Document Viewer/Editor For Selected Thumbnail</p>
                      )                  
                  }
                  {
                    !this.state.selectedfile &&  this.state.error && (  
                        <p>{this.state.error}</p>
                      )                  
                  }
                  {
                    this.state.selectedfile && (
                      <iframe src={this.state.selectedfile.doc}></iframe>
                    )
                  }
              </Row>
            </Col>
          </Row>
        )
    }
}

export default FileManager;