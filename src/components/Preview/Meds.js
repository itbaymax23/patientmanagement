import React from 'react';
import {Row,Col} from 'reactstrap';

class Meds extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    gettitle = () => {
        let array = [];
        let meds = this.props.data.meds;
        
        for(let item in meds)
        {
            let duration = meds[item].duration?'for ' + meds[item].duration + " day":"";
            array.push(meds[item].name + " " + meds[item].dosage + " " + meds[item].sig + " " + duration);
        }

        return array.join(' , ');
    }

    render()
    {
        return (
            <Row>
                <Col lg={3} style={{fontWeight:"bold"}}>
                    <h5 className="title">Current Medications:</h5>
                </Col>
                <Col>
                    <Row>
                        {!this.props.data.freetextenable && (<Col>Include {this.gettitle()}</Col>)}
                    </Row>
                    <Row>
                        {this.props.data.freetextenable && (<Col dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>)}
                    </Row>
                </Col>
            </Row>
        )
    }
}

export default Meds;