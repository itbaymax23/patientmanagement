import React from 'react';
import {Row,Col,Table} from 'reactstrap';
class PO extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <Row>
                <Col lg={12}><h5 className="title">Plan/Orders</h5></Col>
                <Col lg={12}>
                    {
                        this.props.data.action && (
                            <Row>
                                <Col lg={3}>Disposition:</Col>
                                <Col>
                                    {this.props.data.action && (<span><span className="title">Action : </span> <span>{this.props.data.action}</span></span>)}
                                    {this.props.data.location && (<span><span className="title">Location : </span> <span>{this.props.data.location}</span></span>)}
                                    {this.props.data.specificlocation && (<span><span className="title">SpecificLocation : </span> <span>{this.props.data.specificlocation}</span></span>)}
                                </Col>
                            </Row>
                        )
                    }
                    <Row style={{marginTop:20}}>
                        <Col>
                            {this.props.data.diet && (<span style={{marginRight:10}}><span className="title">Type Of Diet</span> : {this.props.data.diet}</span>)}
                            {this.props.data.other && (<span style={{marginRight:10}}><span className="title">Other</span> : {this.props.data.other}</span>)}
                            {this.props.data.cardio && (<span style={{marginRight:10}}><span className="title">Cardiopulmonary Resuscitation</span> : {this.props.data.cardio}</span>)}
                            {this.props.data.medical && (<span style={{marginRight:10}}><span className="title">Medical Interventions</span> : {this.props.data.medical}</span>)}
                            {this.props.data.anti && (<span style={{marginRight:10}}><span className="title">Antibiotics</span> : {this.props.data.anti}</span>)}
                            {this.props.data.affinity && (<span style={{marginRight:10}}><span className="title">Affinitically Administratered Nutrition / Fluid</span> : {this.props.data.affinity}</span>)}
                            {this.props.data.misc && (<span style={{marginRight:10}}><span className="title">Misc</span> : {this.props.data.misc}</span>)}
                        </Col>
                    </Row>
                    
                    <Row style={{marginTop:20}}><Col>Assessments</Col></Row>
                    <Row>
                        <Col>
                            <Table responsive bordered>
                                <thead  className="assessmenttable_header">
                                    <th className="planorderheader">Assessments/Orders</th>
                                    <th>Laboratory</th>
                                    <th>Imaging</th>
                                    <th>Medication</th>
                                    <th>Referrals</th>
                                    <th>Nursing</th>
                                    <th>Instruction</th>
                                </thead>
                                <tbody>
                                    {
                                        this.props.assessments.primary.map((row,index)=>{
                                            return (
                                                <tr key={index}>
                                                    <td><p className="assessmenttable_content" style={{fontStyle:'italic'}}>{row.CommonName?row.CommonName:row.ShortDesc}</p></td>
                                                    <td id={"primary_" + index + "_labs"} className="contenttd" onClick={()=>this.add('primary',index,'labs')}>
                                                        {
                                                            row.labs && row.labs.map((content,index) => {
                                                                return (
                                                                    <p key={index} className="assessmenttable_content">{content.ComonName?content.CommonName:content.BillDesc} ({content.Code})</p>
                                                                )
                                                            })
                                                        }
                                                    </td>
                                                    <td id={"primary_" + index + "_imaging"} className="contenttd" onClick={()=>this.add('primary',index,'imaging')}>
                                                        {
                                                            row.imaging && row.imaging.map((content,index) => {
                                                                return (
                                                                    <p key={index} className="assessmenttable_content">{content.ComonName?content.CommonName:content.BillDesc} ({content.Code})</p>
                                                                )
                                                            })
                                                        }
                                                    </td>
                                                    <td id={"primary_" + index + "_meds"} className="contenttd" onClick={()=>this.add('primary',index,'meds')}>
                                                        {
                                                            row.meds && row.meds.map((content,index)=>{
                                                                return (
                                                                    <p key={index} className="assessmenttable_content">{content.name} ({content.dosage})</p>
                                                                )
                                                            })
                                                        }
                                                    </td>
                                                    <td id={"primary_" + index + "_referal"} className="contenttd" onClick={()=>this.add('primary',index,'referal')}></td>
                                                    <td id={"primary_" + index + "_nursing"} className="contenttd" onClick={()=>this.add('primary',index,'nursing')}></td>
                                                    <td id={"primary_" + index + "_instruction"} className="contenttd" onClick={()=>this.add('primary',index,'instruction')}></td>
                                                </tr>
                                            )
                                        })
                                    }
                                    {
                                        this.props.assessments.secondary.map((row,index)=>{
                                            return (
                                                <tr key={index}>
                                                    <td><p className="assessmenttable_content" style={{fontStyle:'italic'}}>{row.CommonName?row.CommonName:row.ShortDesc}</p></td>
                                                    <td id={"secondary_" + index + "_labs"} className="contenttd" onClick={()=>this.add('secondary',index,'labs')}>
                                                        {
                                                            row.labs && row.labs.map((content,index) => {
                                                                return (
                                                                    <p key={index} className="assessmenttable_content">{content.ComonName?content.CommonName:content.BillDesc} ({content.Code})</p>
                                                                )
                                                            })
                                                        }
                                                    </td>
                                                    <td id={"secondary_" + index + "_imaging"} className="contenttd" onClick={()=>this.add('secondary',index,'imaging')}>
                                                        {
                                                            row.imaging && row.imaging.map((content,index) => {
                                                                return (
                                                                    <p key={index} className="assessmenttable_content">{content.ComonName?content.CommonName:content.BillDesc} ({content.Code})</p>
                                                                )
                                                            })
                                                        }
                                                    </td>
                                                    <td id={"secondary_" + index + "_meds"} className="contenttd" onClick={()=>this.add('secondary',index,'meds')}>
                                                        {
                                                            row.meds && row.meds.map((content,index)=>{
                                                                return (
                                                                    <p key={index} className="assessmenttable_content">{content.name} ({content.dosage})</p>
                                                                )
                                                            })
                                                        }
                                                    </td>
                                                    <td id={'secondary_' + index + '_referal'} className="contenttd" onClick={()=>this.add('secondary',index,'referal')}></td>
                                                    <td id={'secondary_' + index + '_nursing'} className="contenttd" onClick={()=>this.add('secondary',index,'nursing')}></td>
                                                    <td id={'secondary_' + index + '_instruction'} className="contenttd" onClick={()=>this.add('secondary',index,'instruction')}></td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Col>
            </Row>
        )
    }
}

export default PO;