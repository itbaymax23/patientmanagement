import React from 'react';
import HTMLParser from 'html-to-react';
import {Row,Col} from 'reactstrap';

import * as temp from '../../action/templatemodule';
import DynamicContent from '../chattoollayout/DynamicContent';
class CC extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    gettitle = (title) =>{
        let htmlparser = new HTMLParser.Parser();
        let array = title.split(/\{\{DynamicElement data=\'(.*)\'\}\}/g);
        
        let component = [];

        console.log("element_array",array);
        if(array.length == 1)
        {
            component.push( htmlparser.parse(temp.converttemp(array[0],this.props.patient)));
        }

        let last = "";
        if(array[2])
        {
            last = array[2];
        }
        while(array.length > 1)
        {
            let jsondata = array[1].split("'}}")[0];
            component.push(htmlparser.parse(temp.converttemp(array[0],this.props.patient)));
            let id = JSON.parse(jsondata).id;
            component.push(<DynamicContent id={id} dataid={"activity_" + id} patient = {this.props.patient} save={(value)=>this.savedynamic(id,value)} value={this.props.data.dynamic?this.props.data.dynamic[id]:false}></DynamicContent>)
            let data = title.split("{{DynamicElement data='" + jsondata + "'}}");
            if(data.length > 1)
            {
                array = data[1].split(/\{\{DynamicElement data=\'(.*)\'\}\}/g);
            }
            else
            {
                component.push(htmlparser.parse(temp.converttemp(data[0],this.props.patient)));
                break;
            }
        }
        
        component.push(htmlparser.parse(last));
        return component;
        // let htmlparser = new HTMLParser.Parser();
        // return htmlparser.parse(temp.converttemp(title,this.props.patient));
    }
    
    getcomponentfromtextdata = (textdata,index) => {
        let textarray = textdata.split('.');
        let component = [];
        
        for(let item in textarray)
        {
            let component_title = [];
            textarray[item] = textarray[item].replace(/\{\{FreeText data=\"(.*?)\"\}\}/,"  $1");
            if(this.props.data.selected && this.props.data.selected[index] && this.props.data.selected[index].indexOf("" + item) > -1)
            {
                console.log('textdata_array',this.props.data.selected && this.props.data.selected[0] && this.props.data.selected[0].indexOf("" + 0) > -1);
                component_title.push(this.gettitle(textarray[item]));
                component.push(<span>{component_title}.</span>);
            }
        }

        return component;
    }

    render()
    {
        let Tag = 'p';
        if(this.props.data.param == 'paragraph')
        {
            Tag = 'span';   
        }
        let self = this;
        
        return (
            <Row>
                <Col lg={12}>
                    <h5 className="title">Chief Complaint:</h5>
                </Col>
                <Col>
                {
                    !this.props.data.freetext &&  this.props.data.textData.map((row,index)=>{
                        return(
                            <Tag className="default_container main_content" key={index}>
                            {
                                this.getcomponentfromtextdata(row,index)
                            }
                            {
                                index > 0 && self.props.data.problem[index - 1] && self.props.data.problem[index - 1].map((rowvalue,indexvalue)=>{
                                    return (
                                        <span className="value_noborder" key={indexvalue}>
                                            {rowvalue.CommonName?rowvalue.CommonName:rowvalue.ShortDesc}
                                        </span>
                                    )
                                })                       
                            }
                            </Tag>
                        )
                    })
                }
                {
                    (this.props.data.freetext || this.props.data.freetextenable) && (<div dangerouslySetInnerHTML={{__html: this.props.data.freetextdata}}></div>)
                }
                </Col>
            </Row>
        )
    }
}

export default CC;