import React from 'react';
import {Row,Col,FormGroup,Label} from 'reactstrap';
import Moment from 'moment';
class PatientInfo extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <Row>
                <Col>
                    <Row>
                        <Col lg={6} md={6} sm={6} xs={12}>
                            <Row>
                                <Col lg={3}>PATIENT NAME : </Col>
                                <Col>{this.props.patient.FirstName + " " +  this.props.patient.MiddleName + " " +  this.props.patient.LastName}</Col>
                            </Row>
                        </Col>
                        <Col lg={6} md={6} sm={6} xs={12}>
                            <Row>
                                <Col>
                                    <Row>
                                        <Col lg={4}>Provider</Col>
                                        <Col>Kirk DodSon</Col>
                                    </Row>
                                </Col>
                                <Col>
                                    <Row>
                                        <Col lg={4}>Admited : </Col>
                                        <Col>{Moment(this.props.date).format('YYYY-MM-DD')}</Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
            
        )
          
    }
}

export default PatientInfo;