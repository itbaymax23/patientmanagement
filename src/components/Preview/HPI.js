import React from 'react';
import {Row,Col} from 'reactstrap';
import HTMLParser from 'html-to-react';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import DynamicContent from '../chattoollayout/DynamicContent';
import * as temp from '../../action/templatemodule';
//import cookies from 'browser-cookies';
class HPI extends React.Component
{
    editdata = "";
    selectedtarget = "";
    selected_macro = false;

    constructor(props)
    {
        super(props);
        this.state = {
            edititem:false,
            edit:false,
            selectedindex:0,
            textdata:{},
            param:{},
            editrelateddiagnose:false,
            relateddiagnose:[]
        }
    }

   
    
    getcomponentfromtextdata = (textdata,problem) => {
        if(!textdata)
        {
            return [];
        }
        let textarray = textdata.split(' . ');
        let component = [];
        
        for(let item in textarray)
        {
            let component_title = [];
            textarray[item] = textarray[item].replace(/\{\{FreeText data=\"(.*?)\"\}\}/,"  $1");          
            component_title.push(this.gettitle(problem,textarray[item]));
            component.push(<span>{component_title}.</span>);
        }

        return component;
    }

    

    gettitle = (row,title) =>{
        let indexid = row.Id;
        let htmlparser = new HTMLParser.Parser();
        let array = title.split(/\{\{DynamicElement data=\'(.*)\'\}\}/g);
        
        let component = [];

        console.log("element_array",array);
        if(array.length == 1)
        {
            component.push( htmlparser.parse(temp.converttemp(array[0],this.props.patient)));
        }

        let last = "";
        if(array[2])
        {
            last = array[2];
        }
        while(array.length > 1)
        {
            let jsondata = array[1].split("'}}")[0];
            component.push(htmlparser.parse(temp.converttemp(array[0],this.props.patient)));
            let id = JSON.parse(jsondata).id;
            component.push(<DynamicContent dataid={"activity_" + indexid + "_" + id} id={id} patient = {this.props.patient}  value={row.value && row.value[id]?row.value[id]:false}></DynamicContent>)
            let data = title.split("{{DynamicElement data='" + jsondata + "'}}");
            if(data.length > 1)
            {
                array = data[1].split(/\{\{DynamicElement data=\'(.*)\'\}\}/g);
            }
            else
            {
                component.push(htmlparser.parse(temp.converttemp(data[0],this.props.patient)));
                break;
            }
        }
        
        component.push(htmlparser.parse(last));
        return component;
        // let htmlparser = new HTMLParser.Parser();
        // return htmlparser.parse(temp.converttemp(title,this.props.patient));
    }
    
   
    render()
    {   
        let self = this;
        return (
            <Row>
                <Col lg={12}>
                    <h5 className="title">History Of Present Illness:</h5>
                </Col>
                <Col lg={12}>
                {
                        !this.props.data.freetextenable && this.props.data.problem.map((row,index)=>{
                            return(
                                <Row className="default_container">
                                    <div className="main_content">
                                        <span className="title" onClick={()=>this.props.problemgrid()} style={{cursor:"pointer"}}>{row.CommonName?row.CommonName:row.ShortDesc} ({row.Code})</span>
                                        {
                                            row.relateddiagnosis && row.relateddiagnosis.map((value,indexvalue)=>{
                                                return (
                                                    <p className={indexvalue}>{value.CommonName?value.CommonName:value.ShortDesc} ({value.Code})</p>
                                                )
                                            })
                                        }
                                        {this.state.param[row.Id] != "paragraph" && (<p className="freetext">{this.getcomponentfromtextdata(row.textdata?row.textdata:"",row)}</p>)}
                                        {this.state.param[row.Id] == 'paragraph' && (<div className="freetext" style={{display:"inline-block"}}>{this.getcomponentfromtextdata(row,row.textdata?row.textdata:"")}</div>)}
                                    </div>
                                </Row>
                            )
                        })
                    }
                    {
                        (!this.props.data.freetextenable && this.props.data.freetext) && (
                            <p dangerouslySetInnerHTML={{__html:this.props.data.freetext}}></p>
                        )
                    }
                    {
                        this.props.data.freetextenable && (
                            <p dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></p>
                        )
                    }
                </Col>
            </Row>
        )
    }
}

export default HPI;