import React from 'react';
import {Row,Col} from 'reactstrap';

class VS extends React.Component
{
    constructor(props)
    {
        super(props);
        console.log("VS",props);
    }

    gettitle = (row) => {
        let array = ['Date : ' + row.date];
        if(!row.bpfirst)
        {
            row.bpfirst = 0;
        }

        if(!row.bpsecond)
        {
            row.bpsecond = 0;
        }

        array.push("BP : " + row.bpfirst + " / " + row.bpsecond);

        if(row.pulse)
        {
            array.push("PULSE : " + row.pulse);
        }

        if(row.resp)
        {
            array.push("RESP : " + row.resp);
        }

        if(row.temp)
        {
            array.push('TEMP : ' + row.temp);
        }

        let height = "";

        if(row.heightft)
        {
            height = row.heightft + 'ft ';
        }

        if(row.heightin)
        {
            height += row.heightinch + 'in';
        }

        if(height)
        {
            array.push("HEIGHT : " + height);
        }

        if(row.weight)
        {
            array.push("WEIGHT : " + row.weight);
        }

        if(row.bmi)
        {
            array.push('BMI : ' + row.bmi);
        }

        return array.join(' , ');
    }

    render(){
        let self = this;
        return (
            <Row>
                <Col lg={12}>
                    <h5 className="title">Vital Signs</h5>
                </Col>
                <Col lg={12}>
                    {
                        !this.props.data.freetextenable && this.props.data.data && this.props.data.data.map((row,index) => {
                            return (
                                <Row key={index}>
                                    <Col>{self.gettitle(row)}</Col>
                                </Row>
                            )
                        })
                    }
                    {
                        this.props.data.freetextenable && (
                            <Row>
                                <Col dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>
                            </Row>
                        )
                    }
                </Col>
            </Row>
        )
    }
}

export default VS;