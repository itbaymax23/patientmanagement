import React from 'react';
import {Row,Col} from 'reactstrap';

class ROS extends React.Component
{
    constructor(props)
    {
        super(props)
        
    }

    getelementtitle = (icdelement) => {
        return icdelement.CommonName ? icdelement.CommonName:icdelement.ShortDesc;
    }

    renderparagraph = ()=>{
        var list = [];
        for(let item in this.props.data.data)
        {
            if(this.props.data.data[item].positive.length > 0 || this.props.data.data[item].list.length > 0)
            {
                list.push(<span>In {this.props.data.data[item].Name} Patient reports </span>);
            }
            else
            {
                continue;
            }
            var positive = this.gettitle(this.props.data.data[item].positive,item);
            
            if(positive.length > 0)
            {
                list.push(<span> As Positive </span>)
                list.push(<span className="reviewproblem positive">{positive}</span>)
                if(this.props.data.data[item].list.length > 0)
                {
                    list.push(<span> but reports As negative </span>);
                }
            }

            for(let itemlist in this.props.data.data[item].list)
            {
                list.push(<span> no <span className="reviewproblem specific"><span><span>{this.getelementtitle(this.props.data.data[item].list[itemlist])}</span></span></span></span>)                    
            }
        }

        return list;
    }

    gettitle = (positive,sectionId) => {
        let titlelist = [];
        let self = this;
        positive.map((list,index)=>{
            if(index > 0)
            {
                titlelist.push(' , ');
            }
            titlelist.push(<span id={"section" + sectionId + "_" + list.Id}>{self.getelementtitle(list)} {list.description?'(' + list.description + ')':''}</span>)
        })

        return titlelist;
    }

    getinitdata = (initdata,list,subsectionId) => {
        var listelement = [];
        let self = this;

        var listdata = ['Negative for ',''];
        listelement = [listdata[0]];
        if(initdata)
        {
            var listdata = initdata.split('{{ItemList}}');
            listelement = [listdata[0]];
        }
        
        list.map((listrow,index)=>{
            if(index > 0)
            {
                listelement.push(' , ');
            }
            listelement.push(<span>{self.getelementtitle(listrow)}</span>)
        }) 

        listelement.push(listdata[1]);
        return listelement;
    }

    render()
    {
        let self = this;
        console.log(this.props.data.data);
        return (
            <Row>
                <Col lg={12}>
                    <h5 className="title">Review Of Systems:</h5>
                </Col>   
                <Col lg={12}>
                    <Row>
                        <Col>
                        {
                            (this.props.data.param != 'paragraph' && !this.props.data.freetextenable) && Object.keys(this.props.data.data).map((keyName,i)=>{
                                if(self.props.data.data[keyName].perHPI || self.props.data.data[keyName].positive.length > 0 || self.props.data.data[keyName].list.length > 0){
                                return (
                                    <Row className="reviewcontent">
                                        <Col lg={2} md={2} sm={3} xs={4}>
                                            {self.props.data.data[keyName].Name}
                                        </Col>
                                        <Col>
                                            {!self.props.data.data[keyName].perHPI && self.props.data.data[keyName].positive.length > 0 && (
                                                <p className="reviewproblem positive">Positive for {self.gettitle(self.props.data.data[keyName].positive,keyName)}</p>
                                            )}
                                            {
                                                !self.props.data.data[keyName].perHPI && self.props.data.data[keyName].list.length > 0 && (
                                                    <p className="reviewproblem specific">{self.getinitdata(self.props.data.data[keyName].initdata,self.props.data.data[keyName].list,keyName)}</p>
                                                )
                                            }
                                            {
                                                self.props.data.data[keyName].perHPI && (<p>* Per HPI</p>)
                                            }
                                            
                                        </Col>
                                    </Row>
                                )
                            }
                            })
                        }
                        {
                            (!this.props.data.freetextenable && this.props.data.param == 'paragraph') && (
                                <div className="reviewcontent">
                                    {this.renderparagraph()}
                                </div>
                            )
                        }
                        {
                            this.props.data.freetextenable && (
                            <p dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></p>
                            )
                        }
                        </Col>        
                    </Row>
                </Col>
            </Row>
            
        )
             
    }
}

export default ROS;