import React from 'react';
import {Row,Col} from 'reactstrap';

class ADR extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    gettitle = () => {
        let array = [];
        let adr = this.props.data.adversereaction;
        for(let item in adr)
        {
            array.push(adr[item].name + " " + adr[item].reactiontype + " caused by " + adr[item].reaction);
        }

        return array.length>0?array.join(' , '):'No Known ADRs';
    }

    render()
    {
        return (
            <Row>
                <Col lg={3}>
                    <h5 className="title">Adverse Drug Reaction:</h5>
                </Col>
                {
                    !this.props.data.freetextenaable && (
                        <Col>
                            {
                                this.gettitle()
                            }
                        </Col>
                    )
                }
                
                {
                    this.props.data.freetextenaable && (
                        <Col dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>
                    )
                }
            </Row>
        )
    }
}

export default ADR;