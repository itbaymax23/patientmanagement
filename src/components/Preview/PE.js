import React from 'react';
import {Row,Col} from 'reactstrap';

class PE extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    checkparam = (data) => {
        let enable = false;
        for(let item in data.list)
        {
            if(!data.list[item].ParentNodeId)
            {
                if(data.list[item].text)
                {
                    enable = true;
                }
            }
        }

        return enable;
    }

    render()
    {
        return (
            <Row>
                <Col lg={12}><h5 className="title">Physical Exam</h5></Col>
                <Col>
                    <Row>
                        <Col lg={12}>
                        {
                        (!this.props.PE.freetextenable && !this.props.PE.freetextview && this.props.PE.param != 'paragraph') && Object.keys(this.props.PE.data).map((keyName,index)=>{
                            if(this.checkparam(this.props.PE.data[keyName]))
                            {
                                return(
                                    <Row className="signentry">
                                        <Col lg={2} className="description">{this.props.PE.data[keyName].Name}</Col>
                                        <Col>
                                        {
                                            this.props.PE.data[keyName].list.map((item,indexitem)=>{
                                                if(!item.ParentNodeId && item.text)
                                                {
                                                    return (
                                                        <Row className="description">
                                                            <Col>
                                                                <div className="description-container">
                                                                    <span className="param" style={{textDecoration:'underline'}}>{item.Name}</span>
                                                                    <span className="param">{item.text}</span>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    )
                                                }
                                            })
                                        }
                                        </Col>
                                    </Row>
                                )
                            }
                        })
                        }
                        {
                            (!this.props.PE.freetextenable && !this.props.PE.freetextview && this.props.PE.param == 'paragraph') && (
                                Object.keys(this.props.PE.data).map((keyName,index)=>{
                                    if(this.checkparam(this.props.PE.data[keyName]))
                                    {
                                        return (
                                            <span key={index}>
                                                <span className="title">{this.props.PE.data[keyName].Name}</span>
                                                {
                                                    this.props.PE.data[keyName].list.map((item,indexitem)=>{
                                                        if(!item.ParentNodeId && item.text)
                                                        {
                                                            return (
                                                                <span key={indexitem}>
                                                                    <span className="itemname">{item.Name}</span> - 
                                                                    <span>{item.text}</span>
                                                                </span>
                                                            )
                                                        }
                                                    })
                                                }
                                            </span>
                                        )
                                    }
                                })
                            )
                        }
                        </Col>
                        {(!this.props.PE.freetextenable && this.props.PE.freetextview) && (
                            <Col lg={12} dangerouslySetInnerHTML={{__html:this.props.PE.freetext}}></Col>
                        )}
                        {
                            this.props.PE.freetextenable && (
                                <Col lg={12} dangerouslySetInnerHTML={{__html:this.props.PE.freetextdata}}></Col>
                            )
                        }
                    </Row>
                </Col>  
            </Row>
        )
    }
}

export default PE;