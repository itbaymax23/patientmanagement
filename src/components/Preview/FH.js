import React from 'react';
import {Row,Col,Table} from 'reactstrap';
class FH extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    gettitle = () => {
        let data = this.props.data.data;
        
        console.log('fh_relation',data);
        if(!data)
        {
            return '';
        }
        
        let array = [];
        for(let item in data)
        {
            let title = "Patient's " + data[item].relation + " " + data[item].name;

            if(data[item].livestatus == 'Decreased')
            {
                title += " dead ";

                if(data[item].causeofdeath)
                {
                    title += 'cause of ' + data[item].causeofdeath;
                }
            }
            else{
                if(data[item].illness)
                {
                    title += " was ill cause of " + data[item].illness;
                }
                else 
                {
                    title = "Patient has " + data[item].relation + "," + data[item].name;
                }
            }

            array.push(title);
        }

        return array.join(' , ');
    }
    
    getheaders = () => {
        let data = this.props.data;
        let field = {relation:false,name:false,livestatus:false,causeofdeath:false,otherillness:false,notes:false}
        for(let item in data.data)
        {
            for(let fielditem in field)
            {
                if(data.data[item][fielditem])
                {
                    field[fielditem] = true;
                }
            }
        }

        return field;
    }

    render()
    {
        let field = this.getheaders();
        return (
            <Row>
                <Col lg={12}><h5 className="title">Family History:</h5></Col>
                <Col>
                    {
                        (!this.props.data.freetextenable && this.props.param != 'paragraph') && (
                            <Table responsive>
                                <thead style={{borderColor:'white'}}>
                                    {
                                        field.relation && (
                                            <th style={{borderColor:'white'}}>Relation</th>
                                        )
                                    }
                                    {
                                        field.name && (
                                            <th style={{borderColor:'white'}}>Name</th>
                                        )
                                    }
                                    {
                                        field.livestatus && (
                                            <th style={{borderColor:'white'}}>Status</th>
                                        )
                                    }
                                    {
                                        field.causeofdeath && (
                                            <th style={{borderColor:'white'}}>COD</th>
                                        )
                                    }

                                    {
                                        field.otherillness && (
                                            <th style={{borderColor:'white'}}>Other Illnesses</th>
                                        )
                                    }

                                    {
                                        field.notes && (
                                            <th style={{borderColor:'white'}}>Notes</th>
                                        )
                                    }
                                </thead>
                                <tbody>
                                    {
                                        this.props.data.data.map((row,index)=>{
                                            return (
                                                <tr style={{borderColor:'white'}}>
                                                    <td style={{borderColor:'white'}}>{row.relation}</td>
                                                    <td style={{borderColor:'white'}}>{row.name}</td>
                                                    <td style={{borderColor:'white'}}>{row.livestatus}</td>
                                                    <td style={{borderColor:'white'}}>{row.causeofdeath}</td>
                                                    <td style={{borderColor:'white'}}>{row.otherillness}</td>
                                                    <td style={{borderColor:'white'}}>{row.notes}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </Table>
                        )
                    }
                    {
                        this.props.data.freetextenable && (
                            <p dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></p>
                        )
                    }
                    {
                        (!this.props.data.freetextenable && this.props.data.param == 'paragraph') && (
                            <Row>
                                <Col>{this.gettitle()}</Col>
                            </Row>
                        )
                    }
                </Col>
            </Row>
        )
        
    }
}

export default FH;