import React from 'react';
import {Row,Col} from 'reactstrap';

class SH extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    gettitle = () => {
        
        let str_negative = [];
        let str_positive = [];

        for(let item in this.props.data.substance)
        {
            let row = this.props.data.substance[item];

            if(this.props.data.data.substance[row.Name] == undefined || this.props.data.data.substance[row.Name].status == undefined || this.props.data.data.substance[row.Name].status == "Never")
            {
                str_negative.push(row.Name);
            }
            else
            {
                var str = this.props.data.data.substance[row.Name].status + " use ";
                if(this.props.data.data.substance[row.Name].type)
                {
                    str += this.props.data.data.substance[row.Name].type.join(',');
                }
                if(this.props.data.data.substance[row.Name].agestart)
                {
                    str += " from " + this.props.data.data.substance[row.Name].agestart
                }

                if(this.props.data.data.substance[row.Name].agestop)
                {
                    str += " Until " + this.props.data.data.substance[row.Name].agestop;
                }

                

                var enable = false;
                for(let itemunit in row.units)
                {
                    if(this.props.data.data.substance[row.Name]['unit'] == row.units[itemunit].Id)
                    {
                        str += this.props.data.data.substance[row.Name]['amount'] + " " + row.units[itemunit].UnitText + " / " + row.units[itemunit].UnitPeriod;
                        enable = true;
                    }
                }

                if(!enable && this.props.data.data.substance[row.Name].amount)
                {
                    str += " " + this.props.data.data.substance[row.Name].amount;

                    if(row.units.length > 0)
                    {
                        str += " " + row.units[0].UnitText + " / " + row.units[0].UnitPeriod;
                    }
                }
                str_positive.push(str);
            }
        }

        let totalstr = "";
        if(str_negative.length)
        {
            totalstr += "No " + str_negative.join(' , ') + " use";
        }

        totalstr += "   ";

        if(str_positive.length)
        {
            totalstr += str_positive.join(',');
        }

        return totalstr;
    }

    getsubtitle = () => {
        let array = [];
        console.log('socialarray',this.props.data.data);
        for(let item in this.props.data.social)
        {
            let name = this.props.data.social[item].Name;

            if(this.props.data.data[name])
            {
                array.push(name + " - " + this.props.data.data[name]);
            }
        }
        if(array.length == 0)
        {
            return 'Primary Language should be English';
        }
        else
        {
            return array.join(' ; '); 
        }
    }

    render()
    {
        let socialarray = [];
        for(let item in this.props.data.social)
        {
            socialarray.push()
        }
        return (
            <Row>
                <Col lg={3}><h5 className="title">Social History:</h5></Col>
                {
                    !this.props.data.freetextenable && (
                        <Col>
                            <Row>
                                <Col lg={12}>
                                    {this.gettitle()}
                                </Col>
                                <Col lg={12}>
                                    {this.getsubtitle()}
                                </Col>
                            </Row>
                        </Col>
                    )
                }
                {
                    this.props.data.freetextenable && (
                        <Col dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>
                    )
                }
            </Row>
        )
    }
}

export default SH;