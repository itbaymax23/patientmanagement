import React from 'react';
import {Row,Col} from 'reactstrap';

class Assessments extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    

    render()
    {
        let socialarray = [];
        for(let item in this.props.data.social)
        {
            socialarray.push()
        }
        return (
            <Row>
                <Col lg={12}><h5 className="title">Assessments</h5></Col>
                {
                    !this.props.data.freetextenable && (
                     <Col>
                        <Row>
                            <Col>Primary Diagnosis(es)</Col>
                            <Col>Secondary Diagnosis(es)</Col>
                        </Row>
                        <Row>
                            <Col>
                                {
                                    this.props.data.primary.map((row,index)=>{
                                        return (
                                            <Row key={index}>
                                                <Col>{row.CommonName?row.CommonName:row.ShortDesc} - {row.Code}</Col>
                                            </Row>
                                        )
                                    })
                                }
                            </Col>
                            <Col>
                                {
                                    this.props.data.secondary.map((row,index)=>{
                                        return (
                                            <Row key={index}>
                                                <Col>{row.CommonName?row.CommonName:row.ShortDesc} - {row.Code}</Col>
                                            </Row>
                                        )
                                    })
                                }
                            </Col>
                        </Row>
                     </Col>
                    )
                }
                {
                    this.props.data.freetextenable && (
                        <Col dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>
                    )
                }
            </Row>
        )
    }
}

export default Assessments