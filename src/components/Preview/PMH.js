import React from 'react';
import {Row,Col} from 'reactstrap';

class PMH extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <Row>
            <Col lg={12}><h5 className="title">Past Medication History:</h5></Col>
            {
                !this.props.data.freetextenable && (
                    <Col>
                    {
                        this.props.data.problem.map((row,index)=>{
                            let value_index = index + 1;
                            return(
                                <Row className="default_container">
                                    <Col className="main_content">
                                        <span className="number">{value_index} )</span>
                                        <div className="title">
                                            {row.CommonName?row.CommonName:row.ShortDesc} ({row.Code})
                                            {(!this.props.data.param || this.props.data.param[row.Id] != "paragraph") && (<p className="freetext" dangerouslySetInnerHTML={{__html:this.props.data.textdata?this.props.data.textdata[row.Id]:''}}></p>)}
                                            {this.props.data.param && this.props.data.param[row.Id] == 'paragraph' && (<div className="freetext" style={{display:"inline-block"}} dangerouslySetInnerHTML={{__html:this.props.data.textdata?this.props.data.textdata[row.Id]:''}}></div>)}
                                        </div>
                                    </Col>
                                </Row>
                            )
                        })
                    }
                    {
                        this.props.data.freetext && (
                            <p dangerouslySetInnerHTML={{__html:this.props.data.freetext}}></p>
                        )
                    }
                    </Col>
                )
            }
            {
                this.props.data.freetextenable && (
                    <Col dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>
                )
            }
            </Row>
        )
    }
    
}

export default PMH;