import React from 'react';
import {Row,Col} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCheck} from '@fortawesome/free-solid-svg-icons';

class PSH extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <Row>
                <Col lg={12}><h5 className="title">Past Surgical History: </h5></Col>
                {
                    !this.props.data.freetextenable && (
                        <Col lg={12}>
                            {
                                this.props.data.problem.map((row,index)=>{
                                    let value_index = index + 1;
                                    return (
                                        <Row key={index}>
                                            <Col className="main_content">
                                                <span className="number">{value_index} )</span>
                                                <div className="title">{row.CommonName?row.CommonName:row.BillDesc} ({row.Code})</div>
                                            </Col>
                                        </Row>   
                                    )
                                })
                            }
                        </Col>
                    )
                }
                {
                    this.props.data.freetextenable && (
                        <Col dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>
                    )
                }
                
            </Row>
        )
    }
}

export default PSH;