import React from 'react';
import DynamicElement from '../chattoollayout/DynamicElement';

class ContentEditable extends React.Component
{
    html = "";
    constructor(props)
    {
        super(props);
        this.state = {
            updated:false,
            html:props.html
        }
        this.html = props.html;
    }

    splitelement = (title) => {
        console.log('title',title);
        var reg = /\[(.*?)\]/;
        var array = title.split(reg);
        let component = [];
        for(let item in array)
        {
            if(item % 2 == 1)
            {
                component.push(<DynamicElement caption={array[item]}></DynamicElement>);
            }
            else
            {
                component.push(array[item]);
            }
        }

        return component;
    }
    
    rendertextdata = (value) => 
    {
        var myReg = /\[(.*?)\]/g;
        value = value.replace(myReg,'<span class="dynamicelement" contenteditable="false">[$1]</span>');
        console.log(value);
        return value;
    }
    render()
    {
        return (
            <div 
            ref="contentedit"
            onInput={(e)=>this.emitChange(e.target.value)} 
            onBlur={this.emitChange}
            contentEditable
            dangerouslySetInnerHTML={{__html:this.rendertextdata(this.state.html)}}
            >
                {/* {this.splitelement(this.props.html)} */}
            </div>
        )
    }

    getDOMNode = () => {
        return this.refs.contentedit;
    }

    componentDidUpdate()
    {
        this.html = this.getDOMNode().innerHTML;
    }

    // shouldComponentUpdate(nextprops)
    // {
    //     return this.html !== this.getDOMNode().innerHTML;
    // }

    emitChange = (value) => {
        
        var html = this.getDOMNode().innerHTML;

        if (this.props.onChange && html !== this.html) {

            this.props.onChange({
                target: {
                    value: html
                }
            });
            
            this.setState({
                html:html
            })
            this.refs.contentedit.focus();
        }
        this.html = html;
    }
}

export default ContentEditable;