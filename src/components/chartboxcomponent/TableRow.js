import React from 'react';
import {Row,Col} from 'reactstrap';
import PropTypes from '../../utils/propTypes';

const TableRow = ({
    tablerow,
    defaultclassname,
    count,
    ...restprops
})=>{
    let rows = [];
    for(var index = 0;index<count;index++)
    {
        rows.push(index);
    }
    return (
    <div className="tablerow">
    {
        rows.map((value,index)=>{
            return (
                <div className="cell">
                    {
                        tablerow[value] != undefined && (
                            <p className={tablerow[value] && tablerow[value].classname?tablerow[value].classname:defaultclassname}>
                                {
                                    tablerow[value].value?tablerow[value].value:''
                                }
                            </p>
                        )
                    }
                    
                </div>
            )
        })
    }       
    </div>)
}

TableRow.prototype = {
    tablerow:PropTypes.component,
    defaultclassname:PropTypes.String
}

TableRow.defaultProps = {
    tablerow:[],
    defaultclassname:'tablebodytext'
}

export default TableRow;

