import React from 'react';
import PropTypes from '../../utils/propTypes';
import {Col,Row} from 'reactstrap';


class codingComponent extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    save = (value,index,itemindex) => {
        if(this.props.action)
        {
            let data = this.props.data;
            let problemlist = ['problem','data','manageoption']
            if(!data[problemlist[index]])
            {
                data[problemlist[index]] = {};
            }

            if(!data[problemlist[index]][itemindex])
            {
                data[problemlist[index]][itemindex] = [];
            }

            data[problemlist[index]][itemindex].push(value);
            this.props.save(data);
        }
        
    }

    render()
    {
        let self = this;
        let problemlist = ['problem','data','manageoption']
        return (
            <Row>
            <Col lg={12}>
                <Row  className="codingheader">
                    {
                        this.props.componentheader.map((row,index)=>{
                            return (
                                <Col>{row}</Col>
                            )
                        })
                    }
                </Row>
                {
                    this.props.componentbody.map((rowbody,index)=>{
                        return (
                            <Row className="codingbody">
                                {
                                    rowbody.map((element,itemindex)=>{
                                        if(!Array.isArray(element)){
                                           element = [element];
                                        }

                                        return (
                                            <Col>
                                            {
                                                element.map((value)=>{
                                                    return (
                                                        <p className={(self.props.action && self.props.data[problemlist[index]] && self.props.data[problemlist[index]][itemindex] && self.props.data[problemlist[index]][itemindex].indexOf(value) > -1)?"emcodingitem specific":"emcodingitem"} style={{marginTop:10}} onClick={()=>self.save(value,index,itemindex)}>{value}</p>
                                                    )
                                                })
                                            }
                                            </Col>
                                            )
                                    })
                                }
                            </Row>
                        )
                    })
                }
            </Col>
            
        </Row>
        )
    }
}


export default codingComponent;