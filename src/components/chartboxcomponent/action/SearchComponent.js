import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit,faArrowRight,faSearch, faPlus} from '@fortawesome/free-solid-svg-icons';
import {Row,Col,Button,InputGroup,Input,InputGroupAddon} from 'reactstrap';
class SearchComponent extends React.Component
{
    value = "";
    handlesearch  = (value) => {
        this.value  = value;
    }
    searchrun = (keycode) => {
        if(keycode == 13)
        {
            if(this.props.search)
            {
                this.props.search(this.value);
            }
        }
    }
    
    render(){
        return (
            <Col>
                 <Row>
                    <Col lg={5} md={5} sm={6} xs={6} style={{marginLeft:"auto"}}>
                        <InputGroup>
                            <Input placeholder="Start Typing To search" onChange={(e)=>this.handlesearch(e.target.value)} onKeyDown={(e)=>this.searchrun(e.keyCode)}></Input>
                            <InputGroupAddon addonType="append" id="searchcontainer">
                                <Button color="primary" onClick={() => this.props.search(this.value)}><FontAwesomeIcon icon={faSearch}></FontAwesomeIcon></Button>
                                <Button color="primary" style={{marginLeft:10}} onClick={()=>{if(this.props.freetext){this.props.freetext()}}}>T</Button>
                                <Button color="default" style={{marginLeft:10}} onClick={()=>{if(this.props.add){this.props.add()}}}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                            </InputGroupAddon>
                        </InputGroup>
                    </Col>
                    <Col lg={6} md={6} sm={6} xs={6}>
                        <Row>
                            <div style={{marginLeft:"auto"}}>
                                <Button color="default">History</Button>
                                <Button color="default" style={{marginLeft:10}} onClick={()=>{if(this.props.prescribe){this.props.prescribe()}}}>E-Prescribe</Button>
                                <Button color="default" style={{marginLeft:10}} onClick={()=>{if(this.props.refillall){this.props.refillall()}}}>Refill All</Button>
                                <Button color="default" style={{marginLeft:10,marginRight:10}}><FontAwesomeIcon icon={faArrowRight}></FontAwesomeIcon></Button>                       
                            </div>
                        </Row>    
                    </Col>
                </Row>
            </Col>
           
        )
    }
}

export default SearchComponent;