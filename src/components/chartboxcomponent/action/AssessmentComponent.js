import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faArrowDown, faArrowUp, faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

class AssessmentComponent extends React.Component
{
    render()
    {
       return(
       <Col className="assessment">
            <Row className="assessment_header">
                <Col>
                    <Row>
                        <Col className="title"><span>Primary Diagnosis(es)</span></Col>
                        <div className="actions">
                            <span className="icons"><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></span>
                            <span className="icons"><FontAwesomeIcon icon={faArrowDown} style={{marginLeft:20}}></FontAwesomeIcon></span>
                            <span className="icons"><FontAwesomeIcon icon={faArrowUp} style={{marginLeft:20}}></FontAwesomeIcon></span>
                        </div>
                    </Row>
                </Col>
                <div style={{width:80}}></div>
                <Col>
                    <Row>
                        <Col className="title"><span>Secondary Diagnosis(es)</span></Col>
                        <div className="actions">
                            <span className="icons"><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></span>
                            <span className="icons"><FontAwesomeIcon icon={faArrowDown} style={{marginLeft:20}}></FontAwesomeIcon></span>
                            <span className="icons"><FontAwesomeIcon icon={faArrowUp} style={{marginLeft:20}}></FontAwesomeIcon></span>
                        </div>
                    </Row>
                </Col>
            </Row>
            <Row className="assessment_body">
                <Col>
                    <Row>
                        <Col lg={8} md={8} sm={10} xs={12} style={{marginLeft:"auto"}}>
                            <Row style={{alignItems:"flex-end"}}>
                                <Col>
                                    <Row><Col style={{textAlign:"center"}}><span>New</span></Col></Row>
                                    <Row style={{textAlign:"center"}}>
                                        <Col>+w/u</Col>
                                        <Col>-w/u</Col>
                                    </Row>
                                </Col>
                                <span>exec</span>
                                <Col>
                                    <Row><Col style={{textAlign:"center"}}><span>Est</span></Col></Row>
                                    <Row style={{textAlign:"center"}}>
                                        <Col>std</Col>
                                        <Col>min</Col>
                                    </Row>
                                </Col>                            
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <div style={{width:80,margin:"0px"}}>
                    <Row style={{alignItems:"center"}}>
                        <Col lg={12} style={{textAlign:"center"}}><Button color="default"><FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon></Button></Col>
                        <Col lg={12} style={{marginTop:10,textAlign:"center"}}><Button color="default"><FontAwesomeIcon icon={faArrowRight}></FontAwesomeIcon></Button></Col>
                    </Row>
                </div>
                <Col>
                    <Row>
                        <Col lg={8} md={8} sm={10} xs={12} style={{marginLeft:"auto"}}>
                            <Row style={{alignItems:"flex-end"}}>
                                <Col>
                                    <Row><Col style={{textAlign:"center"}}><span>New</span></Col></Row>
                                    <Row style={{textAlign:"center"}}>
                                        <Col>+w/u</Col>
                                        <Col>-w/u</Col>
                                    </Row>
                                </Col>
                                <span>exec</span>
                                <Col>
                                    <Row><Col style={{textAlign:"center"}}><span>Est</span></Col></Row>
                                    <Row style={{textAlign:"center"}}>
                                        <Col>std</Col>
                                        <Col>min</Col>
                                    </Row>
                                </Col>                            
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Col>)
    }
}

export default AssessmentComponent;