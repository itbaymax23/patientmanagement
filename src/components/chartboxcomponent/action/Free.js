import React from 'react';
import {Row,Button,Col} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faBook,faEdit} from '@fortawesome/free-solid-svg-icons';
class Free extends React.Component
{
    constructor(props)
    {
        console.log(props);
        super(props);
    }

    render(){
        return(
            <Col>
                <Row>
                    <div style={{margin:"auto"}}>
                        <Button color="primary" onClick={()=>this.props.resetView("paragraph")}>P</Button>
                        <Button color="primary" style={{marginLeft:20}}  onClick={()=>this.props.resetView("line")}>L</Button>
                        <Button color="primary" style={{marginLeft:20}}  onClick={()=>{if(this.props.editable){this.props.editable()}}}>T</Button>
                        <Button color="default" style={{marginLeft:20}} onClick={()=>{if(this.props.macro){this.props.macro()}}}><FontAwesomeIcon icon={faBook}></FontAwesomeIcon></Button>
                    </div>
                </Row>
            </Col>
        )
    }
}

export default Free;