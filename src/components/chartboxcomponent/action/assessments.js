import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit,faBook} from '@fortawesome/free-solid-svg-icons';
class Assessments extends React.Component
{
    render()
    {
        return (
            <Col>
                <Row>
                    <div style={{margin:"auto"}}>
                        <Button color="primary">L</Button>
                        <Button color="primary" style={{marginLeft:10}} onClick={this.props.addfreetext}>T</Button>
                        <Button color="default" style={{marginLeft:10}} onClick={this.props.showmacro}><FontAwesomeIcon icon={faBook}></FontAwesomeIcon></Button>
                    </div>
                </Row>
            </Col>            
        )
    }
}

export default Assessments;