import React from 'react';
import PropTypes from '../../../utils/propTypes';
import {Row, Col} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from '@fortawesome/free-solid-svg-icons'
import { faCircle } from '@fortawesome/free-regular-svg-icons';
const DetailPage = ({
    title,
    lefticon,
    value,
    righticon,
    righticonstyle,
    className,
    left_class,
    ...restProps
  })=>{
      return (
          <div className="main_content">
              {lefticon && (
                  <span className={left_class}><FontAwesomeIcon icon={lefticon}></FontAwesomeIcon></span>
              )}
              {
                  title && (
                      <span className="title">{title}</span>
                  )
              }
              {
                  value.map((row,i)=>
                      <span className={className}>{row}</span>
                  )
              }
              {
                  righticon &&
                  (
                      <span className="righticon" style={righticonstyle}><FontAwesomeIcon icon={righticon}></FontAwesomeIcon></span>
                  )
              }
          </div>
      )
  }

  DetailPage.propTypes = {
    title:PropTypes.string,
    refticon:PropTypes.component,
    className:PropTypes.string,
    righticon:PropTypes.component,
    value:PropTypes.component,
    righticonstyle:PropTypes.component
  };
  
  DetailPage.defaultProps = {
    title:'',
    className:'value',
    righticon:faPlus,
    lefticon:faCircle,
    righticonstyle:{},
    value:[],
    left_class:"icon"
  };
  
  export default DetailPage;