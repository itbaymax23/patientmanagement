import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit,faPlus,faBook} from '@fortawesome/free-solid-svg-icons';
class BasicAction extends React.Component
{
    render(){
        return(
            <Col>
                <Row>
                    <div style={{margin:"auto"}}>
                        <Button color="default" onClick={(e)=>{if(this.props.basichx)this.props.basichx(e);}}>Basic HX</Button>
                        <Button color="default" style={{marginLeft:20}}  onClick={(e)=>{if(this.props.completehx)this.props.completehx(e);}}>Complete Hx</Button>
                        <Button color="primary" style={{marginLeft:20}} onClick={()=>{if(this.props.freetext)this.props.freetext()}}>T</Button>
                        <Button id={this.props.id?this.props.id:""} color="default" style={{marginLeft:20}} onClick={()=>{if(this.props.add){this.props.add()}}}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                        <Button color="default" style={{marginLeft:20}} onClick={()=>{if(this.props.macro){this.props.macro()}}}><FontAwesomeIcon icon={faBook}></FontAwesomeIcon></Button>
                    </div>
                </Row>
            </Col>
        )
    }
}

export default BasicAction;