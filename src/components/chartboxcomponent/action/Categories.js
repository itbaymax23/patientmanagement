import React from 'react';
import {Row,Col,Button,Nav,NavItem} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAlignLeft,faAlignRight} from '@fortawesome/free-solid-svg-icons';
class Categories extends React.Component
{
    constructor(props)
    {
        super(props);
        
        this.state = {
            open:0,
            style:{
                textAlign:"left"
            }
        }
    }
   

    align = (value)=>
    {
       this.props.changealign(value);
    }

    handleClick = (index) => 
    {
        this.setState(prevState=>{
            return {
                open:index
            }
        })
    }

    render()
    {
        let self = this;
        return (
            <Row className="diagnostic_category">
                <Col>
                    <Row className="category_header">
                        <span>Categories</span>
                        <div style={{marginLeft:"auto"}}>
                            <Button onClick={()=>{this.align("left")}} color={this.props.align == "left"?"primary":"default"}><FontAwesomeIcon icon={faAlignLeft}></FontAwesomeIcon></Button>
                            <Button onClick = {()=>{this.align("right")}} color={this.props.align == "right"?"primary":"default"} style={{marginLeft:10}}><FontAwesomeIcon icon={faAlignRight}></FontAwesomeIcon></Button>
                        </div>
                    </Row>
                    <Row>
                        <Col className="category_body">
                            <Nav vertical style={this.state.style}>
                                <NavItem className={!this.props.selected?"active":""} onClick={()=>this.props.selectcategory(false)}>Show All</NavItem>
                                {
                                    this.props.category.map((row,index)=>{
                                        if(row.type == self.props.type)
                                        {
                                            return (
                                                <NavItem key={index} className={row.Id == this.props.selected?"active":""} onClick={()=>{this.props.selectcategory(row.Id)}}>
                                                    <span>{row.Name}</span>
                                                </NavItem>
                                            )
                                        }
                                        
                                    })
                                }
                            </Nav>
                        </Col>
                    </Row>
                </Col>
            </Row>
        )
    }
}

export default Categories;