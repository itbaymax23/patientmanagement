import React from 'react';
import Category from './Categories';
import {Row,Col,Table} from 'reactstrap';
class DiagnosticContent extends React.Component
{
    state = {
        data:[
            [
                "General", ["Weight","waist","BMI","SBP","Pulse","Temp"],[''],[''],[''],[''],[''],[''],[''],[''],[''],[''],[''],['18.5~24.9']
            ],
            [
                "Chemistries", ["Sodium","Potassium","Chloride","Co2","Bun","Cr","Glucose","Protein, total","Albumin","Globulin, total","Billiburin","Alkarin Phospatase","Ast(SGOT)","ALT(SGPT)","Mg","Phosporus"],
                ['','','','','','1.2'],[''],['','','','','','1.21'],[''],[''],[''],[''],['','','','','','1.56','140'],['','','','','','1.44','100'],
                ['144','4.4','102','25','16','1.36','91','9.3','6.4','4.2',2.2,0.5,67,22,21],['','','','','','1.27'],
                ['134~144 mmol/h','3.5~5mmol/h','96~106 mmol/h','18~29 mmol/h','6~24 mmol/h','0.76~127 mmol/h','6~24 mg/dl','0.76~1.27 mg/dl']
            ]
        ],
        header:[{value:"Date",size:2},
        {value:"1.2.18"},
        {value:"5.3.18"},
        {value:"9.4.18"},
        {value:"10.4.18"},
        {value:"25.4.18"},
        {value:"8.8.18"},
        {value:"10.12.18"},
        {value:"15.12.18"},
        {value:"18.12.18"},
        {value:"25.12.18"},
        {value:"1.1.19"},
        {value:"Reference"}]
    }
    render()
    {
        let self = this;
        return (
            <Row>
                <Col lg={3} md={3} sm={4} xs={6} style={{maxWidth:'20%'}}>
                    <Category/>
                </Col>
                <Col lg={9} md={9} sm={8} xs={6} style={{maxWidth:'80%'}}>
                    <Table responsive bordered hover>
                        <thead>
                            {
                               this.state.header.map((row,index)=>{
                                   return (
                                       <th colSpan={row.size}>{row.value}</th>
                                   )
                               })
                            }
                        </thead>
                        <tbody>
                            {
                                this.state.data.map((row,index)=>{
                                    let count = row[1].length;
                                    console.log(count);
                                    const elements = [];
                                    for(let index = 0;index<count;index++)
                                    {
                                        elements.push(
                                            <tr>
                                            {
                                                index == 0 && (
                                                    <td rowSpan={count} colSpan="1" style={{verticalAlign:"middle"}}><span>{row[0]}</span></td>
                                                )
                                            }
                                            {
                                                row.map((rowdata,indexdata)=>{
                                                    if(indexdata != 0 && !(indexdata == row.length - 1 && rowdata.length == 1))
                                                    {
                                                        return (
                                                            <td colSpan="1">{rowdata[index]}</td>
                                                        )
                                                    }
                                                })
                                            }
                                            {
                                                index == 0 && row[row.length - 1].length == 1 &&
                                                (
                                                    <td rowSpan={count} style={{verticalAlign:"middle",textAlign:"center"}}>{row[row.length - 1]}</td>
                                                )
                                            }
                                            </tr>
                                        );
                                    }
                                    
                                    return elements;
                                })
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
        )
    }
}

export default DiagnosticContent;