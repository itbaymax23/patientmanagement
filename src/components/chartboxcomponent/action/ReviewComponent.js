import React from 'react';
import {Row,Col,Button,NavLink} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import PropType from '../../../utils/propTypes';
const ReviewComponent = ({
    reviewrow,
    classname,
    ...restprops
})=>{
    return (
        <Row className="reviewcontent">
           <Col lg={2} md={2} sm={3} xs={4}>
               {reviewrow[0]}
           </Col>
           <Col className={classname?classname:''}>
                {reviewrow[1]}
           </Col>
           <Col lg={2} md={2} sm={3} xs={4}>
               <div style={{marginLeft:"auto"}}>
                    <span className="default">Default</span>
                    <span className="default" style={{marginLeft:20}}>Per HPI</span>
                    <Button color="secondary" style={{marginLeft:20,padding:"3px 6px",fontSize:10}}><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></Button>
               </div>
           </Col>
       </Row>
    )
}

ReviewComponent.prototype = {
    reviewrow : PropType.component,
    classname:PropType.String
}

ReviewComponent.defaultProps = {
    reviewrow:[],
    classname:''
}

export default ReviewComponent;