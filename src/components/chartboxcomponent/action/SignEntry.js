import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo, faTimes } from '@fortawesome/free-solid-svg-icons';
import {Row,Col,Button} from 'reactstrap';

class SignEntry extends React.Component
{
    state = {
        entry:[
            {
                cons:"Constitutional",description:[["Appearance"]]
            },
            {
                cons:"HEENT",description:[["Head/Face","Normocephalic and atraumatic"],["Eyes","PERRLA, scleare clear"],["Ears","Ears without lesions or deformities"]]
            },
            {
                cons:"Resipiratory",description:[["Respiratory Effort","Inspection of Chest","Auscultation","Palpation of chest"]]
            },
            {
                cons:"Cadiovascular",description:[["Auscultation","Palpation of chest","Peripherial Vasucular System"]]
            },
            {
                cons:"Gastrointestinal",description:[["Abdominial Inspection","AuscultationPalpation / PercussionLiver and spleenHerniasRectal"]]
            },
            {
                cons:"Genitourinary",description:[["Ext. Genitalia / Vagina","Uretha","Bladder","CervixUterus Adnexa"]]
            },
            {
                cons:"Lymphatic",description:[["Neck","Axilla","Groin","Other"]]
            },
            {
                cons:"Musculoskeletal",description:[["Head and Neck","Spine/Back","Ribs","Hibs / Pelvis","Upper Extremities","Lower Extremeties"]]
            },
            {
                cons:"Skin",description:[["Inspection","Palpation"]]
            },
            {
                cons:"Neurologic",description:[["Mental Status Exam","Cranial nerves","Reflexes","Sensation","Gait and Station","Motor function"]]
            },
            {
                cons:"Psychiatric",description:[["Judgement and Insight","Mood and Affect","Through Processes"]]
            }
        ]
    }

    render()
    {
        return (
            <Col>
            {
                this.state.entry.map((row,index)=>{
                    return(
                        <Row className="signentry">
                            <Col lg={2}>{row.cons}</Col>
                            <Col>
                            {
                                row.description.map((rowdesc,indexdesc)=>{
                                    return (
                                        <Row className="description">
                                            <Col>
                                                <div className="description-container">
                                                {
                                                    rowdesc.map((desc,realindex)=>{
                                                        return (
                                                            <span className="param">{desc}</span>
                                                        )
                                                    })
                                                }
            
                                                {
                                                    row.description.length > 1 && (
                                                        <Button color="default">Expand</Button>
                                                    )
                                                }
                                                </div>
                                            </Col>
                                            
                                        </Row>
                                    )
                                    
                                })
                            }
                            </Col>
                            <Col lg={2}>
                                <Row>
                                    <Col><Button color="primary"><FontAwesomeIcon icon={faRedo}></FontAwesomeIcon></Button></Col>
                                    <Col><Button color="secondary"><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></Button></Col>
                                </Row>
                            </Col>
                        </Row>
                    )
                })        
            }
            </Col>
        )
        
    }
}

export default SignEntry;