import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit,faPlus,faBook} from '@fortawesome/free-solid-svg-icons';
import {Row,Col,Button} from 'reactstrap';

class Reaction extends React.Component
{   
    render(){
        return (
            <Col>
                <Row>
                    <div style={{margin:"auto"}}>
                        <Button color="primary" onClick={()=>{if(this.props.addfreetext){this.props.addfreetext()}}}>T</Button>
                        <Button color="default" style={{marginLeft:10}} onClick={()=>{if(this.props.addmedicallist){this.props.addmedicallist()}}}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                        <Button color="default" style={{marginLeft:10}} onClick={()=>{if(this.props.macro){this.props.macro()}}}><FontAwesomeIcon icon={faBook}></FontAwesomeIcon></Button>                        
                    </div>
                </Row>
            </Col>
        )
    }
}

export default Reaction;