import React from 'react';
import {Button,Row,Col,Input,InputGroup,InputGroupAddon} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
import {DateTimePicker,DatePicker} from 'react-widgets';
import {Checkbox,CheckboxGroup} from 'react-ui-icheck';
class ExamComponent extends React.Component
{
    initdata = {
       
    };

    getstring = (date) => {
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
    }
    constructor(props)
    {
        super(props);
        this.state = {
            updated:false
        }
        if(!props.data || !props.date)
        {
            this.initdata = {
                date:this.getstring(new Date()),
                pulseox:95
            }
        }
        else
        {
            this.initdata = props.data;
        }

        console.log("initdata",this.initdata);
    }
    
    save = (date,name) => {
        this.initdata[name] = date;
        this.props.save(this.initdata);
    }

    handleChange = (value,name) => {
        console.log(value);
        this.initdata[name] = value;

        let heightft = this.initdata['heightft'];
        let heightinch = this.initdata['heightinch'];

        let weight = this.initdata['weight'];
        if(Number.isNaN(heightft))
        {
            heightft = 0;
        }

        if(!heightinch || Number.isNaN(heightinch))
        {
            heightinch = 0;
        }

        if(!weight || Number.isNaN(weight))
        {
            weight = 0;
        }
        
        this.initdata.bmi = this.calcbmi(heightft + " ft " + heightinch + " inch",weight);
        if(!this.initdata.diagnosis)
        {
            this.initdata.diagnosis = {};
        }
        this.initdata.diagnosis.data = this.getdiagnosis();
    }

    savechange = () => {
        this.props.save(this.initdata);
    }
    componentWillReceiveProps(props)
    {
        if(!props.data || !props.data.date)
        {
            this.initdata = {
                date:this.getstring(new Date()),
                pulseox:95
            }
        }
        else{
            this.initdata = props.data;
        }
        
        this.setState({
            updated:!this.state.updated
        })
    }

    calculatebmi = () => {
        console.log('initdata',this.initdata);
        if(!this.initdata.bmi || this.initdata.bmi < 19)
        {
            return 'UW';
        }
        else if(this.initdata.bmi >= 19 && this.initdata.bmi <25)
        {
            return 'Ideal';
        }
        else if(this.initdata.bmi >= 25 && this.initdata.bmi <30)
        {
            return 'OW';
        }
        else if(this.initdata.bmi >= 30 && this.initdata.bmi < 40)
        {
            return 'OB';
        }
        else if(this.initdata.bmi >= 40)
        {
            return 'MO';
        }
    }

    calcbmi = (height,weight) => {
        let heightarray = height.split(' ');
        let feet = heightarray[0];
        let inch = heightarray[2];

        let meter = Number(feet) / 3.281 + Number(inch) / 39.37;
        
        console.log("meter",inch);
        if(meter == 0)
        {
            return 0;
        }

        let bmi = (Number(weight) / (meter * meter * 2.2)).toFixed(2);

        
        return bmi;
    }

    handlediagnosis = (value) => {
        if(this.getdiagnosis())
        {
            this.initdata.diagnosis = {data:this.getdiagnosis(),added:value};
            this.props.save(this.initdata);
        }
        
        
    }

    getdiagnosis = () => {
        if(!this.initdata.bmi || this.initdata.bmi < 19)
        {
            return {Code:'R63.6',CommonName:'Under Weight',Id:"exam_R63.6"};
        }
        else if(this.initdata.bmi >= 19 && this.initdata.bmi <25)
        {
            return false;
        }
        else if(this.initdata.bmi >= 25 && this.initdata.bmi <30)
        {
            return {Code:'E66.3',CommonName:'Overweight ',Id:"exam_E66.3"};
        }
        else if(this.initdata.bmi >= 30 && this.initdata.bmi < 40)
        {
            return {Code:'R66.09',CommonName:'Obesity ',Id:"exam_R66.09"};
        }
        else if(this.initdata.bmi >= 40)
        {
            return {Code:'R66.01',CommonName:'Morbid obesity ',Id:"exam_R66.01"};
        }
    }

    render(){ 
        Moment.locale('en');
        momentLocalizer();
        return(
            <div className="examrow">
                <div className="action element"><Button color="secondary" style={{padding:'3px 6px',marginTop:"50%",fontSize:8,verticalAlign:"middle"}} onClick={()=>this.props.delete(this.props.index)}><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></Button></div>
                <div className="datetime element">
                    <Row>
                        <Col>
                            <DatePicker 
                                placeholder="Date"
                                defaultValue={new Date(this.initdata.date)}
                                onChange={(value)=>this.save(this.getstring(value),'date')}
                            >
                            </DatePicker>
                        </Col>
                    </Row>
                </div>
                <div className="datetime element">
                    <Row>
                        <Col>
                            <DateTimePicker placeholder="Time" date={false} defaultValue={new Date(this.initdata.date)} onChange={(value)=>this.save(this.getstring(value),'date')}></DateTimePicker>
                        </Col>
                    </Row>
                </div>
                <div className="bp element">
                    <Row style={{alignItems:'center'}}>
                        <Col><Input className="form-control" defaultValue={this.initdata.bp} onChange={(e)=>this.handleChange(e.target.value,"bpfirst")} onBlur={(e)=>this.savechange()}></Input></Col>
                        <div>/</div>
                        <Col><Input className="form-control" defaultValue={this.initdata.bp} onChange={(e)=>this.handleChange(e.target.value,"bpsecond")} onBlur={(e)=>this.savechange()}></Input></Col>
                    </Row>
                </div>
                <div className="pulse element"><Input className="form-control" defaultValue={this.initdata.pulse} onChange={(e)=>this.handleChange(e.target.value,'pulse')} onBlur={(e)=>this.savechange()}></Input></div>
                <div className="resp element"><Input className="form-control" defaultValue={this.initdata.resp} onChange={(e)=>this.handleChange(e.target.value,'resp')} onBlur={(e)=>this.savechange()}></Input></div>
                <div className="temp element"><Input className="form-control" defaultValue={this.initdata.temp}  onChange={(e)=>this.handleChange(e.target.value,'temp')} onBlur={(e)=>this.savechange()}></Input></div>
                <div className="height element">
                    <Row>
                        <Col style={{paddingRight:0}}>
                            <InputGroup>
                                <Input defaultValue={this.initdata.heightft}  onChange={(e)=>this.handleChange(e.target.value,'heightft')} onBlur={(e)=>this.savechange()}></Input>
                                <InputGroupAddon addonType="append">
                                    <select className="form-control">
                                        <option value="fit">ft</option>
                                    </select>
                                </InputGroupAddon>
                            </InputGroup>
                        </Col>
                        <Col>
                            <InputGroup>
                                <Input defaultValue={this.initdata.heightinch}  onChange={(e)=>this.handleChange(e.target.value,'heightinch')} onBlur={(e)=>this.savechange()}></Input>
                                <InputGroupAddon addonType="append">
                                    <select className="form-control">
                                        <option value="fit">in</option>
                                    </select>
                                </InputGroupAddon>
                            </InputGroup>
                        </Col>
                    </Row>
                </div>
                <div className="weight element">
                    <Row>
                        <Col>
                            <InputGroup>
                                <Input defaultValue={this.initdata.weight} onChange={(e)=>this.handleChange(e.target.value,'weight')} onBlur={(e)=>this.savechange()}></Input>
                                <InputGroupAddon addonType="append">
                                    <select className="form-control">
                                        <option value="lbs">lbs</option>
                                    </select>
                                </InputGroupAddon>
                            </InputGroup>
                        </Col>
                    </Row>
                </div>
                <div className="bmi element">
                    <Row style={{alignItems:'center'}}>
                        <Col>
                            <Input defaultValue={this.initdata.bmi} placeholder="<19" onChange={(e)=>this.handleChange(e.target.value,'bmi')} onBlur={(e)=>this.savechange()}></Input>
                        </Col>
                        <div style={{paddingRight:15}}>
                            <CheckboxGroup
                                checkboxWrapClassName="form-check"
                                checkboxWrapTag="div"
                                >
                                <Checkbox
                                    checkboxClass="icheckbox_square-blue"                                    
                                    label={this.calculatebmi()}
                                    onChange = {(value)=>this.handlediagnosis(this.getdiagnosis(),value)}
                                />
                            </CheckboxGroup>
                        </div>
                    </Row>
                </div>
                <div className="pulseox element">
                    <Row>
                        <Col>
                            <InputGroup>
                                <Input defaultValue={this.initdata.pulseox}></Input>
                                <InputGroupAddon addonType="append">
                                    <select className="form-control">
                                        <option value="roomair">room air</option>
                                        <option value="1 liter">1 liter</option>
                                        <option value="2 liters">2 liters</option>
                                        <option value="3 liters">3 liters</option>
                                        <option value="4 liters">4 liters</option>
                                        <option value="5 liters">5 liters</option>
                                        <option value="50% venti-mask">50% venti-mask</option>
                                        <option value="100% non-rebreather">100% non-rebreather</option>
                                    </select>
                                </InputGroupAddon>
                            </InputGroup>
                        </Col>
                    </Row>
                </div>
                <div className="action element"></div>
            </div>
        )}
}

export default ExamComponent;