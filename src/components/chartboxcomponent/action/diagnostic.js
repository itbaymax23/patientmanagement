import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFileArchive} from '@fortawesome/free-regular-svg-icons';
import {faBriefcase} from '@fortawesome/free-solid-svg-icons';
import circle from '../../../assets/img/icon/icon_pic.png';
class Diagnostic extends React.Component
{
    render()
    {
        return(
            <Col>
                <Row>
                    <div style={{margin:"auto"}}>
                        <Button color={this.props.type == 'lab'?'primary':'default'} style={this.props.type == 'lab'?{background:"#1e4c92"}:{}} onClick={()=>this.props.selecttype('lab')}><FontAwesomeIcon icon={faFileArchive}></FontAwesomeIcon></Button>
                        <Button color={this.props.type == 'imaging'?'primary':'default'} style={this.props.type == 'imaging'?{marginLeft:10,background:"#1e4c92"}:{marginLeft:10}} onClick={()=>this.props.selecttype("imaging")}><img src={circle}/></Button>
                        <Button color="default" style={{marginLeft:10,color:"grey"}}><FontAwesomeIcon icon={faBriefcase}></FontAwesomeIcon></Button>
                    </div>
                </Row>
            </Col>
        )        
    }
}

export default Diagnostic;