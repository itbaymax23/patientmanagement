import React from 'react';
import {Col,Button,Row} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faPlus, faAngleLeft,faAngleRight } from '@fortawesome/free-solid-svg-icons';
import ExamComponent from './ExamComponent';
import $ from 'jquery';

class EditExam extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            exam:[{}]
        }
    }
    prev(e)
    {
        var element = $(e.target).parents('.editexam').eq(0).find('.editexamcontainer').eq(0);
        var left = element.attr("left");
        if(!left)
        {
            left = 0;
        }
        else
        {
            left = left.split("px")[0];
        }
        var rect = element[0].getClientRects()[0];
        var rect_parent = element.parent()[0].getClientRects()[0];
        
        element.animate({
            left: Math.min(left + rect_parent.width,0)
        },300);
       
    }

    next(e)
    {
        var element = $(e.target).parents('.editexam').eq(0).find('.editexamcontainer').eq(0);
        var left = element.attr("left");
        if(!left)
        {
            left = 0;
        }
        else
        {
            left = left.split("px")[0];
        }
        var rect = element[0].getClientRects()[0];
        var rect_parent = element.parent()[0].getClientRects()[0];

        var min = rect_parent.width - rect.width;
        
        console.log(min);
        if(min < 0)
        {
            element.animate({
                left: Math.max(left - rect_parent.width,min)
            },300);
        }
    }

    addcomponent = () => {
       let dataelement = this.props.data;
       if(!dataelement)
       {
            dataelement = [];
       }
       
       dataelement.push({});
       this.props.save(dataelement);
    }
    
    delete = (index) => {
        if(index)
        {
            let dataelement = this.props.data;
            dataelement.splice(index,1);
            this.props.save(dataelement);
        }
        
    }

    save = (data,index) => {
        let dataelement = this.props.data;
        if(!dataelement)
        {
            dataelement = [];
        }

        if(index != undefined)
        {
            dataelement[index] = data;
        }
        else
        {
            dataelement.push(data);
        }
        
        let diagnosis = [];
        let idarray = [];
        for(let item in dataelement)
        {
            if(dataelement[item].diagnosis && dataelement[item].diagnosis.data && dataelement[item].diagnosis.added && idarray.indexOf(dataelement[item].diagnosis.data.Id) == -1)
            {
                idarray.push(dataelement[item].diagnosis.data.Id);
                diagnosis.push(dataelement[item].diagnosis.data)
            }
        }

        let diag = this.props.diagnose;
        let item = 0;
        console.log("idarray",idarray);
        while(diag.primary[item])
        {
            if(idarray.indexOf(diag.primary[item].Id) > -1)
            {
                idarray.splice(idarray.indexOf(diag.primary[item].Id),1);
                diagnosis.splice(idarray.indexOf(diag.primary[item].Id),1);
                item ++;
            }
            else if((diag.primary[item].Id + "").split('exam_').length > 1)
            {
                console.log(diag.primary[item].Id);
                diag.primary.splice(item,1);
            }
            else
            {
                item ++;
            }
        }
        
        item = 0;
        while(diag.secondary[item])
        {
            if(idarray.indexOf(diag.secondary[item].Id) > -1)
            {
                idarray.splice(idarray.indexOf(diag.secondary[item].Id),1);
                diagnosis.splice(idarray.indexOf(diag.secondary[item].Id),1);
                item ++;
            }
            else if((diag.secondary[item].Id + "").split('exam_') > 1)
            {
                diag.secondary.splice(item,1);
            }
            else
            {
                item ++;
            }
        }

        diag.primary = diag.primary.concat(diagnosis);
        console.log("examcomp",diag);
        this.props.savediagnosis(diag)
        this.props.save(dataelement);
    }

    render(){
        return (

                <div className="editexam">
                    <div className="editexamcontainer">
                        <div className="examheader">
                            <div className="action element"><span onClick={this.addcomponent}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></span></div>
                            <div className="datetime element title"></div>
                            <div className="datetime element title"></div>
                            <div className="bp element title">BP</div>
                            <div className="pulse element title">Pulse</div>
                            <div className="resp element title">Resp</div>
                            <div className="temp element title">Temp</div>
                            <div className="height element title">Height</div>
                            <div className="weight element title">Weight</div>
                            <div className="bmi element title">BMI</div>
                            <div className="pulseox element title">Pulse Ox</div>
                            <div className="action element"><span onClick={this.addcomponent}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></span></div>
                        </div>
                        <div className="exambody">
                            {
                                this.props.data && this.props.data.length > 0 && this.props.data.map((row,index)=>{
                                     return (
                                        <ExamComponent key={index} delete = {this.delete} index={index} data={row} save={(data)=>this.save(data,index)} adddiagnosis = {(diagnose)=>this.props.add(diagnose)}/>
                                     )                                  
                                })
                            }
                            {
                                (!this.props.data || this.props.data.length == 0) && (
                                    <ExamComponent save={(data)=>this.save(data)}></ExamComponent>
                                )
                            }
                            
                        </div>
                    </div>
                    <div className="examfooter" style={{position:"relative",bottom:30}}>
                        <div style={{margin:"auto"}}>
                            <span className="prev" onClick={this.prev}><FontAwesomeIcon icon={faAngleLeft}></FontAwesomeIcon></span>
                            <span className="next" onClick={this.next}><FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon></span>
                        </div>
                    </div>
                </div>
            
        )
    }
}

export default EditExam;