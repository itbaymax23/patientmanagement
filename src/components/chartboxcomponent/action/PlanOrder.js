import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faBook, faPrint } from '@fortawesome/free-solid-svg-icons';
import { faCalendar } from '@fortawesome/free-regular-svg-icons';

class PlanOrder extends React.Component
{
    render()
    {
        return (
            <Col>
                <Row>
                    <div style={{margin:"auto"}}>
                        <Button color="primary">L</Button>
                        <Button color="primary" style={{marginLeft:10}} onClick={()=>{if(this.props.freetext){this.props.freetext()}}}>
                            T
                        </Button>
                        <Button color="default" style={{marginLeft:10}}  onClick={()=>{if(this.props.showmacro){this.props.showmacro()}}}>
                            <FontAwesomeIcon icon={faBook}></FontAwesomeIcon>
                        </Button>
                        <Button color="default" style={{marginLeft:10}} onClick={()=>{if(this.props.previewsection){this.props.previewsection();}}}>
                            <FontAwesomeIcon icon={faPrint}></FontAwesomeIcon>
                        </Button>
                    </div>
                </Row>
            </Col>
        )
    }
}

export default PlanOrder;