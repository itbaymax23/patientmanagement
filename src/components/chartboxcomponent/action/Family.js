import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
class Family extends React.Component
{
    constructor(props)
    {
        super(props);
    }
    render()
    {
        return(
            <Col>
                <Row>
                    <div style={{margin:"auto"}}>
                        <Button color={this.props.showtype == 'line'?"primary":"default"} onClick={()=>this.props.showtree()}>L</Button>
                        <Button color={this.props.showtype == 'tree'?"primary":"default"} style={{marginLeft:10}} onClick={()=>this.props.showtree()}>Tree</Button>
                        <Button color="default" style={{marginLeft:10,marginRight:10}} onClick={()=>{if(this.props.add){this.props.add()}}}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                    </div>
                </Row>
            </Col>
        )
    }
}

export default Family;