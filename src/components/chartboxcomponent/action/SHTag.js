import React from 'react';
import {Row,Button,Col} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faBook,faEdit} from '@fortawesome/free-solid-svg-icons';
class SHTag extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render(){
        return(
            <Col>
                <Row>
                    <div style={{margin:"auto"}}>
                        <Button color="primary" onClick={()=>this.props.resetView("paragraph")}>P</Button>
                        <Button color="primary" style={{marginLeft:20}}  onClick={()=>this.props.resetView("line")}>L</Button>
                    </div>
                </Row>
            </Col>
        )
    }
}

export default SHTag;