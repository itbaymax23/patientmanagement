import React from 'react';
import  ReviewComponent from './ReviewComponent';
import PropTypes from '../../../utils/propTypes';
import {Col} from 'reactstrap';
const ReviewData = ({
    reviewdata,
    ...restprops
}) => {
    return (
        <Col>
            {
                reviewdata.map((row,index)=>{                
                    return (
                        <ReviewComponent reviewrow={row.value} classname={row.classname}></ReviewComponent>
                    )
                })
            }
        </Col>
    )
}

ReviewData.prototype = {
    reviewdata:PropTypes.component
}

ReviewData.defaultProps = {
    reviewdata:[]
}

export default ReviewData;