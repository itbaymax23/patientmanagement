import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFileAlt} from '@fortawesome/free-regular-svg-icons';
import {faEdit,faBook} from '@fortawesome/free-solid-svg-icons';

class ReviewSide extends React.Component
{
    render()
    {
        return (
            <Col>
                <Row>
                    <div style={{margin:"auto"}}>
                        <Button color="primary" onClick={()=>this.props.setviewparam('paragraph')}>P</Button>
                        <Button color="primary" style={{marginLeft:10}} onClick={()=>this.props.setviewparam('line')}>L</Button>
                        <Button color="primary" style={{marginLeft:10}} onClick={()=>{if(this.props.editable){this.props.editable()}}}>T</Button>
                        <Button color="default" style={{marginLeft:10}} onClick={()=>{if(this.props.showmacro){this.props.showmacro(this.props.sectionId);}}}><FontAwesomeIcon icon={faBook}></FontAwesomeIcon></Button>
                    </div>
                </Row>
            </Col>
        )
    }
}

export default ReviewSide;