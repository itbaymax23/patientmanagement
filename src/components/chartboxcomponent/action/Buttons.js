import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope} from '@fortawesome/free-regular-svg-icons';
import alerticon from '../../../assets/img/icon/alert.png';
import foldericon from '../../../assets/img/icon/folder.png';
import docicon from '../../../assets/img/icon/doc.png';
import {Button,Col} from 'reactstrap';
class ActionButtons extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render(){
        return (
            <Col>
                <Button color="primary"  style={{marginLeft:10}}><FontAwesomeIcon icon={faEnvelope} style={{marginRight:10}}></FontAwesomeIcon>New Message/Task</Button>
                <Button color="primary"  style={{marginLeft:10}}><img src={docicon} style={{marginRight:10}}></img>Plan/Orders</Button>
                <Button color="primary" style={{marginLeft:10}}><img src={foldericon}  style={{marginRight:10}}></img> Documents</Button>
                <Button color="primary" style={{marginLeft:10}}> FlowSheets</Button>
                <Button color="primary"  style={{marginLeft:10}}>Health Maintenance</Button>
                <Button color="primary"  style={{marginLeft:10}}>Summary</Button>
            </Col>

        )
    }
}

export default ActionButtons;