import React from 'react';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.base.css';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css';
import JqxScheduler,{jqx,ISchedulerProps} from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxscheduler';
import {Row,Col,Button,ButtonGroup,InputGroup,Input,UncontrolledPopover,PopoverHeader,PopoverBody,Label,FormGroup,Form,InputGroupAddon,InputGroupText} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarAlt,  faSlidersH, faAngleLeft, faCaretLeft, faCaretRight } from '@fortawesome/free-solid-svg-icons';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Moment from 'react-moment';
class Scheduler extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            appointment:[
                {id:"id1",start:new Date(2019,7,2,10,0,0),end:new Date(2019,7,2,11,30,0),calendar:"Dan Smith",subject:"10.0 - 11.30"},
                {id:"id2",start:new Date(2019,7,2,10,20,0),end:new Date(2019,7,2,11,0,0),calendar:"Kate Smith",subject:"10.20 - 11.0"},
                {id:"id3",start:new Date(2019,7,2,10,0,0),end:new Date(2019,7,2,11,30,0),calendar:"Sparah Lee", subject:"10.0 - 11.30"},
                {id:"id5",start:new Date(2019,7,2,11,0,0),end:new Date(2019,7,2,12,0,0),calendar:"Paul Harris",subject:"11.0 - 12.0"},
                {id:"id6",start:new Date(2019,7,2,11,0,0),end:new Date(2019,7,2,12,0,0),calendar:"Leon Kenedy",subject:"11.0 - 12.0"},
                {id:"id7",start:new Date(2019,7,2,12,0,0),end:new Date(2019,7,2,14,0,0),calendar:"Jhon Doe",subject:"12.0 - 2.0"},
                {id:"id8",start:new Date(2019,7,2,12,0,0),end:new Date(2019,7,2,14,0,0),calendar:"Ada Wong",subject:"12.0 - 2.0"},
                {id:"id9",start:new Date(2019,7,2,13,30,0),end:new Date(2019,7,2,15,55,0),calendar:"Dan Smith",subject:"1.30 - 3.55"},
                {id:"id10",start:new Date(2019,7,2,13,30,0),end:new Date(2019,7,2,15,55,0),calendar:"Sparah Lee",subject:"1.30 - 3.55"},
                {id:"id11",start:new Date(2019,7,2,13,0,0),end:new Date(2019,7,2,14,0,0),calendar:"Kate Smith",subject:"1.00 - 2.00"},
                {id:"id12",start:new Date(2019,7,2,14,0,0),end:new Date(2019,7,2,15,0,0),calendar:"Paul Harris",subject:"2.00 - 3.00"}
              ],
              views:[
                {type:"dayView",workTime:{fromHour:10,toHour:20},showWeekends:false, timeRuler: { hidden: false }},{type:"weekView",workTime:
                {
                  fromDayOfWeek: 1,
                  toDayOfWeek: 5,
                  fromHour: 7,
                  toHour: 19
                }},{type:"monthView"}
              ],
              datafield:{
                  from: "start",
                  id: "id",
                  resourceId: "calendar",
                  subject: "subject",
                  to: "end",
                  resizable:true
              },
              view:"dayView",
              resourceview:false,
              displaydate:false,
              date : new Date(),
              from:new Date(),
              to:new Date(),
              fromdisplay:false,
              todisplay:false
        }        
    }
    handleeditdialog = (dialog,fields,editAppointment) => 
    {
        console.log(fields);
        fields.locationLabel.hide()
        fields.location.hide();
        fields.timeZone.hide();
        fields.timeZoneLabel.hide();
        fields.description.hide();
        fields.descriptionLabel.hide();
        fields.color.hide();
        fields.colorLabel.hide();
        fields.status.hide();
        fields.statusLabel.hide();
        fields.repeatContainer.hide();
        fields.allDayLabel.hide();
        fields.allDay.hide();
    }

    handleAppoint = (field) => {
        console.log(field);
    }

    handlemenu = (field) => {
        console.log(field);
    }

    hideresource = (event) => {
        console.log("event");
        console.log(event);
    }

    ready = (event) => {
        console.log(event);
    }

    handleSchdeduleView = (view) =>{
        this.setState(prevState => {
            return {
                view:view
            }
        })
    }

    displayresource = () => {
        this.setState(prevState=>{
            let resourceview = prevState.resourceview;
            console.log(resourceview);
            return {
                resourceview:!resourceview
            }
        })
    }

    prevdate = () => {
        this.setState(prevState => {
            let date = prevState.date;
            date.setDate(date.getDate() - 1);

            return {
                date:date
            }
        })
    }

    nextdate = () => {
        this.setState(prevState => {
            let date = prevState.date;
            date.setDate(date.getDate() + 1);

            return {
                date:date
            }
        })
    }
    displaydate = () =>{
        this.setState(prevState => {
            return {
                displaydate:!prevState.displaydate
            }
        })
    }

    handleChangeDate = (date) =>{
        console.log(date);
        this.setState(prevState => {
            return {
                displaydate:false,
                date:new Date(date)
            }
        })
    }

    viewchange = (event) => {
        console.log(event);
    }

    resourcehide = (resourceid) => {
        console.log(resourceid);
    }
    
    setFrom = () => {
        this.setState(prevState => {
            return {
                fromdisplay:!prevState.fromdisplay
            }
        })
    }

    setTo = () => {
        this.setState(prevState => {
            return {
                todisplay:!prevState.todisplay
            }
        })
    }

    setFromDate = (date) => {
        this.setState(prevState => {
            return {
                from: new Date(date)
            }
        })
    }

    setToDate = (date) => {
        this.setState(prevState => {
            return {
                to:new Date(date)
            }
        })
    }
    
    render()
    {
        let source = {
            dataType:"array",
            dataFields:[
                { name: 'id', type: 'string' },
                { name: 'subject', type: 'string' },
                { name: 'calendar', type: 'string' },
                { name: 'start', type: 'date' },
                { name: 'end', type: 'date' }
            ],
            id:"id",
            localData:this.state.appointment
            }
        
            let  resources = {
                dataField: "calendar",
                source:  new jqx.dataAdapter(source)
            }

            if(this.state.resourceview)
            {
                resources.orientation = "horizontal";
            }
            else
            {
                resources.orientation = "none";
            }

            
            console.log(resources);
            let date = new jqx.date(this.state.date.getFullYear(),this.state.date.getMonth() + 1,this.state.date.getDate())
            console.log(date);
        return (
            <Row className="scheduler" style={{marginRight:5}}>
                <Col lg={12}>
                    <div className="schedulertool">
                        <Row>
                            <Col></Col>
                            <Col>
                                <h3 className="title">Scheduler</h3>
                            </Col>    
                            <Col style={{marginRight:5}}>
                                <InputGroup>
                                    <Input placeholder="Search Here ... "></Input>
                                    <Button color="primary">Go</Button>
                                </InputGroup>
                            </Col>
                        </Row>
                        
                        <Row style={{alignItems:"center"}}>
                            <ButtonGroup className="day_select">
                                <Button color={this.state.view == 'dayView'?'default deactive':'default'} onClick={()=>{this.handleSchdeduleView('dayView')}}>Day</Button>
                                <Button color={this.state.view == 'weekView'?'default deactive':'default'} onClick={()=>{this.handleSchdeduleView('weekView')}}>Week</Button>
                                <Button color={this.state.view == 'monthView'?'default deactive':'default'} onClick={()=>{this.handleSchdeduleView('monthView')}}>Month</Button>
                            </ButtonGroup>
                            <div className="daybtn" style={{margin:"auto"}} onClick={this.displaydate}>
                                <FontAwesomeIcon icon={faCalendarAlt} style={{marginRight:10}}></FontAwesomeIcon>
                                <Moment format="MMMM DD YYYY">{this.state.date}</Moment>
                            </div>
                            <div style={{marginRight:20}}>
                                <Button color={this.state.resourceview?'default deactive':'default'} style={{marginLeft:5}} onClick={this.displayresource}>Resource</Button>
                                <Button id="slide" color="default" style={{marginLeft:5}}><FontAwesomeIcon icon={faSlidersH}></FontAwesomeIcon></Button>
                                <Button color="default" style={{marginLeft:5}} onClick={this.prevdate}><FontAwesomeIcon icon={faCaretLeft}></FontAwesomeIcon></Button>
                                <Button color="default" style={{marginLeft:5}} onClick={this.nextdate}><FontAwesomeIcon icon={faCaretRight}></FontAwesomeIcon></Button>
                            </div>
                            <UncontrolledPopover target="slide" trigger="legacy" placement="bottom">
                                <PopoverHeader>
                                    Select Date
                                </PopoverHeader>
                                <PopoverBody>
                                    <Form>
                                        <FormGroup inline={true} row={true}>
                                            <Col><Label for="from" style={{paddingLeft:15}}>From</Label></Col>
                                            <Col lg={8}>
                                                <InputGroup onFocus={this.setFrom}>
                                                    <Input id="from" placeholder=""></Input>
                                                    <InputGroupAddon addonType="append">
                                                        <InputGroupText><FontAwesomeIcon icon={faCalendarAlt}></FontAwesomeIcon></InputGroupText>
                                                    </InputGroupAddon>
                                                </InputGroup>
                                            </Col>
                                        </FormGroup>
                                        <FormGroup inline={true} row={true}>
                                            <Col><Label for="to" style={{paddingLeft:15}}>To</Label></Col>
                                            <Col lg={8}>
                                                <InputGroup onFocus={this.setTo}>
                                                    <Input id="to" placeholder=""></Input>
                                                    <InputGroupAddon addonType="append">
                                                        <InputGroupText><FontAwesomeIcon icon={faCalendarAlt}></FontAwesomeIcon></InputGroupText>
                                                    </InputGroupAddon>
                                                </InputGroup>
                                            </Col>
                                        </FormGroup>
                                        <FormGroup style={{paddingLeft:15}}>
                                            <Button color="primary" style={{width:"100%"}}>Apply Changes</Button>
                                        </FormGroup>
                                    </Form>
                                </PopoverBody>
                            </UncontrolledPopover>
                        </Row>
                    </div>
                </Col>
                {
                    this.state.displaydate && (
                        <DatePicker
                            selected={this.state.date}
                            onChange={this.handleChangeDate}
                            dateFormat="M/dd/yyyy"
                            withPortal
                            inline
                        ></DatePicker>
                    )
                }

                {
                    this.state.fromdisplay && (
                        <DatePicker
                            selected={this.state.from}
                            onChange={this.setFromDate}
                            dateFormat="M/dd/yyyy"
                            withPortal
                            inline
                        ></DatePicker>
                    )
                }
                {
                    this.state.todisplay && (
                        <DatePicker
                            selected={this.state.to}
                            onChange={this.setToDate}
                            dateFormat="M/dd/yyyy"
                            withPortal
                            inline
                        ></DatePicker>
                    )
                }
                <Col lg={12}>
                    <JqxScheduler 
                        date={date}
                        width={"100%"} 
                        height={600} 
                        source={new jqx.dataAdapter(source)} 
                        views={this.state.views} 
                        appointmentDataFields={this.state.datafield}
                        resources={resources}
                        editDialogCreate={this.handleeditdialog}
                        onAppointmentAdd={this.handleAppoint}
                        contextMenuCreate={this.handlemenu}
                        theme="energyblue"
                        showToolbar={false}
                        showLegend={true}
                        legendPosition="bottom"
                        onViewChange={this.ready}
                        legendHeight={100}
                        view={this.state.view}
                        hideAppointmentsByResource = {this.hideresource}
                        onCellClick={this.hideresource}
                    />
                </Col>             
            </Row>
            
        )      
    }
}

export default Scheduler;