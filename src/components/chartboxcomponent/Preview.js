import React from 'react';
import {Modal,ModalBody,ModalHeader,Col,Button,Row,ModalFooter,Form,FormGroup,Label} from 'reactstrap';
import ReactToPrint from 'react-to-print';
import {createContactLink} from 'react-protected-mailto';
import {Checkbox,CheckboxGroup} from 'react-ui-icheck';
import html2canvas from 'html2canvas';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPrint} from '@fortawesome/free-solid-svg-icons';

import PreviewPrint from './PreviewPrint';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';

class Preview extends React.Component
{
    printcontent = React.createRef();
    
    constructor(props)
    {
        super(props);
        this.state = {
            email:false,
            type:"Email",
            emailaddress:""
        }
    }

    showemail = () => {
        this.setState({
            email:!this.state.email
        })
    }

    close = () => {
        this.setState({
            email:false
        })

        this.props.close();
    }

    validate = () => {
        if(this.state.type == 'Email' || this.state.type == 'Fax')
        {
            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(re.test(this.state.emailaddress))
            {
                return true;
            }
        }
        return false;
    }
    
    setvalue = (item,e) => {
        if(e.target.checked)
        {
            this.setState({
                type:item,
                emailaddress:""
            })
        }
        else{
            this.setState({
                type:"",
                emailaddress:""
            })
        }
    }

    changeinput = (value) => {
        this.setState({
            emailaddress:value
        })
    }

    send = () => {
        var element = document.getElementById("print");
        let self = this;
        html2canvas(element).then(function(canvas){
            console.log(canvas);
            const imagedata = canvas.toDataURL("image/png");
            console.log(imagedata);
            var link = createContactLink(null,null,null,self.state.emailaddress,{Subject:"Report for Patient",body:'<img src="' + imagedata + '"/>'})
            window.location.href = link;
        })
        //console.log(this.printcontent.current);
        
    }
    render()
    {
        return (
            <Modal isOpen={this.props.preview} className="modal-container custom-modal">
                <ModalHeader toggle={this.close} tag={Col}>
                    <Row>
                        <Col>
                            Preview
                        </Col>
                        <div style={{marginRight:15}} id="print">
                            <ReactToPrint
                            trigger={()=><Button color="success"><FontAwesomeIcon icon={faPrint} style={{marginRight:10}}></FontAwesomeIcon>Print</Button>}
                            content={()=>this.printcontent.current}
                            >
                            </ReactToPrint>
                            <Button color="primary" style={{marginLeft:10}} onClick={this.showemail}>
                                <FontAwesomeIcon icon={faEnvelope} style={{marginRight:10}}></FontAwesomeIcon>Email
                            </Button>
                        </div>
                    </Row>
                </ModalHeader>
                
                <ModalBody>
                    <PreviewPrint 
                    ref={this.printcontent}
                    {...this.props}
                    ></PreviewPrint>
                </ModalBody>
                <Modal isOpen={this.state.email}>
                    <ModalHeader toggle={this.showemail}>
                        Send Email
                    </ModalHeader>
                    <ModalBody>
                        <CheckboxGroup
                            checkboxWrapClassName="form-check"
                            checkboxWrapTag="div"
                        >   
                            <Checkbox 
                                label="By Email"
                                checkboxClass="icheckbox_square-blue"
                                checked={this.state.type == "Email"}
                                onChange={(value)=>this.setvalue("Email",value)}
                                ></Checkbox>
                            <Checkbox 
                            label="By Fax"
                            checkboxClass="icheckbox_square-blue"
                            checked={this.state.type == "Fax"}
                            onChange={(value)=>this.setvalue("Fax",value)}
                            ></Checkbox>                            
                        </CheckboxGroup>
                        <Form>
                        {
                            this.state.type && (
                               <FormGroup row>
                                   <Label lg={4}>{this.state.type}</Label>
                                   <Col>
                                        <input className="form-control" onChange={(e)=>this.changeinput(e.target.value)} defaultvalue={this.state.emailaddress}></input>
                                   </Col>
                               </FormGroup> 
                            )   
                        }
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={()=>this.setState({email:false})} style={{marginRight:10}}>Cancel</Button>
                        {
                            !this.validate() && (
                                <Button color="default" style={{backgroundColor:"lightgrey"}}>Send</Button>
                            )
                        }
                        { this.validate && (
                            <Button color="success" onClick={this.send}>Send</Button>
                        )
                        }
                    </ModalFooter>
                </Modal>
            </Modal>
        )
    }
}

export default Preview;