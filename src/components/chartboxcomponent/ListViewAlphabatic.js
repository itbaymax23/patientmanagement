import React from 'react';
import {Button,Input,Col,Row} from 'reactstrap';
import $ from 'jquery';
import {Scrollbars} from 'react-custom-scrollbars';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faAngleDoubleLeft } from '@fortawesome/free-solid-svg-icons';

import * as Problem from '../../action/Problem';
class ListViewAlphabetic extends React.Component
{
    selectedid = 'problem';
    search = "";
    idarray = [];
    selected = "common";
    
    constructor(props)
    {
        super(props);
        this.state = {
            buttons:['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
            problems:[],
            symptoms:[]
        }
    }

    componentDidMount(){
        this.initdata();
    }

    initdata = () => {
        let self = this;
        this.selected = 'common';
        Problem.getproblemcommon().then(function(data){
            self.setState({
                problems:data.data
            })
        })

        Problem.getcptcommon().then(function(data){
            self.setState({
                symptoms:data.data
            })
        })
    }
    componentWillReceiveProps(props)
    {
        //let problems = props.data;
        //let buttons = this.getbuttonlist(problems,props.itemName);
        
        this.idarray = [];
        
        for(let item in props.selectedproblem)
        {
            this.idarray.push(props.selectedproblem[item].Code);
        }
        
        if(props.selectedid != this.selectedid)
        {
            this.selectedid = "" + props.selectedid;
            this.initdata();
        }
        
        // this.setState({
        //     problems:problems
        // })
    }
    
    getbuttonlist = (data,itemName) => {
        let buttons = [];
        for(let item in data)
        {
            var name = data[item]['CommonName']?data[item]['CommonName']:data[item][itemName];
            if(buttons.indexOf(name.charAt(0).toUpperCase()) == -1)
            {
                buttons.push(name.charAt(0).toUpperCase());
            }
        }

        return buttons;
    }

    handleChange = (e) => {
        this.search = e.target.value;
        this.searchdata(this.search);

        //this.props.searchproblem(this.search);
    }

    searchdata = (value) => {
        let self = this;
        if(this.props.selectedid == 'problem')
            {
                Problem.getproblems(value).then(function(data){
                    self.setState({
                        problems:data.data
                    })
                })
            }
            else
            {
                Problem.getcpt(value).then(function(data){
                    self.setState({
                        symptoms:data.data
                    })
                })
            }
    }

    scrollto = (problems,character) => {
        let top = $('.problemcontainer')[0].getBoundingClientRect().top;
        let element_top = 0;
        var enable = false;
        let self = this;
        $('.problemcontainer').find('li').each(function(index){
            let value = problems[index].CommonName?problems[index].CommonName:problems[index][self.props.itemName];
            let firstchar = value.charAt(0).toUpperCase();
            if(enable)
            {
                return;
            }
            if(firstchar == character && !enable)
            {
                element_top = this.getBoundingClientRect().top - top;
                enable  = true;
            }
        })

        if(enable)
        {
            $('.problemcontainer').animate({
                scrollTop:$('.problemcontainer').scrollTop() + element_top
            },1000);
    
        }
    }

    sort = (problems) => {
        let self = this;
        problems.sort(function(a,b){
            let namea = a.CommonName?a.CommonName:a[self.props.itemName];
            let nameb = b.CommonName?b.CommonName:b[self.props.itemName];

            if(namea < nameb)
            {
                return -1;
            }
            else if(namea > nameb)
            {
                return 1;
            }
        })

        return problems;
    }

    select = (name) => {
        if(name == 'common')
        {
            this.initdata();
        }
        else if(name == 'search')
        {
            this.selected = name;
            let self = this;
            this.searchdata("");
        }
    }

    render()
    {
        let self = this;
        let problems = this.props.selectedid == 'problem'?this.state.problems:this.state.symptoms;
        problems = this.sort(problems);
        return (
            <div className="toolbarcontainer">
                {
                    this.selected == 'search' && (
                        <div className="searchinput">
                            <Input placeholder="Search here..." defaultValue={this.search} onChange={(e)=>this.handleChange(e)}></Input>
                        </div>
                    )
                }
                <div className="title" style={{display:"flex",alignItems:"center"}}>
                    <h6>{this.props.title}</h6>
                    <span className="sliderleft">
                        <FontAwesomeIcon icon={faAngleDoubleLeft} onClick={this.props.showtoolbar}></FontAwesomeIcon>
                    </span>
                </div>
                <Col className="tab">
                    <Row>
                        <Col className={this.selected == 'common'?"tabitem selected":'tabitem'} onClick={()=>this.select('common')}>Common</Col>
                        <Col className={this.selected == 'search'?"tabitem selected":'tabitem'} onClick={()=>this.select('search')}>Search</Col>
                    </Row>
                </Col>
                <div className="problems">
                    <Scrollbars style={{height:"100%",width:50}}>
                        <div className="buttons">
                            <div className="buttonlist">
                                {
                                    this.state.buttons.map((row,index)=>{
                                        return (<Button color="default" onClick={()=>this.scrollto(problems,row)}>{row}</Button>)
                                    })
                                }                
                            </div>
                        </div>
                    </Scrollbars>
                    <Col className="problemcontainer">
                        <ul>
                            {
                                problems.map((row,index)=>{
                                    if(self.idarray.indexOf(row.Code) == -1)
                                    {
                                        return (<li onClick={()=>self.props.add(row)}>{row.CommonName?row.CommonName:row[self.props.itemName]} ({row.Code})</li>)        
                                    }
                                })
                            }
                        </ul>
                    </Col>
                </div>
            </div>
        )
        
    }
}

export default ListViewAlphabetic;