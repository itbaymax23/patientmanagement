import React from 'react';
import PropTypes from '../../utils/propTypes';
import {Row,Col} from 'reactstrap';
const PlanOrderRow = ({
    children,
    subdirectory:Tag,
    tagsubdirectory,
    title,
    titletag:Tag1,
    style,
    ...restprops
})=>{
    
    return(
        <Row className="planorderrow" style={style}>
            <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                {
                    title && (
                        <Row className="title"><Col><Tag1>{title}:</Tag1></Col></Row>
                    )
                }
                <Row className="sub">
                    <Col lg={12}>
                    {
                        tagsubdirectory && (
                            <Tag className="subtitle">{tagsubdirectory}</Tag>
                        )
                    }
                    </Col>
                </Row>
            </Col>
            <Col>
                <Row>
                    {children}
                </Row>
                
            </Col>
        </Row>
    )
}   

PlanOrderRow.propTypes = {
    children:PropTypes.component,
    subdirectory:PropTypes.String,
    tagsubdirectory:PropTypes.component,
    title:PropTypes.String
}

PlanOrderRow.defaultProps = {
    titletag:'p',
    subdirectory:'p'
}

export default PlanOrderRow;
