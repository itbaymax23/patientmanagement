import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import {Checkbox,CheckboxGroup,Radio,RadioGroup} from 'react-ui-icheck';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck,faInfoCircle, faListUl } from '@fortawesome/free-solid-svg-icons';
import CodingComponent from './CodingComponent';

class Coding extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            codeinfo:{'Office':{
                "new":{'99201 (10)':['PF','PF','SF'],'99202 (20)':['EPF','EPF','SF'],'99203 (30)':['DET','DET','LOW'],'99204 (45)':['COMP','COMP','MOD'],'99205 (60)':['COMP','COMP','HIGH']},
                "established":{'99211 (5)':['NR','NR','MIN'],'99212 (10)':['PF','PF','SF'],'99213 (15)':['EPF','EPF','LOW'],'99214 (25)':['DET','DET','MOD'],'99214 (25)':['COMP','COMP','HIGH']},
                "consults":{'99241 (15)':['PF','PF','SF'],'99242 (30)':['EPF','EPF','SF'],'99243 (40)':['DET','DET','LOW'],'99244 (60)':['COMP','COMP','MOD'],'99214 (25)':['COMP','COMP','HIGH']}
            },
            "Hospital":{
                "admit":{'99221 (30)':['DET','DET','LOW'],'99222 (50)':['COMP','COMP','HIGH'],'99223 (70)':['COMP','COMP','HIGH']},
                "samedayobs":{'99234 (40)':['PF','PF','SF'],'99235 (50)':['EPF','EPF','MOD'],'99236 (55)':['DET','DET','HIGH']},
                "overnightobs":{'99218 (30)':['DET','DET','SF'],'99219 (50)':['COMP','COMP','MOD'],'99220 (70)':['COMP','COMP','HIGH']},
                "f/u":{'99231 (15)':['PF','PF','SF'],'99232 (25)':['EPF','EPT','MOD'],'99233 (35)':['DET','DET','HIGH']},
                "consults":{'99251 (20)':['PF','PF','SF'],'99252 (40)':['EPF','EPF','SF'],'99253 (55)':['DET','DET','LOW'],'99254 (80)':["COMP","COMP","MOD"],'99255 (110)':["COMP","COMP","HIGH"]}
            },
            "Nursing Home":{
                "initial":{'99304 (25)':['DET','DET','LOW'],'99305 (35)':['COMP','COMP','MOD'],'99306 (45)':['COMP','COMP','HIGH']},
                "f/u":{'99307 (10)':['PF','PF','SF'],'99308 (15)':['EPF','EPF','LOW'],'99309 (25)':['DET','DET','MOD'],'99310 (35)':['COMP','COMP','HIGH']}
            },
            "Personal Care Home":{
                "initial":{'99324 (20)':['PF','PF','SF'],'99325 (30)':['EPF','EPF','SF'],'99326 (45)':['DET','DET','LOW'],'99327 (60)':['COMP','COMP','MOD'],'99328 (75)':['COMP','COMP','HIGH']},
                "f/u":{'99334 (15)':['PF','PF','SF'],'99335 (25)':['EPF','EPF','LOW'],'99336 (40)':['DET','DET','MOD'],'99337 (60)':['COMP','COMP','MOD']}
            },
            "Home Visit":{
                "initial":{'99341 (20)':['PF','PF','SF'],'99342 (30)':['EPF','EPF','SF'],'99343 (45)':['DET','DET','LOW'],'99344 (60)':['COMP','COMP','MOD'],'99345 (75)':['COMP','COMP','HIGH']},
                "f/u":{'99347 (15)':['PF','PF','SF'],'99348 (25)':['EPF','EPF','LOW'],'99349 (40)':['DET','DET','MOD'],'99350 (60)':['COMP','COMP','MOD']}
            },
            "Emergency Services":{
                "initial":{'99281':['PF','PF','SF'],'99282':['EPF','EPF','SF'],'99283':['DET','DET','LOW'],'99284':['COMP','COMP','MOD'],'99285':['COMP','COMP','HIGH']}
            }
            },
            table1:{
                header:['History','PF','EPF','DET','COMP'],
                body:[
                    ["Chief Complaint","Required","Required","Required","Required"],
                    [(<p>HPI<span style={{marginLeft:10,color:'#ffce66'}}>{props.data.HPI.problem.length}</span><span style={{marginLeft:10,color:'#f17171'}}>0</span></p>),'1 - 3 elements','1 - 3 elements',['> 3 chronic diseases','> 4 elements OR'],['> 3 chronic diseases','> 4 elements OR']],
                    [(<p>ROS <span style={{marginLeft:10,color:'#ffce66'}}>0</span></p>),'NR','1 system','> 2 - 9 systems','> 10 systems'],
                    [(<p>PMH/SH/FH <span style={{marginLeft:10,color:'#ffce66'}}>0</span></p>),'NR','NR','1 element','> 2 elements']
                ]
            },
            table2:{
                header:["Examination","PF","EPF","DET","Comp"],
                body:[
                    ["",["1 brief exam of affected system","(1 - 5) elements"],["2 brief systems (affected +1 other)","(6 - 11) elements"],
                    ["Extended PE - several systems","2 elements in 6 systems","OR","12 elements in 2 or more systems"],["Extended PE - several systems","2 elements in 9 systems","OR","All elements in a single system"]]
                ]
            },																																					
            table3:{
                header:[(<p>Medical Decision Making <FontAwesomeIcon icon={faInfoCircle} style={{marginLeft:10}}></FontAwesomeIcon></p>),"Straight Forward","Low","Moderate","High"],
                body:[
                    ["Problems","1","2","3","4"],
                    [(<p>Data <span style={{marginLeft:10,color:"#397ce7"}}><FontAwesomeIcon icon={faListUl}></FontAwesomeIcon></span></p>),"1","2","3","4"],
                    ["Risk","Minimal","Low","Moderate","High"]
                ]
            },
            
            
            table4:{
                header:[(<p>RISK <FontAwesomeIcon icon={faInfoCircle} style={{marginLeft:10}}></FontAwesomeIcon></p>),"Presenting Problem","Diagnostics","Management Options"],
                body:[
                    ["Minimal",["1 self limited or minor problem","(URI, insect bite, tinea corporis)"],["Labs requiring venipuncture","Imaging","UA","KOH prep"],["Rest","Gargles","Elastic bandage","Superficial dressingv"]],
                    ["Low",["2 or more self limited or minor"," 1 stable chronic problem (HTN, DM, BPH, etc)","Acute uncomplicated problem (cystitis, sprain, allergies)"],
                    ["Tests not under stress (PFTs)","Non CV imaging studies w/ contrast (BE)","Superficial needle biopsy","Labs requiring artial puncture (ABG)","Skin biopsy"],
                    ["OTC Drugs","Minor surgery w/ no risk factors","PT/OT","IV fluids w/o additives"]],
                    ["Moderate",["1 or more chronic illnesses w/ mild exacerbation","Two or more stable chronic problems","Undiagnosed new problem w/ uncertain prognosis (breast lump)","Acute illness w/ systemic manifestations (pyelo, colitis, PNA","Acute complicated injury (head injury w/ brief LOC)"],
                    ["Tests under stress (cardiac)","Endoscopy w/ no risk factors","Deep needle or incisional biopsy","Cardiac imaging w/ contrast and no risk factors (catheterization)","Obtaining fluid from body cavity"],
                    ["Minor surgery with risk factors","Elective major surgery w/o risk factors","Prescription drug management","Therapeutic nuclear medicine","IV fluids with additives","Closed treatment of fracture"]
                    ],
                    ["High",["1 or more chronic illnesses w/ severe exac, progression, or treatment SE","Acute complicated injury (head injury w/ brief LOC)"],
                    ["CV imaging with risk factors","Cardiac EP tests","Endoscopy w/ risk factors"],["Elective major surgery w/ risk factors","Emergency major surgery","Parenteral controlled substances","Drug therapy requiring monitoring for toxicity","Decision to pursue DNR or de-escelate care given poor prognosis"]]
                ]
            }
        }
    }
    
    calculateros = (data) => {
        let index = 0;
        for(let item in data.data)
        {
            if(data.data[item].positive.length > 0 || data.data[item].list.length > 0 || data.data[item].perHPI)
            {
                index ++;
            }
        }

        return index;
    }

    calcexam = (data) => {
        let system = 0; let system_2 = 0;
        let allselected = false;
        for(let item in data)
        {

            var systemenable = false; var count = 0; var enablesystem = true;
            for(let itemlist in data[item].list)
            {
                if(data[item].list[itemlist].text)
                {
                    if(!systemenable)
                    {
                        system ++;
                        systemenable = true;
                    }

                    count ++;

                    if(count == 2)
                    {
                        system_2 ++;
                    }
                    
                }
                else
                {
                    enablesystem = false;
                }
            }

            if(!allselected)
            {
                allselected = enablesystem;
            }
        }

        return {system:system,system2:system_2,selected:allselected};
    }

    handleChangeType = (row,checked) => {
        if(checked)
        {
            this.props.save('EM',{type:row},16);
        }
        else
        {
            this.props.save('EM',{type:""},16);
        }
    }

    save = (type,value,item) => {
        
    }
    render()
    {
        let self = this;
        console.log("coding pe",this.props.locationtype);
        let selecteditem = this.calcexam(this.props.data.PE.data?this.props.data.PE.data:[]);
        return(
            <Row className="em_coding">
                <Col lg={12}>
                    <Row>
                        <Col lg={3} md={4} sm={6} xs={6}>
                            <Row>
                               <Col>
                                    <span className="title">E/M Code:</span>
                                    <span>Not enough Data</span>
                               </Col> 
                            </Row>
                            
                            <Row style={{alignItems:"center",paddingTop:30}}>
                                <span style={{marginLeft:15}}>
                                    <Checkbox
                                        checkboxClass="icheckbox_flat-grey"
                                        label="Time Based"
                                        defaultChecked
                                    />
                                </span>
                                <Col>
                                    <FontAwesomeIcon icon={faInfoCircle} style={{marginLeft:5,zIndex:3}}></FontAwesomeIcon>
                                    <Button color="default" style={{marginLeft:10,zIndex:30}}>Min</Button>
                                </Col>
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <Col>
                                    <span className="title">Location:</span>
                                    <span>{this.props.location}</span>
                                </Col>
                            </Row>
                            <Row style={{paddingTop:30}}>
                                <Col lg={12}>
                                {
                                    this.props.locationtype && (
                                        <CheckboxGroup  
                                            checkboxWrapClassName="form-check form-check-inline"
                                            checkboxWrapTag="div"
                                            className="d-flex align-items-center"
                                        >
                                            {
                                                Object.keys(this.state.codeinfo[this.props.locationtype]).map((row,index)=>{
                                                    return (
                                                        <Checkbox
                                                            key={index}
                                                            checkboxClass="icheckbox_flat"
                                                            label={(row == 'new' || row == 'established')?"This is " + row + " patient":"This is " +  self.props.locationtype + " " + row}
                                                            checked={this.props.EM.type == row?true:false}
                                                            onChange = {(e)=>{this.handleChangeType(row,e.target.checked)}}
                                                        />
                                                    )
                                                })
                                            }
                                            
                                        </CheckboxGroup>
                                    )
                                }
                                    
                                </Col>
                                <Col lg={12} style={{marginTop:15}}>
                                    {
                                        (this.props.EM.type && this.props.locationtype && this.state.codeinfo[this.props.locationtype] && this.state.codeinfo[this.props.locationtype][this.props.EM.type]) && (
                                            <RadioGroup className="d-flex align-items-center"
                                            name="radio"
                                            onChange={(event, value) => this.setState({ radioValue: value })}
                                            radioWrapClassName="form-check form-check-inline"
                                            radioWrapTag="div"
                                            value={this.props.EM.code}
                                            onChange={(e,value)=>this.props.save('EM',{code:value},16)}
                                            >
                                                {
                                                    Object.keys(this.state.codeinfo[this.props.locationtype][this.props.EM.type]).map((row,index)=>{
                                                        return (
                                                            <Radio
                                                            key={index}
                                                            label={row}
                                                            radioClass="iradio_square-blue"
                                                            value={row}
                                                            ></Radio>
                                                        )
                                                    })
                                                }
                                            </RadioGroup>
                                        )
                                    }
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>

                <Col lg={12}>
                    <Row className="codingheader">
                        {
                            this.state.table1.header.map((row,index)=>{
                                return (
                                    <Col key={index}>{row}</Col>
                                )
                            })
                        }
                    </Row>
                    <Row className="codingbody">
                        {
                            this.state.table1.body[0].map((row,index)=>{
                                return (
                                    <Col key={index}>
                                        <p style={{marginTop:10}}>{row}</p>
                                    </Col>
                                )
                            })
                        }
                    </Row>
                    <Row className="codingbody">
                        <Col>
                            <p style={{marginTop:10}}>
                                HPI
                                <span style={{marginLeft:10,color:'#ffce66'}}>{this.props.data.HPI.problem.length}</span><span style={{marginLeft:10,color:'#f17171'}}>{(this.props.data.CC.problem.length > 0 && this.props.data.CC.problem[0].length)?this.props.data.CC.problem[0].length:0}</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={(this.props.data.HPI.problem.length > 0 && this.props.data.HPI.problem.length < 4)?'codingitem specific':'codingitem'}>1-3 elements</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={(this.props.data.CC.problem.length > 0 && (this.props.data.CC.problem[0].length < 4 || this.props.data.CC.problem[0].length > 0))?'specific':''}>1-3 Chronic Disease</span>
                            </p>
                        </Col>
                        <Col className={(this.props.data.CC.problem.length > 0 && this.props.data.CC.problem[0].length > 3) || (this.props.data.HPI.problem.length > 3)?'specific':''}>
                            <p style={{marginTop:10}}>> 3 chronic diseases</p>
                            <p style={{marginTop:10}}>> 4 elements OR</p>
                        </Col>
                        <Col className={(this.props.data.CC.problem.length > 0 && this.props.data.CC.problem[0].length > 3) || (this.props.data.HPI.problem.length > 3)?'specific':''}>
                            <p style={{marginTop:10}}>> 3 chronic diseases</p>
                            <p style={{marginTop:10}}>> 4 elements OR</p>
                        </Col>
                    </Row>
                    <Row className="codingbody">
                        <Col>
                            <p style={{marginTop:10}}>
                                ROS
                                <span style={{marginLeft:10,color:'#ffce66'}}>{this.calculateros(this.props.data.ROS)}</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={this.calculateros(this.props.data.ROS) == 0?'specific':''}>NR</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={this.calculateros(this.props.data.ROS) == 1?'specific':''}>1 System</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={this.calculateros(this.props.data.ROS) > 1 && this.calculateros(this.props.data.ROS) < 10?'specific':''}>>2 - 9 Systems</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={this.calculateros(this.props.data.ROS) == 10?'specific':''}>10 Systems</span>
                            </p>
                        </Col>
                    </Row>
                    <Row className="codingbody">
                        <Col>
                            <p style={{marginTop:10}}>
                                PMH/SH/FH
                                <span style={{marginLeft:10,color:'#ffce66'}}>{this.props.data.PMH.problem.length}</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={this.props.data.PMH.problem.length == 0?'specific':''}>NR</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={this.props.data.PMH.problem.length == 0?'specific':''}>NR</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={this.props.data.PMH.problem.length == 1?'specific':''}>1 element</span>
                            </p>
                        </Col>
                        <Col>
                            <p style={{marginTop:10}}>
                                <span className={this.props.data.PMH.problem.length > 1?'specific':''}>> 2 elements</span>
                            </p>
                        </Col>
                    </Row>
                </Col>
                <Col lg={12}>
                    <Row className="codingheader">
                        {
                            this.state.table2.header.map((row,index)=>{
                                return (
                                    <Col key={index}>{row}</Col>
                                )
                            })
                        }
                    </Row> 
                    <Row className="codingbody">
                        <Col>
                           
                        </Col>
                        <Col className={selecteditem.system == 1?'specific':''}>
                            <p style={{marginTop:10}}>
                                1 brief exam of affected system
                            </p>
                            <p style={{marginTop:10}}>(1 - 5) elements</p>
                        </Col>
                        <Col className={selecteditem.system == 2?'specific':''}>
                            <p style={{marginTop:10}}>
                                2 brief systems (affected +1 other)
                            </p>
                            <p style={{marginTop:10}}>(6 - 11) elements</p>
                        </Col>
                        <Col className={(selecteditem.system2 == 6 || (selecteditem.system < 9) && (selecteditem.system > 2))?'specific':''}>
                            <p style={{marginTop:10}}> Extended PE - several systems</p>
                            <p style={{marginTop:10}}> 2 elements in 6 systems</p>
                            <p style={{marginTop:10}}>OR</p>
                            <p style={{marginTop:10}}> 12 elements in 2 or more systems</p>
                        </Col>
                        <Col className={(selecteditem.selected || selecteditem.system >= 9)?'specific':''}>
                            <p style={{marginTop:10}}> Extended PE - several systems</p>
                            <p style={{marginTop:10}}> 2 elements in 9 systems</p>
                            <p style={{marginTop:10}}>OR</p>
                            <p style={{marginTop:10}}> All elements in a single system</p>
                        </Col>
                    </Row>
                    {/* <CodingComponent componentheader={this.state.table2.header} componentbody={this.state.table2.body}/> */}
                    <CodingComponent componentheader={this.state.table3.header} componentbody={this.state.table3.body}/>
                    <CodingComponent componentheader={this.state.table4.header} componentbody={this.state.table4.body}/>
                </Col>
                
            </Row>
        )
    }
}

export default Coding;