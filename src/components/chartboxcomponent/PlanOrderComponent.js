import React from 'react';
import {Row,Col,Progress,Table} from 'reactstrap';
import PlanOrderCell from './PlanOrderCell';
import PlanOrderRow from './PlanOrderRow';
import Directive from './Directive';
import AssessmentTable from './AssessmentTable';

class PlanOrderComponent extends React.Component
{
    state = {
        action:["None","Admit","Discharge","Transfer"],
        location:["Home","Hospital","Nursing Home","Personal Care Home"],
        condition:["stable","Improving","Unstable","Guarded"],
        diet:["Bland","Cardiac","Clear liquids","Full liquids","High fiber","Ice chips","Low cholesterol","Low fat","Low Sodium","Mechanical Soft","No added salt","No Concentrated sweets","NPO","Regular"],
        other:["1 liter fluid restiction/24 hours",'1.5 liter fluid restriction/24 hours','2 liter fluid restriction/24 hours'],
        cardio:["Attempt Resuscitation(CTR)","Allow Natural Death(AND) - Do Not Attempt Resuscitation"],
        medical:["Comfort Measures","Limited Additional inventions","Full Treatment"],
        anti:["No Antibiotics","Determine use or limitation of antibiotics when infection occurs","Use antibiotics if life can be prolonged"],
        affinity:["Yes","No Artificial nutrition by tube","No IV Fluids","Trial Period of artifical nutrition by tube","Trial Period of IV Fluids","Long Term Artifical Nutrition By Tube","Long Term IV Fluids"],
        misc:['Lorem Ipsum is simply dummy text.','Lorem Ipsum is simply dummy text.','Lorem Ipsum is simply dummy text.','Lorem Ipsum is simply dummy text.','Lorem Ipsum is simply dummy text.']
    }

    render()
    {
        return(
            <Col className="planorder">
                <Row>
                    <Col lg={9} md={8} sm={12} xs={12}>
                        <PlanOrderRow title="Disposition">
                            <PlanOrderCell title="Action:" component={this.state.action} selected="Admit" style={{borderWidth:1,borderRightStyle:"solid",borderColor:"#e2e2e2"}}></PlanOrderCell>
                            <PlanOrderCell title="Location:" component={this.state.location} selected="Hospital"></PlanOrderCell>
                            <Col><select className="form-control"><option value="">Specific Location</option></select></Col>
                        </PlanOrderRow>         
                        <PlanOrderRow title="Condition">
                            <PlanOrderCell component={this.state.condition} selected="Improving"></PlanOrderCell>
                        </PlanOrderRow> 
                        <PlanOrderRow title="Diet" tagsubdirectory="Type of Diet">
                            <PlanOrderCell component={this.state.diet} style={{paddingTop:10}}></PlanOrderCell>
                        </PlanOrderRow>
                        <PlanOrderRow title="" tagsubdirectory="Other Categories">
                            <Col lg={6} style={{padding:"10px 0px"}}><Progress style={{height:40,borderWidth:1}}/></Col>
                        </PlanOrderRow>
                        <PlanOrderRow tagsubdirectory="Other">
                            <PlanOrderCell component={this.state.other} style={{paddingTop:10}}></PlanOrderCell>
                        </PlanOrderRow>   
                        <PlanOrderRow title="Advanced Directives" tagsubdirectory="<p>(A)</p><p>Cardiopulmonary</P><p>Resuscitation</p>" subdirectory={Directive}>
                            <PlanOrderCell component={this.state.cardio} style={{paddingTop:80}} selected={this.state.cardio[0]}></PlanOrderCell>
                        </PlanOrderRow>
                        <PlanOrderRow tagsubdirectory="<p>(B)</p><p>Medical</P><p>Interventions</p>" subdirectory={Directive}>
                            <PlanOrderCell component={this.state.medical} style={{paddingTop:10}} selected={this.state.medical[2]}></PlanOrderCell>
                        </PlanOrderRow>   
                        <PlanOrderRow tagsubdirectory="<p>(C)</p><p>Antibiotics</P>" subdirectory={Directive}>
                            <PlanOrderCell component={this.state.anti} style={{paddingTop:10}} selected={this.state.medical[2]}></PlanOrderCell>
                        </PlanOrderRow>  
                        <PlanOrderRow tagsubdirectory="<p>(D)</p><p>Affinitically</P><p>Administratered</p><p>Nutrition / Fluid</p>" subdirectory={Directive}>
                            <PlanOrderCell component={this.state.affinity} style={{paddingTop:10}} selected={this.state.affinity[0]}></PlanOrderCell>
                        </PlanOrderRow>  
                        <PlanOrderRow title="Misc">
                            <PlanOrderCell component={this.state.misc} style={{paddingTop:10}}></PlanOrderCell>
                        </PlanOrderRow>
                        <PlanOrderRow title="Disposition">
                            <Table responsive bordered style={{marginBottom:0}}>
                                <thead style={{backgroundColor:"#ececec"}}>
                                    <th>Provider</th>
                                    <th>Location</th>
                                    <th>Select Form</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Jhon Doe</td>
                                        <td>Washington Internal Medicine</td>
                                        <td>Day:15 / Week:4/ Month:August</td>
                                    </tr>
                                    <tr>
                                        <td>Mike Miller</td>
                                        <td>Harper's Personal Care Home</td>
                                        <td>Day:20 / Week:1/ Month:August</td>
                                    </tr>
                                    <tr>
                                        <td>Leon Kenedy</td>
                                        <td>Wills Memorial Hospital</td>
                                        <td>Day:8 / Week:2/ Month:July</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </PlanOrderRow>
                        <AssessmentTable/>
                    </Col>
                    <Col className="ordercontainer" lg={3} md={4} sm={12} xs={12} style={{paddingTop:10,paddingBottom:10}}>
                        <div className="neworder" style={{height:"50%"}}>
                            <div className="orderheader">
                                New Order(s)
                            </div>
                            <div className="orderbody"></div>
                        </div>
                        <div className="neworder" style={{height:"50%"}}>
                            <div className="orderheader">
                                Current Order(s)
                            </div>
                            <div className="orderbody"></div>
                        </div>
                    </Col>
                </Row>
            </Col>
        )
    }
}

export default PlanOrderComponent;