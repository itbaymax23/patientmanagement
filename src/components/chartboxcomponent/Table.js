import React from 'react';
import {Col,Button,Row} from 'reactstrap';
import PropTypes from '../../utils/propTypes';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import TableRow from './TableRow';

const Table = ({
    tableheader,
    tablebody,
    rightbutton,
    rightbuttonname,
    ...restProps
})=>{
    let count = 0;
    for(let item in tablebody)
    {
        if(tablebody[item].length > count)
        {
            count = tablebody[item].length;
        }
    }

    console.log(tablebody);
    return (
        <div className="mytable">
            <Col>
                <div className="tableheader">
                    {
                        tableheader.map((row,index)=>{
                            return (
                                <div className={row.classname?row.classname:'header'}>
                                    <p>{row.value}</p>
                                    <p>{row.description}</p>
                                </div>
                            )
                        })
                    }
                    {
                        rightbutton && (
                            <div style={{width:"10%",float:"left"}}>
                                <Row>
                                    <Button color="primary" style={{marginLeft:"auto"}}>
                                        <FontAwesomeIcon icon={rightbutton} style={{marginRight:10}}></FontAwesomeIcon> {rightbuttonname}
                                    </Button>
                                </Row>
                            </div>
                        )
                    }
                </div>
                <div className="tablebody">
                    {
                        tablebody.map((row,index)=>{
                            return(
                                <TableRow tablerow={row} count={count}></TableRow>
                            )
                        })
                    }
                </div>
            </Col>
        </div>
    )
}

Table.prototype = {
    tableheader:PropTypes.component,
    tablebody:PropTypes.component
}

Table.defaultProps = {
    tableheader:[],
    tablebody:[]
}

export default Table;