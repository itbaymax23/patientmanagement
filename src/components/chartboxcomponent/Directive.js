import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import PropTypes from '../../utils/propTypes';
const Directive = ({
    children
}) => {
    return (
            <Row className="directive">
                <Col dangerouslySetInnerHTML={{__html:children}}>
                    
                </Col>
                <Button color="default" className="directive_btn"><FontAwesomeIcon icon={faCheckCircle}></FontAwesomeIcon></Button>
            </Row>
    )
}

Directive.propTypes = {
    children:PropTypes.component
};

export default Directive;