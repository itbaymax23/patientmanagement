import React from 'react';
import {Button,Input,Col} from 'reactstrap';
import $ from 'jquery';
import {Scrollbars} from 'react-custom-scrollbars';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faAngleDoubleLeft } from '@fortawesome/free-solid-svg-icons';
import * as Problem from '../../action/Problem';
class ListViewCPT extends React.Component
{
    search = "";
    constructor(props)
    {
        super(props);
        this.state = {
            buttons:[],
            problems:[]
        }
        this.updateinformation();
    }
    
    updateinformation = () => {
        let self = this;
        Problem.getcpt().then(function(data){
            let problems = data.data;
            let buttons = self.getbuttonlist(problems);
            self.setState({
                buttons:buttons,
                problems:problems
            })
        })
    }

    componentWillReceiveProps(props)
    {
       //this.updateinformation();
    }

    getbuttonlist = (data) => {
        let buttons = [];
        for(let item in data)
        {
            if(buttons.indexOf(data[item].BillDesc.charAt(0).toUpperCase()) == -1)
            {
                buttons.push(data[item].BillDesc.charAt(0).toUpperCase());
            }
        }

        return buttons;
    }

    handleChange = (e) => {
        this.search = e.target.value;
        let problems = [];
        for(let item in this.props.data)
        {
            if(this.props.data[item].CommonName.toUpperCase().indexOf(this.search.toUpperCase()) > -1)
            {
                problems.push(this.props.data[item]);
            }
        }

        console.log(problems);
        let buttons = this.getbuttonlist(problems);

        this.setState({
            problems:problems,
            buttons:buttons
        });
    }

    scrollto = (character) => {
        let top = $('.problemcontainer')[0].getBoundingClientRect().top;
        let element_top = 0;
        var enable = false;
        let self = this;
        $('.problemcontainer').find('li').each(function(index){
            let value = self.state.problems[index].CommonName;
            let firstchar = value.charAt(0).toUpperCase();
            if(enable)
            {
                return;
            }
            if(firstchar == character)
            {
                console.log(this.getBoundingClientRect().top);
                console.log(top);
                element_top = this.getBoundingClientRect().top - top;
                enable  = true;
            }
        })

        console.log(element_top);
        $('.problemcontainer').animate({
            scrollTop:$('.problemcontainer').scrollTop() + element_top
        },1000);
    }

    render()
    {
        return (
            <div className="toolbarcontainer">
                <div className="searchinput">
                    <Input placeholder="Search here..." defaultValue={this.search} onChange={(e)=>this.handleChange(e)}></Input>
                </div>
                <div className="title" style={{display:"flex",alignItems:"center"}}>
                    <h6>{this.props.title}</h6>
                    <span className="sliderleft">
                        <FontAwesomeIcon icon={faAngleDoubleLeft} onClick={this.props.showtoolbar}></FontAwesomeIcon>
                    </span>
                </div>
                <div className="problems">
                    <Scrollbars style={{height:"100%",width:50}}>
                        <div className="buttons">
                            <div className="buttonlist">
                                {
                                    this.state.buttons.map((row,index)=>{
                                        return (<Button color="default" onClick={()=>this.scrollto(row)}>{row}</Button>)
                                    })
                                }                
                            </div>
                        </div>
                    </Scrollbars>
                    <Col className="problemcontainer">
                        <ul>
                            {
                                this.state.problems.map((row,index)=>{
                                    return (<li onClick={()=>this.props.add(row)}>{row.BillDesc}</li>)        
                                })
                            }
                        </ul>
                    </Col>
                </div>
            </div>
        )
        
    }
}

export default ListViewCPT;