//library
import React from 'react';
import {Modal,ModalHeader,ModalBody,Row,Col,UncontrolledPopover,PopoverBody} from 'reactstrap'
import {Checkbox,CheckboxGroup} from 'react-ui-icheck';
import uniqueId from 'lodash/uniqueId';
import Sortable from 'react-sortablejs';

//icon
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCogs} from '@fortawesome/free-solid-svg-icons';

//component
import {Problem,PSH,Meds,Allergies,SH,FH} from '../Summary';

class Summary extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            data:[
                {item:"PMH",title:"PMH/Problem List",component:Problem,sectionid:3},
                {item:"PSH",title:"PSH",component:PSH},
                {item:"MEDS",title:"Meds",component:Meds},
                {item:"Allergies",title:"Allergies",component:Allergies},
                {item:"SHI",title:"SH",component:SH},
                {item:'fh',title:"FH",default:"No Records Found",component:FH},
                {item:'VS',title:"Vital Signs",default:"No Vital Signs",component:FH},
                {item:"CS",title:"Consultants",default:"No Consultants",component:FH},
                {item:"ADR",title:"Advanced Directives",default:"No Advanced Directives",component:FH},
                {item:"cms",title:"Communication",default:"No Communication",component:FH},
                {item:"misc",title:"MISC",default:"No Misc",component:FH}
            ],
            settings:[
                {item:"PMH",title:"PMH/Problem List",component:Problem,sectionid:3},
                {item:"PSH",title:"PSH",component:PSH},
                {item:"MEDS",title:"Meds",component:Meds},
                {item:"Allergies",title:"Allergies",component:Allergies},
                {item:"SHI",title:"SH",component:SH},
                {item:'fh',title:"FH",component:FH},
                {item:'VS',title:"Vital Signs",default:"No Vital Signs",component:FH},
                {item:"CS",title:"Consultants",default:"No Consultants",component:FH},
                {item:"ADR",title:"Advanced Directives",default:"No Advanced Directives",component:FH},
                {item:"cms",title:"Communication",default:"No Communication",component:FH},
                {item:"misc",title:"MISC",default:"No Misc Record",component:FH}
            ],items: [1, 2, 3, 4, 5, 6]
        }
    }

    //check if the display setting include this section
    haschecked = (checkeditem) => {
        for(let item in this.state.settings)
        {
            if(this.state.settings[item].item == checkeditem)
            {
                return true;
            }
        }

        return false;
    }
    
    //add the view to setting view
    addview = (item,e)=>
    {
        let settings = this.state.settings;
        if(e.target.checked)
        {
            settings.push(item);
        }
        else
        {
            for(let itemsetting in settings)
            {
                if(settings[itemsetting].item == item.item)
                {
                    settings.splice(itemsetting,1);
                    break;
                }
            }
        }

        this.setState({
            settings:settings
        })
    }

    render()
    {
        // sortable items
       let item = this.state.settings.map((row,index)=>{
            let Tag = false;
            if(row.component)
            {
                Tag = row.component;
            }
            
            var listdata = this.props.data[row.item];

            if(row.item == 'PMH')
            {
                listdata = {
                    PMH:this.props.data.PMH,
                    CC:this.props.data.CC
                }
            }

            return (
                <li data-id={index} key={uniqueId()} className="col-lg-6 col-md-6 col-xs-6 col-xs-12">
                    {
                        row.component && (
                        <Tag 
                        title={row.title} 
                        data={listdata}
                        save = {(content)=>this.props.save(row.item,content,row.sectionid)}
                        default={row.default}
                        >    
                        </Tag>)
                    }
                    
                </li>
            )
       })


        return (
            <Modal isOpen={this.props.isopen} className="modal-container custom-modal">
                <ModalHeader tag={Col} toggle={this.props.close}> Health Summary <FontAwesomeIcon id="settings" icon={faCogs} style={{marginLeft:30,cursor:'pointer'}}></FontAwesomeIcon></ModalHeader>
                <ModalBody>
                    <Col>
                        <Sortable 
                            tag="ul"
                            onChange={(order,sortable,evt)=>{
                                let state = [];
                                for(let item in order)
                                {
                                    state.push(this.state.settings[order[item]]);
                                }

                                this.setState({settings:state});
                            }}
                            className="row"
                            >
                            {item}
                        </Sortable>
                    </Col>
                </ModalBody>
                <UncontrolledPopover target="settings" trigger="legacy" placement="bottom">
                    <PopoverBody>
                        <CheckboxGroup
                        checkboxWrapClassName="form-check"
                        checkboxWrapTag="div"
                        >
                            {
                                this.state.data.map((row,index)=>{
                                    return (
                                        <Checkbox
                                        checkboxClass="icheckbox_square-blue"
                                        label={row.title}
                                        defaultChecked={this.haschecked(row.item)}
                                        onChange={(e)=>this.addview(row,e)}
                                        ></Checkbox>
                                    )
                                })
                            }
                        </CheckboxGroup>
                    </PopoverBody>
                </UncontrolledPopover>
            </Modal>
        )
    }
}

export default Summary;