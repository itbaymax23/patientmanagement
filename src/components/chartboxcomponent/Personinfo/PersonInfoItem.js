import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimesCircle} from '@fortawesome/free-regular-svg-icons';

class PersonInfoItems extends React.Component
{
    constructor(props)
    {
        super(props);
    }
    
   
    close = () =>{
        this.props.close(this.props.index);
    }

    render(){
        return(
            <div  id={"personinfo_" + this.props.patientid} className="personinfoitem">
                <span className="name" onClick={()=>this.props.toggle(this.props.index)}>{this.props.name}</span>
                <span className="close" onClick={this.close}><FontAwesomeIcon icon={faTimesCircle}></FontAwesomeIcon></span>
            </div>
        )
    }

}

export default PersonInfoItems;