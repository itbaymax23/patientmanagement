import React from 'react';
import {Row,Col} from 'reactstrap';

class Phamacy extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <Row>
                <Col className="detailcontainer">
                    <Row>
                        <div className="label">Name</div>
                        <Col>{this.props.phamacy.Name}</Col>
                    </Row>
                    <Row>
                        <div className="label">Address</div>
                        <Col>{this.props.phamacy.address}</Col>
                    </Row>
                    <Row>
                        <div className="label">City</div>
                        <Col>{this.props.phamacy.City}</Col>
                    </Row>
                    <Row>
                        <div className="label">State</div>
                        <Col>{this.props.phamacy.State}</Col>
                    </Row>
                    <Row>
                        <div className="label">Zip</div>
                        <Col>{this.props.phamacy.PostCode}</Col>
                    </Row>
                    <Row>
                        <div className="label">Phone</div>
                        <Col>{this.props.phamacy.Telephone}</Col>
                    </Row>
                    <Row>
                        <div className="label">Fax</div>
                        <Col>{this.props.phamacy.fax}</Col>
                    </Row>
                    <Row>
                        <div className="label">Email</div>
                        <Col>{this.props.phamacy.email}</Col>
                    </Row>
                </Col>
            </Row>
        )
       
    }
}

export default Phamacy;