import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimesCircle, faSave, faArrowAltCircleUp} from '@fortawesome/free-regular-svg-icons';
import styled from 'styled-components';
import noimage from '../../../assets/img/no-image.png';
import { faPencilAlt, faPhoneAlt, faAngleRight, faTimes } from '@fortawesome/free-solid-svg-icons';
import Switch from 'react-toggle-switch';
import EmergencyContact from './EmergencyContact';
import Employeer from './Employeers';
import * as User from '../../../action/user';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
import {DatePicker,NumberPicker} from 'react-widgets';
import simpleNumberLocalizer from 'react-widgets-simple-number'; 
import { PopoverBody,Popover,Modal,ModalHeader,ModalBody,ModalFooter,Button ,Tab,Row,Col,TabContent,TabPane,Nav,NavItem,NavLink,Form,FormGroup,Label,Badge} from 'reactstrap';
import classnames from 'classnames';
import Democratic from './Democratic';
import $ from 'jquery';
import Phamacy from './Phamacy';
import ElectricCommunication from './ElectricCommunication';
import {Checkbox,CheckboxGroup} from 'react-ui-icheck';
class ActivePersonInfo extends React.Component
{
    emergencycontact = [{}];
    data = {
        reminders : [],
        reaction : [],
        reminderseditable : {},
        reactioneditable : {},
        FED:new Date(),
        LED:new Date(),
        TEN:0,
        demo:{
            electricdiagnose:{},
            employer:[],
            pharmacy:[],
            general:{}
        },
        counter:[]
    }

    constructor(props)
    {
        super(props);
        this.state = {
            selected:false,
            toggled:false,
            emergencycontact:[],
            reminders:[],
            reaction:[],
            counter:[],
            dataenabled:false,
            emergency:false,
            emergencytab:0,
            demomodal:false,
            reminderseditable:{},
            reactioneditable:{},
            demo:{
                general:{},
                electronic:[],
                employeer:[],
                pharmecy:[]
            },
            editcounter:false
        }

        this.updateinformation(props);
    }

    collapse = (e) => {
        $(e.target).parents('.titlebar').eq(0).parent().find('.infocontent').eq(0).toggle();
    }

   close = (e,index) => 
   {
       if(this.state.toggle)
       {
            this.setState({                
                toggle:false
            })
       }
       else
       {
            this.props.close(index);
            this.setState({
                selected:0,
                toggle:false
            })
        }

        if(!this.state.toggle)
        {
            $(e.target).parents('.personinfoactive').eq(0).addClass('active');
        }
        else
        {
            $(e.target).parents('.personinfoactive').eq(0).removeClass('active');
        }
   }

   getlocation = (location) => {
       var elementlist = [];
       for(let item in this.props.location)
       {
           if(location == this.props.location[item].value)
           {
               elementlist.push((<span>{this.props.location[item].label}</span>))
           }
       }
       return elementlist;
   }
   deleteemergency = (index) => {
       this.emergencycontact.splice(index,1);
       this.setState({emergencytab:0})
   }
   addeditable = (item,row) => {
        let state = this.state;
        if(item == 'reminderseditable')
        {
            this.data.reminderseditable[row.id] = row.text;
        }
        else
        {
            this.data.reactioneditable[row.id] = row.text;
        }
        
        this.setState({
            updated:!this.state.updated
        })
    }

    update = (item,id) => {
        let value = this.data[item + 'editable'][id];
        if(value)
        {
            this.save(item,{id:id,value:value,profile:this.props.patientid});
        }
        this.remove_editable(item + "editable",id);
    }

   toggle = (e) => {
        this.setState({
            toggle:!this.state.toggle
        })

        if(!this.state.toggle)
        {
            $(e.target).parents('.personinfoactive').eq(0).addClass('active');
        }
        else
        {
            $(e.target).parents('.personinfoactive').eq(0).removeClass('active');
        }
    }

    calold=(date)=>{
        console.log(date);
        return new Date().getFullYear() - new Date(date).getFullYear();
    }

    getsex = (sex) => {
        return sex=="M"?"Male":"Female";
    }

    handleChange = (e,item,index) => {
        this.data[item][index] = e.target.value;
    }
    remove = (item,index) => {
        this.data[item].splice(index,1);
        this.setState({
            updated:!this.state.updated
        })
    }

    updatecounter = () => {
        this.setState({
            editcounter:!this.state.editcounter
        })

        if(!this.state.editcounter)
        {
            this.data.FED = this.state.counter.length > 0?new Date(this.state.counter[0].FED):new Date();
            this.data.LED = this.state.counter.length > 0?new Date(this.state.counter[0].LED):new Date();
            this.data.TEN = this.state.counter.length > 0?this.state.counter[0].TEN:0;
        }
        else
        {
            this.save("counter",{FED:this.data.FED,LED:this.data.LED,TEN:this.data.TEN,patientid:this.props.patientid});
        }
    }

    savedata = (item,index) => {
        let data = this.data[item][index];
        this.save(item,{value:data,profile:this.props.patientid});
        this.data[item].splice(index,1);
    }

    save = (item,data) => 
    {
        let self = this;
         User.saveuserinfo(item,data).then(function(data){
            let state = self.state;
            state[item] = data.data;
            self.setState(state);
         })
    }
    remove_editable = (item,id) =>
    {
        this.data[item][id] = false;
        this.setState({
            updated:!this.state.updated
        });        
    }
    handleCounter = (data,item) => {
        if(item == 'FED' || item == 'LED')
        {
            this.data[item] = new Date(data);
        }
        else
        {
            this.data[item] = data;
        }
    }

    saveemergency = (id) => {
         let data = this.emergencycontact;
         let savedata = [];
         let contact = ["MobileNumber","HomeNumber","WorkNumber","emailaddress"];
         for(let item in data)
         {
             let enable = true;
             if(!data[item]['FirstName'])
             {
                 enable = false;
             }
 
             if(enable)
             {
                 enable = false;
                 for(let itemcontact in contact)
                 {
                     if(data[item][contact[itemcontact]])
                     {
                         enable = true;
                     }
                 }
             }
 
             if(enable)
             {
                 savedata.push(data[item]);
             }
         }
 
         let self = this;
         User.saveemergency(id,savedata).then(function(data){
             self.emergencycontact = data.data;
             self.setState({
                 emergencycontact:data.data,
                 emergency:false,
                 toggle:true
             })
         })
    }
    
    toggleemergency = () => {
        this.setState({
            emergency:false,
            toggle:true
        })
    }

    showemergency = () => {
        this.setState({
            emergency:true,
            toggle:false
        })
    }
    remove = (item,id)=>{
        let self = this;
         User.deleteitem(item,id).then(function(data){
             let state = self.state;
             console.log(data.data);
             if(data.data.success)
             {
                 state[item].map(function(row,index){
                     console.log(row.id);
                     if(row.id == id)
                     {
                         state[item].splice(index,1);
                     }
                 })
                 
                 self.setState(state);
             }
         })
    }
    addemergencycontact = () => {
        this.emergencycontact.push({});
        this.setState({
            activeTab:this.emergencycontact.length - 1
        })
    }

    toggleemergencytab = (tabid) => {
        this.setState({
            emergencytab:tabid
        })
    }

    handleEmergency = (e,index,item) => {
        this.emergencycontact[index][item] = e.target.value;
    }

    editdemo = () => {
        this.setState({
            demomodal:true,
            toggle:false
        })
    }

    add = (item) => {
        this.data[item].push("");
        this.setState({
            updated:!this.state.updated
        })
    }
   closedemo = () => {
        this.setState({
            demomodal:false,
            toggled:true
        })
    }

    emergency = () => {
        this.setState({
            emergency:true,
            toggled:false
        })
    }

    delete = (item,index) => {
        this.data[item].splice(index,1);
        this.setState({
            updated:!this.state.updated
        })
    }

    componentWillReceiveProps(props)
    {
        if(props.updated)
        {
            this.updateinformation(props);
        }
    }

    updateinformation = (props) =>{
        let self = this;
        User.getuserinfo(props.patientid).then(function(data){
            self.data.reminderseditable = [];
            self.emergencycontact = data.data.emergency;
            self.data.reactioneditable = [];
            self.data.FED = data.data.counter.length> 0?new Date(data.data.counter[0].FED):new Date();
            self.data.TEN = data.data.counter.length > 0?data.data.counter[0].TEN:0;
            self.data.LED = data.data.counter.length > 0?new Date(data.data.counter[0].LED):new Date();
            self.setState({
                emergencycontact:data.data.emergency,
                reaction:data.data.reaction,
                counter:data.data.counter,
                demo:data.data.demos,
                reminders:data.data.reminder
            })
        })
    }
    render()
    {
        const StyledPopover = styled(Popover)`
                min-width: 100vw;
        `
        const uploaduri = "http://localhost:4000/";

        this.input = [];
        let inputindex = -1;
        Moment.locale('en')
        momentLocalizer()
        simpleNumberLocalizer();        
        let self = this;
        return (
            <div  id="activepersoninfo" className="personinfoitem">
                <div className="profile">
                    <img src={this.state.demo.general.profile?uploaduri + this.state.demo.general.profile:noimage}></img>
                </div>
                <div className="description">
                    <h3 className="name" onClick={(e)=>this.toggle(e)}>{this.props.name}</h3>
                    <p className="desc">
                        {Moment(new Date(this.state.demo.general.DateOfBirth)).format('MM-DD-YYYY')} , {this.calold(this.state.demo.general.DateOfBirth)} y/o {this.getsex(this.state.demo.general.Sex)}
                    </p>
                </div>
                
                <span className="close" onClick={(e)=>this.close(e,this.props.index)}><FontAwesomeIcon icon={faTimesCircle}></FontAwesomeIcon></span>
                <StyledPopover placement='bottom-end' isOpen={this.state.toggle} target="activepersoninfo">
                    <div className="patientinfo">
                        <PopoverBody>
                            <Row>
                                <Col lg={12} className="titlebar">
                                    <Row>
                                        <Col>Demo Information</Col>
                                        <FontAwesomeIcon icon={faPencilAlt} style={{marginRight:20}} onClick={this.editdemo}></FontAwesomeIcon>
                                        <Badge href="" color="lightblue" style={{marginRight:20,cursor:"pointer"}} onClick={(e)=>this.collapse(e)}>
                                            <FontAwesomeIcon icon={faArrowAltCircleUp}></FontAwesomeIcon>
                                        </Badge>
                                    </Row>
                                </Col>
                                <Col className="infocontent">
                                    <Row className="infoheader">
                                        <Col>Patient Info</Col>
                                        <Col>Allergies</Col>
                                        <Col>Reminders</Col>
                                        <Col>Physcians</Col>
                                        <Col>Patient Counter</Col>
                                        <Col>Address1</Col>
                                        <Col>Address2</Col>
                                    </Row>
                                    <Row className="infobody">
                                        <Col className="detail">
                                            <Row>
                                                <Col className="detailcontainer">
                                                    <Row>
                                                        <div className="label">Phone(Mobile)</div>
                                                        <Col>{this.state.demo.general.MobileNumber}</Col>
                                                    </Row>
                                                    <Row>
                                                        <div className="label">Phone(Work)</div>
                                                        <Col>{this.state.demo.general.WorkNumber}</Col>
                                                    </Row>
                                                    <Row>
                                                        <div className="label">Phone(Home)</div>
                                                        <Col>{this.state.demo.general.HomeNumber}</Col>
                                                    </Row>
                                                    <Row>
                                                        <div className="label">Email</div>
                                                        <Col>{this.state.demo.general.emailaddress}</Col>
                                                    </Row>
                                                    <Row>
                                                        <div className="label">DOB</div>
                                                        <Col>{Moment(new Date(this.state.demo.general.DateOfBirth)).format('MM-DD-YYYY')}</Col>
                                                    </Row>
                                                    <Row>
                                                        <div className="label">Sex</div>
                                                        <Col>{this.getsex(this.state.demo.general.Sex)}</Col>
                                                    </Row>
                                                    <Row>
                                                        <div className="label">SS#</div>
                                                        <Col>{this.state.demo.general.AccountNumber}</Col>
                                                    </Row>
                                                    <Row>
                                                        <div className="label">Location</div>
                                                        <Col>{this.getlocation(this.state.demo.general.location)}</Col>
                                                    </Row>
                                                </Col>
                                                <Col lg={12} className="action">
                                                    <Button color="primary" onClick={this.edit}>Edit</Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col className="detail">
                                            <Row>
                                                <Col lg={12} className="detailcontainer">
                                                    {
                                                        this.state.reaction.map((row,index)=>{
                                                        
                                                            return (
                                                                <Row>
                                                                    <div className="label">
                                                                        <FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon>
                                                                    </div>
                                                                    <Col>{this.data.reactioneditable[row.id]?(<input className="form-control" defaultValue={this.data.reactioneditable[row.id]} onChange={(e)=>this.handleChange(e,"reactioneditable",row.id)}></input>):row.text}</Col>
                                                                    {
                                                                        !this.data.reactioneditable[row.id] && (
                                                                            <div className="action">
                                                                                <Button color="success" onClick={()=>this.addeditable("reaactioneditable",row)}>
                                                                                    <FontAwesomeIcon icon={faPencilAlt}></FontAwesomeIcon>
                                                                                </Button>
                                                                                <Button color="secondary" style={{marginLeft:5}} onClick={() => this.remove("reaction",row.id)}>
                                                                                    <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                                                                </Button>
                                                                            </div>
                                                                        )
                                                                    }
                                                                    {
                                                                        this.data.reactioneditable[row.id] && (
                                                                            <div className="action">
                                                                                <Button color="success" onClick={()=>{this.update("reaction",row.id)}}><FontAwesomeIcon icon={faSave}></FontAwesomeIcon></Button>
                                                                                <Button color="secondary" style={{marginLeft:5}} onClick={() => this.remove_editable("reactioneditable",row.id)}>
                                                                                        <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                                                                    </Button>
                                                                            </div>
                                                                        )
                                                                    }
                                                                </Row>
                                                            )
                                                        })
                                                    }
                                                    {
                                                        this.data.reaction.map((row,index)=>{
                                                            inputindex ++;
                                                            let indexvalue = inputindex;
                                                            
                                                            return (
                                                                <Row>
                                                                    <div className="label">
                                                                        <FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon>
                                                                    </div>
                                                                    <Col><input className="form-control" key={"input_" + index} defaultValue={row} onChange ={(e)=>{this.handleChange(e,"reaction",index)}}></input></Col>
                                                                    <div className="action">
                                                                        <Button color="success">
                                                                            <FontAwesomeIcon icon={faSave} onClick={()=>this.savedata("reaction",index)}></FontAwesomeIcon>
                                                                        </Button>
                                                                        <Button color="secondary" style={{marginLeft:5}} onClick={()=>this.delete("reaction",index)}>
                                                                            <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                                                        </Button>
                                                                    </div>
                                                                </Row>
                                                            )
                                                        })
                                                    }
                                                </Col>
                                                <Col lg={12}>
                                                    <Button color="primary" onClick={()=>this.add('reaction')}>Add more</Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col className="detail">
                                            <Row>
                                                <Col lg={12} className="detailcontainer">
                                                    {
                                                        this.state.reminders.map((row,index)=>{
                                                            return (
                                                                <Row>
                                                                    <div className="label">
                                                                        <FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon>
                                                                    </div>
                                                                    <Col>{this.data.reminderseditable[row.id]?(<input className="form-control" defaultValue={this.data.reminderseditable[row.id]} onChange={(e)=>this.handleChange(e,"reminderseditable",row.id)}></input>):row.text}</Col>
                                                                    {
                                                                        !this.data.reminderseditable[row.id] && (
                                                                            <div className="action">
                                                                                <Button color="success" onClick={()=>this.addeditable("reminderseditable",row)}>
                                                                                    <FontAwesomeIcon icon={faPencilAlt}></FontAwesomeIcon>
                                                                                </Button>
                                                                                <Button color="secondary" style={{marginLeft:5}} onClick={() => this.remove("reminders",row.id)}>
                                                                                    <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                                                                </Button>
                                                                            </div>
                                                                        )
                                                                    }
                                                                    {
                                                                        this.data.reminderseditable[row.id] && (
                                                                            <div className="action">
                                                                                <Button color="success" onClick={()=>{this.update("reminders",row.id)}}><FontAwesomeIcon icon={faSave}></FontAwesomeIcon></Button>
                                                                                <Button color="secondary" style={{marginLeft:5}} onClick={() => this.remove_editable("reminderseditable",row.id)}>
                                                                                        <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                                                                    </Button>
                                                                            </div>
                                                                        )
                                                                    }
                                                                </Row>
                                                            )
                                                        })
                                                    }
                                                    {
                                                        this.data.reminders.map((row,index)=>{
                                                            return (
                                                                <Row>
                                                                    <div className="label">
                                                                        <FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon>
                                                                    </div>
                                                                    <Col><input className="form-control" key={"input_" + index} defaultValue={row} onChange ={(e)=>{this.handleChange(e,"reminders",index)}}></input></Col>
                                                                    <div className="action">
                                                                        <Button color="success">
                                                                            <FontAwesomeIcon icon={faSave} onClick={()=>this.savedata("reminders",index)}></FontAwesomeIcon>
                                                                        </Button>
                                                                        <Button color="secondary" style={{marginLeft:5}} onClick={()=>this.delete("reminders",index)}>
                                                                            <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                                                        </Button>
                                                                    </div>
                                                                </Row>
                                                            )
                                                        })
                                                    }
                                                </Col>
                                                <Col lg={12}>
                                                    <Button color="primary" onClick={()=>this.add('reminders')}>Add more</Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col className="detail"></Col>
                                        <Col className="detail">
                                            <Row>
                                                <Col lg={12} className="detailcontainer" style={{overflowY:"visible"}}>
                                                    <Row>
                                                        <div className="label">
                                                            FED
                                                        </div>
                                                        <Col>
                                                            {!this.state.editcounter && (this.state.counter.length > 0?this.state.counter[0].FED:'')}
                                                            {this.state.editcounter && (<DatePicker defaultValue={this.data.FED} onChange={(date)=>this.handleCounter(date,'FED')}></DatePicker>)}
                                                        </Col>
                                                    </Row>    
                                                    <Row>
                                                        <div className="label">
                                                            LED
                                                        </div>
                                                        <Col>
                                                            {!this.state.editcounter && (this.state.counter.length > 0?this.state.counter[0].LED:'')}
                                                            {this.state.editcounter && (<DatePicker defaultValue={this.data.LED} onChange={(date)=>this.handleCounter(date,'LED')}></DatePicker>)}
                                                        </Col>                                            
                                                    </Row>    
                                                    <Row>
                                                        <div className="label">
                                                            TEN
                                                        </div>
                                                        <Col>
                                                            {!this.state.editcounter && (this.state.counter.length > 0?this.state.counter[0].TEN:'')}
                                                            {this.state.editcounter && (<NumberPicker defaultValue={this.data.TEN} onChange={(date)=>this.handleCounter(date,'TEN')}></NumberPicker>)}
                                                        </Col>
                                                    </Row>    
                                                </Col>
                                                <Col lg={12}>
                                                    <Button color="primary" onClick={this.updatecounter}>{!this.state.editcounter?'Update Patient Counter':'Save'}</Button>
                                                    {this.state.editcounter && (<Button color="secondary" style={{marginLeft:5,padding:"6px 12px"}} onClick={()=>self.setState({editcounter:false})}>Cancel</Button>)}                                    
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col className="detail">
                                            <Row>
                                                <Col lg={12} className="detailcontainer">
                                                    <div className="label">Street</div>
                                                    <Col></Col>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col className="detail">
                                            <Row>
                                                <Col lg={12} className="detailcontainer">
                                                    <div className="label">Street</div>
                                                    <Col></Col>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row className="patientinfoitem">
                                <Col lg={12} className="titlebar">
                                    <Row>
                                        <Col>Emergency Contact</Col>
                                        <Badge href="" color="lightblue" style={{marginRight:20,cursor:"pointer"}} onClick={(e)=>this.collapse(e)}><FontAwesomeIcon icon={faArrowAltCircleUp}></FontAwesomeIcon></Badge>
                                    </Row>
                                </Col>
                                <Col className="infocontent">
                                    <Row className="infoheader">
                                        <Col>Emergency Contact1</Col>
                                        <Col>Emergency Contact2</Col>
                                        <Col>Emergency Contact3</Col>
                                    </Row>
                                    <EmergencyContact  emergencycontact = {this.state.emergencycontact}/>
                                </Col>
                            </Row>
                            <Row className="patientinfoitem">
                                <Col lg={12} className="titlebar">
                                    <Row>
                                        <Col>Employeers</Col>
                                        <Badge href="" color="lightblue" style={{marginRight:20,cursor:"pointer"}} onClick={(e)=>this.collapse(e)}><FontAwesomeIcon icon={faArrowAltCircleUp}></FontAwesomeIcon></Badge>
                                    </Row>
                                </Col>
                                <Col className="infocontent">
                                    <Row className="infoheader">
                                        <Col>Employeer1</Col>
                                        <Col>Employeer2</Col>
                                        <Col>Employeer3</Col>
                                    </Row>
                                    <Row className="infobody">
                                        <Col className="detail">
                                            <Employeer employeer={this.state.demo.employeer[0]?this.state.demo.employeer[0]:{}}></Employeer>
                                        </Col>
                                        <Col className="detail">
                                            <Employeer employeer={this.state.demo.employeer[1]?this.state.demo.employeer[1]:{}}></Employeer>
                                        </Col>
                                        <Col className="detail">
                                            <Employeer employeer={this.state.demo.employeer[2]?this.state.demo.employeer[2]:{}}></Employeer>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row className="patientinfoitem">
                                <Col lg={12} className="titlebar">
                                    <Row>
                                        <Col>Pharmacies</Col>
                                        <Badge href="" color="lightblue" style={{marginRight:20,cursor:"pointer"}} onClick={(e)=>this.collapse(e)}><FontAwesomeIcon icon={faArrowAltCircleUp}></FontAwesomeIcon></Badge>
                                    </Row>
                                </Col>
                                <Col className="infocontent">
                                    <Row className="infoheader">
                                        <Col>Pharmacy1</Col>
                                        <Col>Pharmacy2</Col>
                                        <Col>Pharmacy3</Col>
                                    </Row>
                                    <Row className="infobody">
                                        <Col className="detail">
                                            <Phamacy phamacy={this.state.demo.pharmecy[0]?this.state.demo.pharmecy[0]:{}}></Phamacy>
                                        </Col>
                                        <Col className="detail">
                                            <Phamacy phamacy={this.state.demo.pharmecy[0]?this.state.demo.pharmecy[0]:{}}></Phamacy>
                                        </Col>
                                        <Col className="detail">
                                            <Phamacy phamacy={this.state.demo.pharmecy[0]?this.state.demo.pharmecy[0]:{}}></Phamacy>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row className="patientinfoitem">
                                <Col lg={12} className="titlebar">
                                    <Row>
                                        <Col>Others</Col>
                                        <Badge href="" color="lightblue" style={{marginRight:20,cursor:"pointer"}} onClick={(e)=>this.collapse(e)}><FontAwesomeIcon icon={faArrowAltCircleUp}></FontAwesomeIcon></Badge>
                                    </Row>
                                </Col>
                                <Col className="infocontent">
                                    <Row className="infoheader">
                                        <Col>Electronic Communication</Col>
                                        <Col>Insurance</Col>
                                    </Row>
                                    <Row className="infobody">
                                        <Col className="detail">
                                            <Row>
                                                <ElectricCommunication electronic = {this.state.demo.electronic?this.state.demo.electronic:{}} electric={this.state.demo.general.electric_communication}/>
                                            </Row>
                                        </Col>
                                        <Col className="detail">
                                            <Row>
                                                <Col lg={12} className="detailcontainer">
                                                    <CheckboxGroup
                                                        checkboxWrapClassName="form-check"
                                                        checkboxWrapTag="div"
                                                    >
                                                        <Checkbox
                                                            checkboxClass="icheckbox_square-blue"
                                                            defaultChecked={this.state.demo.general.insurance && this.state.demo.general.insurance.split(',').findIndex("Medicare") > -1?true:false}
                                                            label="Medicare"
                                                            disabled
                                                        ></Checkbox>
                                                        <Checkbox
                                                            checkboxClass="icheckbox_square-blue"
                                                            defaultChecked={this.state.demo.general.insurance && this.state.demo.general.insurance.split(',').findIndex("Medicaid") > -1}
                                                            label="Medicaid"
                                                            disabled
                                                        ></Checkbox>
                                                        <Checkbox
                                                            checkboxClass="icheckbox_square-blue"
                                                            defaultChecked={this.state.demo.general.insurance && this.state.demo.general.insurance.split(',').findIndex("SelfPay") > -1}
                                                            label="SelfPay"
                                                            disabled
                                                        ></Checkbox>
                                                        <Checkbox
                                                            checkboxClass="icheckbox_square-blue"
                                                            defaultChecked={this.state.demo.general.insurance && this.state.demo.general.insurance.split(',').findIndex("Other") > -1}
                                                            label="Other"
                                                            disabled
                                                        ></Checkbox>
                                                    </CheckboxGroup>
                                                </Col>
                                            </Row>
                                        </Col>
                                        
                                    </Row>
                                </Col>
                            </Row>
                        </PopoverBody>                   
                    </div>
                    
                </StyledPopover>
                {
                    this.state.demo.general.Id  && (
                        <Modal isOpen={this.state.emergency} size="lg">
                            <ModalHeader toggle={this.toggleemergency}>Emergency Contact For {this.data.demo.general.FirstName + " " + this.data.demo.general.LastName}</ModalHeader>
                            <ModalBody>
                            <Nav tabs>
                                {
                                    this.emergencycontact.map((row,index)=>{
                                        return (
                                            <NavItem>
                                                <NavLink
                                                    className={classnames({active:this.state.emergencytab == index})}
                                                >
                                                    <span  onClick={()=>this.toggleemergencytab(index)}>Emergency{index + 1} </span>{index > 0 && (<FontAwesomeIcon icon={faTimesCircle} style={{marginLeft:5}} onClick={()=>this.deleteemergency(index)}></FontAwesomeIcon>)}
                                                </NavLink>
                                            </NavItem>
                                        )
                                    })
                                    }
                            </Nav>
                            <TabContent activeTab={this.state.emergencytab} style={{marginTop:15}}>
                                    {
                                        this.emergencycontact.map((row,index)=>{
                                            return (
                                                <TabPane tabId={index}>
                                                    <Form>
                                                        <Row>
                                                            <Col>
                                                                <FormGroup row>
                                                                    <Label lg={4}>First Name</Label>
                                                                    <Col><input className="form-control" defaultValue={row.FirstName} onChange={(e)=>this.handleEmergency(e,index,"FirstName")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Label lg={4}>Last Name</Label>
                                                                    <Col><input className="form-control" defaultValue={row.LastName} onChange={(e)=>this.handleEmergency(e,index,"LastName")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Label lg={4}>Phone(Mobile)</Label>
                                                                    <Col><input className="form-control" defaultValue={row.MobileNumber} onChange={(e)=>this.handleEmergency(e,index,"MobileNumber")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Label lg={4}>Phone(Work)</Label>
                                                                    <Col><input className="form-control" defaultValue={row.WorkNumber} onChange={(e)=>this.handleEmergency(e,index,"WorkNumber")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Label lg={4}>Address1</Label>
                                                                    <Col><input className="form-control" defaultValue={row.AddressLine1} onChange={(e)=>this.handleEmergency(e,index,"AddressLine1")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Col lg={4}>City</Col>
                                                                    <Col><input className="form-control" defaultValue={row.City} onChange={(e)=>this.handleEmergency(e,index,"City")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Col lg={4}>Zip</Col>
                                                                    <Col><input className="form-control" defaultValue={row.Zip} onChange={(e)=>this.handleEmergency(e,index,"Zip")}></input></Col>
                                                                </FormGroup>
                                                            </Col>
                                                            <Col>
                                                                <FormGroup row>
                                                                    <Label lg={4}>Middle Name</Label>
                                                                    <Col><input className="form-control" defaultValue={row.MiddleName} onChange={(e)=>this.handleEmergency(e,index,"MiddleName")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Label lg={4}>Relation</Label>
                                                                    <Col><input className="form-control" defaultValue={row.relation} onChange={(e)=>this.handleEmergency(e,index,"relation")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Label lg={4}>Phone(Home)</Label>
                                                                    <Col><input className="form-control" defaultValue={row.HomeNumber} onChange={(e)=>this.handleEmergency(e,index,"HomeNumber")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Label lg={4}>Email Address</Label>
                                                                    <Col><input className="form-control" defaultValue={row.emailaddress} onChange={(e)=>this.handleEmergency(e,index,"emailaddress")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Label lg={4}>Address2</Label>
                                                                    <Col><input className="form-control" defaultValue={row.AddressLine2} onChange={(e)=>this.handleEmergency(e,index,"AddressLine2")}></input></Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Label lg={4}>State</Label>
                                                                    <Col><input className="form-control" defaultValue={row.State} onChange={(e)=>this.handleEmergency(e,index,"State")}></input></Col>
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>
                                                    </Form>
                                                </TabPane>
                                            )
                                        })
                                    }
                            </TabContent>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="success" onClick={this.addemergencycontact}>Add EmergencyContact</Button>
                                <Button color="primary" onClick={()=>this.saveemergency(this.props.patientid)} style={{marginLeft:10}}>Save</Button>
                                <Button color="secondary" onClick={this.toggleemergency} style={{marginLeft:10}}>Cancel</Button>
                            </ModalFooter>
                        </Modal>        
                    )
                }
                
                {
                    this.state.demo.general.Id && (
                        <Democratic
                        isOpen={this.state.demomodal} 
                        location={this.props.location} 
                        races={this.props.races}
                        demo={this.state.demo}
                        close={this.closedemo}
                        demoid={this.props.patientid}
                        ></Democratic>
                    )
                }
            </div>
            
        )
    }
}

export default ActivePersonInfo;

