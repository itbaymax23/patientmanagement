import React from 'react';
import {Row,Col,Nav,NavItem,NavLink,TabContent,TabPane,Form,FormGroup,Label,Button} from 'reactstrap';
import classnames from 'classnames';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus, faTimes} from '@fortawesome/free-solid-svg-icons';
class Employeer extends React.Component
{
    employeers = [{}];
    constructor(props)
    {
        super(props);
        this.state = {
            selected_employeer:"employeer0"
        }
    }

    componentWillReceiveProps(props)
    {
        if(props.employeer.length > 0)
        {
            this.employeers = props.employeer;
        }
        else
        {
            this.employeers = [{}];
        }
        
        this.setState({
            selected_employeer:"employeer0"
        })
    }

    addEmployeer = () => {
        this.employeers.push({});
        this.setState({
            selected_employeer:"employeer" + (this.employeers.length - 1)
        })
    }

    selecttab = (item) =>{
        console.log(item);
        this.setState({
            selected_employeer:item
        })
    }

    adddepartment = (index) => {
        if(!this.employeers[index].department)
        {
            this.employeers[index].department = [];
        }

        this.employeers[index].department.push({});

        this.selecttab(this.state.selected_employeer);
    }

    deletedepartment = (index,index_item) =>{
        this.employeers[index].department.splice(index_item,1);
        this.selecttab(this.state.selected_employeer);
        this.props.save("employeer",this.employeers);
        
    }
    
    deleteemployeer = (index) => {
        this.employeers.splice(index,1);
        if(index > this.employeers.length - 1)
        {
           this.setState({
               selected_employeer:"employeer" + (this.employeers.length - 1)
           })
        }
        else
        {
            this.setState({
                selected_employeer:"employeer" + index
            })
        }
        this.props.save("employeer",this.employeers);
    }

    handleFieldChange = (e,item,index) => {
        this.employeers[index][item] = e.target.value;
        this.props.save("employeer",this.employeers);
    }

    handleDepartmentChange = (e,index,index_item,item) => 
    {
        if(!this.employeers[index].department)
        {
            this.employeers[index].department = [];
        }

        if(!this.employeers[index].department[index_item])
        {
            this.employeers[index].department[index_item] = {};
        }

        this.employeers[index].department[index_item][item] = e.target.value;
        this.props.save("employeers",this.employeers);
    }
    render()
    {
        let self = this;
        return (
            <Row>
                <Col>
                    <Nav tabs>
                        {
                            this.employeers.map((row,index)=>{
                                return (
                                    <NavItem>
                                        <NavLink className={classnames({active:this.state.selected_employeer == 'employeer' + index})} style={{padding:"10px 20px"}}>
                                            <span  onClick={()=>self.selecttab("employeer" + index)}>{"Employeer #" + index}</span>
                                            {index > 0 && (
                                                <FontAwesomeIcon icon={faTimes} style={{marginLeft:10}} onClick={() => self.deleteemployeer(index)}></FontAwesomeIcon>
                                            )}
                                        </NavLink>
                                        
                                    </NavItem>
                                )
                            })
                        }
                        
                        <NavItem>
                            <NavLink style={{padding:"10px 20px",height:"100%"}} onClick={this.addEmployeer}>
                                <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.selected_employeer} style={{marginTop:20}}>
                        {
                            this.employeers.map((row,index)=>{
                                return (
                                    <TabPane tabId={"employeer" + index}>
                                        <Form>
                                            <Row>
                                                <Col>
                                                    <FormGroup row>
                                                        <Label lg={4}>Name</Label>
                                                        <Col><input className="form-control" defaultValue={row.name} onChange={(e)=>self.handleFieldChange(e,"name",index)}></input></Col>
                                                    </FormGroup>
                                                    <FormGroup row>
                                                        <Label lg={4}>Address</Label>
                                                        <Col><textarea className="form-control" defaultValue={row.address} onChange={(e)=>self.handleFieldChange(e,"address",index)}></textarea></Col>
                                                    </FormGroup>
                                                    <FormGroup row>
                                                        <Label lg={4}>Zip</Label>
                                                        <Col>
                                                            <input className="form-control" defaultValue={row.zip}  onChange={(e)=>self.handleFieldChange(e,"zip",index)}></input>
                                                        </Col>
                                                    </FormGroup>
                                                </Col>
                                                <Col>
                                                    <FormGroup row>
                                                        <Label lg={4}>City</Label>
                                                        <Col>
                                                            <input className="form-control" defaultValue={row.city} onChange={(e)=>self.handleFieldChange(e,"city",index)}></input>
                                                        </Col>
                                                    </FormGroup>
                                                    <FormGroup row>
                                                        <Label lg={4}>State</Label>
                                                        <Col>
                                                            <input className="form-control" defaultValue={row.state} onChange={(e)=>self.handleFieldChange(e,"state",index)}></input>
                                                        </Col>
                                                    </FormGroup>
                                                    <FormGroup row>
                                                        <Button color="primary" style={{marginLeft:"auto",marginRight:15}} onClick={()=>self.adddepartment(index)}>
                                                            <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                                                            Add Department
                                                        </Button>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            {
                                                row.department && row.department.map((item,index_item)=>{
                                                    return (
                                                        <Row>
                                                            <Col>
                                                                <FormGroup row>
                                                                    <Col>
                                                                        <Row>
                                                                            <Label lg={2}>Department #1</Label>
                                                                            <Col>
                                                                                <input className="form-control" defaultValue={item.name} onChange={(e)=>self.handleDepartmentChange(e,index,index_item,'name')}></input>
                                                                            </Col>
                                                                            <Button color="secondary" onClick={()=>this.deletedepartment(index,index_item)} style={{height:35,marginRight:15}}>
                                                                                <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                                                            </Button>
                                                                        </Row>
                                                                    </Col>
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Col>
                                                                        <Row>
                                                                            <Label lg={4}>Contact</Label>
                                                                            <Col>
                                                                                <input className="form-control" defaultValue={item.contact} onChange={(e)=>self.handleDepartmentChange(e,index,index_item,"contact")}></input>
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                    <Col>
                                                                        <Row>
                                                                            <Label lg={4}>Phone</Label>
                                                                            <Col>
                                                                                <input className="form-control" defaultValue={item.phone} onChange={(e)=>self.handleDepartmentChange(e,index,index_item,"phone")}></input>
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                    
                                                                </FormGroup>
                                                                <FormGroup row>
                                                                    <Col>
                                                                        <Row>
                                                                            <Label lg={4}>Fax</Label>
                                                                            <Col>
                                                                                <input className="form-control" defaultValue={item.fax} onChange={(e)=>self.handleDepartmentChange(e,index,index_item,"fax")}></input>
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                    <Col>
                                                                        <Row>
                                                                            <Label lg={4}>Email</Label>
                                                                            <Col>
                                                                                <input className="form-control" defaultValue={item.email} onChange={(e)=>self.handleDepartmentChange(e,index,index_item,"email")}></input>
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>
                                                    )
                                                })
                                            }
                                        </Form>
                                    </TabPane>
                                )
                            })
                        }
                        
                    </TabContent>
                </Col>
            </Row>
            
        )
    }
}

export default Employeer;