import React from 'react';
import {Row,Col} from 'reactstrap';
import {RadioGroup,Radio} from 'react-ui-icheck';

class ElectricCommunication extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            get:"false"
        }
    }

    render()
    {
        return (
                <Col className="detailcontainer">
                    <Row>
                        <div className="label" style={{marginLeft:15,display:"flex"}}>
                            Do you wish to receive electronic communication from us?
                            <RadioGroup
                            className="d-flex align-items-center"
                            name="communicationconnection"
                            radioWrapClassName="form-check form-check-inline"
                            radioWrapTag="div"
                            value={this.props.electric}
                            disabled
                            >
                                <Radio 
                                label="Yes"
                                radioClass="iradio_square-blue"
                                value={1}
                                disabled
                                >
                                </Radio>
                                <Radio 
                                label="No"
                                radioClass="iradio_square-blue"
                                value={0}
                                disabled
                                ></Radio>
                            </RadioGroup>
                        </div>
                    </Row>
                    <Row>
                        <div className="label" style={{marginLeft:15,display:"flex"}}>
                            Send Information to
                            <RadioGroup
                            className="d-flex align-items-center"
                            name="informationto"
                            radioWrapClassName="form-check form-check-inline"
                            radioWrapTag="div"
                            value={this.props.electronic && this.props.electronic.Id?"false":"true"}
                            >
                                <Radio 
                                label="Self"
                                radioClass="iradio_square-blue"
                                value="true"
                                disabled
                                >
                                </Radio>
                                <Radio 
                                label="Other"
                                radioClass="iradio_square-blue"
                                value="false"
                                disabled
                                ></Radio>
                            </RadioGroup>
                        </div>
                    </Row>
                    <Row>
                        <Col>
                            <Row>
                                <div className="label">First Name</div>
                                <Col>{this.props.electronic.FirstName}</Col>
                            </Row>
                            <Row>
                                <div className="label">Last Name</div>
                                <Col>{this.props.electronic.LastName}</Col>
                            </Row>
                            <Row>
                                <div className="label">Relation</div>
                                <Col>{this.props.electronic.relation}</Col>
                            </Row>
                            <Row>
                                <div className="label">Street</div>
                                <Col>{this.props.electronic.AddressLine1}</Col>
                            </Row>
                            <Row>
                                <div className="label">City</div>
                                <Col>{this.props.electronic.City}</Col>
                            </Row>
                            <Row>
                                <div className="label">State</div>
                                <Col>{this.props.electronic.State}</Col>
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <div className="label">Phone(Mobile)</div>
                                <Col>{this.props.electronic.MobileNumber}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(Work)</div>
                                <Col>{this.props.electronic.WorkNumber}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(Home)</div>
                                <Col>{this.props.electronic.HomeNumber}</Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
        )
    }
}

export default ElectricCommunication;