import React from 'react';
import {Row,Col,Nav,NavItem,TabContent,TabPane,FormGroup,Label,NavLink,Form} from 'reactstrap';
import classnames from 'classnames';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus, faTimes} from '@fortawesome/free-solid-svg-icons';

class Pharmecy extends React.Component
{
    pharmecy = [];
    constructor(props)
    {
        super(props);
        this.state = {
            selected_pharmecy:'pharmecy0'
        }
        this.pharmecy = props.pharmecy;
        if(this.pharmecy.length == 0)
        {
            this.pharmecy = [{}];
        }
        this.setState({
            selected_pharmecy:"pharmecy0"
        })
    }

    componentWillReceiveProps(props)
    {
        this.pharmecy = props.pharmecy;
        if(this.pharmecy.length == 0)
        {
            this.pharmecy = [{}];
        }
        this.setState({
            selected_pharmecy:"pharmecy0"
        })
    }

    selecttab = (item) => {
        this.setState({
            selected_pharmecy:item
        })
    }

    addpharmecy = () => {
        this.pharmecy.push({});
        this.selecttab("pharmecy" + (this.pharmecy.length - 1));
    }

    deletepharmecy = (index) =>{
        this.pharmecy.splice(index,1);
        this.selecttab("pharmecy" + Math.min(this.pharmecy.length-1,index))
    }

    handleChange = (e,index,item)=>
    {
        if(!this.pharmecy[index])
        {
            this.pharmecy[index] = {};
        }
        this.pharmecy[index][item] = e.target.value;
        this.props.save("pharmacies",this.pharmecy);
    }

    render()
    {
        let self = this;
        return (
            <Row>
                <Col>
                    <Nav tabs>
                        {
                            this.pharmecy.map((row,index)=>{
                                return (
                                    <NavItem>
                                        <NavLink key={index} className={classnames({active:this.state.selected_pharmecy == 'pharmecy' + index})} style={{padding:"10px 20px"}}>
                                            <span  onClick={()=>this.selecttab("pharmecy" + index)}>{"Pharmecy #" + (index + 1)}</span>
                                            {index > 0 && (
                                                <FontAwesomeIcon icon={faTimes} onClick={()=>self.deletepharmecy(index)} style={{marginLeft:20}}></FontAwesomeIcon>
                                            )}
                                        </NavLink>
                                    </NavItem>            
                                )
                            })
                        }
                        <NavItem>
                            <NavLink style={{padding:"10px 20px",height:"100%"}} onClick={this.addpharmecy}>
                                <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.selected_pharmecy} style={{marginTop:20}}>
                        {
                            this.pharmecy.map((item,index_item)=>{
                                return (
                                    <TabPane key={index_item} tabId={"pharmecy" + index_item}>
                                        <Form>
                                            <Row>
                                                <Col>
                                                    <FormGroup row>
                                                        <Col>
                                                            <Row>
                                                                <Label lg={4}>Name</Label>
                                                                <Col><input className="form-control" defaultValue={item.Name} onChange={(e)=>this.handleChange(e,index_item,"Name")}></input></Col>
                                                            </Row>
                                                        </Col>
                                                        <Col>
                                                            <Row>
                                                                <Label lg={4}>City</Label>
                                                                <Col>
                                                                    <input className="form-control" defaultValue={item.City} onChange={(e)=>this.handleChange(e,index_item,"City")}></input>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </FormGroup>
                                                    <FormGroup row>
                                                        <Col>
                                                            <Row>
                                                                <Label lg={4}>Address</Label>
                                                                <Col><textarea className="form-control" onChange={(e)=>this.handleChange(e,index_item,"Address")} defaultValue={item.address}></textarea></Col>
                                                            </Row>
                                                        </Col>
                                                        <Col>
                                                            <Row>
                                                                <Label lg={4}>State</Label>
                                                                <Col>
                                                                    <input className="form-control" defaultValue={item.State} onChange={(e)=>this.handleChange(e,index_item,"State")}></input>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </FormGroup>
                                                    <FormGroup row>
                                                        <Col>
                                                            <Row>
                                                                <Label lg={4}>Zip</Label>
                                                                <Col>
                                                                    <input className="form-control" defaultValue={item.PostCode} onChange={(e)=>this.handleChange(e,index_item,"PostCode")}></input>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                        <Col>
                                                            <Row>
                                                                <Label lg={4}>Phone</Label>
                                                                <Col>
                                                                    <input className="form-control" defaultValue={item.Telephone} onChange={(e)=>this.handleChange(e,index_item,"Telephone")}></input>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </FormGroup>
                                                    <FormGroup row>
                                                        <Col>
                                                            <Row>
                                                                <Label lg={4}>Fax</Label>
                                                                <Col>
                                                                    <input className="form-control" defaultValue={item.fax} onChange={(e)=>this.handleChange(e,index_item,"fax")}></input>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                        <Col>
                                                            <Row>
                                                                <Label lg={4}>Email Address</Label>
                                                                <Col>
                                                                    <input className="form-control" defaultValue={item.emailaddress} onChange={(e)=>this.handleChange(e,index_item,"emailaddress")}></input>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </Form>
                                    </TabPane>
                                )
                            })
                        }
                        
                    </TabContent>
                </Col>
            </Row>
        )
    }
}

export default Pharmecy;