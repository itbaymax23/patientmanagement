import React from 'react';
import {Row,Col,FormGroup,Form,Label} from 'reactstrap';
import {Radio,RadioGroup} from 'react-icheck';
class ElectricDesignee extends React.Component
{
    electricdesignee = {};
    constructor(props)
    {
        super(props);
        console.log(props.enable);
        this.state = {
            enable:props.enable,
            sendto:"self"
        }
    }

    componentWillReceiveProps(props)
    {
        let sendto = 'self';
        if(props.electric.Id)
        {
            sendto = 'other';
            this.electricdesignee = props.electric;
        }

        this.setState({
            enable:props.enable,
            sendto:sendto
        })        
    }

    changeenable = (e) => {
        this.props.enablecommunication(e.target.value);
        this.setState({
            enable:e.target.value
        })
    }
    
    handleValue = (e) => {
        this.setState({
            sendto:e.target.value
        })
    }

    handleChange = (e,item) => {
        this.electricdesignee[item] = e.target.value;
        this.props.save("electronic",this.electricdesignee);
    }

    render(){
        return (
            <Row>
                <Col>
                    <Row>
                        <div style={{paddingLeft:15,marginRight:20}}>
                            Do you want to receive electronic communications from us?
                        </div>
                        <RadioGroup name="radio" value={this.state.enable} onChange={(e)=>this.changeenable(e)}>
                            <Radio
                                value={1}
                                radioClass="iradio_square-blue"
                                increaseArea="10%"
                                label="Yes"
                            />
                            <Radio
                                value={0}
                                radioClass="iradio_square-blue"
                                increaseArea="10%"
                                label="No"
                                
                            />
                        </RadioGroup>
                    </Row>
                    <Row>
                        <Col lg={4}>Send the Information to:</Col>
                        <RadioGroup name="sendto" value={this.state.sendto} onChange={(e)=>this.handleValue(e)}>
                            <Radio
                                value="self"
                                radioClass="iradio_square-blue"
                                increaseArea="10%"
                                label="Self"
                                disabled={this.state.enable != 1}
                            />
                            <Radio
                                value="other"
                                radioClass="iradio_square-blue"
                                increaseArea="10%"
                                label="Other"
                                disabled={this.state.enable != 1}
                            />
                        </RadioGroup>
                    </Row>
                    <Form>
                        <FormGroup row>
                            <Col>
                                <Row>
                                    <Label lg={4}>Last Name</Label>
                                    <Col>
                                        <input 
                                            className="form-control"  
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.LastName}
                                            onChange={(e)=>this.handleChange(e,"LastName")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Label lg={4}>First Name</Label>
                                    <Col>
                                        <input 
                                            className="form-control"  
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.FirstName}
                                            onChange={(e)=>this.handleChange(e,"FirstName")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col>
                                <Row>
                                    <Label lg={4}>Middle Name</Label>
                                    <Col>
                                        <input
                                            className="form-control"  
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.MiddleName}
                                            onChange={(e)=>this.handleChange(e,"MiddleName")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Label lg={4}>Relation</Label>
                                    <Col>
                                        <input
                                            className="form-control" 
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.relation}
                                            onChange={(e)=>this.handleChange(e,"relation")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col>
                                <Row>
                                    <Label lg={4}>Phone(Mobile)</Label>
                                    <Col>
                                        <input 
                                            className="form-control" 
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.MobileNumber}
                                            onChange={(e)=>this.handleChange(e,"MobileNumber")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Label lg={4}>Phone(Home)</Label>
                                    <Col>
                                        <input
                                            className="form-control" 
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.HomeNumber}
                                            onChange={(e)=>this.handleChange(e,"HomeNumber")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col>
                                <Row>
                                    <Label lg={4}>Phone(Work)</Label>
                                    <Col>
                                        <input 
                                            className="form-control" 
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.WorkNumber}
                                            onChange={(e)=>this.handleChange(e,"WorkNumber")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Label lg={4}>Email Address</Label>
                                    <Col>
                                        <input 
                                            className="form-control" 
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.emailaddress}
                                            onChange={(e)=>this.handleChange(e,"emailaddress")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col>
                                <Row>
                                    <Label lg={4}>Address</Label>
                                    <Col>
                                        <textarea 
                                            className="form-control" 
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.AddressLine1}
                                            onChange={(e)=>this.handleChange(e,"AddressLine1")}
                                        ></textarea>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Label lg={4}>City</Label>
                                    <Col>
                                        <input 
                                            className="form-control" 
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.City}
                                            onChange={(e)=>this.handleChange(e,"City")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col>
                                <Row>
                                    <Label lg={4}>State</Label>
                                    <Col>
                                        <input 
                                            className="form-control" 
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.State}
                                            onChange={(e)=>this.handleChange(e,"State")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Label lg={4}>Zip</Label>
                                    <Col>
                                        <input 
                                            className="form-control" 
                                            disabled={this.state.enable != '1' || this.state.sendto != 'other'}
                                            defaultValue={this.electricdesignee.PostCode}
                                            onChange={(e)=>this.handleChange(e,"PostCode")}
                                        ></input>
                                    </Col>
                                </Row>
                            </Col>
                        </FormGroup>
                    </Form>
                
                </Col>
            </Row>
        )
    }
}

export default ElectricDesignee;