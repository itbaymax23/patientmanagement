import React from 'react';
import {Form,FormGroup,Label,Input,FormText,Col,Row} from 'reactstrap';
import Select from 'react-select';
class General extends React.Component
{
    generalinfo = {};
    constructor(props)
    {
        super(props);
        if(props.general)
        {   
            this.generalinfo = props.general;
        }
    }

    handleChange = (value,item) => {
        this.generalinfo[item] = value;
        this.props.save('general',this.generalinfo);
    }

    componentWillReceiveProps(props)
    {
        if(props.general)
        {   
            this.generalinfo = props.general;
        }
    }

    render()
    {
        let races = [];
        let selected = {};
        for(let item in this.props.races)
        {
            if(this.props.races[item].Id == this.generalinfo.RaceId)
            {
                selected = {label:this.props.races[item].Name,value:this.props.races[item].Id}
            }
            races.push({label:this.props.races[item].Name,value:this.props.races[item].Id})
        }

        let selectedlocation = {};

        for(let item in this.props.location)
        {
            if(this.props.location[item].value == this.generalinfo.location)
            {
                selectedlocation = this.props.location[item];
            }
        }

        return (
            <Form>
                <Row>
                    <Col>
                        <FormGroup row>
                            <Label lg={4}>First Name</Label>
                            <Col>
                                <Input defaultValue={this.generalinfo.FirstName} onChange={(e)=>this.handleChange(e.target.value,"FirstName")}></Input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Middle Name</Label>
                            <Col>
                                <Input defaultValue={this.generalinfo.MiddleName} onChange={(e)=>this.handleChange(e.target.value,"MiddleName")}></Input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Last Name</Label>
                            <Col>
                                <Input defaultValue={this.generalinfo.LastName} onChange={(e)=>this.handleChange(e.target.value,"MiddleName")}></Input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>DOB</Label>
                            <Col>
                                <Input defaultValue={this.generalinfo.DateOfBirth} onChange={(e)=>this.handleChange(e.target.value,"DateOfBirth")}></Input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Sex</Label>
                            <Col>
                                <select className="form-control" defaultValue={this.generalinfo.Sex} onChange={(e)=>this.handleChange(e.target.value,"Sex")}>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                </select>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Race</Label>
                            <Col>
                                <Select options={races} defaultValue={selected} onChange={(value)=>this.handleChange(value.value,"RaceId")}></Select>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>State</Label>
                            <Col>
                                <input className="form-control" defaultValue={this.generalinfo.State} onChange={(e)=>this.handleChange(e.target.value,"State")}></input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Address 1</Label>
                            <Col>
                                <textarea className="form-control" defaultValue={this.generalinfo.AddressLine1} onChange={(e)=>this.handleChange(e.target.value,"AddressLine1")}></textarea>
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup row>
                            <Label lg={4}>Phone(MOBILE)</Label>
                            <Col>
                                <input className="form-control" defaultValue={this.generalinfo.MobileNumber} onChange={(e)=>this.handleChange(e.target.value,"MobileNumber")}></input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Phone(WORK)</Label>
                            <Col>
                                <input className="form-control" defaultValue={this.generalinfo.WorkNumber}  onChange={(e)=>this.handleChange(e.target.value,"WorkNumber")}></input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Phone(HOME)</Label>
                            <Col>
                                <input className="form-control" defaultValue={this.generalinfo.HomeNumber} onChange={(e)=>this.handleChange(e.target.value,"HomeNumber")}></input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Email Address</Label>
                            <Col>
                                <input className="form-control" defaultValue={this.generalinfo.emailaddress} onChange={(e)=>this.handleChange(e.target.value,"emailaddress")}></input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Location</Label>
                            <Col>
                                <Select options={this.props.location} defaultValue={selectedlocation} onChange={(e)=>this.handleChange(e.value,"location")}></Select>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>City</Label>
                            <Col>
                                <input className="form-control" defaultValue={this.generalinfo.City} onChange={(e)=>this.handleChange(e.target.value,"City")}></input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>PostCode</Label>
                            <Col>
                                <input className="form-control" defaultValue={this.generalinfo.PostCode} onChange={(e)=>this.handleChange(e.target.value,"PostCode")}></input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Address 2</Label>
                            <Col>
                                <textarea className="form-control" onChange={(e)=>this.handleChange(e.target.value,"AddressLine2")} defaultValue={this.generalinfo.AddressLine2}></textarea>
                            </Col>
                        </FormGroup>
                    </Col>
                </Row>
            </Form>
        )
    }
}

export default General;