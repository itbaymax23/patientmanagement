import React from 'react';
import PersonInfoItems from './PersonInfoItem';
import * as User from '../../../action/user';
import ActivePersonInfo from './ActivePersoninfo';

class PersonInfo extends React.Component
{
    emergencycontact = [{}];
    constructor(props)
    {
        super(props);
        this.state = {
            selected:0,
            toggled:false,
            emergencycontact:[],
            reminders:[],
            reaction:[],
            counter:[],
            dataenabled:false,
            emergency:false,
            emergencytab:0,
            demomodal:false,
            demo:{
                general:{},
                electronic:[],
                employeer:[],
                pharmecy:[]
            }
        }
    }

   close(index)
   {
      this.props.close(index);
   }

   toggle = (index) => 
   {
       if(this.state.selected != index)
       {
            this.props.selectpatient(index);        
       }

       this.setState({
           selected:index
       })
   }

   toggleSwitch = (index) => {
        this.props.activate(index);
   }

   save = (item,data) => 
   {
       let self = this;
        User.saveuserinfo(item,data).then(function(data){
            console.log(data);
            let state = self.state;
            state[item] = data.data;
            self.setState(state);
        })
   }

   saveemergency = (id) => {
        let data = this.emergencycontact;
        let savedata = [];
        let contact = ["MobileNumber","HomeNumber","WorkNumber","emailaddress"];
        for(let item in data)
        {
            let enable = true;
            if(!data[item]['FirstName'])
            {
                enable = false;
            }

            if(enable)
            {
                enable = false;
                for(let itemcontact in contact)
                {
                    if(data[item][contact[itemcontact]])
                    {
                        enable = true;
                    }
                }
            }

            if(enable)
            {
                savedata.push(data[item]);
            }
        }

        let self = this;
        User.saveemergency(id,savedata).then(function(data){
            self.setState({
                emergencycontact:data.data,
                emergency:false,
                toggled:true
            })
        })
   }

   remove = (item,id)=>{
       let self = this;
        User.deleteitem(item,id).then(function(data){
            let state = self.state;
            if(data.data.success)
            {
                state[item].map(function(row,index){
                    if(row.id == id)
                    {
                        state[item].splice(index,1);
                    }
                })

                self.setState(state);
            }
        })
   }

   emergency = () => {
       this.setState({
           emergency:true,
           toggled:false
       })
   }

   toggleemergency = () => {
        this.setState({
            emergency:false,
            toggled:true
        })
   }

   toggleemergencytab = (tabid) => {
       this.setState({
           emergencytab:tabid
       })
   }

   addemergencycontact = () => {
        this.emergencycontact.push({});
        this.setState({
            activeTab:this.emergencycontact.length - 1
        })
   }

   handleEmergency = (e,index,item) => {
        this.emergencycontact[index][item] = e.target.value;
   }

   editdemo = () => {
        this.setState({
            demomodal:true,
            toggled:false
        })
   }

   closedemo = () => {
       this.setState({
            demomodal:false,
            toggled:true
       })
   }

   render(){
      let self = this;
      let patientdata = this.props.patient.slice(0,7); 
      console.log(patientdata[this.state.selected]);
       return(
           <div className="personinfo">
            {
                patientdata.map((row,index) => {
                    return self.state.selected != index && (
                    <PersonInfoItems 
                    key={`personinfoitem-${index}`} 
                    name={row.FirstName + " " + row.LastName} 
                    close={(index)=>self.close(index)} 
                    index={index}
                    patientid={row.Id} 
                    toggle={(index)=>this.toggle(index)}
                    ></PersonInfoItems>
                )})
            }
           </div>

           
            
       );
   }
}

export default PersonInfo;