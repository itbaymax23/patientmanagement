import React from 'react';
import {Modal,ModalHeader,ModalBody,ModalFooter,Nav,NavItem,NavLink,TabContent,TabPane,Button} from 'reactstrap';
import General from './General';
import ElecticDesignee from './electricdesinee';
import Employeer from './Employeer';
import Pharmecy from './Permecy';
import classnames from 'classnames';
import * as User from '../../../action/user';
class Democratic extends React.Component
{
    demo = {};
    constructor(props)
    {
        super(props);
        this.state = {
            selected_tabindex: "general"
        }

        this.demo = props.demo;
    }

    selecttab = (index) => {
        this.setState({
            selected_tabindex:index
        })
    }

    componentWillReceiveProps(props)
    {
        this.demo = props.demo;
        console.log(this.demo);
    }

    save = (item,data) => {
        this.demo[item] = data;
    }

    savedemocratic = () => {
        let self = this;
        User.updatedemocratics(this.demo,this.props.demoid).then(function(data){
            self.props.close();
        })
    }
    enablecommunication = (value) => {
        this.demo.general.electric_communication = value;
    }
    render()
    {
        return (
            <Modal isOpen={this.props.isOpen} size="lg">
                <ModalHeader toggle={this.props.close}>Democratic Information</ModalHeader>
                <ModalBody>
                    <Nav tabs>
                        <NavItem onClick={()=>this.selecttab("general")}>
                            <NavLink className={classnames({active:this.state.selected_tabindex == 'general'})} style={{padding:"10px 20px"}}>
                                <span>General</span>
                            </NavLink>
                        </NavItem>
                        <NavItem onClick={()=>this.selecttab("electronic")}>
                            <NavLink className={classnames({active:this.state.selected_tabindex == 'electronic'})} style={{padding:"10px 20px"}}>
                                <span>Electronic Designee</span>
                            </NavLink>
                        </NavItem>
                        <NavItem onClick={()=>this.selecttab("employeer")}>
                            <NavLink className={classnames({active:this.state.selected_tabindex == 'employeer'})} style={{padding:"10px 20px"}}>
                                <span>Employeer</span>
                            </NavLink>
                        </NavItem>
                        <NavItem onClick={()=>this.selecttab("pharmecy")}>
                            <NavLink className={classnames({active:this.state.selected_tabindex == 'pharmecy'})} style={{padding:"10px 20px"}}>
                                <span>Pharmecy</span>
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.selected_tabindex} style={{marginTop:20}}>
                        <TabPane tabId="general">
                            <General location={this.props.location} races={this.props.races} general={this.demo.general} save={(item,data)=>this.save(item,data)}/>
                        </TabPane>
                        <TabPane tabId="electronic">
                            <ElecticDesignee electric={this.demo.electronic} save={(item,data)=>this.save(item,data)} enable={this.demo.general ? this.demo.general.electric_communication:'0'} enablecommunication={(value)=>this.enablecommunication(value)}></ElecticDesignee>
                        </TabPane>
                        <TabPane tabId="employeer">
                            <Employeer employeer = {this.demo.employeer} save={(item,data)=>this.save(item,data)}></Employeer>
                        </TabPane>
                        <TabPane tabId="pharmecy">
                            <Pharmecy pharmecy={this.demo.pharmecy} save={(item,data)=>this.save(item,data)}/>
                        </TabPane>
                    </TabContent>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={()=>this.props.close()}>Cancel</Button>
                    <Button color="success" style={{marginLeft:10}} onClick={this.savedemocratic}>Save Changes</Button>
                </ModalFooter>
            </Modal>
        )
    }
}

export default Democratic;