import React from 'react';
import {Row,Col} from 'reactstrap';

class Employeer extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <Row>
                <Col lg={12} className="detailcontainer">
                    <Row>
                        <div className="label">Name</div>
                        <Col>{this.props.employeer.name}</Col>
                    </Row>
                    <Row>
                        <div className="label">Street</div>
                        <Col>{this.props.employeer.address}</Col>
                    </Row>
                    <Row>
                        <div className="label">State</div>
                        <Col>{this.props.employeer.state}</Col>
                    </Row>
                    <Row>
                        <div className="label">Zip</div>
                        <Col>{this.props.employeer.zip}</Col>
                    </Row>
                    <Row>
                        {
                            this.props.employeer.department && this.props.employeer.department.map((row,index)=>{
                                return (
                                    <Col lg={12}>
                                        <Row>
                                            <Col lg={12}>
                                                <h6>Department # {index}</h6>
                                            </Col>
                                            <Col lg={12}>
                                                <Row>
                                                    <div className="label">Contact</div>
                                                    <Col>{row.contact}</Col>
                                                </Row>
                                                <Row>
                                                    <div className="label">Phone</div>
                                                    <Col>{row.phone}</Col>
                                                </Row>
                                                <Row>
                                                    <div className="label">Email</div>
                                                    <Col>{row.email}</Col>
                                                </Row>
                                                <Row>
                                                    <div className="label">Fax</div>
                                                    <Col>{row.fax}</Col>
                                                </Row>
                                            </Col>
                                
                                        </Row>
                                    </Col>
                                    )
                                })
                        }
                    </Row>
                </Col>
            </Row>
        )
    }
}

export default Employeer;