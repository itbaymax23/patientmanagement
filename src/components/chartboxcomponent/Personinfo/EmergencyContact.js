import React from 'react';
import {Row,Col} from 'reactstrap';

class EmergencyContact extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <Row className="infobody">
                <Col className="detail">
                    <Row>
                        <Col lg={12} className="detailcontainer">
                            <Row>
                                <div className="label">First Name</div>
                                <Col>{this.props.emergencycontact[0]?this.props.emergencycontact[0].FirstName:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Last Name</div>
                                <Col>{this.props.emergencycontact[0]?this.props.emergencycontact[0].LastName:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Relation</div>
                                <Col>{this.props.emergencycontact[0]?this.props.emergencycontact[0].relation:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(mobile)</div>
                                <Col>{this.props.emergencycontact[0]?this.props.emergencycontact[0].MobileNumber:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(Home)</div>
                                <Col>{this.props.emergencycontact[0]?this.props.emergencycontact[0].HomeNumber:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(Work)</div>
                                <Col>{this.props.emergencycontact[0]?this.props.emergencycontact[0].WorkNumber:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Email Address</div>
                                <Col>{this.props.emergencycontact[0]?this.props.emergencycontact[0].emailaddress:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Street</div>
                                <Col></Col>
                            </Row>
                            <Row>
                                <div className="label">City</div>
                                <Col></Col>
                            </Row>
                            <Row>
                                <div className="label">State</div>
                                <Col></Col>
                            </Row>
                            <Row>
                                <div className="label">Zip</div>
                                <Col></Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col className="detail">
                    <Row>
                        <Col lg={12} className="detailcontainer">
                            <Row>
                                <div className="label">First Name</div>
                                <Col>{this.props.emergencycontact[1]?this.props.emergencycontact[1].FirstName:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Last Name</div>
                                <Col>{this.props.emergencycontact[1]?this.props.emergencycontact[1].LastName:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Relation</div>
                                <Col>{this.props.emergencycontact[1]?this.props.emergencycontact[1].relation:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(mobile)</div>
                                <Col>{this.props.emergencycontact[1]?this.props.emergencycontact[1].MobileNumber:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(Home)</div>
                                <Col>{this.props.emergencycontact[1]?this.props.emergencycontact[1].HomeNumber:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(Work)</div>
                                <Col>{this.props.emergencycontact[1]?this.props.emergencycontact[1].WorkNumber:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Email Address</div>
                                <Col>{this.props.emergencycontact[1]?this.props.emergencycontact[1].emailaddress:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Street</div>
                                <Col></Col>
                            </Row>
                            <Row>
                                <div className="label">City</div>
                                <Col></Col>
                            </Row>
                            <Row>
                                <div className="label">State</div>
                                <Col></Col>
                            </Row>
                            <Row>
                                <div className="label">Zip</div>
                                <Col></Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col className="detail">
                    <Row>
                        <Col lg={12} className="detailcontainer">
                            <Row>
                                <div className="label">First Name</div>
                                <Col>{this.props.emergencycontact[2]?this.props.emergencycontact[2].FirstName:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Last Name</div>
                                <Col>{this.props.emergencycontact[2]?this.props.emergencycontact[2].LastName:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Relation</div>
                                <Col>{this.props.emergencycontact[2]?this.props.emergencycontact[2].relation:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(mobile)</div>
                                <Col>{this.props.emergencycontact[2]?this.props.emergencycontact[2].MobileNumber:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(Home)</div>
                                <Col>{this.props.emergencycontact[2]?this.props.emergencycontact[2].HomeNumber:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Phone(Work)</div>
                                <Col>{this.props.emergencycontact[2]?this.props.emergencycontact[2].WorkNumber:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Email Address</div>
                                <Col>{this.props.emergencycontact[2]?this.props.emergencycontact[2].emailaddress:""}</Col>
                            </Row>
                            <Row>
                                <div className="label">Street</div>
                                <Col></Col>
                            </Row>
                            <Row>
                                <div className="label">City</div>
                                <Col></Col>
                            </Row>
                            <Row>
                                <div className="label">State</div>
                                <Col></Col>
                            </Row>
                            <Row>
                                <div className="label">Zip</div>
                                <Col></Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
                                
        )
    }
}

export default EmergencyContact;