import React from 'react';
import {Row,Col} from 'reactstrap';


//component
import {PatientInfo,CC,PMH,PSH,Meds,SH,ROS, HPI, ADR, FH, VS,Assessments, PE,PO} from '../Preview';


class PreviewPrint extends React.Component
{
    constructor(props)
    {
        super(props);
    }
    
    notetype = [{
        label:'History & Physical',value:1
    },{
        label:'Progress & Note',value:2
    },{
        label:'Consult Note & Template',value:3
    },{
        label:'Discurage Summary & Template',value:4
    }];

    getnotetype = (id) => {
        for(let item in this.notetype)
        {
            if(this.notetype[item].value == id)
            {
                return this.notetype[item].label;
            }
        }

        return "";
    }

    render()
    {

        return (
            <Row style={{marginTop:40}} className="preview">
                <Col lg={12} style={{textAlign:"center"}}>
                    <h3>{this.props.location}</h3>
                </Col>
                <Col lg={12} style={{textAlign:"center"}}>
                    <h5>{this.getnotetype(this.props.notetype)}</h5>
                </Col>
                <Col lg={12} className="patientcontainer">
                    <PatientInfo
                     patient={this.props.patientdata}
                     date={this.props.date}
                     ></PatientInfo>
                </Col>
                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 1)) && (
                        <Col lg={12} className="CC">
                            <CC
                            data={this.props.data.CC}
                            patient={this.props.patientdata}
                            ></CC>
                        </Col>
                    )
                }
                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 2)) && (
                        <Col lg={12} className="HPI">
                            <HPI data={this.props.data.HPI} patient={this.props.patientdata}></HPI>
                        </Col>
                    )
                }
                
                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 3)) && (
                        <Col lg={12} className="PMH">
                            <PMH
                                data={this.props.data.PMH}
                            ></PMH>
                        </Col>
                    )
                }
                
                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 4)) && (
                        <Col lg={12} className="PMH">
                            <PSH data={this.props.data.PSH}></PSH>
                        </Col>
                    )
                }
                
                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 5)) && (
                        <Col lg={12} className="previewcontainer">
                            <Meds data={this.props.data.MEDS}></Meds>
                        </Col>
                    )
                }
                
                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 6)) && (
                        <Col lg={12} className="ADR">
                            <ADR data={this.props.data.ADR}></ADR>
                        </Col>
                    )
                }
                
                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 7)) && (
                        <Col lg={12}  className="previewcontainer">
                            <SH data={this.props.data.SHI}></SH>
                        </Col>
                    )
                }

                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 8)) && (
                        <Col lg={12} className="FH">
                            <FH data={this.props.data.FH}></FH>
                        </Col>
                    )
                }

                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 9)) && (
                        <Col lg={12} className="previewcontainer">
                            <ROS data={this.props.data.ROS}></ROS>
                        </Col>
                    )
                }

                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 10)) && (
                        <Col lg={12} className="previewcontainer">
                            <VS data={this.props.data.VitalSign}></VS>
                        </Col>
                    )
                }

                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 11)) && (
                        <Col lg={12} className="previewcontainer">
                            <PE PE={this.props.data.PE}></PE>
                        </Col>
                    )
                }

                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 12)) && (
                        <Col lg={12} className="previewcontainer">
                            <Assessments data={this.props.data.Assessments}></Assessments>
                        </Col>
                    )
                }

                {
                    (!this.props.previewonly || (this.props.previewonly && this.props.previewsectionid == 13)) && (
                        <Col lg={12} className="previewcontainer">
                            <PO data={this.props.data.PO} assessments={this.props.data.Assessments}></PO>
                        </Col>
                    )
                }   
            </Row>
        )
    }
}

export default PreviewPrint;