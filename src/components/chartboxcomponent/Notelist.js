import React from 'react';
import {Row,Col,Modal,ModalHeader,ModalBody,Table,Button} from 'reactstrap';
import Moment from 'moment';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEye} from '@fortawesome/free-solid-svg-icons';
//component for preload notes
class Notelist extends React.Component
{
    NoteType =[{
                label:'History & Physical',value:1
            },
            {
                label:'Progress & Note',value:2
            },
            {
                label:'Consult Note & Template',value:3
            },
            {
                label:'Discurage Summary & Template',value:4
            }];

    constructor(props)
    {
        super(props);
    }

    getnotetype = (noteid) => {
        for(let item in this.NoteType)
        {
            if(this.NoteType[item].value == noteid)
            {
                return this.NoteType[item].label;
            }
        }
    }

    render()
    {
        return (
            <Modal isOpen={this.props.isopen} size="lg">
                <ModalHeader toggle={this.props.toggle}>Select Previous Note to Pull Forward</ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>DocType</th>
                                        <th>Author</th>
                                        <th>Title</th>
                                        <th>Preview</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.props.notelist.map((row,index)=>{
                                            return (
                                                <tr key={index}>
                                                    <td>{Moment(new Date(row.NoteDate)).format("MM/DD/YYYY")}</td>
                                                    <td>{this.getnotetype(row.NoteTypeId)}</td>
                                                    <td>KirkLand Dodson</td>
                                                    <td>{row.Name}</td>
                                                    <td>
                                                        <Button color="success" onClick={()=>this.props.viewnotes(row.Id)}>
                                                            <FontAwesomeIcon icon={faEye} style={{marginRight:10}}></FontAwesomeIcon> View
                                                        </Button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>
        )
    }
}

export default Notelist;