import React from 'react';
import {Row,Col,Table,Popover,PopoverBody} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import * as Macro from '../../action/Macro';
import $ from 'jquery';
class AssessmentTable extends React.Component
{

    selecteditem = false;
    selectedindex = 0;
    selectedtype = "labs";
    
    constructor(props)
    {
        super(props);
        this.state = {
            data:[],
            selectedid:false,
            open:false
        }
    }

    componentDidMount()
    {
        let self = this;
        $(document).on('click',function(event){
            if($(event.target).parents('.assessmentadd').length == 0)
            {
                self.setState({
                    selectedid:false,
                    open:false
                })
            }
        })
    }

    add = (item,index,type) => {
        this.selecteditem = item;
        this.selectedindex = index;
        this.selectedtype = type;
        let self = this;
        console.log('add_item',item);
        Macro.searchplan({type:type,value:""}).then(function(data){
            self.setState({
                selectedid:item + "_" + index + "_" + type,
                open:true,
                data:data.data
            })    
        })
    }

    search = (value) => {
        let self = this;

        Macro.searchplan({type:this.selectedtype,value:value}).then(function(data){
            self.setState({
                data:data.data
            })
        })
    }

    adddata = (row) => {
        let data = this.props.assessments;
        if(!data[this.selecteditem][this.selectedindex][this.selectedtype])
        {
            data[this.selecteditem][this.selectedindex][this.selectedtype] = [];
        }

        if(this.isselected(row) > -1)
        {
            data[this.selecteditem][this.selectedindex][this.selectedtype].splice(this.isselected(row),1);    
        }
        else
        {
            data[this.selecteditem][this.selectedindex][this.selectedtype].push(row);
        }
        

        this.props.save('Assessments',data,12);
    }

    isselected = (row) => {
        let data = this.props.assessments[this.selecteditem];
        if(!data[this.selectedindex][this.selectedtype])
        {
            return -1;
        }

        if(this.selectedtype == 'labs' || this.selectedtype == 'imaging')
        {
            let code = [];
            for(let item in data[this.selectedindex][this.selectedtype])
            {
                code.push(data[this.selectedindex][this.selectedtype][item].Code);
            }

            if(code.indexOf(row.Code) > -1)
            {
                return code.indexOf(row.Code);
            }
            else
            {
                return -1;
            }
        }
        else if(this.selectedtype == 'meds')
        {
            let meds = [];

            for(let item in data[this.selectedindex][this.selectedtype])
            {
                meds.push(data[this.selectedindex][this.selectedtype][item].name + '(' + data[this.selectedindex][this.selectedtype][item].dosage + ')');
            }

            if(meds.indexOf(row.name + '(' + row.dosage + ')') > -1)
            {
                return meds.indexOf(row.name + '(' + row.dosage + ')');
            }
            else
            {
                return -1;
            }
        }
    }
    render(){
        let self = this;
        console.log('selected',this.state.selectedid);
        return (
            <Row>
                <Col>
                    <Table responsive bordered>
                        <thead  className="assessmenttable_header">
                            <th className="planorderheader">Assessments/Orders</th>
                            <th>Laboratory</th>
                            <th>Imaging</th>
                            <th>Medication</th>
                            <th>Referrals</th>
                            <th>Nursing</th>
                            <th>Instruction</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td colSpan={7}><p className="assessmenttable_title">Primary Diagnosis(es) <FontAwesomeIcon icon={faPlus} style={{cursor:"pointer",color:'#4187f7'}} onClick={()=>this.props.showproblem('Assessments','primary',12)}></FontAwesomeIcon></p></td>
                            </tr>
                            {
                                this.props.assessments.primary.map((row,index)=>{
                                    return (
                                        <tr key={index}>
                                            <td><p className="assessmenttable_content" style={{fontStyle:'italic'}}>{row.CommonName?row.CommonName:row.ShortDesc}</p></td>
                                            <td id={"primary_" + index + "_labs"} className="contenttd" onClick={()=>this.add('primary',index,'labs')}>
                                                {
                                                    row.labs && row.labs.map((content,index) => {
                                                        return (
                                                            <p key={index} className="assessmenttable_content">{content.ComonName?content.CommonName:content.BillDesc} ({content.Code})</p>
                                                        )
                                                    })
                                                }
                                            </td>
                                            <td id={"primary_" + index + "_imaging"} className="contenttd" onClick={()=>this.add('primary',index,'imaging')}>
                                                {
                                                    row.imaging && row.imaging.map((content,index) => {
                                                        return (
                                                            <p key={index} className="assessmenttable_content">{content.ComonName?content.CommonName:content.BillDesc} ({content.Code})</p>
                                                        )
                                                    })
                                                }
                                            </td>
                                            <td id={"primary_" + index + "_meds"} className="contenttd" onClick={()=>this.add('primary',index,'meds')}>
                                                {
                                                    row.meds && row.meds.map((content,index)=>{
                                                        return (
                                                            <p key={index} className="assessmenttable_content">{content.name} ({content.dosage})</p>
                                                        )
                                                    })
                                                }
                                            </td>
                                            <td id={"primary_" + index + "_referal"} className="contenttd" onClick={()=>this.add('primary',index,'referal')}></td>
                                            <td id={"primary_" + index + "_nursing"} className="contenttd" onClick={()=>this.add('primary',index,'nursing')}></td>
                                            <td id={"primary_" + index + "_instruction"} className="contenttd" onClick={()=>this.add('primary',index,'instruction')}></td>
                                        </tr>
                                    )
                                })
                            }
                            <tr>
                                <td  colSpan={7}><p className="assessmenttable_title">Secondary Diagnosis(es) <FontAwesomeIcon icon={faPlus} style={{cursor:"pointer",color:'#4187f7'}} onClick={()=>this.props.showproblem('Assessments','secondary',12)}></FontAwesomeIcon></p></td>
                            </tr>
                            {
                                this.props.assessments.secondary.map((row,index)=>{
                                    return (
                                        <tr key={index}>
                                            <td><p className="assessmenttable_content" style={{fontStyle:'italic'}}>{row.CommonName?row.CommonName:row.ShortDesc}</p></td>
                                            <td id={"secondary_" + index + "_labs"} className="contenttd" onClick={()=>this.add('secondary',index,'labs')}>
                                                {
                                                    row.labs && row.labs.map((content,index) => {
                                                        return (
                                                            <p key={index} className="assessmenttable_content">{content.ComonName?content.CommonName:content.BillDesc} ({content.Code})</p>
                                                        )
                                                    })
                                                }
                                            </td>
                                            <td id={"secondary_" + index + "_imaging"} className="contenttd" onClick={()=>this.add('secondary',index,'imaging')}>
                                                {
                                                    row.imaging && row.imaging.map((content,index) => {
                                                        return (
                                                            <p key={index} className="assessmenttable_content">{content.ComonName?content.CommonName:content.BillDesc} ({content.Code})</p>
                                                        )
                                                    })
                                                }
                                            </td>
                                            <td id={"secondary_" + index + "_meds"} className="contenttd" onClick={()=>this.add('secondary',index,'meds')}>
                                                {
                                                    row.meds && row.meds.map((content,index)=>{
                                                        return (
                                                            <p key={index} className="assessmenttable_content">{content.name} ({content.dosage})</p>
                                                        )
                                                    })
                                                }
                                            </td>
                                            <td id={'secondary_' + index + '_referal'} className="contenttd" onClick={()=>this.add('secondary',index,'referal')}></td>
                                            <td id={'secondary_' + index + '_nursing'} className="contenttd" onClick={()=>this.add('secondary',index,'nursing')}></td>
                                            <td id={'secondary_' + index + '_instruction'} className="contenttd" onClick={()=>this.add('secondary',index,'instruction')}></td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </Table>
                    {
                        this.state.selectedid && (
                            <Popover target={this.state.selectedid} isOpen={this.state.open} placement="bottom">
                                <PopoverBody>
                                    <Row className="assessmentadd">
                                        <Col lg={12}>
                                            <ul className="assessmentstable">
                                                {
                                                    this.state.data.map((row,index) => {
                                                    return (
                                                        <li className={self.isselected(row) > -1?'specific':''} key={index} onClick={()=>this.adddata(row)}>{(self.selectedtype == 'labs' || self.selectedtype == 'imaging')?row.CommonName?row.CommonName:row.BillDesc:self.selectedtype == 'meds'?row.name + ' (' + row.dosage + ')':''} {(self.selectedtype == 'labs' || self.selectedtype == 'imaging')?'(' + row.Code + ')':''}</li>
                                                    ) 
                                                    })
                                                }
                                            </ul>
                                        </Col>
                                        <Col>
                                            <input className="form-control" onChange={(e)=>this.search(e.target.value)}></input>
                                        </Col>
                                    </Row>
                                    
                                </PopoverBody>   
                            </Popover>
                        )
                    }
                </Col>
            </Row>
            
        )
    }
}

export default AssessmentTable;