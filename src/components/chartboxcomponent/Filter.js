import React from 'react';
import Select from 'react-select';
import {Row,Col,Button} from 'reactstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSortDown} from '@fortawesome/free-solid-svg-icons';
class Filter extends React.Component
{
    name = "Demo Title";
    constructor(props)
    {
        super(props);
        this.state = {
            notetype:[{
                label:'History & Physical',value:1
            },{
                label:'Progress & Note',value:2
            },{
                label:'Consult Note & Template',value:3
            },{
                label:'Discurage Summary & Template',value:4
            }],
            isopen:false,
            rename:false
        }
    }

    handleChange = (date) => {
        this.props.save(date,"date");
        this.toggleCalendar();
    }     
    
    toggleCalendar = () => {
        this.setState(prevState=>{
            let isopen = prevState.isopen;
            return {
                isopen:!isopen
            }
        })      
    }

    handleClick = () => {
        this.toggleCalendar();
    }

    format = (date) => {
        return (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
    }

    getdefaulttitle = (notetype,date) => {
        return this.getnotetype(notetype) + " By System On " + this.format(date);
    }
    rename = () => {
        if(this.state.rename)
        {
            this.props.save(this.name,"title");
            this.name = "";
        }
        else
        {
            this.name = {...this.props}.title;
        }
        this.setState({
            rename:!this.state.rename
        })
    }

    getlocation = (locationid) => {
        if(locationid)
        {
           for(let item in this.props.location)
           {
               if(this.props.location[item].value == locationid)
               {
                   return this.props.location[item];
               }
           } 
        }
        else
        {
            return {};
        }
    }

    getnotetype = (typeid) => {
        for(let item in this.state.notetype)
        {
            if(this.state.notetype[item].value == typeid)
            {
                return this.state.notetype[item].label;
            }
        }

        return "";
    }
    render(){
        let location = this.getlocation(this.props.locationinfo);
        
        return (
           <Row className="chatfilter">
               <Col lg={7} md={7} sm={7} xs={12}>
                   <Row>
                       <Col style={{paddingLeft:0}}><Select options={this.props.location} placeholder="Select location" value={location} onChange={(value)=>this.props.save(value.value,"locationdata")}></Select></Col>
                       <Col>
                            <select className="form-control" defaultValue={this.props.notetype} onChange={(e)=>this.props.save(e.target.value,"notetype")}>
                                {
                                    this.state.notetype.map((row,index)=>{
                                        return (<option key={index} value={row.value}>{row.label}</option>)
                                    })
                                }
                            </select>
                       </Col>
                       <Col lg={3} md={3} sm={4} xs={12}>
                           <Button color="default" onClick={this.toggleCalendar} style={{width:'100%'}}>
                            {this.format(this.props.date, "M/dd/yyyy")}
                            <FontAwesomeIcon icon={faSortDown} style={{float:"right"}}></FontAwesomeIcon>
                           </Button>
                        </Col>
                        {
                            this.state.isopen && (
                                <DatePicker 
                                    selected={this.props.date} 
                                    onChange={this.handleChange} 
                                    dateFormat="M/dd/yyyy" 
                                    withPortal 
                                    inline ></DatePicker>
                            )
                        }
                   </Row>
                </Col>
                <Col>
                    <div style={{float:"right",marginRight:20,display:'flex',alignItems:"center"}}>
                        {!this.state.rename && (<span style={{marginRight:20}}>{this.props.title?this.props.title:this.getdefaulttitle(this.props.notetype,this.props.date)}</span>)}
                        {this.state.rename && (<Col><input className="form-control" defaultValue={this.name} onChange={(e)=>{this.name = e.target.value}}></input></Col>)}
                        <Button color="primary" onClick={this.rename}>{this.state.rename?'Save':'Rename'}</Button>
                    </div>
                </Col>                
            </Row>
        )
    }
}

export default Filter;