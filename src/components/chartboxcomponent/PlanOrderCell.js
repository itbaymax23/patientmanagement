import React from 'react';
import PropTypes from '../../utils/propTypes';
import {Row,Col} from 'reactstrap';
const PlanOrderCell = ({
    title,
    selected,
    component,
    style,
    ...restprops
}) => {
    return (
        <div className="planordercell" style={style}>
            {
                title && (
                    <Col lg={12} md={12} sm={12} xs={12} className="planordertitle">{title}</Col>
                )
            }
            <Col lg={12} md={12} sm={12} xs={12}>
            {
                component.map((row,index)=>{
                    let className = '';

                    if(row == selected)
                    {
                        className = "specific";
                    }
                    return (
                        <span className={"planordercontent " + className}>{row}</span>
                    )
                })
            }
            </Col>
        </div>
    )
}

PlanOrderCell.propTypes = {
    title:PropTypes.String,
    selected:PropTypes.String,
    component:PropTypes.Array,
    style:PropTypes.Object
}

PlanOrderCell.defaultProps = {
    title:'',
    selected:'',
    style:{},
    component:[]
}

export default PlanOrderCell;