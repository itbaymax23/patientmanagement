//library
import React from 'react';
import {Row,Col,UncontrolledPopover,PopoverBody,Button} from 'reactstrap';

//icon and image
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAngleDoubleDown, faTimes} from '@fortawesome/free-solid-svg-icons';

//api
import * as ProblemAPI from '../../action/Problem';


class Problem extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            problemlist:[]
        }

        this.updateproblemlist();
    }

    //get the common problem list
    updateproblemlist = () => {
        let self = this;
        ProblemAPI.getproblemcommon().then(function(data){
            self.setState({
                problemlist:data.data
            })
        })
    }
    
    //save the problem
    save = (problem) => {
        let data = this.props.data.PMH;
        data.problem.push(problem);
        this.props.save(data);
    }

    //delete the problem

    delete = (index) => {
        let data = this.props.data.PMH;
        data.problem.splice(index,1);
        this.props.save(data);
    }


    render()
    {
        let problemlist = [];
        for(let item in this.props.data.PMH.problem)
        {
            problemlist.push(this.props.data.PMH.problem[item].Id)
        }
        
        let CC = []; let Ch = [];
        for(let item in this.props.data.CC.problem)
        {
            for(let item_cc in this.props.data.CC.problem[item])
            {
                if(item  == 0)
                {
                    CC.push(this.props.data.CC.problem[item][item_cc].Id);
                }
                else
                {
                    Ch.push(this.props.data.CC.problem[item][item_cc].Id);
                }
            }
        }

        return (
            <Row className="summary">
                <Col lg={11} className="title">
                    {this.props.title}
                    <FontAwesomeIcon icon={faAngleDoubleDown} style={{marginLeft:10,cursor:"pointer"}} id="problemlist"></FontAwesomeIcon>
                </Col>
                <Col lg={11} className="problembody">
                    {
                        this.props.data.PMH.problem.length == 0 && (
                            <p>There is no Problem List yet</p>
                        )
                    }
                    {
                        this.props.data.PMH.problem.map((row,index)=>{
                            return (
                                <Row className="pmhlist">
                                    <Col lg={8}>
                                        {row.CommonName?row.CommonName:row.ShortDesc}
                                    </Col>
                                    <Col>
                                        {
                                            CC.indexOf(row.Id) > -1? "Current":Ch.indexOf(row.Id)>-1?"Chronic":"Intermediate"
                                        }
                                        
                                        <Button color="secondary" onClick={()=>this.delete(index)}>
                                            <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                        </Button>
                                    </Col>
                                </Row>
                            )
                        })
                    }
                </Col>
                <UncontrolledPopover target="problemlist" trigger="legacy" placement="bottom">
                    <PopoverBody>
                        <ul className="common_problem">
                            {
                                this.state.problemlist.map((row,indexvalue)=>{
                                    if(problemlist.indexOf(row.Id) == -1)
                                    {
                                        return (
                                            <li onClick={() => this.save(row)}>{row.CommonName?row.CommonName:row.ShortDesc}</li>
                                        )    
                                    }
                                })
                            }
                        </ul>
                    </PopoverBody>
                </UncontrolledPopover>
            </Row>        
        )
       
    }
}

export default Problem;