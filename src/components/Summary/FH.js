import React from 'react';
import {Row,Col} from 'reactstrap';

class FH extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <Row className="summary">
                <Col lg={11} className="title">
                    {this.props.title}
                </Col>
                <Col lg={11} className="problembody">
                    {this.props.default}
                </Col>
            </Row>
        )
    }
}

export default FH;