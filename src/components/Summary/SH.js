import React from 'react';
import {Row,Col,Button} from 'reactstrap';

class SH extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    gettitle = (row) => {
        if(this.props.data.substance[row.Name] == undefined || this.props.data.substance[row.Name].status == undefined || this.props.data.substance[row.Name].status == "Never")
        {
            return 'Access Denies';
        }
        else
        {
            var str = this.props.data.substance[row.Name].status + " use ";
            if(this.props.data.substance[row.Name].type)
            {
                str += this.data.substance[row.Name].type.join(',');
            }
            if(this.props.data.substance[row.Name].agestart)
            {
                str += " from " + this.props.SHI.data.substance[row.Name].agestart
            }

            if(this.props.data.substance[row.Name].agestop)
            {
                str += " Until " + this.props.SHI.data.substance[row.Name].agestop;
            }

            

            var enable = false;
            for(let itemunit in row.units)
            {
                if(this.props.data.substance[row.Name]['unit'] == row.units[itemunit].Id)
                {
                    str += this.props.data.substance[row.Name]['amount'] + " " + row.units[itemunit].UnitText + " / " + row.units[itemunit].UnitPeriod;
                    enable = true;
                }
            }

            if(!enable && this.props.data.substance[row.Name].amount)
            {
                str += " " + this.props.data.substance[row.Name].amount;

                if(row.units.length > 0)
                {
                    str += " " + row.units[0].UnitText + " / " + row.units[0].UnitPeriod;
                }
            }
            return str;
        }
    }

    render()
    {
        return (
            <Row className="summary">
                <Col lg={11} className="title">
                    {this.props.title}
                </Col>
                <Col lg={11}>
                    <Row className="cardentry">
                        <Col lg={11} className="cardbody problembody">
                            <div className="mytable">
                                <Col>
                                    <div className="tableheader">
                                        <div className="title_header">
                                            <p>Substance Use</p>
                                        </div>
                                        <div className="header">
                                        {
                                            this.props.data.substance.map((row,index)=>{
                                                return (
                                                        <p key={"param_" + index}>
                                                        {
                                                            this.props.data.substance[row.Name] && this.gettitle(row)
                                                        }
                                                        </p>
                                                )
                                            })
                                        }
                                        </div>
                                    </div>
                                    <div className="tablebody">
                                        {
                                            this.props.data.social.map((row,index)=>{
                                                return (
                                                    <div key={"table_" + index} className="tablerow">
                                                        <div className="cell">
                                                            <p className="body_title">{row.Name}</p>
                                                        </div>
                                                        <div className="cell">
                                                            <p className="tablebodytext">
                                                            {
                                                                this.props.data[row.Name]?this.props.data[row.Name]:"unSpecified"
                                                            }
                                                            </p>
                                                        </div>
                                                        
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </Col>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>
        )
    }
}

export default SH;