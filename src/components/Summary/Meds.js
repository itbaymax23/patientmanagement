//library
import React from 'react';
import {Row,Col,Button} from 'reactstrap';

//icon
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
class Meds extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    delete = (index) => {
        var data = this.props.data.meds;
        data.meds.splice(index,1);
        this.props.save(data);
    }

    render()
    {
        return (
            <Row className="summary">
                <Col lg={11} className="title">
                    {this.props.title}
                </Col>
                <Col lg={11} className="problembody">
                    {
                        this.props.data.meds && this.props.data.meds.length == 0 && (
                            <Row>
                                <Col>
                                    No Record Meds
                                </Col>
                            </Row>

                        )
                    }
                    {
                        this.props.data.meds && this.props.data.meds.map((row,index)=>{
                            return(
                                <Row>
                                    <Col lg={6}>
                                        {row.Drug} ({row.Brand})
                                    </Col>
                                    <Col>{row.DoseStrength} {row.value} {row.day} {row.type}</Col>
                                    <Button color="secondary" onClick={(index)=>this.delete(index)} style={{marginRight:30}}>
                                        <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                    </Button>
                                </Row>
                            )
                        })
                    }
                </Col>
            </Row>
        )
    }
}

export default Meds;