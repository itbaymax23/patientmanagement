//library
import React from 'react';
import {Row,Col,UncontrolledPopover,PopoverBody,Button} from 'reactstrap';

//icon and image
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAngleDoubleDown, faTimes} from '@fortawesome/free-solid-svg-icons';

//api
import * as ProblemAPI from '../../action/Problem';


class PSH extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            cptlist:[]
        }

        this.updateproblemlist();
    }

    //get the common problem list
    updateproblemlist = () => {
        let self = this;
        ProblemAPI.getcptcommon().then(function(data){
            self.setState({
                cptlist:data.data
            })
        })
    }
    
    //save the problem
    save = (problem) => {
        let data = this.props.data;
        data.problem.push(problem);
        this.props.save(data);
    }

    //delete the problem

    delete = (index) => {
        let data = this.props.data;
        data.problem.splice(index,1);
        this.props.save(data);
    }


    render()
    {
        let problemlist = [];
        for(let item in this.props.data.problem)
        {
            problemlist.push(this.props.data.problem[item].Code)
        }
    

        return (
            <Row className="summary">
                <Col lg={11} className="title">
                    {this.props.title}
                    <FontAwesomeIcon icon={faAngleDoubleDown} style={{marginLeft:10,cursor:"pointer"}} id="cptlist"></FontAwesomeIcon>
                </Col>
                <Col lg={11} className="problembody">
                    {
                        this.props.data.problem.length == 0 && (
                            <p>No Surgical History Recorded</p>
                        )
                    }
                    {
                        this.props.data.problem.map((row,index)=>{
                            return (
                                <Row className="pmhlist">
                                    <Col lg={12}>
                                        {row.CommonName?row.CommonName:row.ShortDesc}
                                        <Button color="secondary" onClick={()=>this.delete(index)}>
                                            <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                        </Button>
                                    </Col>
                                </Row>
                            )
                        })
                    }
                </Col>
                <UncontrolledPopover target="cptlist" trigger="legacy" placement="bottom">
                    <PopoverBody>
                        <ul className="common_problem">
                            {
                                this.state.cptlist.map((row,indexvalue)=>{
                                    if(problemlist.indexOf(row.Code) == -1)
                                    {
                                        return (
                                            <li key={indexvalue} onClick={() => this.save(row)}>{row.CommonName?row.CommonName:row.ShortDesc}</li>
                                        )    
                                    }
                                })
                            }
                        </ul>
                    </PopoverBody>
                </UncontrolledPopover>
            </Row>        
        )
       
    }
}

export default PSH;