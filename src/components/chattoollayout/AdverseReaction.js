import React from 'react';
import Druglist from './MedicationList';
import Card from '../Card';
import {Reaction} from '../chartboxcomponent/action';
import {Editor} from 'react-draft-wysiwyg';
import {convertToRaw} from 'draft-js';
import draftToHTML from 'draftjs-to-html';
import {Modal,ModalBody,ModalHeader,ModalFooter,Button,Form,FormGroup,Label,Row,Col} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faArrowUp, faPencilAlt,faTimes } from '@fortawesome/free-solid-svg-icons';
import * as Problem from '../../action/Problem';

import * as Macro from '../../action/Macro';
class AdverseReaction extends React.Component
{
    editdata = "";
    data = {};
    constructor(props)
    {
        super(props);
        this.state = {
            freetext:false,
            searchlist:false,
            selecteditem:{},
            form:false,
            reaction:[],
            severity:[],
            selecteditem:-1
        }        
    }

    componentDidMount()
    {
        let self = this;
        Problem.getalergyinfo().then(function(data){
            self.setState({
                reaction:data.data.reaction,
                severity:data.data.severity
            })
        })
    }

    componentWillReceiveProps(props)
    {
        if(props.updatemacro && props.selectedSectionId == 6 && props.macro.SectionId == 6)
        {
            let data = props.ADR;
            let self  = this;
            Macro.getallergy(props.macro.Content).then(function(result){
                let allergy_list = [];
                for(let item in result.data)
                {
                    allergy_list.push({
                        name:result.data[item].name,
                        reactiontype:result.data[item].defaultType,
                        severity:result.data[item].defaultSeverity,
                        reaction:result.data[item].defaultReaction
                    })
                }
                
                self.props.save("ADR",{adversereaction:allergy_list,freetextdata:""},6);
                
            })
        }
    }

    toggle = () => {
        this.setState({
            freetext:!this.state.freetext
        })
    }

    onEditorStateChange = (state) => {
        this.editdata = draftToHTML(convertToRaw(state.getCurrentContent()));
    }

    addfreetext = () => {
        this.props.save("ADR",{freetextdata:this.editdata},6);
        this.setState({
            freetext:false
        })
    }

    togglesearchlist = () => {
        this.setState({
            searchlist:!this.state.searchlist,
            form:false,
            selecteditem:-1
        })
    }

    select = (data) => {
        this.data = {
            name:data.name,
            reactiontype:data.defaultType,
            severity:data.defaultSeverity,
            reaction:data.defaultReaction
        }

        console.log('adr_reaction',this.data);
        this.setState({
            form:true
        })
    }

    saveitem = (value,item)=>{
        this.data[item] = value;
    }

    save = () => {
        let data = this.props.ADR.adversereaction;

        let dataelement = this.data;

        if(this.state.selecteditem > -1)
        {   
            data[this.state.selecteditem] = dataelement;
        }
        else
        {
            data.push(dataelement);
        }
        
        this.props.save("ADR",{adversereaction:data},6);
        this.data = {};
        this.setState({
            form:false,
            searchlist:false,
            selecteditem:-1
        })

    }

    update = (index,row) => {
        this.data = row;
        this.setState({
            selecteditem:index,
            form:true,
            searchlist:true
        })
    }

    delete = (index) => {
        let data = this.props.ADR.adversereaction;
        data.splice(index,1);
        this.props.save("ADR",{adversereaction:data},6);
    }

    render()
    {
        return (
            <Card title="Adverse Drug Reaction" tag={Reaction} addmedicallist={this.togglesearchlist} addfreetext={this.toggle} macro={()=>this.props.showmacro(6)}>
                {
                    !this.props.ADR.freetextenable && this.props.ADR.adversereaction.length > 0 && (
                        <Row className="defaultcontroller">
                            <Col className="title">Name</Col>
                            <Col className="title">Type</Col>
                            <Col className="title">Reaction</Col>
                            <Col className="title">Severity</Col>
                            <Col className="title">Notes</Col>
                            <Col className="title"></Col>
                        </Row>
                    )
                }
                {
                    !this.props.ADR.freetextenable && this.props.ADR.adversereaction.length == 0 && (
                        <Row className="defaultcontroller">
                            <Col>No Known ADRs</Col>
                        </Row>
                    )
                }
                {
                    !this.props.ADR.freetextenable && this.props.ADR.adversereaction.map((row,index)=>{
                        return (
                            <Row className="default_container">
                                <Col>{row.name}</Col>
                                <Col>{row.reactiontype}</Col>
                                <Col>{row.reaction}</Col>
                                <Col>{row.severity}</Col>
                                <Col>{row.note}</Col>
                                <Col>
                                    <Row>
                                        <div style={{margin:"auto"}}>
                                            <FontAwesomeIcon icon={faPencilAlt} onClick={()=>this.update(index,row)} style={{marginRight:10}} style={{cursor:"pointer"}}></FontAwesomeIcon>
                                            <FontAwesomeIcon icon={faTimes} onClick={()=>this.delete(index)} style={{marginRight:10}} style={{marginLeft:10,cursor:"pointer",color:"red"}} onClick={()=>this.delete(index)}></FontAwesomeIcon>
                                        </div>
                                    </Row>
                                </Col>
                            </Row>
                        )
                    })
                }
                
                <Modal isOpen={this.state.freetext} size="lg">
                    <ModalHeader toggle={this.toggle}>Add Free Text</ModalHeader>
                    <ModalBody>
                        <Editor 
                            onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                            editorStyle={{minHeight:300}}
                            >
                        </Editor>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                        <Button color="success" style={{marginLeft:10}} onClick={this.addfreetext}>Save</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.searchlist} size="lg">
                    <ModalHeader toggle={this.togglesearchlist}>Add Alergy Reaction</ModalHeader>
                    <ModalBody>
                        {
                            !this.state.form && (
                                <Druglist select={(data)=>this.select(data)}></Druglist>
                            )
                        }
                        {
                            this.state.form && (
                                <Form>
                                    <Row className="reactioncontainer">
                                        <Col className="title">{this.data.name} <FontAwesomeIcon icon={faArrowUp} style={{cursor:"pointer",fontSize:20}} onClick={()=>this.setState({form:false})}></FontAwesomeIcon></Col>
                                    </Row>
                                    <FormGroup row>
                                        <Label lg={4}>ReactionType</Label>
                                        <Col>
                                            <select className="form-control" onChange={(e)=>{this.saveitem(e.target.value,"reactiontype")}} defaultValue={this.data.reactiontype}>
                                                <option value="">Select Reaction Type</option>
                                                <option value="Not Sure">Not Sure</option>
                                                <option value="Allergy">Allergy</option>
                                                <option value="Adverse Reaction">Adverse Reaction</option>
                                            </select>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={4}>Reaction</Label>
                                        <Col>
                                            <select className="form-control" onChange = {(e)=>{this.saveitem(e.target.value,"reaction")}} defaultValue={this.data.reaction}>
                                                <option value="">Select Reaction</option>
                                                {
                                                    this.state.reaction.map((row,index)=>{
                                                        return (
                                                            <option value={row.reactionText}>{row.reactionText}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={4}>Reaction Severity</Label>
                                        <Col>
                                            <select className="form-control" onChange={(e)=>{this.saveitem(e.target.value,"severity")}} defaultValue={this.data.severity}>
                                                <option value="">Select Severity</option>
                                                {
                                                    this.state.severity.map((row,index)=>{
                                                        return (
                                                            <option value={row.severityText}>{row.severityText}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={4}>Comments</Label>
                                        <Col>
                                            <textarea className="form-control" onChange={(e)=>this.saveitem(e.target.value,"note")} defaultValue={this.data.note}></textarea>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            )
                        }
                    </ModalBody>
                    {
                        this.state.form && (
                            <ModalFooter>
                                <Button color="secondary" onClick={this.togglesearchlist}>Cancel</Button>
                                <Button color="success" style={{marginLeft:10}} onClick={this.save}>Save</Button>
                            </ModalFooter>
                        )
                    }
                </Modal>

                <Row className="default_controller main_content">
                    {this.props.ADR.freetextdata && (
                        <Col dangerouslySetInnerHTML={{__html:this.props.ADR.freetextdata}}></Col>
                    )}
                </Row>
            </Card>
        )
    }
}

export default AdverseReaction;