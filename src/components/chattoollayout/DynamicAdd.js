import React from 'react';
import {Modal,ModalHeader,ModalBody,ModalFooter,Form,FormGroup,Col,Label,Row,Button} from 'reactstrap';
import ContentEditable from 'react-contenteditable';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faArrowUp, faArrowDown , faPlus,faTimes} from '@fortawesome/free-solid-svg-icons';
import * as Problem from '../../action/Problem';

class DynamicAdd extends React.Component
{
    
    constructor(props)
    {
        super(props);
        console.log('dynamicadd',props);

        this.state = {
            controltype:[
                {label:"Single Select List",value:'SSL'},
                {label:"Admits/denies combo list",value:'ACL'},
                {label:'Free text',value:'FT'},
                {label:'Date Field',value:'DF'},
                {label:'Multiple Select',value:'MSL'},
                {label:'Patient Name',value:'PN'},
                {label:'He/She',value:'HS'},
                {label:'PatientAge',value:'AG'},
                {label:'Male/Female',value:'MF'},
                {label:'Own',value:'OW'}
            ],
            datasource:[{label:'Free Text',value:'freetext'}],
            data:{
                caption:props.caption?props.caption:'',
                listoption:[],
                id:''
            },
            selected:-1
        }
    }

    componentWillReceiveProps(props)
    {
        let data = this.state.data;
        data.caption = props.caption?props.caption:'';
        data.id = props.id?props.id:'';
        
        if(data.id)
        {
            this.updateinfo(data.id);
        }
        else
        {
            this.setState({data:data});
        }
        
    }

    updateinfo = (id) => {
        let self = this;
        Problem.getdynamicelement(id).then(function(data){

            if(data.data.listoption)
            {
                data.data.listoption = data.data.listoption.split(',');
            }
            else
            {
                data.data.listoption = [];
            }

            self.setState({
                data:data.data
            })
        })
    }

    handleChange = (value,index) => {
        let data = this.state.data;
        data.listoption[index] = value.replace('&nbsp;',' ');
        this.setState({
            data:data
        })
    }
    
    handleitem = (name,value) => {
        let data = this.state.data;
        data[name] = value.replace('&nbsp;',' ');
        this.setState({
            data:data
        });
    }

    addoption = () => {
        let data = this.state.data;
        if(!data.listoption) {data.listoption = [];}
        data.listoption.push('');
        this.setState({
            data:data,
            selected:data.listoption.length - 1
        });
    }

    downoption = () => {
        let data = this.state.data;
        let selected = this.state.selected;
        if(selected < this.state.data.listoption.length - 1)
        {
            data.listoption = this.array_move(data.listoption,selected,selected + 1);
            this.setState({
                data:data,
                selected:selected + 1
            });
        }
    }

    upoption = () => {
        let data = this.state.data;
        let selected = this.state.selected;
        if(selected > 0)
        {
            data.listoption = this.array_move(data.listoption,selected,selected - 1);
            this.setState({
                data:data,
                selected:selected - 1
            })
        }
    }

    array_move = (array,new_index,old_index) => {
        var backup = array[old_index];
        array.splice(old_index,1,array[new_index]);
        array.splice(new_index,1,backup);
        return array;
    }

    deleteoption = () => {
        let data = this.state.data;
        data.listoption = data.listoption.splice(this.state.selected,1);
        let selected = this.state.selected;
        if(selected >= data.listoption.length)
        {
           selected = data.listoption.length - 1; 
        }

        this.setState({
            data:data,
            selected:selected
        })
    }

    save = () => {
        let self = this;
        Problem.savedynamicelement(this.state.data).then(function(res){
            if(res.data.id)
            {
                self.props.adddynamic({id:res.data.id,caption:self.state.data.caption});
            }
            else
            {
                self.props.close();
            }
        })
    }

    close = () => {
        if(this.state.data.id)
        {
            this.props.close();
        }
        else
        {
            this.props.adddynamic();
        }
    }

    render()
    {
        let self = this;
        return (
            <Modal isOpen={this.props.showdynamic}>
                <ModalHeader toggle={this.close}>Edit Dynamic Element</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup row>
                            <Label lg={4}>Caption</Label>
                            <Col lg={8}>
                                <input className="form-control" defaultValue={this.state.data.caption} onChange={(e)=>this.handleitem('caption',e.target.value)}></input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label lg={4}>Control Type</Label>
                            <Col lg={8}>
                                <select className="form-control" defaultValue={this.state.data.controltype} onChange={(e)=>this.handleitem('controltype',e.target.value)}>
                                    <option value="">Select Control type</option>
                                    {
                                        this.state.controltype.map((row,index)=>{
                                            return (
                                                <option key={index} value={row.value}>{row.label}</option>
                                            )
                                        })
                                    }
                                </select>
                            </Col>
                        </FormGroup>
                        {
                            (this.state.data.controltype == 'SSL' || this.state.data.controltype == 'ACL' || this.state.data.controltype == 'MSL') && (
                                <FormGroup row>
                                    <Label lg={4}>Data Source</Label>
                                    <Col lg={8}>
                                        <select className="form-control" defaultValue={this.state.data.datasource} onChange={(e)=>this.handleitem('datasource',e.target.value)}>
                                            <option value="">Select Data Source</option>
                                            {
                                                this.state.datasource.map((row,index)=>{
                                                    return (
                                                        <option key={index} value={row.value}>{row.label}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    </Col>
                                </FormGroup>
                            )
                        }
                        {
                            (this.state.data.controltype == 'SSL' || this.state.data.controltype == 'ACL' || this.state.data.controltype == 'MSL') && (
                                <FormGroup row>
                                    <Label lg={12}>List Options</Label>
                                    <Col lg={12}>
                                        <Row>
                                            <Col className="dynamic_listview" lg={9}>
                                                {
                                                    this.state.data.listoption && this.state.data.listoption.map((row,index)=>{
                                                        return (
                                                            <Row key={index}>
                                                                <Col className={self.state.selected == index?'selected':''} onClick={()=>{self.setState({selected:index})}}>
                                                                    {
                                                                        self.state.data.datasource == 'freetext' && (
                                                                            <ContentEditable html={row} onChange={(e)=>this.handleChange(e.target.value,index)}></ContentEditable>
                                                                        )
                                                                    }
                                                                </Col>
                                                            </Row>
                                                        )
                                                    })
                                                }
                                            </Col>
                                            <Col className="dynamic_list_action">
                                                <div>   
                                                    <Button color="default" onClick={this.addoption}>
                                                        <FontAwesomeIcon icon={faPlus} style={{color:'#bcd5f0'}}></FontAwesomeIcon>
                                                    </Button>
                                                </div>
                                                <div>   
                                                    <Button color="default" onClick={this.upoption}>
                                                        <FontAwesomeIcon icon={faArrowUp} style={{color:'#2CB0D1'}}></FontAwesomeIcon>
                                                    </Button>
                                                </div>
                                                <div>   
                                                    <Button color="default" onClick={this.downoption}>
                                                        <FontAwesomeIcon icon={faArrowDown} style={{color:'#2CB0D1'}}></FontAwesomeIcon>
                                                    </Button>
                                                </div>
                                                <div>   
                                                    <Button color="default" onClick={this.deleteoption}>
                                                        <FontAwesomeIcon icon={faTimes} style={{color:'red'}}></FontAwesomeIcon>
                                                    </Button>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>
                                </FormGroup>
                            )
                        }
                        
                        <FormGroup row>
                            <Label lg={4}>Default Value:</Label>
                            <Col>
                                <input className="form-control" defaultValue={this.state.data.defaultvalue} onChange={(e)=>this.handleitem('defaultvalue',e.target.value)}></input>
                            </Col>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={this.close}>Cancel</Button>
                    <Button color="success" onClick={this.save}>Save</Button>
                </ModalFooter>
            </Modal>
        )
    }
}

export default DynamicAdd;