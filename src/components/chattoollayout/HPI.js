import React from 'react';
import Card from '../../components/Card';
import {Row,Col,Modal,ModalBody,ModalHeader,ModalFooter,Button,Popover,PopoverBody} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimesCircle,faPlus, faSortDown,faEdit,faBook,faTimes} from '@fortawesome/free-solid-svg-icons';
import HTMLParser from 'html-to-react';
import { DetailPage } from '../chartboxcomponent/action';
import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {convertToRaw} from 'draft-js';
import draftToHTML from 'draftjs-to-html';
import $ from 'jquery';

import * as Problem from '../../action/Problem';
import * as Macro from '../../action/Macro';
import DynamicContent from './DynamicContent';
import * as temp from '../../action/templatemodule';
//import cookies from 'browser-cookies';
class HPI extends React.Component
{
    editdata = "";
    selectedtarget = "";
    selected_macro = false;

    constructor(props)
    {
        super(props);
        this.state = {
            edititem:false,
            edit:false,
            selectedindex:0,
            textdata:{},
            param:{},
            editrelateddiagnose:false,
            relateddiagnose:[]
        }
    }

    onEditorStateChange = (state) => {
        this.editdata = draftToHTML(convertToRaw(state.getCurrentContent()));
    }

    edit = () => {
        this.setState({
            edit:!this.state.edit
        })
    }

    toggle = () => {
        this.setState({
            edit:false,
            edititem:false
        })
    }

    select_macro = (id) => {
        this.selected_macro = id;
        this.props.showmacro(2);
    }
    
    componentDidMount()
    {
        let self = this;
        window.addEventListener('click',function(e){
            if($(e.target).parents('.relateddiagnosis').length == 0 && e.target.className != 'righticon' && $(e.target).parents('.righticon').length == 0)
            {
                self.selectedtarget = "";
                self.setState({
                    editrelateddiagnose:false
                })
            }
        })
    }

    componentWillReceiveProps(props)
    {
        let data = props.data;
        let itemarray = [];
        let idarray = [];
        for(let item in data.problem)
        {
            if(!data.problem[item].addedfreetext)
            {
                itemarray.push(data.problem[item].Code);
                idarray.push(data.problem[item].Id);
                data.problem[item].addedfreetext = true;
            }
        }

        if(itemarray.length > 0)
        {
            let self = this;
            Macro.gethpiproblems(itemarray).then(function(res){
                for(let item in res.data)
                {
                    for(let itemindex in data.problem)
                    {
                        if(data.problem[itemindex].Id == idarray[itemarray.indexOf(item)])
                        {
                            data.problem[itemindex].textdata = res.data[item].Content;
                        }
                    }
                }

                self.props.save('HPI',data,2);
            })
        }

        if(props.updatemacro && props.selectedsection == 2 && props.macro.SectionId == 2)
        {
            data.freetextenable = false;
            data.freetextdata = "";
            let self = this;
            Macro.getproblemlistwithcode(props.macro.HPIChronicProblems).then(function(res){
                for(let itemindex in data.problem)
                {
                    if(data.problem[itemindex].Id == self.selected_macro)
                    {
                        data.problem[itemindex].textdata = props.macro.Content;
                        data.problem[itemindex].relateddiagnosis = res.data;
                    }
                }
                self.props.save('HPI',data,2);
            })
        }
    }
    
    
    getcomponentfromtextdata = (problem,textdata) => {
        if(!textdata)
        {
            return [];
        }
        let textarray = textdata.split('.');
        let component = [];
        
        for(let item in textarray)
        {
            let component_title = [];
            textarray[item] = textarray[item].replace(/\{\{FreeText data=\"(.*?)\"\}\}/,"  $1");          
            component_title.push(this.gettitle(problem,textarray[item]));
            component.push(<span>{component_title}.</span>);
        }

        return component;
    }

    savedynamic = (indexid,id,value) => {
        let data = this.props.data;

        for(let item in data.problem)
        {
            if(data.problem[item].Id == indexid)
            {
                if(!data.problem[item].value)
                {
                    data.problem[item].value = {};
                }

                data.problem[item].value[id] = value;
            }
        }

        this.props.save('HPI',data,2);
    }

    gettitle = (row,title) =>{
        let indexid = row.Id;
        let htmlparser = new HTMLParser.Parser();
        let array = title.split(/\{\{DynamicElement data=\'(.*)\'\}\}/g);
        
        let component = [];

        console.log("element_array",array);
        if(array.length == 1)
        {
            component.push( htmlparser.parse(temp.converttemp(array[0],this.props.patient)));
        }

        let last = "";
        if(array[2])
        {
            last = array[2];
        }
        while(array.length > 1)
        {
            let jsondata = array[1].split("'}}")[0];
            component.push(htmlparser.parse(temp.converttemp(array[0],this.props.patient)));
            let id = JSON.parse(jsondata).id;
            component.push(<DynamicContent dataid={"activity_" + indexid + "_" + id} id={id} patient = {this.props.patient} save={(value)=>this.savedynamic(indexid,id,value)} value={row.value && row.value[id]?row.value[id]:false}></DynamicContent>)
            let data = title.split("{{DynamicElement data='" + jsondata + "'}}");
            if(data.length > 1)
            {
                array = data[1].split(/\{\{DynamicElement data=\'(.*)\'\}\}/g);
            }
            else
            {
                component.push(htmlparser.parse(temp.converttemp(data[0],this.props.patient)));
                break;
            }
        }
        
        component.push(htmlparser.parse(last));
        return component;
        // let htmlparser = new HTMLParser.Parser();
        // return htmlparser.parse(temp.converttemp(title,this.props.patient));
    }
    addfreetext = () => {
        var data = this.props.data;
        if(this.state.edit)
        {
            data.freetext = this.editdata;
        }
        else if(this.state.edititem)
        {
            for(let item in data.problem)
            {
                if(data.problem[item].Id == this.state.selectedindex)
                {
                    data.problem[item].textdata = this.editdata;
                    break;
                }
            }
        }

        this.props.save("HPI",data,2);
        this.setState({
            edit:false,
            edititem:false
        })
    }
    
    edititem = (id) => {
        this.setState({
            edititem:true,
            selectedindex:id
        })
    }

    delete = (index) => {
        let data = this.props.data;
        if(data.textdata)
        {
            data.textdata[data.problem[index].Id] = "";
        }
        data.problem.splice(index,1);
        this.selectedtarget = "";

        this.props.save("HPI",data)
    }

    setviewParam = (id,param) => {
        let paramarray = this.state.param;
        paramarray[id] = param;
        this.setState({
            param:paramarray
        })
    }
    
    selectdiagnosis = (index) => {
        //this.props.showproblem('HPI',index,2)
        this.selectedtarget = "select_" + index;
        let self = this;
        Problem.getrelateddiagnosis(this.props.data.problem[index].Id).then(function(data){
            self.setState({
                relateddiagnose:data.data,
                editrelateddiagnose:true
            })
        })
    }

    isselected = (array,code) => {
        for(let item in array)
        {
            if(array[item].Code == code)
            {
                return true;
            }
        }
        return false;
    }

    editdiagnose = (value) => {
        let problem = this.props.data.problem;
        if(this.selectedtarget.split('select_')[1])
        {
            let index = this.selectedtarget.split('select_')[1];
            if(!problem[index].relateddiagnosis)
            {
                problem[index].relateddiagnosis = [];
            }

            if(!this.isselected(problem[index].relateddiagnosis,value.Code))
            {
                problem[index].relateddiagnosis.push(value);
            }
            else
            {
                for(let item in problem[index].relateddiagnosis)
                {
                    if(problem[index].relateddiagnosis[item].Code == value.Code)
                    {
                        problem[index].relateddiagnosis.splice(item,1);
                    }
                }
            }
        }

        this.props.save('HPI',{problem:problem},2);
    }

    undo = ()=>{
        this.props.save('HPI',{problem:[],param:'line',textdata:{},freetext:"",freetextenable:false,freetextdata:""})
    }

    render()
    {   
        let self = this;
        return (
        <Card title="History of Present Illness" edit={this.edit} undo={this.undo}>
            {
                !this.props.data.freetextenable && this.props.data.problem.map((row,index)=>{
                    return(
                        <Row className="default_container">
                            <div className="main_content">
                                <span className="title" onClick={()=>this.props.problemgrid()} style={{cursor:"pointer"}}>{row.CommonName?row.CommonName:row.ShortDesc} ({row.Code})</span>
                                <span className="righticon" style={{marginLeft:5}} id={"select_" + index} onClick={()=>this.selectdiagnosis(index)}>
                                    <FontAwesomeIcon icon={faSortDown}></FontAwesomeIcon>
                                </span>
                                {
                                    row.relateddiagnosis && row.relateddiagnosis.map((value,indexvalue)=>{
                                        return (
                                            <p className={indexvalue}>{value.CommonName?value.CommonName:value.ShortDesc} ({value.Code})</p>
                                        )
                                    })
                                }
                                {this.state.param[row.Id] != "paragraph" && (<p className="freetext">{this.getcomponentfromtextdata(row,row.textdata?row.textdata:"",row)}</p>)}
                                {this.state.param[row.Id] == 'paragraph' && (<div className="freetext" style={{display:"inline-block"}}>{this.getcomponentfromtextdata(row,row.textdata?row.textdata:"")}</div>)}
                            </div>
                            <Col lg={5} md={5} sm={6} xs={6} style={{marginLeft:"auto"}}>
                                <Row>
                                    <div style={{marginLeft:"auto"}}>
                                        <Button color="primary" onClick={() => this.setviewParam(row.Id,"paragraph")}>P</Button>
                                        <Button color="primary" style={{marginLeft:10}} onClick={() => this.setviewParam(row.Id,"line")}>L</Button>
                                        <Button color="primary" style={{marginLeft:10}} onClick={()=>this.edititem(row.Id)}>T</Button>
                                        <Button color="default" style={{marginLeft:10}} onClick={()=>{this.select_macro(row.Id)}}><FontAwesomeIcon icon={faBook}></FontAwesomeIcon></Button>
                                        <Button color="secondary" style={{marginLeft:10,fontSize:10,padding:"2px 3px"}} onClick={()=>this.delete(index)}><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></Button>
                                    </div>
                                </Row>
                            </Col>
                        </Row>
                    )
                })
            }
            {
                !this.props.data.freetextenable && this.props.data.freetext && (
                    <p dangerouslySetInnerHTML={{__html:this.props.data.freetext}}></p>
                )
            }

            {
                this.props.data.freetextenable && (
                    <p dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></p>
                )
            }
            <Modal isOpen={this.state.edititem || this.state.edit} size="lg">
                <ModalHeader toggle={this.toggle}>Add Free Text</ModalHeader>
                <ModalBody>
                    <Editor 
                        onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                        editorStyle={{minHeight:300}}
                        >
                    </Editor>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    <Button color="success" style={{marginLeft:10}} onClick={this.addfreetext}>Save</Button>
                </ModalFooter>
            </Modal>
            
            {
                this.selectedtarget && (
                    <Popover target={this.selectedtarget} isOpen={this.state.editrelateddiagnose} placement="bottom">
                        <PopoverBody>
                            <ul className="relateddiagnosis">
                                {
                                    this.state.relateddiagnose.map((row,index)=>{
                                        return (
                                            <li key={index} className={self.isselected(this.props.data.problem[this.selectedtarget.split('select_')[1]].relateddiagnosis?this.props.data.problem[this.selectedtarget.split('select_')[1]].relateddiagnosis:[],row.Code)?'checkeditem':''} onClick={()=>this.editdiagnose(row)}>{row.CommonName?row.CommonName:row.ShortDesc} ({row.Code})</li>
                                        )
                                    })
                                }

                                <li className="relateditem" onClick={()=>{this.props.showproblem('HPI',this.selectedtarget.split('select_')[1],2); this.setState({editrelateddiagnose:false}); this.selectedtarget = "";}}>Add Another</li>
                            </ul>
                        </PopoverBody>
                    </Popover>
                )
            }
        </Card>)
    }
}

export default HPI;