import React from 'react';
import {Row,Col,Pagination,PaginationItem,PaginationLink,InputGroup,InputGroupAddon} from 'reactstrap';
import * as Problem from '../../action/Problem';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faAngleDoubleLeft,faAngleLeft, faAngleDoubleRight, faAngleRight } from '@fortawesome/free-solid-svg-icons';
class MedsList extends React.Component
{
    query = {};
    constructor(props)
    {
        super(props);
        this.state = {
            selected:'favourite',
            data:[],
            count:0,
            page:1,
            totalpage:1,
            query:''
        }
    }   

    componentDidMount()
    {
        this.getinfo({favourite:true});
    }

    getinfo = (data) => {
        let self = this;
        Problem.getmedsinfo(data).then(function(data){
            data = data.data;
            self.setState({
                count:data.total,
                page:data.currentpage,
                totalpage:data.page,
                data:data.data
            })
        })
    }

    select = (item) => {
        let self = this;
        if(item != this.state.selected)
        {
            if(item == 'favourite')
            {
                this.query = {favourite:true};
                this.getinfo({favourite:true})
            }
            else if(item == 'common')
            {
                this.query = {common:true};
                this.getinfo({common:true});
            }
            else
            {
                this.query = {};
                this.getinfo();
            }
            this.setState({
                selected:item
            })
        }
    }

    page = (page) => {
        if(page != this.state.page && page > 0 && page <= this.state.totalpage)
        {
            let query = this.query;
            query.page = page;
            this.getinfo(query);
        }
    }

    handlePage = (value) => {
        if(!Number.isNaN(value) && value >= 1 && value <= this.state.totalpage)
        {
            let query = this.query;
            query.page = value;
            this.getinfo(query);
        }
    }

    handleSearch = (value) => {
        this.query = {
            query:value
        };

        this.getinfo(this.query);
    }

    render()
    {
        return (
            <Row>
                <Col lg={12}>
                    <ul className="medsnav">
                        <li className={this.state.selected == 'favourite'?'item selected':'item'} onClick={()=>this.select('favourite')}>Favourite</li>
                        <li className={this.state.selected == 'common'?'item selected':'item'} onClick={()=>this.select('common')}>Common</li>
                        <li className={this.state.selected == 'system'?'item selected':'item'} onClick={()=>this.select('system')}>System</li>
                        <li className={this.state.selected == 'search'?'item selected':'item'} onClick={()=>this.select('search')}>Search</li>
                    </ul>
                </Col>
                <Col lg={12}>
                    <Row className="medslist_info">
                        {
                            this.state.selected == 'search' && (
                                <Col lg={12} style={{marginBottom:10}}>
                                    <input className="form-control" onChange={(e)=>this.handleSearch(e.target.value)}></input>
                                </Col>
                            )
                        }
                        <Col lg={12}>
                            <ul className="pagination">
                                <li className="button" onClick={()=>this.page(1)}><FontAwesomeIcon icon={faAngleDoubleLeft}></FontAwesomeIcon></li>
                                <li className="button" onClick={()=>this.page(this.state.page - 1)}><FontAwesomeIcon icon={faAngleLeft}></FontAwesomeIcon></li>
                                <li className="search_input">
                                    <InputGroup>
                                        <input className="form-control" value={this.state.page} onChange={(e)=>this.handlePage(e.target.value)}></input>
                                        <InputGroupAddon addonType="append">
                                           <span style={{marginLeft:10}}> / {this.state.totalpage}</span>
                                        </InputGroupAddon>
                                    </InputGroup>
                                    
                                </li>
                                <li className="button" onClick={()=>this.page(Number(this.state.page) + 1)}><FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon></li>
                                <li className="button" onClick={()=>this.page(this.state.totalpage)}><FontAwesomeIcon icon={faAngleDoubleRight}></FontAwesomeIcon></li>
                            </ul>
                        </Col>
                        <Col lg={12} style={{marginTop:10}}>
                            {
                                this.state.data.map((row,index)=>{
                                    return (
                                        <Row>
                                            <Col className="listitem" onClick={()=>this.props.select(row)}>{row.name}</Col>
                                        </Row>
                                    )
                                })
                            }
                        </Col>
                    </Row>
                </Col>
            </Row>
        )
        
    }
}

export default MedsList;