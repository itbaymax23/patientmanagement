import React from 'react';
import Card from '../../components/Card';
import {Free} from '../chartboxcomponent/action';
import {Col,Modal,ModalBody,ModalHeader,ModalFooter,Button,Row} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimesCircle,faPlus,faEdit} from '@fortawesome/free-solid-svg-icons';
import HTMLParser from 'html-to-react';
import {Editor} from 'react-draft-wysiwyg';
import {convertToRaw} from 'draft-js';
import draftToHTML from 'draftjs-to-html';
import $ from 'jquery'; 
import ContentEditable from 'react-contenteditable'
import ReactDomServer from 'react-dom/server';

import * as temp from '../../action/templatemodule';
import * as Template from '../../action/template';
import * as Macro from '../../action/Macro';
import DynamicAdd from './DynamicAdd';
import DynamicContent from './DynamicContent';
class CC extends React.Component
{
    history = [];
    countable = 0;
    prevState = [];
    firstinstall = true;
    updatable = false;
    editdata = "";
    textData = [];
    updated = false;
    selected = -1;
    selectedid = false;
    selectedcomponent = false;
    editcomponent = "";
    selected_textcomponent = 0;
    selected_edittextdata = 0;

    constructor(props)
    {
        super(props);
        this.state = {
            editable:false,
            freetext:false,
            textdataedit:false,
            updated:false,
            dynamicelement:false,
            editcomponent:false
        }

        // console.log("sessionupdate",props.sessionupdate);
        // if(!props.sessionupdate)    
        // {
        //     this.updateinfo();
        // }
        
    }

    updateinfo = () => {
        let self = this;
        Template.gettemplate(1).then(function(data){
            let templatedata = data.data;

            let textdata = [];
            for(let item in templatedata)
            {
                if(templatedata[item].SectionId == 1)
                {
                    textdata.push(templatedata[item].TextData);
                }
            }

            self.history = {textData:textdata,problem:[],freetextdata:"",freetext:false,value:{},selected:[]};

            self.props.save("CC",{textData:textdata,problem:[],freetextdata:"",freetext:false,value:{},selected:[],freetextenable:false},1);
        })
    }

    componentWillReceiveProps(props)
    {
        let self = this;
        this.textData = JSON.parse(JSON.stringify(props.data.textData));
        if(props.initdata && !props.sessionupdate)
        {
            console.log("initdata",props.initdata);
            console.log("updatedata",props.sessionupdate);
            this.updateinfo();    
        }

        if(props.updatemacro && props.selectedsection == 1 && props.macro.SectionId == 1)
        {
            let data = props.data;

            data.textData[props.macro.SubsectionId - 1] = props.macro.Content;
            let idarray = [props.macro.Id];
            if(!data.selected)
            {
                data.selected = [];
            }
            data.selected[props.macro.SubsectionId - 1] = [];
            console.log('cc_content',props.macro.content);
            for(let item in props.macro.list)
            {
                data.textData[props.macro.list[item].SubsectionId - 1] = props.macro.list[item].Content;
                data.selected[props.macro.list[item].SubsectionId - 1] = [];
                idarray.push(props.macro.list[item].Id);
            }

            for(let index = 0;index < 3;index ++)
            {
                if(!data.problem[index])
                {
                    data.problem[index] = [];
                }
            }

            Macro.getmacroccproblem(idarray).then(function(result){
                for(let item in result.data)
                {
                    if(item > 1)
                    {
                        data.problem[item - 2] = result.data[item];
                    }
                }
                data.freetext = false;
                data.freetextdata = "";
                console.log('cc_macro',data);    
                self.props.save('CC',data,1);
            })

            
        }
        
    }

    componentDidMount() {

       console.log("event");
       let self = this;
       $(document).on('click','.dynamicelement',function(){
            self.selected = $(this).attr('key');
            self.selectedid = $(this).attr('id');
            self.selectedcomponent = $(this);
            self.setState({
                dynamicelement:true
            })
       })
    }

    componentWillUpdate(nextprops,nextstate)
    {
       if(this.updatable && !this.underable)
       {
        //    let state = this.state;
        //    console.log("understate",this.history);
        //    this.history.push(state);
       }
    }

    componentDidUpdate()
    {
        this.updatable = false;
        let self = this;
        console.log('updated',this.selected);
        console.log('updated',this.updated);
        if(this.selected > -1 && this.updated)
        {
            self.setState({
                dynamicelement:!self.state.dynamicelement
            })
            self.updated = false;
            
        }
        
    }

    undostate = () => {
        this.updateinfo();
    }

    savedynamic = (id,value) => {
        let data = this.props.data;
        if(!data.dynamic)
        {
            data.dynamic = {};
        }

        data.dynamic[id] = value;

        this.props.save('dynamic',data.dynamic);
    }

    gettitle = (title) =>{
        let htmlparser = new HTMLParser.Parser();
        let array = title.split(/\{\{DynamicElement data=\'(.*)\'\}\}/g);
        
        let component = [];

        console.log("element_array",array);
        if(array.length == 1)
        {
            component.push( htmlparser.parse(temp.converttemp(array[0],this.props.patient)));
        }

        let last = "";
        if(array[2])
        {
            last = array[2];
        }
        while(array.length > 1)
        {
            let jsondata = array[1].split("'}}")[0];
            component.push(htmlparser.parse(temp.converttemp(array[0],this.props.patient)));
            let id = JSON.parse(jsondata).id;
            component.push(<DynamicContent id={id} dataid={"activity_" + id} patient = {this.props.patient} save={(value)=>this.savedynamic(id,value)} value={this.props.data.dynamic?this.props.data.dynamic[id]:false}></DynamicContent>)
            let data = title.split("{{DynamicElement data='" + jsondata + "'}}");
            if(data.length > 1)
            {
                array = data[1].split(/\{\{DynamicElement data=\'(.*)\'\}\}/g);
            }
            else
            {
                component.push(htmlparser.parse(temp.converttemp(data[0],this.props.patient)));
                break;
            }
        }
        
        component.push(htmlparser.parse(last));
        return component;
        // let htmlparser = new HTMLParser.Parser();
        // return htmlparser.parse(temp.converttemp(title,this.props.patient));
    }

    edit_component = (index,indexdata) => {
        let textdata = this.props.data.textData;
        let textarray = textdata[index].split('.');
        let data = textarray[indexdata].split(/\{\{FreeText data=\"(.*?)\"\}\}/);
        if(data.length > 1)
        {
            this.editcomponent = data[1];
        }
        else
        {
            this.editcomponent = "";
        }


        this.selected_textcomponent = index;
        this.selected_edittextdata = indexdata;

        this.setState({
            selectedcomponent:true
        })
    }

    addselect = (index,item,e) => {
        if(e.target.className && Object.keys(e.target.className).length > 0 && e.target.className.split('itemCC').length > 1)
        {
            let selected = this.props.data.selected;
            if(selected && selected[index] && selected[index].indexOf(item) > -1)
            {
                selected[index].splice(selected[index].indexOf(item),1);
                this.props.save('CC',{selected:selected},1);
                return;
            }
        }

        let selected = this.props.data.selected;
        if(!selected)
        {
            selected = {};
        }

        if(!selected[index])
        {
            selected[index] = [];
        }

        if(selected[index].indexOf(item) == -1)
        {
            selected[index].push(item);
        }

        this.props.save('CC',{selected:selected},1);
    }

    getcomponentfromtextdata = (textdata,index) => {
        let textarray = textdata?textdata.split('.'):[];
        let component = [];
        console.log('textdata_array',textarray);
        for(let item in textarray)
        {
            let component_title = [];
            textarray[item] = textarray[item].replace(/\{\{FreeText data=\"(.*?)\"\}\}/,"  $1");
            component_title.push(this.gettitle(textarray[item]));
            component_title.push(<span className="edit_component" onClick={()=>this.edit_component(index,item)}><FontAwesomeIcon icon={faEdit}></FontAwesomeIcon></span>);
            component.push(<span onClick={(e)=>this.addselect(index,item,e)} className={(this.props.data.selected && this.props.data.selected[index] && this.props.data.selected[index].indexOf(item) > -1)?"selected itemCC":"unselected itemCC"}>{component_title}.</span>);
        }

        return component;
    }

    resetview = (view) =>
    {
        setTimeout(() => {
            console.log("propdata",this.props.data);
            this.props.undoRedo.addStep();
          });
        this.props.save("CC",{param:view},1);
    }

    gettitlefromvalue(value)
    {
        let valuearray = [];
        for(let item in value)
        {
            valuearray.push(value[item].CommonName?value[item].CommonName:value[item].ShortDesc);
        }

        let last = '';
        if(valuearray.length > 1)
        {
            //last = value.pop();
        }

        let valuestr = valuearray.join(' , ');

        if(last)
        {
            valuestr += ' and ' + last;
        }
        return valuestr;
    }

    delete = (index,indexvalue) => {
        let data = this.props.data.problem;
        let deletedid = data[index][indexvalue].Id;
        let deleteddata = data[index][indexvalue];

        data[index].splice(indexvalue,1);

        let Assessments = this.props.Assessments;
        let HPI = this.props.HPI;
        let undeleted = 'primary'; let deleteindex = 'secondary';
        if(index < 2)
        {
            undeleted = 'secondary';
            deleteindex = 'primary';
        }

        let deleted = true;

        for(let item in data.problem)
        {
            if(item == index)
            {
                continue;
            }

            for(let problemindex in data.problem[item])
            {
                if(data.problem[item][problemindex].Id == deletedid)
                {
                    deleted = false;
                    break;
                }        
            }

            if(!deleted)
            {
                break;
            }
        }

        if(deleted)
        {
            for(let item in Assessments)
            {
                for(let problemindex in Assessments[item])
                {
                    if(Assessments[item][problemindex].Id == deletedid)
                    {
                        Assessments[item].splice(problemindex,1);
                        this.props.save('Assessments',Assessments,11);
                        break;
                    }
                }
            }

            for(let item in HPI.problem)
            {
                if(HPI.problem[item].Id == deletedid)
                {
                    HPI.problem.splice(item,1);
                    this.props.save('HPI',HPI,2);
                }
            }
        }
        else
        {
            let deletedassessments = true;
            for(let problemindex in Assessments[undeleted])
            {
                if(Assessments[undeleted][problemindex].Id == deletedid)
                {
                    deletedassessments = false;
                }
            }

            if(deletedassessments)
            {
                Assessments[undeleted].push(deleteddata);
                for(let problemindex in Assessments[deleteindex])
                {
                    if(Assessments[deleteindex][problemindex].Id == deletedid)
                    {
                        Assessments[deleteindex].splice(problemindex,1);
                        break;
                    }
                }

                this.props.save('Assessments',Assessments,11);
            }
        }

        this.props.save("CC",data,1);
    }
    rendercontent = () =>
    {
        let elementlist = [];
        let self = this;
        return (
            <div className="default_container main_content">
                {
                    this.props.data.textData.map((row,index)=>{
                        elementlist.push(self.getcomponentfromtextdata(row,index));
                        index > 0 && self.props.data.problem[index - 1] && self.props.data.problem[index - 1].map((rowvalue,indexvalue)=>{
                            elementlist.push(
                                <span className="value_noborder" key={indexvalue}>
                                    {rowvalue.CommonName?rowvalue.CommonName:rowvalue.ShortDesc} ({rowvalue.Code})
                                    <FontAwesomeIcon className="righticon" style={{color:"red"}} icon={faTimesCircle} onClick={()=>self.delete(index - 1,indexvalue)}></FontAwesomeIcon>
                                </span>
                            )
                        })

                        if(index > 0)
                        {
                            elementlist.push(<FontAwesomeIcon icon={faPlus} className="righticon" onClick={()=>self.props.showproblem('CC',index - 1,1)} style={{marginRight:10}}></FontAwesomeIcon>)
                        }
                    })
                }
                {elementlist}
            </div>
        )
       
    }

    editable = () => {
        this.setState({
            freetextview:!this.state.freetextview
        })
    }

    onEditorStateChange = (state) => {
        this.editdata = draftToHTML(convertToRaw(state.getCurrentContent()));
    }

    addfreetext = (data) => {
        
       this.props.save("CC",{freetext:true,freetextdata:this.editdata,problem:[]},1);
       this.setState({
           freetextview:false
       })
    }

    edittextdata = () => {
        this.textData = JSON.parse(JSON.stringify(this.props.data.textData));
        this.setState({
            textdataedit:!this.state.textdataedit
        })
    }

    handleDiagnosticElement = (value,index) => {
        this.textData[index] = value;
        this.setState({
            updated:!this.state.updated
        })
    }
    
    
    rendertextdata = (value,index) => 
    {
        var myReg = /\{\{DynamicElement data=\'(.*?)\'\}\}/g;

        if(!value)
        {
            return '';
        }
        var arraysplit = value.split(myReg);
        console.log('array',arraysplit);

        for(let item in arraysplit)
        {
            if(item % 2 == 1)
            {
                arraysplit[item] = '<span key="' + index + '" class="dynamicelement" id="' + JSON.parse(arraysplit[item]).id + '" contenteditable="false">' + JSON.parse(arraysplit[item]).caption + '</span>';
            }
        }

        value = arraysplit.join("");
        //value = value.replace(myReg,ReactDomServer.renderToString(<DynamicElement id="$1"></DynamicElement>));
        let array = value.split(/\[(.*?)\]/g);
        if(array.length > 1 && this.selected == -1)
        {
            this.selected = index;
            this.caption = array[1];
            this.updated = true;
            this.setState({
                dynamicelement:true
            })
        }

        console.log(value);
        return value;
    }
    
    addfreetextdata = () => {
        let textdata = this.props.data.textData;
        let textarray = textdata[this.selected_textcomponent].split('.');
        let match_data = textarray[this.selected_edittextdata].split(/\{\{FreeText data=\"(.*?)\"\}\}/);
        if(match_data.length == 1)
        {
            textarray[this.selected_edittextdata] += '{{FreeText data="' + this.editcomponent + '"}}';
        }
        else
        {
            textarray[this.selected_edittextdata] = textarray[this.selected_edittextdata].replace(/\{\{FreeText data=\"(.*?)\"\}\}/,'{{FreeText data="' + this.editcomponent + '"}}');
        }

        textdata[this.selected_textcomponent] = textarray.join('.');

        this.setState({
            selectedcomponent:false
        })
        this.props.save('CC',{textData:textdata},1);
        this.render();
    }

    adddynamic = (data) => {

        console.log("dynamic",data);
        if(!data)
        {
            this.textData[this.selected] = this.textData[this.selected].replace(/\[(.*?)\]/g,"");
        }
        else
        {
            this.textData[this.selected] = this.textData[this.selected].replace(/\[(.*)\]/g,"{{DynamicElement data='" + JSON.stringify(data) + "'}}");
        }

        if(this.selectedcomponent && data)
        {
            this.selectedcomponent.innerHTML = data.caption;
        }
        this.selected = -1;
        this.selectedid = false;
        this.updated = false;
        this.selectedcomponent = false;
        this.setState({
            dynamicelement:false
        })
    }

    savetextdata = () => {
        console.log(this.textData);
        for(let item in this.textData)
        {
            this.textData[item] = this.textData[item].replace(/\<span key=\"(.*?)\" class=\"dynamicelement\" id=\"(.*?)\"(.*?)\>(.*?)\<\/span\>/g,"{{DynamicElement data='{\"id\":$2,\"caption\":\"$4\"}'}}");
        }
        console.log(this.textData);
        this.props.save('CC',{textData:this.textData},1);
        this.edittextdata();
    }
    render()
    {
        let Tag = Col;
        let self = this;
        const setEditorReference = (ref) => {
            this.editorReferece = ref;
            this.editorReferece.focus();
          }
        //this.gettitle(this.state.data[0].title);
        return (
            <Card title="Chief Complaint" tag = {Free} resetView={this.resetview} edit={this.edittextdata} undo={this.undostate} problemgrid={()=>this.props.problemgrid()} editable={this.editable} macro={()=>this.props.showmacro(1)}>
                {
                    (!this.props.data.freetext && !this.props.data.freetextenable) && (this.props.data.param == "line" || !this.props.data.param) && this.props.data.textData.map((row,index)=>{
                        return(
                            <Tag className="default_container main_content" key={index}>
                            {
                                this.getcomponentfromtextdata(row,index)
                            }
                            {
                               index > 0 && self.props.data.problem[index - 1] && self.props.data.problem[index - 1].map((rowvalue,indexvalue)=>{
                                    return (
                                        <span className="value_noborder" key={indexvalue}>
                                            {rowvalue.CommonName?rowvalue.CommonName:rowvalue.ShortDesc} ({rowvalue.Code})
                                            <FontAwesomeIcon className="righticon" style={{color:"red"}} icon={faTimesCircle} onClick={()=>self.delete(index - 1,indexvalue)}></FontAwesomeIcon>
                                        </span>
                                    )
                                })                       
                            }
                            {index > 0 && (<FontAwesomeIcon icon={faPlus} className="righticon" onClick={()=>this.props.showproblem('CC',index - 1)}></FontAwesomeIcon>)}
                            </Tag>
                        )
                    })
                }
                {
                    !this.props.data.freetext && this.props.data.param == "paragraph" && this.rendercontent()
                }
                {
                    (this.props.data.freetext || this.props.data.freetextenable) && (<div dangerouslySetInnerHTML={{__html: this.props.data.freetextdata}}></div>)
                }
                <Modal isOpen={this.state.freetextview} size="lg">
                    <ModalHeader toggle={this.editable}>Add Free Text</ModalHeader>
                    <ModalBody>
                        <Editor 
                            onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                            editorStyle={{minHeight:300}}
                            >
                        </Editor>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.editable}>Cancel</Button>
                        <Button color="success" style={{marginLeft:10}} onClick={this.addfreetext}>Save</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.textdataedit} size="lg">
                    <ModalHeader toggle={this.edittextdata}>Edit CC Content</ModalHeader>
                    <ModalBody>
                        {
                            this.textData.map((row,index)=>{
                                return (
                                    <ContentEditable onChange={(e)=>this.handleDiagnosticElement(e.target.value,index)} html={this.rendertextdata(row,index)} editorRef={setEditorReference}></ContentEditable>
                                    // <div contenteditable="true" disabled={false} key={index} onChange={this.handleDiagnoticElement} style={{marginTop:10}} dangerouslySetInnerHTML={{__html:this.rendertextdata(row)}}></div>
                                )
                            })
                        }
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.edittextdata}>Cancel</Button>
                        <Button color="success" onClick={this.savetextdata}>Save</Button>
                    </ModalFooter>
                </Modal>
                <DynamicAdd caption={this.caption} showdynamic={this.state.dynamicelement} adddynamic={this.adddynamic} id={this.selectedid} close={()=>{this.selected = -1;this.selectedid = false;this.updated = false;
                    this.selectedcomponent = false;this.setState({dynamicelement:false})}}></DynamicAdd>
                <Modal isOpen={this.state.selectedcomponent}>
                    <ModalHeader toggle={()=>this.setState({selectedcomponent:false})}>Add Free Text</ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col>
                                <textarea className="form-control" defaultValue={this.editcomponent} onChange={(e)=>{this.editcomponent = e.target.value;}}></textarea>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={()=>this.setState({selectedcomponent:false})}>Cancel</Button>
                        <Button color="success" onClick={this.addfreetextdata}>Save</Button>
                    </ModalFooter>
                </Modal>
            </Card>

            
        )
    }
}

export default CC;