import React from 'react';
import * as Social from '../../action/user';
import Card from '../../components/Card';
import {Col,Row,Button,Modal,ModalBody,ModalHeader,ModalFooter,Form,FormGroup,Label} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import {Checkbox,CheckboxGroup} from 'react-ui-icheck';
import {SHTag} from '../chartboxcomponent/action';
class SH extends React.Component
{
    substancedata = {};
    constructor(props)
    {
        super(props);
        this.state = {
            substanceedit:false,
            updated:false
        }
       
        this.updateinfo();
    }

    updateinfo = () => {
        let self = this;
       
        Social.getsocialhistory().then(function(data){
            

            for(let item in data.data.substance)
            {
                self.substancedata[data.data.substance[item].Name] = {};
            }

            let SHIdata = self.props.SHI.data;
            self.props.save("SHI",{substance:data.data.substance,social:data.data.section,data:SHIdata,freetextdata:"",freetextenable:false},7);
        })
    }

   componentWillReceiveProps(props)
   {
        if(props.initdata)
        {
            this.updateinfo();
        }
   }

    setitem = (item,value) => {
        let data = this.props.SHI.data;
        data[item] = value;
        this.props.save("SHI",{data:data});
    }

    toggle = () => {
        this.setState({
            substanceedit:!this.state.substanceedit
        })
    }

    changestatus = (item,value) => {
        this.substancedata[item].status = value;
        this.setState({
            updated:!this.state.updated
        })
    }

    gettitle = (row) => {
        if(this.props.SHI.data.substance[row.Name] == undefined || this.props.SHI.data.substance[row.Name].status == undefined || this.props.SHI.data.substance[row.Name].status == "Never")
        {
            return 'Access Denies';
        }
        else
        {
            var str = this.props.SHI.data.substance[row.Name].status + " use ";
            if(this.props.SHI.data.substance[row.Name].type)
            {
                str += this.props.SHI.data.substance[row.Name].type.join(',');
            }
            if(this.props.SHI.data.substance[row.Name].agestart)
            {
                str += " from " + this.props.SHI.data.substance[row.Name].agestart
            }

            if(this.props.SHI.data.substance[row.Name].agestop)
            {
                str += " Until " + this.props.SHI.data.substance[row.Name].agestop;
            }

            

            var enable = false;
            for(let itemunit in row.units)
            {
                if(this.props.SHI.data.substance[row.Name]['unit'] == row.units[itemunit].Id)
                {
                    str += this.props.SHI.data.substance[row.Name]['amount'] + " " + row.units[itemunit].UnitText + " / " + row.units[itemunit].UnitPeriod;
                    enable = true;
                }
            }

            if(!enable && this.props.SHI.data.substance[row.Name].amount)
            {
                str += " " + this.props.SHI.data.substance[row.Name].amount;

                if(row.units.length > 0)
                {
                    str += " " + row.units[0].UnitText + " / " + row.units[0].UnitPeriod;
                }
            }
            return str;
        }
    }

    savesubstance = () => {
        let data = this.props.SHI.data;
        data.substance = this.substancedata;
        this.props.save("SHI",{data:data},5);
        this.setState({
            substanceedit:false
        })
    }

    changetype = (e,type,name) => {
        if(!this.substancedata[name].type)
        {
            this.substancedata[name].type = [];
        }

        if(e.target.checked)
        {
            this.substancedata[name].type.push(type);
        }
        else
        {
            let index = this.substancedata[name].type.indexOf(type);
            if(index > -1)
            {
                this.substancedata[name].type.splice(index,1);
            }
            
        }
    }

    changeitem = (name,item,value) => {
        this.substancedata[name][item] = value;
    }

    gettitleinparam = () => {
        let substance = this.props.SHI.substance;
        let itemarray = [];
        let positive = [];
        for(let item in substance)
        {
            if(this.gettitle(substance[item]) == 'Access Denies')
            {
                itemarray.push(substance[item].Name);
            }
            else
            {
                positive.push(this.gettitle(substance[item]));
            }
        }

        let positivestr =  positive.join(',');
        if(itemarray.length > 0)
        {
            positivestr += " and No Use " + itemarray.join(' , ');
        }

        return positivestr;
    }

    gettitlearray = () => {
        let titlearray = [];
        for(let item in this.props.SHI.social)
        {
            titlearray.push(this.props.SHI.social[item].Name);
        }

        return titlearray;
    }

    undo = () => {
        this.updateinfo();
    }
    render()
    {
        let self = this;
        return (
            <Card title="Social History" edit={this.toggle} tag={SHTag} resetView={(param)=>this.props.save('SHI',{param:param},7)} undo={this.undo}>
                {
                    (!this.props.SHI.freetextenable && this.props.SHI.param != "paragraph") && (
                        <div className="mytable">
                            <Col>
                                <div className="tableheader">
                                    <div className="title_header">
                                        <p>Substance Use</p>
                                    </div>
                                    {
                                        this.props.SHI.substance.map((row,index)=>{
                                            return (
                                                <div key={index} className="header">
                                                    <p>{row.Name}</p>
                                                    {
                                                        this.props.SHI.data.substance[row.Name] && (
                                                            <p key={"param_" + index}>
                                                                {
                                                                    this.gettitle(row,this.props.SHI.data.substance)
                                                                }
                                                            </p>
                                                        )
                                                    }
                                                </div>
                                            )
                                        })
                                    }
                                    {/* <div style={{width:"10%",float:'left'}}>
                                        <Row>
                                            <Button color="primary" style={{marginLeft:"auto"}} onClick={this.toggle}>
                                                <FontAwesomeIcon icon={faPencilAlt} style={{marginRight:10}}></FontAwesomeIcon>Edit
                                            </Button>
                                        </Row>
                                    </div> */}
                                </div>
                                <div className="tablebody">
                                    {
                                        this.props.SHI.social.map((row,index)=>{
                                            return (
                                                <div key={"table_" + index} className="tablerow">
                                                    <div className="cell">
                                                        <p className="body_title">{row.Name}</p>
                                                    </div>
                                                    <div className={!this.props.SHI.data[row.Name]?"cell specific":"cell"}>
                                                        <p className="tablebodytext" onClick={()=>this.setitem(row.Name,false)}>unspecified</p>
                                                    </div>
                                                    {
                                                        row.options.map((item,index)=>{
                                                            return (
                                                                <div key={index} className={(this.props.SHI.data[row.Name] == item.OptionText)?"cell specific":"cell"}>
                                                                    <p className="tablebodytext" onClick={()=>this.setitem(row.Name,item.OptionText)}>{item.OptionText}</p>
                                                                </div>
                                                            )
                                                        })
                                                    }
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </Col>
                        </div>
                    )
                }
                {
                    (!this.props.SHI.freetextdata && this.props.SHI.param == 'paragraph') && (
                        <div className="mytable">
                            <Col>
                                <span className="items"><span className="title">In Substance </span><span>{this.gettitleinparam()}</span></span>
                                {
                                    this.props.SHI.data && Object.keys(this.props.SHI.data).map((row,index)=>{
                                        let array = self.gettitlearray();

                                        if(array.indexOf(row) > -1)
                                        {
                                            return (
                                                <span className="items">
                                                    <span className="title">{row}</span><span> is {this.props.SHI.data[row]}</span>
                                                </span>
                                            )    
                                        }
                                        
                                    })
                                }
                            </Col>
                        </div>
                    )
                }
                {
                    this.props.SHI.freetextenable && (
                        <div className="table" dangerouslySetInnerHTML={{__html:this.props.SHI.freetextdata}}></div>
                    )
                }

                <Modal isOpen={this.state.substanceedit} size="lg">
                    <ModalHeader toggle={this.toggle}>Social Information</ModalHeader>
                    <ModalBody>
                        <Row>
                        {
                            this.props.SHI.substance.map((row,index)=>{
                                return (
                                    <Col lg={12} style={{marginTop:20}}>
                                        <h4>{row.Name}</h4>
                                        <Form>
                                            <Row className="substancecontainer">
                                                <Col lg={12}>
                                                    <FormGroup row>
                                                        <Label style={{marginLeft:15}}>Use Status</Label>
                                                        <Col>
                                                            <select className="form-control" defaultValue={self.substancedata[row.Name] != undefined?self.substancedata[row.Name].status:''} onChange={(e)=>this.changestatus(row.Name,e.target.value)}>
                                                                {
                                                                   
                                                                   row.status.map((status,index_status)=>{
                                                                        return (
                                                                            <option value={status.Name}>{status.Name}</option>
                                                                        )
                                                                    })
                                                                }
                                                            </select>
                                                        </Col>
                                                    </FormGroup>
                                                    {
                                                        self.substancedata[row.Name] && self.substancedata[row.Name].status != undefined && self.substancedata[row.Name].status != "Never" && (
                                                            <FormGroup row>
                                                            {
                                                                (self.substancedata[row.Name].status != undefined && self.substancedata[row.Name].status != "Never" && row.HasTypes == 1) && (
                                                                    <Col>
                                                                        <Row>
                                                                            <Label style={{marginLeft:15}}>Type:</Label>
                                                                            <Col className="icheck_wrap">
                                                                            <CheckboxGroup checkboxWrapClassName="form-check">
                                                                            {
                                                                                row.types.map((type,index_type)=>{
                                                                                    return (
                                                                                        <Checkbox
                                                                                        checkboxClass="icheckbox_flat-grey"
                                                                                        label={type.Name}
                                                                                        labelTag="div"
                                                                                        labelTagClassName="d-inline"
                                                                                        defaultChecked={this.substancedata[row.Name].type && this.substancedata[row.Name].type.indexOf(type.Name) > -1}
                                                                                        onChange = {(e)=>this.changetype(e,type.Name,row.Name)}
                                                                                        style={{marginRight:5}}
                                                                                        increaseArea="10%"
                                                                                        >
                                                                                        </Checkbox>
                                                                                    )
                                                                                })
                                                                            }
                                                                            </CheckboxGroup>
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                )
                                                            }
                                                            {
                                                                row.UsesAmount == 1 && (
                                                                    <Col>
                                                                        <Row>
                                                                            <Label style={{marginLeft:15}}>Amount</Label>
                                                                            <Col lg={3}>
                                                                                <input className="form-control" onChange={(e)=>this.changeitem(row.Name,"amount",e.target.value)}></input>
                                                                            </Col>
                                                                            <Col>
                                                                                <select className="form-control" onChange={(e)=>this.changeitem(row.Name,"unit",e.target.value)} defaultValue={this.substancedata[row.Name].unit}>
                                                                                    {
                                                                                        row.units.map((unit)=>{
                                                                                            return (
                                                                                                <option value={unit.Id}>{unit.UnitText} per {unit.UnitPeriod}</option>
                                                                                            )
                                                                                        })
                                                                                    }
                                                                                </select>
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                )
                                                            }
                                                            {
                                                                row.UsesAge == 1 && (
                                                                    <Col>
                                                                        <Row>
                                                                            <Col>
                                                                                <Row>
                                                                                    <Label>Age Start:</Label>
                                                                                    <Col>
                                                                                        <input className="form-control" onChange={(e)=>this.changeitem(row.Name,'agestart',e.target.value)} defaultValue={this.substancedata[row.Name]['agestart']}></input>
                                                                                    </Col>
                                                                                </Row>
                                                                            </Col>
                                                                            <Col>
                                                                                <Row>
                                                                                    <Label>Age Stop:</Label>
                                                                                    <Col>
                                                                                        <input className="form-control" onChange={(e)=>this.changeitem(row.Name,'agestop',e.target.value)} defaultValue={this.substancedata[row.Name]['agestop']}></input>
                                                                                    </Col>
                                                                                </Row>
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                )
                                                            }
                                                        </FormGroup>
                                                        )
                                                    }
                                                    
                                                </Col>
                                            </Row>
                                        </Form>
                                    </Col>
                                )
                            })
                        }
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                        <Button color="success" onClick={this.savesubstance} style={{marginLeft:15}}>Save</Button>
                    </ModalFooter>
                </Modal>
            </Card>
        )
    }
}

export default SH;