import React from 'react';
import Card from '../Card';
import {Diagnostic,Categories} from '../chartboxcomponent/action';
import * as Problem from '../../action/Problem';
import {Row,Col,Table,Button,Modal,ModalBody,ModalFooter,ModalHeader,Form,FormGroup,Label} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import {DatePicker} from 'react-widgets';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
import Select from 'react-select';
class DD extends React.Component
{
    adddata = {
        test:{},
        date:new Date(),
        result:""
    }

    editvalue = "";
    constructor(props)
    {
        super(props);
        this.state = {
            category:[],
            item:[],
            datelist:[],
            addnew:false,
            selectedcategory:false,
            type:'lab',
            align:'left'
        }
        this.updateinfo();
    }

    componentDidMount()
    {
        let datelist = [];
        for(let index = -2;index<3;index++)
        {
            let subdate = new Date();
            subdate.setDate(subdate.getDate() + index);
            datelist.push(Moment(subdate).format('MM.DD.YY'));
        }
        this.setState({
            datelist:datelist
        })
    }

    updateinfo = () => {
        let self = this;
        Problem.getdiagnostic().then(function(data){
            self.setState(data.data);
        })
    }

    toggle = () => {
        this.setState({
            addnew:!this.state.addnew
        })
    }

    addnew = () => {
        this.setState({
            addnew:!this.state.addnew
        })
    }

    handleData = (value,item) => {
        this.adddata[item] = value;
    }

    savedata = () => {
        let item = this.state.item;
        var date = this.adddata.date;
        let datestring = Moment(date).format('MM.DD.YY');
        
        var datelist = this.state.datelist;
        if(datelist.indexOf(datestring) == -1)
        {
            datelist.push(datestring);    
        }

        datelist.sort();
        var test = this.adddata.test.value;

        console.log(this.adddata);
        
        for(let itemlist in item)
        {
            for(let element in item[itemlist])
            {
                if(item[itemlist][element].Code == test)
                {
                    if(!item[itemlist][element].list)
                    {
                        item[itemlist][element].list = {};
                    }
                    console.log(datestring);
                    item[itemlist][element].list[datestring] = this.adddata.result;
                    this.setState({
                        item:item,
                        datelist:datelist,
                        addnew:false
                    })
                    return;
                }
            }
        }
    }

    handleEdit = (value) => {
        this.editvalue = value;
    }
    focused = (value) => {
        this.editvalue = value;
    }

    save = (date,id,itemindex) => {
        let item = this.state.item;
        if(!item[id][itemindex].list)
        {
            item[id][itemindex].list = {};
        }

        item[id][itemindex].list[date] = this.editvalue;
        this.setState({
            item:item
        })
    }  
    
    selectcategory = (category) => {
        this.setState({
            selectedcategory:category
        })
    }

    selecttype = (type) => {
        this.setState({
            type:type
        })
    }

    changealign = (align) => {
        this.setState({
            align:align
        })
    }

    undo = () => {

        let data = this.props.data;
        for(let item in data)
        {
            data[item] = false;
        }
        this.props.save('DD',data,12);
    }

    render()
    {
        Moment.locale('en');
        momentLocalizer();
        let self = this;
        let options = [];
        for(let item in this.state.item)
        {
            for(let list in this.state.item[item])
            {
                options.push({value:this.state.item[item][list].Code,label:this.state.item[item][list].CommonName?this.state.item[item][list].CommonName:this.state.item[item][list].BillDesc})
            }
        }

        if(!this.adddata.test)
        {
            this.adddata.test = {};
        }
        if(!this.adddata.test.value)
        {
            this.adddata.test = options[0];
        }
        return (
            <Card title="Diagnostic Data" tag={Diagnostic} type={this.state.type?this.state.type:'lab'} selecttype = {this.selecttype} undo={this.undo}>
                {
                    this.props.data.freetextenable && (
                        <Row>
                            <Col dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>
                        </Row>    
                    )
                }
                {
                    !this.props.data.freetextenable && (
                        <Row>
                            {
                                this.state.align == 'left' && (
                                    <Col lg={3} md={3} sm={4} xs={6} style={{maxWidth:'20%'}}>
                                        <Categories category={this.state.category} selected={this.state.selectedcategory} selectcategory={this.selectcategory} type={this.state.type?this.state.type:'lab'} changealign = {this.changealign} align={this.state.align}/>
                                    </Col>
                                )
                            }
                            
                            <Col lg={9} md={9} sm={8} xs={6} style={{maxWidth:'80%'}}>
                                <Table responsive bordered hover>
                                    <thead>
                                        <tr>
                                            <th colSpan={2}>Date <Button color="primary" style={{float:"right"}} onClick={this.addnew}>Add New <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button></th>
                                            {
                                                this.state.datelist.map((row,index)=>{
                                                    return (<th key={index}>{row}</th>)
                                                })
                                            }
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.category.map((row,index)=>{
                                                if(row.type == self.state.type)
                                                {
                                                    return (
                                                        (self.state.selectedcategory == row.Id || !self.state.selectedcategory) && self.state.item[row.Id] && self.state.item[row.Id].map((rowitem,itemindex)=>{
                                                            return (
                                                                <tr>
                                                                    {
                                                                        itemindex == 0 && (
                                                                            <td rowSpan={self.state.item[row.Id].length}>{row.Name}</td>
                                                                        )
                                                                    }
                                                                    <td>{rowitem.CommonName?rowitem.CommonName:rowitem.BillDesc}</td>
            
                                                                    {
                                                                        self.state.datelist.map((datelist,indexlist)=>{
                                                                            return (
                                                                                <td><input className="form-control" defaultValue={rowitem.list && rowitem.list[datelist]?rowitem.list[datelist]:''} onChange={(e)=>this.handleEdit(e.target.value)} onBlur={()=>this.save(datelist,row.Id,itemindex)} onFocus={(e)=>{this.focused(e.target.value)}}></input></td>
                                                                            )
                                                                        })
                                                                    }
                                                                </tr>
                                                            )
                                                        })
                                                    ) 
                                                }
                                                
                                            })
                                        }
                                    </tbody>
                                </Table>
                            </Col>

                            {
                                this.state.align == 'right' && (
                                    <Col lg={3} md={3} sm={4} xs={6} style={{maxWidth:'20%'}}>
                                        <Categories category={this.state.category} selected={this.state.selectedcategory} selectcategory={this.selectcategory} type={this.state.type?this.state.type:'lab'} changealign = {this.changealign} align={this.state.align}/>
                                    </Col>
                                )
                            }
                            
                        </Row>
                    )}
                    
                <Modal isOpen={this.state.addnew}>
                    <ModalHeader toggle={this.toggle}>
                        Lab Test Result
                    </ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup row>
                                <Label lg={4}>Test</Label>
                                <Col>
                                    <Select options={options} placeholder="Select Test" defaultValue={this.adddata.test} onChange={(value)=>{this.handleData(value,"test")}}></Select>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label lg={4}>Date</Label>
                                <Col>
                                    <DatePicker defaultValue={this.adddata.date} onChange={(date)=>this.handleData(date,"date")}></DatePicker>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label lg={4}>Result</Label>
                                <Col>
                                    <input className="form-control" defaultValue="" onChange={(e)=>this.handleData(e.target.value,"result")}></input>
                                </Col>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.savedata}>Save</Button>
                        <Button color="secondary" style={{marginLeft:10}} onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </Card>
        )
    }
}

export default DD;