import React from 'react';
import Card from '../Card';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimesCircle} from '@fortawesome/free-regular-svg-icons';
import {Row,ButtonGroup,Button,Popover,PopoverHeader,Col,PopoverBody,Modal,ModalBody,ModalHeader,ModalFooter,Form,FormGroup,Label,InputGroupAddon,InputGroup} from 'reactstrap';
import {PopoverContent} from 'react-bootstrap';
import {DetailPage,SearchComponent} from '../chartboxcomponent/action';
import * as Problem from '../../action/Problem';
import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css';
import styled from 'styled-components';
import { faPlay, faPause, faPlus, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import {Editor} from 'react-draft-wysiwyg';
import {convertToRaw} from 'draft-js';
import draftToHTML from 'draftjs-to-html';
import moment from 'moment';
import MedsList from './Medslist';
import {DatePicker,NumberPicker} from 'react-widgets';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
import simpleNumberLocalizer from 'react-widgets-simple-number'; 
import $ from 'jquery';
class MEDS extends React.Component
{
    editchange = "";
    editdata = "";
    name = "";
    info = "";
    selectedmeds = {};
    selecteddiagnose = "";
    openeddiagnosis = [];

    constructor(props)
    {
        super(props);
        this.state = {
            drugsearch:true,
            drugedit:false,
            module:false,
            drugs:[],
            meds:[],
            selectedid:-1,
            playmodule:false,
            selectedmeds:{},
            medsdialog:false,
            medsedit:{},
            editdetail:{
                diag:[],
                package:[],
                sig:[],
                dosage:[]
            },
            addinfo:false,
            opendiagnosis:false
        }
    }

    
    add = (row) => {
        let meds = this.props.meds;
        
        row.sig = row.value +" " + " " + row.type + " " + row.day;
        row.duration = 1;
        meds.push(row);
        this.props.save("MEDS",{meds:meds},5);
        this.setState({
            module:false
        })        
    }

    toggle = (value) => {
        this.setState({
            module:!this.state.module
        })
    }
    
    componentDidMount()
    {
        let self = this;
        $(document).on('click',function(event){
            if($(event.target).parents('.searchcontent').length == 0)
            {
                self.setState({module:false});
            }
            if($(event.target).parents('.diagnost_list').length == 0)
            {
                self.setState({opendiagnosis:false});
            }
        })
    }

    search = (value) => {
        let self = this;
        if(value)
        {   
            Problem.searchmeds(value).then(function(data){
                for(let item in data.data)
                {
                    data.data[item].type = "QD";
                    data.data[item].day = "PO";
                    data.data[item].value = 1;
                }
                self.setState({
                    drugs:data.data,
                    module:true
                })
            })
        }
    }

    selectmeds = (row) => {
        let self = this;
        this.selectedmeds = {};
        Problem.getmedsdetail(row.itemId).then(function(result){
            var data = {};
            data.diag = result.data.diag;
            data.sig = result.data.sig;
            data.dosage = result.data.dosage;
            data.package = result.data.package;
            
            self.selectedmeds.name = row.name;
            self.selectedmeds.itemId = row.itemId;
            self.selectedmeds.diag = data.diag.length > 0?data.diag[0].optionText:"";
            self.selectedmeds.sig = data.sig.length>0?data.sig[0].optionText:"";
            self.selectedmeds.dosage=data.dosage.length>0?data.dosage[0].optionText:"";
            self.selectedmeds.package = data.package.length>0?data.package[0].optionText:"";
            
            self.setState({
                editdetail:data,
                selectedmeds:row.itemId
            })
        })
    }

    changeslider = (value,index) => {
        let drugs = this.state.drugs;
        drugs[index].value = value;
        this.setState({
            drugs:drugs
        })
    }

    saveitem = (item,value,index) => {
        let drugs = this.state.drugs;
        drugs[index][item] = value;
        this.setState({
            drugs:drugs
        })
    }

    setitem = (item,index) => {
        let meds = this.props.meds;
        meds[index][item] = meds[index][item]?false:true;
        if(meds[index][item])
        {
            meds[index].date = moment(new Date()).format('MM/DD/YYYY');
        }
        this.props.save("MEDS",{meds:meds},5);
    }

    run = (index) => {
        this.selectmeds(this.props.meds[index]);
        this.selectedmeds = {};
        for(let item in this.props.meds[index])
        {
            this.selectedmeds[item] = this.props.meds[index][item]
        }

        let self = this;
        this.setState({
            selectedid:index,
            medsdialog:true
        });
        // Problem.getmedsdetail(this.props.meds[index].itemId).then(function(result){
        //     var data = {};
        //     data.diag = result.data.diag;
        //     data.sig = result.data.sig;
        //     data.dosage = result.data.dosage;
        //     data.package = result.data.package;
            
            
        //     self.setState({
        //         editdetail:data,
        //         selectedmeds:self.selectedmeds.itemId,
        //         selectedid:index,
        //         medsdialog:true
        //     })
        // })
    }

    runsave = () => {
        let selectedmeds = this.state.selectedmeds;
        selectedmeds.duration = 1;
        let meds = this.props.meds;
        meds.push(selectedmeds);
        this.props.save("MEDS",{meds:meds},5);
        
        this.setState({
            playmodule:false
        })
    }

    saverun = (item,value) => {
        let selectedmeds = this.state.selectedmeds;
        selectedmeds[item] = value;
        this.setState({
            selectedmeds:selectedmeds
        })
    }

    changeinput = (value) =>{
        this.editchange = value;
    }
    
    setvalue = (item,index,value) => {
        let meds = this.props.meds;
        meds[index][item] = value;
        this.props.save("MEDS",{meds:meds},5);
    }

    refillall = () => {
        let meds = this.props.meds;
        for(let item in meds)
        {
            meds[item].refill = true;
            meds[item].date = moment(new Date()).format('MM/DD/YYYY');
        }

        this.props.save("MEDS",{meds:meds},5);
    }

    onEditorStateChange = (state) => {
        this.editdata = draftToHTML(convertToRaw(state.getCurrentContent()));
    }
    
    editable = () => {
        this.setState({
            freetext:!this.state.freetext
        })
    }

    addfreetext = () => {
        this.props.save("MEDS",{freetextdata:this.editdata},5);
        this.setState({
            freetext:false
        })
    }

    addmeds = () => {
        this.setState({
            medsdialog:!this.state.medsdialog,
            selectedid:-1
        })
    }

    addindication = (name) => {
        this.name = name;
        this.info = "";
        this.showindication();
    }

    showindication = () => {
        this.setState({
            addinfo:!this.state.addinfo
        })
    }

    handleChangeforedit = (value) => {
        this.info = value;
    }

    handleChange = (name,value) => {
        this.selectedmeds[name] = value;
    }

    saveindication = () => {
        console.log(this.state.selectedmeds);
        if(this.info && this.state.selectedmeds)
        {
            let self = this;
            Problem.addindication(this.state.selectedmeds,this.name,this.info).then(function(result){
                let state = self.state.editdetail;
                state[self.name] = result.data;
                self.selectedmeds[self.name] = result.data[result.data.length - 1].optionText;
                self.setState({
                    editdetail:state
                });
            })
        }
        this.showindication();
    }

    savemeds = () => {
        let meds = this.selectedmeds;
        let medsprops = this.props.meds;
        if(this.state.selectedid > -1)
        {
            medsprops[this.state.selectedid] = meds;
        }
        else
        {
            medsprops.push(meds);
        }
        
        this.addPMH(medsprops);
        this.props.save('MEDS',{meds:medsprops},5);
        this.addmeds();
    }

    deleteselected = () => {
        let index = this.state.selectedid;
        let meds = this.props.meds;
        meds.splice(index,1);
        this.addPMH(meds);
        this.props.save('MEDS',{meds:meds},5);
        this.addmeds();
    }

    getstringdate = (date) => {
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    }

    getdefaultdispense = (row) => {
        let amount = 1;
        if(row.type == 'BID')
        {
            amount = 2;
        }
        else if(row.type == 'TID')
        {
            amount = 3;
        }
        else if(row.type == 'QOD')
        {
            amount = 4;
        }

        return row.duration * amount;
    }

    selectdiagnose = (index) => {
        this.selecteddiagnose = "diagnose" + index;
        let self = this;
        Problem.getmedsdetail(this.props.meds[index].itemId).then(function(data){
            self.openeddiagnosis = data.data.diag;
            self.setState({opendiagnosis:!self.state.opendiagnosis})
        })
    }

    adddiagnose = (value) => {
        let array = this.selecteddiagnose.split("diagnose");
        if(array.length > 1)
        {
            var meds = this.props.meds;
            meds[array[1]].diag = value;
        }

        this.props.save('MEDS',{meds:meds},5);
        this.addPMH(meds);
    }

    addPMH = (meds) => {
        let titlearray = [];

        let problem = this.props.PMH.problem;

        let item = 0;
        while(problem[item])
        {
            var title = this.props.PMH.problem[item].CommonName?this.props.PMH.problem[item].CommonName:this.props.PMH.problem[item].ShortDesc;
            if((problem[item].Id + "").split("meds_").length > 1)
            {
                problem.splice(item,1);
            }
            else
            {
                titlearray.push(title);
                item ++;
            }
        }

        for(item in meds)
        {
            if(titlearray.indexOf(meds[item].diag) == -1)
            {
                titlearray.push(meds[item].diag);

                problem.push({CommonName:meds[item].diag,Id:"meds_" + meds[item].diag});
            }
        }

        this.props.save('PMH',{problem:problem},3);
    }

    undo = () => {
        let data = this.props.meds;
        for(let item in data)
        {
            data[item] = false;
        }

        this.props.save('MEDS',data,5);
    }
    render()
    {
        const StyledPopover = styled(Popover)`
                min-width: 800px;
        `
        Moment.locale('en')
        momentLocalizer()
        simpleNumberLocalizer();   
        return (
            <Card title="Medications" tag={SearchComponent} search={this.search} refillall={this.refillall} freetext={this.editable} add={this.addmeds} undo={this.undo}>
                {
                    !this.props.meds.freetextenable && this.props.meds.map((row,index)=>{
                        return (
                            <Col className="default_container main_content">
                                <Row>
                                    <Col className="title">Name</Col>                                    
                                    <Col>SIG</Col>
                                    <Col>DISP</Col>
                                    <Col>Phamacies</Col>
                                    <Col>Prescriber</Col>
                                    <Col>Comments</Col>
                                    <Col style={{maxWidth:"20%"}}></Col>
                                </Row>
                                <Row style={{marginTop:10}}>
                                    <Col className="title">
                                        <Row>
                                            <Col>
                                                {row.name} {row.dosage}
                                                <FontAwesomeIcon icon={faCaretDown} style={{color:"#3a7ce7"}} id={"diagnose" + index} onClick={()=>this.selectdiagnose(index)}></FontAwesomeIcon>
                                            </Col>
                                        </Row>
                                        {
                                            row.diag && (
                                                <Row style={{marginTop:5}}>
                                                    <Col>IND : {row.diag}</Col>
                                                </Row>
                                            )
                                        }
                                    </Col>
                                    <Col>
                                        {row.sig} {row.duration?"for " + row.duration + ' days':""}
                                    </Col>
                                    <Col>
                                        {row.dispense?row.dispense:this.getdefaultdispense(row)} with {row.refills?row.refills:0} refills
                                    </Col>
                                    <Col><input className="form-control" placeholder="Phamacies" defaultValue={row.prescriber} onChange={(e)=>this.changeinput(e.target.value)} onBlur={()=>this.setvalue("phamacies",index,this.editchange)}></input></Col>
                                    <Col><input className="form-control" placeholder="Prescriber" defaultValue={row.prescriber} onChange={(e)=>this.changeinput(e.target.value)} onBlur={()=>this.setvalue("prescriber",index,this.editchange)}></input></Col>
                                    <Col><input className="form-control" placeholder="Comments" defaultValue={row.comments} onChange={(e)=>this.changeinput(e.target.value)} onBlur={()=>this.setvalue("comments",index,this.editchange)}></input></Col>
                                    <Col  style={{maxWidth:"20%"}}>
                                        <Row>
                                            <Col lg={5}>
                                                <Row>
                                                    <Col lg={12}>
                                                        {
                                                            row.stop?"P":row.dc?"DC":row.refill?"RF":""
                                                        }
                                                    </Col>
                                                    <Col lg={12}>
                                                        {
                                                            (row.stop || row.dc || row.refill) &&  row.date
                                                        }
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col>
                                                <Row>
                                                    <Button color={row.refill?"success":"default"} onClick={()=>this.setitem("refill",index)}>RF</Button>
                                                    <Button color="default" style={{margin:"auto"}} id={"play" + index} onClick={()=>this.run(index)}>+ / -</Button>
                                                    <Button color={row.dc?"secondary":"default"} onClick={()=>this.setitem("dc",index)}>DC</Button>
                                                </Row>
                                                {/* <Row style={{marginTop:10}}>
                                                    <Button color={row.stop?"warning":"default"} style={{margin:"auto"}} onClick={()=>this.setitem("stop",index)}><FontAwesomeIcon icon={faPause}></FontAwesomeIcon></Button>
                                                </Row> */}
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        )
                    })
                }
               
                <Col  className="default_container main_content" dangerouslySetInnerHTML={{__html:this.props.freetextdata}}></Col>                
                {/* {
                    this.props.meds[this.state.selectedid] && (
                        <Popover target={"play" + this.state.selectedid} isOpen={this.state.playmodule} placement="bottom">
                            <PopoverBody>
                                <Row>
                                    <Col><input className="form-control" defaultValue={this.state.selectedmeds.value} onChange={(e)=>this.changeinput(e.target.value)} onBlur={()=>this.saverun("value",this.editchange)}></input></Col>
                                </Row>
                                <Row style={{marginTop:20}}>
                                    <Col>
                                        <Button color={this.state.selectedmeds.day == 'PO'?"primary":"default"} style={{marginRight:10,marginTop:10}} onClick={()=>this.saverun('day',"PO")}>PO</Button>
                                        <Button color={this.state.selectedmeds.day == 'IV'?"primary":"default"} style={{marginRight:10,marginTop:10}} onClick={()=>this.saverun('day',"IV")}>IV</Button>
                                        <Button color={this.state.selectedmeds.day == 'SL'?"primary":"default"} style={{marginRight:10,marginTop:10}} onClick={()=>this.saverun('day',"SL")}>SL</Button>
                                        <Button color={this.state.selectedmeds.day == 'IM'?"primary":"default"} style={{marginRight:10,marginTop:10}} onClick={()=>this.saverun('day',"IM")}>IM</Button>
                                        <Button color={this.state.selectedmeds.day == 'SC'?"primary":"default"} style={{marginTop:10}} onClick={()=>this.saverun('day',"SC")}>SC</Button>
                                    </Col>
                                </Row>
                                <Row style={{marginTop:20}}>
                                    <Col>
                                        <Button color={this.state.selectedmeds.type == 'QD'?"primary":"default"} style={{marginRight:10,marginTop:10}} onClick={()=>this.saverun('type',"QD")}>QD</Button>
                                        <Button color={this.state.selectedmeds.type == 'BID'?"primary":"default"} style={{marginRight:10,marginTop:10}} onClick={()=>this.saverun('type',"BID")}>BID</Button>
                                        <Button color={this.state.selectedmeds.type == 'TID'?"primary":"default"} style={{marginRight:10,marginTop:10}} onClick={()=>this.saverun('type',"TID")}>TID</Button>
                                        <Button color={this.state.selectedmeds.type == 'QID'?"primary":"default"} style={{marginRight:10,marginTop:10}} onClick={()=>this.saverun('type',"QID")}>QID</Button>
                                        <Button color={this.state.selectedmeds.type == 'QOD'?"primary":"default"} style={{marginTop:10}} onClick={()=>this.saverun('type',"QOD")}>QOD</Button>
                                    </Col>
                                </Row>
                                <Row style={{marginTop:20}}>
                                    <div style={{margin:"auto"}}>
                                        <Button color="default" onClick={this.runsave}>OK</Button>
                                        <Button color="default" style={{marginLeft:10}} onClick={()=>this.setState({playmodule:false})}>Cancel</Button>
                                    </div>
                                </Row>
                            </PopoverBody>
                        </Popover>
                    )
                } */}
                
                <StyledPopover target="searchcontainer" isOpen={this.state.module} placement="bottom">

                    <PopoverContent>
                        <PopoverHeader>
                            <Row>
                                <Col>Select The Drug from list</Col>
                                <div><FontAwesomeIcon icon={faTimesCircle} onClick={this.toggle} style={{cursor:"pointer"}}></FontAwesomeIcon></div>
                            </Row>
                        </PopoverHeader>
                        <PopoverBody>
                            <Row className="searchcontent">
                                <Col>
                                    {
                                        this.state.drugs.map((row,index)=>{
                                            return (
                                                <Row className="searchitem">
                                                    <Col lg={12}>
                                                        <Row>
                                                            <Col lg={4}>
                                                                <Row>
                                                                    <Col><input className="form-control" defaultValue={row.value?row.value:1} onChange={(e)=>this.changeinput(e.target.value)} onBlur={()=>this.changeslider(this.editchange,index)}></input></Col>
                                                                    <Col>{row.dosage}</Col>
                                                                </Row>
                                                            </Col>
                                                            <Col onClick={()=>this.add(row)} className="title">
                                                                {row.name}
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    <Col lg={12} style={{marginTop:10}}>
                                                        <Row>
                                                            <Col>
                                                                <div className="slider custom-labels">
                                                                    <Slider
                                                                        min={0.5}
                                                                        max={3}
                                                                        step={0.5}
                                                                        handleLabel={row.value?row.value:1}
                                                                        value={row.value?row.value:1}
                                                                        onChange={(value)=>this.changeslider(value,index)}
                                                                    ></Slider>
                                                                </div>
                                                            </Col>
                                                            <Col>
                                                                <ButtonGroup>
                                                                    <Button color={row.day == 'PO'?"primary":"default"} onClick={()=>this.saveitem("day","PO",index)}>PO</Button>
                                                                    <Button color={row.day=="IV"?"primary":"default"}  onClick={()=>this.saveitem("day","IV",index)}>IV</Button>
                                                                    <Button color={row.day == "SL"?"primary":"default"} onClick={()=>this.saveitem("day","SL",index)}>SL</Button>
                                                                    <Button color={row.day == 'IM'?"primary":"default"} onClick={()=>this.saveitem("day","IM",index)}>IM</Button>
                                                                    <Button color={row.day == 'SC'?"primary":"default"} onClick={()=>this.saveitem("day","SC",index)}>SC</Button>
                                                                </ButtonGroup>
                                                            </Col>
                                                            <Col>
                                                                <ButtonGroup>
                                                                    <Button color={row.type=="QD"?"primary":"default"} onClick={()=>this.saveitem("type","QD",index)}>QD</Button>
                                                                    <Button color={row.type == 'BID'?"primary":"default"} onClick={()=>this.saveitem("type","BID",index)}>BID</Button>
                                                                    <Button color={row.type == 'QOD'?"primary":"default"} onClick={()=>this.saveitem("type","QOD",index)}>QOD</Button>
                                                                    <Button color={row.type == 'TID'?"primary":"default"} onClick={()=>this.saveitem("type","TID",index)}>TID</Button>
                                                                </ButtonGroup>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            )
                                        })
                                    }
                                </Col>
                            </Row>
                        </PopoverBody>
                    </PopoverContent>
                </StyledPopover>
                <Modal isOpen={this.state.freetext} size="lg">
                    <ModalHeader toggle={this.editable}>Add Free Text</ModalHeader>
                    <ModalBody>
                        <Editor 
                            onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                            editorStyle={{minHeight:300}}
                            >
                        </Editor>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.editable}>Cancel</Button>
                        <Button color="success" style={{marginLeft:10}} onClick={this.addfreetext}>Save</Button>
                    </ModalFooter>
                </Modal>
                
                <Modal  className="meds_add" isOpen={this.state.medsdialog} size="lg">
                    <ModalHeader toggle={this.addmeds}>Add Medication</ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col lg={4}>
                                <MedsList select = {this.selectmeds}/>
                            </Col>
                            <Col lg={8}>
                                <Form>
                                    <FormGroup row>
                                        <Label lg={3}>Name</Label>
                                        <Col lg={9}>
                                            <input className="form-control" defaultValue={this.selectedmeds.name} onChange={(e)=>this.handleChange("name",e.target.value)}></input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={3}>Indication</Label>
                                        <Col lg={9}>
                                            <InputGroup>
                                                <select className="form-control" defaultValue={this.selectedmeds.indication} onChange={(e)=>this.handleChange("diag",e.target.value)}>
                                                {
                                                    this.state.editdetail.diag && this.state.editdetail.diag.map((row,index)=>{
                                                        return (
                                                            <option key={index} value={row.optionText}>{row.optionText}</option>
                                                        )
                                                    })
                                                }
                                                </select>
                                                <InputGroupAddon addonType="append">
                                                    <Button color="default" onClick={()=>this.addindication('diag')}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                                                </InputGroupAddon>                                            
                                            </InputGroup>
                                            
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={3}>Dosage</Label>
                                        <Col lg={9}>
                                            <InputGroup>
                                                <select className="form-control" onChange={(e)=>this.handleChange("dosage",e.target.value)} defaultValue={this.selectedmeds.dosage}>
                                                    {
                                                        this.state.editdetail.dosage.map((row,index)=>{
                                                            return (<option key={index} value={row.optionText}>{row.optionText}</option>)
                                                        })
                                                    }
                                                </select>
                                                <InputGroupAddon addonType="append">
                                                    <Button color="default" onClick={()=>this.addindication('dosage')}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                                                </InputGroupAddon>   
                                            </InputGroup>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={3}>Sig</Label>
                                        <Col lg={9}>
                                            <InputGroup>
                                                <select className="form-control" onChange={(e)=>this.handleChange('sig',e.target.value)} defaultValue={this.selectedmeds.sig}>
                                                    {
                                                        this.state.editdetail.sig.map((row,index)=>{
                                                            return (<option key={index} value={row.optionText}>{row.optionText}</option>)
                                                        })
                                                    }
                                                </select>
                                                <InputGroupAddon addonType="append">
                                                    <Button color="default" onClick={()=>this.addindication('sig')}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                                                </InputGroupAddon>   
                                            </InputGroup>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col>
                                            <Row>
                                                <Label lg={6}>Duration</Label>
                                                <Col lg={6}>
                                                    <NumberPicker defaultValue={this.selectedmeds.duration?this.selectedmeds.duration:0} onChange={(value)=>this.handleChange("duration",value)} min={0}></NumberPicker>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <Label lg={6}>Start</Label>
                                                <Col lg={6}>
                                                    <DatePicker defaultValue={this.selectedmeds.startdate?new Date(this.selectedmeds.startdate):new Date()} onChange={(date)=>this.handleChange("startdate",this.getstringdate(date))}></DatePicker>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col>
                                            <Row>
                                                <Label lg={6}>Dispense</Label>
                                                <Col lg={6}>
                                                    <NumberPicker defaultValue={this.selectedmeds.dispense?this.selectedmeds.dispense:0} onChange={(value)=>this.handleChange("dispense",value)} min={0}></NumberPicker>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <Label lg={6}>Refills</Label>
                                                <Col lg={6}>
                                                    <NumberPicker defaultValue={this.selectedmeds.refills?this.selectedmeds.refills:0} onChange={(value)=>this.handleChange("refills",value)} min={0}></NumberPicker>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={3}>Packages</Label>
                                        <Col lg={9}>
                                            <InputGroup>
                                                <select className="form-control" defaultValue={this.selectedmeds.package} onChange={(e)=>this.handleChange('package',e.target.value)}>
                                                    {
                                                        this.state.editdetail.package.map((row,index)=>{
                                                            return (<option key={index} value={row.optionText}>{row.optionText}</option>)
                                                        })
                                                    }
                                                </select>
                                                <InputGroupAddon addonType="append">
                                                    <Button color="default" onClick={()=>this.addindication('package')}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                                                </InputGroupAddon>   
                                            </InputGroup>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={3}>Phamacies</Label>
                                        <Col lg={9}>
                                            <input className="form-control" defaultValue={this.selectedmeds.phamacies} onChange={(e)=>this.handleChange('phamacies',e.target.value)}></input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={3}>Prescriber</Label>
                                        <Col lg={9}>
                                            <input className="form-control" defaultValue={this.selectedmeds.prescriber} onChange={(e)=>this.handleChange('prescriber',e.target.value)}></input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={3}>Special Instruction</Label>
                                        <Col lg={9}>
                                            <textarea className="form-control" defaultValue={this.selectedmeds.comments} onChange={(e)=>this.handleChange('comments',e.target.value)}></textarea>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.addmeds}>Cancel</Button>
                        {
                            this.state.selectedid > -1 && (
                                <Button color="primary" onClick={()=>this.deleteselected()}>Delete</Button>
                            )
                        }
                        <Button color="success" style={{marginLeft:10}} onClick={this.savemeds}>Add</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.addinfo}>
                    <ModalHeader toggle={this.showindication}>Add {this.name}</ModalHeader>
                    <ModalBody className="meds_add_body">
                        <Row>
                            <Col>
                               <textarea className="form-control" rows={5} onChange={(e)=>this.handleChangeforedit(e.target.value)}></textarea>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary">Cancel</Button>
                        <Button color="success" style={{marginLeft:10}} onClick={this.saveindication}>Save</Button>
                    </ModalFooter>
                </Modal>
                {
                    this.selecteddiagnose && (
                        <Popover target={this.selecteddiagnose} isOpen={this.state.opendiagnosis} placement="bottom">
                            <PopoverBody>
                                <ul className="diagnost_list">
                                    {
                                        this.openeddiagnosis.map((row,index)=>{
                                            return (
                                                <li onClick={()=>this.adddiagnose(row.optionText)}>{row.optionText}</li>
                                            )
                                        })
                                    }
                                </ul>
                            </PopoverBody>
                        </Popover>
                    )
                }
            </Card>
        )
    }
}

export default MEDS;