import React from 'react';
import Card from '../Card';
import {Assessments} from '../chartboxcomponent/action';
import {Row,Col,Button,Modal,ModalBody,ModalHeader,ModalFooter} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowUp,faArrowLeft,faPlus,faArrowDown,faArrowRight,faTimes,faCommentAlt} from '@fortawesome/free-solid-svg-icons';
import {RadioGroup,Radio} from 'react-ui-icheck';
import {Editor} from 'react-draft-wysiwyg';
import {convertToRaw,EditorState,ContentState,convertFromHTML} from 'draft-js';
import draftToHTML from 'draftjs-to-html';

class Assessment extends React.Component
{
    editdata = "";
    selectedindex = 0;
    selecteditem = "primary";
    freedata = "";
    constructor(props)
    {
        super(props);
        this.state  = {
            description:false,
            freetext:false,
            textstate:EditorState.createEmpty()
        }
    }

    componentWillReceiveProps(props)
    {
        let state = EditorState.createEmpty();
        if(props.freetextdata && props.freetextdata != '<p></p>')
        {
            state = EditorState.createWithContent(
                ContentState.createFromBlockArray(
                convertFromHTML(this.props.initdata)
                )
            );
        }

        this.setState({textstate:state});
        
    }
    //delete item
    delete = (item,index) => {
        let assessments = this.props.data;
        assessments[item].splice(index,1);
        this.props.save("Assessments",assessments,12);
    }

    //add item to select
    addselect = (item,index) => {
        let assessments = this.props.data;
        assessments[item][index].selected = !assessments[item][index].selected;
        this.props.save('Assessments',assessments,12);
    }

    //change type in selected
    changetype = (item,index,value) => {
        console.log("value",value);
        let assessments = this.props.data;
        assessments[item][index].type = value;
        this.props.save('Assessments',assessments,12);
    }

    //show description dialog
    showdescription = (item,index) => {
        this.editdata = this.props.data[item][index].description;
        this.selectedindex = index;
        this.selecteditem = item;

        this.setState({
            description:true
        })
    }
    
    //close description dialog
    close = () => {
        this.setState({
            description:false
        })
    }

    
    //handle edit text

    handleChange = (value) => {
        this.editdata = value;
    }

    //save description
    savedescription = () => {
        let assessments = this.props.data;
        assessments[this.selecteditem][this.selectedindex].description = this.editdata;
        this.props.save("Assessments",assessments,12);
        this.close();
    }

    //change display order
    changeorderup = (item) => {
        let assessments = this.props.data;
        var enable = false;
        for(let index = 0;index < assessments[item].length;index++)
        {
            if(assessments[item][index].selected)
            {
                if(index == 0)
                {
                    break;
                }
                else
                {
                    console.log(index);
                    enable = true;
                    assessments[item] = this.move(assessments[item],index - 1,index);
                }
            }
        }
        
        if(enable)
        {
            this.props.save("Assessments",assessments,12);
        }
        
    }

    changeorderdown = (item) => {
        let assessments = this.props.data;
        for(let index = assessments[item].length - 1;index >= 0;index--)
        {
            if(assessments[item][index].selected)
            {
                if(index == assessments[item].length - 1)
                {
                    break;
                }
                else
                {
                    assessments[item] = this.move(assessments[item],index,index + 1);
                }
            }
        }

        this.props.save("Assessments",assessments,12);
    }

    //change order in array
    move = (arr,old_index,new_index) => {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; 
    }

    convert =(destination) => {
        var data = this.props.data;
        var currentitem = 'primary';
        if(destination == 'primary')
        {
            currentitem = 'secondary';
        }

        console.log(currentitem);
        
        let item = 0;
        while(data[currentitem][item])
        {
            if(data[currentitem][item].selected)
            {
                data[destination].push(data[currentitem][item])
                data[currentitem].splice(item,1);
            }
            else
            {
                item++;
            }
        }

        this.props.save('Assessments',data,12);
    }

    onEditorStateChange  = (state) => {
        this.freedata = draftToHTML(convertToRaw(state.getCurrentContent()));
    }

    addfreetext = () => {
        this.props.save('Assessments',{freetextdata:this.freedata,freetextenable:true},12);
        this.setState({
            freetext:false
        })
    }

    edit = () => {
        this.setState({
            freetext:true
        })
    }

    render()
    {
        let self = this;
        return (
            <Card title="Assessment(s)" tag={Assessments} addfreetext={this.edit} showmacro={()=>this.props.showmacro(12)} undo={()=>this.props.save("Assessments",{freetextenable:false,freetextdata:"",primary:[],secondary:[]},12)}>
                {
                    !this.props.data.freetextenable && (                    
                        <Col className="assessment">
                            <Row className="assessment_header">
                                <Col>
                                    <Row>
                                        <Col className="title"><span>Primary Diagnosis(es)</span> <span className="icons" onClick={()=>this.props.showproblem('Assessments',"primary",12)}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></span></Col>
                                        <div className="actions">
                                            <span className="icons" onClick={()=>this.changeorderdown("primary")}><FontAwesomeIcon icon={faArrowDown} style={{marginLeft:20}}></FontAwesomeIcon></span>
                                            <span className="icons" onClick={()=>this.changeorderup('primary')}><FontAwesomeIcon icon={faArrowUp} style={{marginLeft:20}}></FontAwesomeIcon></span>
                                        </div>
                                    </Row>
                                </Col>
                                <Col>
                                    <Row>
                                        <Col className="title"><span>Secondary Diagnosis(es)</span> <span className="icons" onClick={()=>this.props.showproblem('Assessments',"secondary",12)}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></span></Col>
                                        <div className="actions">
                                            <span className="icons" onClick={()=>this.changeorderdown("secondary")}><FontAwesomeIcon icon={faArrowDown} style={{marginLeft:20}}></FontAwesomeIcon></span>
                                            <span className="icons" onClick={()=>this.changeorderdown("secondary")}><FontAwesomeIcon icon={faArrowUp} style={{marginLeft:20}}></FontAwesomeIcon></span>
                                        </div>
                                    </Row>
                                </Col>
                            </Row>
                            <Row className="assessment_body">
                                <Col>
                                    <Row>
                                        <Col>

                                        </Col>
                                        <Col lg={6} md={6} sm={12} xs={12}>
                                            <Row style={{alignItems:"flex-end"}}>
                                                <Col style={{borderColor:'#e2e2e2',borderWidth:1,borderStyle:'solid',textAlign:"center",maxWidth:'40%',padding:0}}>
                                                    <span>New</span>
                                                </Col>
                                                <Col  style={{borderColor:'#e2e2e2',borderWidth:1,borderStyle:'solid',textAlign:"center",maxWidth:'60%',padding:0}}>
                                                    <span>Est</span>
                                                </Col>                            
                                            </Row>
                                            <Row>
                                                <Col className="form-check" style={{padding:0,textAlign:'center'}}>+w/u</Col>
                                                <Col className="form-check"  style={{padding:0,textAlign:'center'}}>-w/u</Col>
                                                <Col className="form-check" style={{padding:0,textAlign:'center'}}>exec</Col>
                                                <Col className="form-check"  style={{padding:0,textAlign:'center'}}>stbl</Col>
                                                <Col className="form-check"  style={{padding:0,textAlign:'center'}}>min</Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    {
                                        this.props.data.primary.map((row,index)=>{
                                            return (
                                                <Row clasName="assessmentitem" key={"primary_" + index}>
                                                    <Col className={row.selected?"itemtitle selected":"itemtitle"}>
                                                        <span><FontAwesomeIcon icon={faTimes} style={{marginLeft:15,color:"red",marginRight:5,cursor:"pointer"}} onClick={()=>self.delete("primary",index)}></FontAwesomeIcon></span>
                                                        <span  style={{cursor:"pointer"}} onClick={()=>self.addselect("primary",index)}>{row.CommonName?row.CommonName:row.ShortDesc} ({row.Code})</span>
                                                        <span onClick={()=>self.showdescription("primary",index)}><FontAwesomeIcon icon={faCommentAlt} style={{marginLeft:5,cursor:"pointer",color:"blue"}}></FontAwesomeIcon></span>
                                                    </Col>
                                                    <Col lg={6} md={6} sm={12} xs={12}>
                                                        <RadioGroup
                                                            name="type"
                                                            radioWrapClassName="col form-check form-check-inline"
                                                            radioWrapTag="div"
                                                            value={row.type}
                                                            className="row"
                                                            onChange={(event,value)=>self.changetype("primary",index,value)}
                                                        >   
                                                            <Radio radioClassName="iradio_flat-blue" value="w+" labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                            <Radio radioClassName="iradio_flat-blue" value="w-"  labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                            <Radio radioClassName="iradio_flat-blue" value="exec" labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                            <Radio radioClassName="iradio_flat-blue" value="stbl" labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                            <Radio radioClassName="iradio_flat-blue" value="min" labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                            )
                                        })
                                    }
                                </Col>
                                <div style={{width:80,margin:"0px"}}>
                                    <Row style={{alignItems:"center"}}>
                                        <Col lg={12} style={{textAlign:"center"}}><Button color="default" onClick={()=>this.convert('primary')}><FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon></Button></Col>
                                        <Col lg={12} style={{marginTop:10,textAlign:"center"}}><Button color="default" onClick={()=>this.convert('secondary')}><FontAwesomeIcon icon={faArrowRight}></FontAwesomeIcon></Button></Col>
                                    </Row>
                                </div>
                                <Col>
                                    <Row>
                                        <Col>

                                        </Col>
                                        <Col lg={6} md={6} sm={12} xs={12}>
                                            <Row style={{alignItems:"flex-end"}}>
                                                <Col style={{borderColor:'#e2e2e2',borderWidth:1,borderStyle:'solid',textAlign:"center",maxWidth:'40%',padding:0}}>
                                                    <span>New</span>
                                                </Col>
                                                <Col style={{borderColor:'#e2e2e2',borderWidth:1,borderStyle:'solid',textAlign:"center",maxWidth:'60%',padding:0}}>
                                                    <span>Est</span>
                                                </Col>                            
                                            </Row>
                                            <Row>
                                                <Col className="form-check" style={{padding:0,textAlign:'center'}}>+w/u</Col>
                                                <Col className="form-check" style={{padding:0,textAlign:'center'}}>-w/u</Col>
                                                <Col className="form-check" style={{padding:0,textAlign:'center'}}>exec</Col>
                                                <Col className="form-check" style={{padding:0,textAlign:'center'}}>stbl</Col>
                                                <Col className="form-check" style={{padding:0,textAlign:'center'}}>min</Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    {
                                        this.props.data.secondary.map((row,index)=>{
                                            return (
                                                <Row clasName="assessmentitem" key={"secondary" + index}>
                                                    <Col className={row.selected?"selected":""}>
                                                        <span><FontAwesomeIcon icon={faTimes} style={{marginLeft:15,color:"red",marginRight:5,cursor:"pointer"}} onClick={()=>this.delete("secondary",index)}></FontAwesomeIcon></span>
                                                        <span style={{cursor:'pointer'}} onClick={()=>self.addselect("secondary",index)}>{row.CommonName?row.CommonName:row.ShortDesc} ({row.Code})</span>
                                                        <span onClick={()=>self.showdescription("secondary",index)}><FontAwesomeIcon icon={faCommentAlt} style={{marginLeft:5,cursor:"pointer",color:"blue"}}></FontAwesomeIcon></span>
                                                    </Col>
                                                    <Col lg={6} md={6} sm={12} xs={12}>
                                                        <RadioGroup
                                                            name="type"
                                                            radioWrapClassName="col form-check form-check-inline"
                                                            radioWrapTag="div"
                                                            value={row.type}
                                                            onChange={(event,value)=>self.changetype("secondary",index,value)}
                                                            className="row"
                                                        >   
                                                            <Radio radioClassName="iradio_flat-blue" value="w+" labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                            <Radio radioClassName="iradio_flat-blue" value="w-"  labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                            <Radio radioClassName="iradio_flat-blue" value="exec" labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                            <Radio radioClassName="iradio_flat-blue" value="stbl" labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                            <Radio radioClassName="iradio_flat-blue" value="min" labelTag="div" labelTagClassName="d-inline" label=" "></Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                            )
                                        })
                                    }
                                </Col>
                        </Row>
                    </Col>
                    )}
                {
                    this.props.data.freetextenable && (
                        <Col lg={12} dangerouslySetInnerHTML={{__html:this.props.data.freetextdata?this.props.data.freetextdata:''}}>
                    
                        </Col>
                    )
                }
                <Modal isOpen={this.state.description}>
                    <ModalHeader toggle={this.close}>Description for {this.props.data[this.selecteditem].length > 0?this.props.data[this.selecteditem][this.selectedindex].CommonName?this.props.data[this.selecteditem][this.selectedindex].CommonName:this.props.data[this.selecteditem][this.selectedindex].ShortDesc:""}</ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col>
                                <textarea className="form-control" defaultValue={this.editdata} onChange={(e)=>this.handleChange(e.target.value)} rows={5}></textarea>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.close}>Cancel</Button>
                        <Button color="success" style={{marginLeft:10}} onClick={this.savedescription}>Save</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.freetext} size='lg'>
                    <ModalHeader toggle={()=>this.setState({freetext:false})}>Add Free Text</ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col>
                                <Editor  onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                                        editorStyle={{minHeight:300}}
                                        defaultEditorState={this.state.textstate}></Editor>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={()=>this.setState({freetext:false})}>Cancel</Button>
                        <Button color="success" onClick={this.addfreetext} style={{marginLeft:10}}>Save</Button>
                    </ModalFooter>
                </Modal>
            </Card>
        )
    }
}

export default Assessment;