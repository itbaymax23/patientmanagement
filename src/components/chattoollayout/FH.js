//library
import React from 'react';
import {Modal,ModalHeader,ModalBody,ModalFooter,Row,Col,Form,FormGroup,Input,Label,Button,Table} from 'reactstrap';
import FamilyComponent from 'react-family-tree';
import {IFamilyNode,IFamilyExtNode} from 'relatives-tree';
import {Typeahead} from 'react-bootstrap-typeahead';
import Select from 'react-select';
import {Radio,RadioGroup} from 'react-ui-icheck';

//icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTimes } from '@fortawesome/free-solid-svg-icons';


///api
import * as FamilyAPI from '../../action/Family';
import * as Problem from '../../action/Problem';
import * as User from '../../action/user';

//component
import Card from '../Card';
import {Family} from '../chartboxcomponent/action';
import FamilyNode from './FamilyComponent';
import {DatePicker} from 'react-widgets';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
// import nodes from './sample.json';


class FH extends React.Component
{
    familydata = {};
    problems = [];
    relation = ["Grand Father","Grand Mother","Father","Mother","Sister","Wife","Brother","Daughter","Son"];
    livestatus = ["Alive","Decreased","Unknown"];
    constructor(props)
    {
        super(props);
        this.state = {
            data:[],
            relations:[],
            modal:false,
            updated:false,
            familytree:false,
            patientdata:[],
            addanother:false
        }
    }
    
    showtree = () => {
        this.setState({familytree:!this.state.familytree});
    }

    getfamilydata = (patientid) => {
        let self = this;
        FamilyAPI.getfamilyhistory(patientid).then(function(data){
            self.props.save('FH',{data:data.data},8);
            self.setState({
                data:data.data
            })
        })
    }

    componentWillReceiveProps(props)
    {
        if(props.initdata)
        {
            console.log("familyinit",props.initdata);
            this.getfamilydata(props.patient.Id);
        }
    }

    componentDidMount(){
        let self = this;
         
        Problem.getproblemcommon().then(function(data){
            let items = [];
            for(let item in data.data)
            {
                items.push({label:data.data[item].CommonName?data.data[item].CommonName:data.data[item].ShortDesc,id:data.data[item].Id});
            }

            self.problems = items;
        })

        User.getpatients().then(function(data){
            let items = [];
            for(let item in data.data)
            {
                items.push({label:data.data[item].FirstName + " " + data.data[item].LastName,key:data.data[item].Id,DOB:data.data[item].DateOfBirth})
            }

            self.setState({
                patientdata:items
            });
        })

        this.getfamilydata(this.props.patient.Id);
    }
    

   
    getfamilytree = (treedatas) => {
       
        var patientdata = this.props.patient;
        var treedatainfo ={id:"patient_" + patientdata.Id,name:patientdata.FirstName + " " + patientdata.LastName,relation:"SELF"};

        let treedata = [...treedatas];

        treedata.push(treedatainfo);
        let element_list = [...treedata]


        for(let item in treedata)
        {
            treedata[item].parents = [];
            treedata[item].siblings = [];
            treedata[item].spouses = [];
            treedata[item].children = [];
            for(let item_element in element_list)
            {
                if(item == item_element)
                {
                    continue;
                }

                var relation_current = treedata[item].relation;
                var relation_list = element_list[item_element].relation;

                if(relation_current == 'Grand Father' | relation_current == 'Grand Mother')
                {
                    switch(relation_list)
                    {
                        case 'Father':
                            treedata[item].children.push(this.copydata(element_list[item_element]));
                            break;
                        case 'Grand Mother':
                        case 'Grand Father':
                            treedata[item].spouses.push(this.copydata(element_list[item_element]));
                            break;
                    }
                }
                else if(relation_current == 'Father' || relation_current == 'Mother')
                {
                    switch(relation_list)
                    {   

                        case 'Grand Mother':
                        case 'Grand Father':
                            if(relation_current == 'Father')
                            {
                                treedata[item].parents.push(this.copydata(element_list[item_element]));
                            }
                            break;
                        case 'Mother':
                        case 'Father':
                            treedata[item].spouses.push(this.copydata(element_list[item_element]));
                            break;
                        case 'SELF':
                        case 'Brother':
                        case 'Sister':
                            var itemelement = {};
                            if(relation_list == 'SELF')
                            {
                                itemelement = {id:element_list[item_element].id,name:element_list[item_element].name}
                            }
                            else
                            {
                                itemelement = this.copydata(element_list[item_element]);
                            }

                            treedata[item].children.push(itemelement);
                            break;
                            
                    }
                }
                else if(relation_current == 'Wife')
                {
                    switch(relation_list)
                    {
                        case 'SELF':
                            treedata[item].spouses.push({id:element_list[item_element].id,name:element_list[item_element].name});
                            break;
                        case 'Daughter':
                        case 'Son':
                            treedata[item].children.push(this.copydata(element_list[item_element]));
                            break;
                    }
                }
                else if(relation_current == 'SELF' || relation_current == 'Brother' || relation_current == 'Sister')
                {
                    switch(relation_list)
                    {
                        case "Father":
                        case "Mother":
                            treedata[item].parents.push(this.copydata(element_list[item_element]));
                            break;
                        case 'SELF':
                        case 'Brother':
                        case "Sister":
                            treedata[item].siblings.push(this.copydata(element_list[item_element]));
                            break;
                        case 'Daughter':
                        case 'Son':
                            if(relation_current == 'SELF')
                            {
                                treedata[item].children.push(this.copydata(element_list[item_element]));
                            }
                            
                            break;
                        case 'Wife':
                            if(relation_current == 'SELF')
                            {
                                treedata[item].spouses.push(this.copydata(element_list[item_element]));
                                
                            }
                            break;
                        default:
                            if(relation_current == 'SELF' && this.relation.indexOf(relation_list) == -1)
                            {
                                //treedata[item].spouses.push(this.copydata(element_list[item_element]));
                                break;
                            }
                    }
                }
                else if(relation_current == 'Son' || relation_current == 'Daughter')
                {
                    switch(relation_list)
                    {
                        case 'Son':
                        case 'Daughter':
                            treedata[item].siblings.push(this.copydata(element_list[item_element]));
                            break;
                        case 'SELF':
                            treedata[item].parents.push({id:element_list[item_element].id,name:element_list[item_element].name});
                            break;
                    }
                }
                else
                {
                    // if(relation_list == 'SELF')
                    // {
                    //     treedata[item].spouses.push({id:element_list[item_element].id,name:element_list[item_element].name});
                    // }
                }
            }
        }

        return treedata;
    }

    copydata = (element) => {
        var data = {
            id:element.id,
            patientid:element.patientid?element.patientid:"",
            relation:element.relation?element.relation:"",
            name:element.name?element.name:"",
            livestatus:element.livestatus?element.livestatus:"",
            causeofdeath:element.causeofdeath?element.causeofdeath:"",
            illness:element.illness?element.illness:"",
            note:element.note?element.note:"",
            DOB:element.DOB?element.DOB:""
        };
        
        return data;
    }
    
    save = (level) => {
        let self = this;
        
        this.familydata.patientid = this.props.patient.Id;
        let causedeath = [];
        for(let item in this.familydata.causeofdeath)
        {
            causedeath.push(this.familydata.causeofdeath[item].label);
        }

        let illness = [];
        for(let item in this.familydata.illness)
        {
            illness.push(this.familydata.illness[item].label);
        }

        this.familydata.causeofdeath = causedeath.join(',');
        this.familydata.illness = illness.join(',');
        FamilyAPI.savefamilyhistory(this.familydata).then(function(data){
            let modal = false;
            if(level == 'continue')
            {
                modal = true;
            }
            let state = self.state.data;
            var enable = false;
            for(let item in state)
            {
                if(state[item].id == data.data.id)
                {
                    state[item] = data.data;
                    enable = true;
                }
            }
            
            if(!enable)
            {
                state.push(data.data);
            }
            
            self.familydata = {};
            self.setState({
                data:state,
                modal:modal
            })

            self.props.save("FH",{data:state},8);
        })
    }

    handleChange = (item,value) => {
       this.familydata[item] = value;
    }

    update = (row) => {
        this.familydata = {...row};
        let addanother = false;
        if(this.relation.indexOf(this.familydata.relation) == -1)
        {
            addanother = true;
        }

        if(this.familydata.causeofdeath)
        {
            let array = this.familydata.causeofdeath.split(',');
            this.familydata.causeofdeath = [];
            for(let item in array)
            {
                this.familydata.causeofdeath.push({label:array[item],id:item});
            }
        }

        if(this.familydata.illness)
        {
            let array = this.familydata.illness.split(',');
            this.familydata.illness = [];
            for(let item in array)
            {
                this.familydata.illness.push({label:array[item],id:item});
            }
        }
        
        
        this.setState({
            modal:true,
            addanother:addanother
        })
    }

    delete = (index) => {
        let self = this;
        FamilyAPI.deletefamilyhistory(this.state.data[index].id).then(function(data){
            if(data.data.success)
            {   
                let data = self.state.data;
                data.splice(index,1);
                self.props.save('FH',{data:data},8);
                self.setState({
                    data:data
                })
            }
        })
    }

    deletebyId = (id) => {
        let self = this;
        FamilyAPI.deletefamilyhistory(id).then(function(data){
            if(data.data.success)
            {
                let data = self.state.data;
                for(let item in data)
                {
                    if(data[item].id == id)
                    {
                        data.splice(item,1);
                        break;
                    }
                }

                self.props.save('FH',{data:data},8);
                self.setState({
                    data:data
                })
            }
        })
    }

    showmodal = () => {
        this.familydata = {};
        this.setState({
            modal:!this.state.modal,
            addanother:false
        })
    }
    
    selectchange = (item,value)=>{
        this.familydata[item] = value;
        if(value == 'Alive')
        {
            this.familydata.causeofdeath = [];
        }

        this.setState({
            updated:!this.state.updated
        })
        
    }

    handleSelect = (item,value) => {
        this.familydata[item] = value;
    }

    render()
    {
       let self = this;

       let tree = self.getfamilytree([...this.state.data]);
       Moment.locale('en')
       momentLocalizer();
       console.log("tree",tree);
        return (
            <Card title="Family History" tag={Family} add={this.showmodal} showtype={this.state.familytree?'tree':'line'} showtree={this.showtree}>
                <Row>
                    {!this.props.data.freetextenable && (<Col>
                        {!this.state.familytree && (<Table responsive hover bordered>
                            <thead>
                                <tr>
                                    <th>Relation</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>COD</th>
                                    <th>Other Illness</th>
                                    <th>Notes</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.data.map((row,index)=>{
                                        return (
                                            <tr key={index}>
                                                <td>{row.relation}</td>
                                                <td>{row.name}</td>
                                                <td>{row.livestatus}</td>
                                                <td>{row.causeofdeath}</td>
                                                <td>{row.illness}</td>
                                                <td>{row.note}</td>
                                                <td>
                                                    <FontAwesomeIcon icon={faPencilAlt} style={{cursor:"pointer"}} onClick={()=>this.update(row)}></FontAwesomeIcon>
                                                    <FontAwesomeIcon icon={faTimes} style={{color:"red",marginLeft:10,cursor:"pointer"}} onClick={()=>this.delete(index)}></FontAwesomeIcon>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </Table>)}
                        {
                            this.state.familytree && (
                                <div className="family">
                                    <FamilyComponent
                                        nodes={tree}
                                        rootId={"patient_" + this.props.patient.Id}
                                        width={200}
                                        height={120}
                                        canvasClassName="tree"
                                        renderNode={(node:IFamilyExtNode)=>(
                                            <FamilyNode 
                                            key={node.id}
                                            node={node} 
                                            style={{width:200,height:120,transform: `translate(${node.left * 100}px, ${node.top * 60}px)`}}
                                            Select={()=>self.update(node)}
                                            delete={()=>self.deletebyId(node.id)}
                                            get_relationname={self.get_relationname}
                                            >
                                            </FamilyNode>
                                        )}
                                        >
                                    </FamilyComponent>
                                </div>
                            )
                        }
                        
                    </Col>)}
                    {
                        this.props.data.freetextenable && (
                            <Col dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>
                        )
                    }
                </Row>
                <Modal isOpen={this.state.modal} size="lg">
                    <ModalHeader toggle={this.showmodal}>Edit Family Information</ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col lg={12}>
                                <Form>
                                    <FormGroup row>
                                        <Label lg={4}>Name</Label>
                                        <Col>
                                            <Typeahead 
                                            options={this.state.patientdata}
                                            labelKey="label"
                                            placeholder="Type in Name"
                                            id="key"
                                            onInputChange={(value)=>this.handleChange("name",value)}
                                            onChange = {(e)=>{if(e.length > 0){this.handleChange("name",e[0].label); console.log(e[0].DOB);this.familydata.DOB = e[0].DOB; this.setState({updated:!this.state.updated})}}}
                                            defaultInputValue={this.familydata.name}
                                            ></Typeahead>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={4}>Relation</Label>
                                        <Col>
                                        {
                                            this.state.addanother && (
                                                <input className="form-control" defaultValue={this.familydata.relation} onChange={(e)=>this.handleChange("relation",e.target.value)}></input>
                                            )
                                        }
                                        {
                                            !this.state.addanother && (
                                                <RadioGroup
                                                    className="row"
                                                    name="relation"
                                                    radioWrapClassName="form-check form-check-inline"
                                                    radioWrapTag="div"
                                                    value={this.familydata.relation}
                                                    onChange={(e)=>{this.handleChange("relation",e.target.value)}}
                                                >
                                                    {
                                                        this.relation.map((relation,index)=>{
                                                            return (
                                                                <Radio
                                                                    key={index}
                                                                    label={relation}
                                                                    value={relation}
                                                                    radioClassName="iradio_square"
                                                                    ></Radio>
                                                            )
                                                        })
                                                    }
                                                </RadioGroup>
                                            )
                                        }
                                           
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={4}>Status</Label>
                                        <Col>
                                        <RadioGroup
                                            className="row"
                                            name="livestatus"
                                            radioWrapClassName="form-check form-check-inline"
                                            radioWrapTag="div"
                                            value={this.familydata.livestatus}
                                            onChange={(e)=>this.selectchange("livestatus",e.target.value)}
                                           >
                                               {
                                                   this.livestatus.map((status,index)=>{
                                                       return (
                                                        <Radio
                                                            key={index}
                                                            label={status}
                                                            value={status}
                                                            radioClassName="iradio_square"
                                                            ></Radio>
                                                       )
                                                   })
                                               }
                                           </RadioGroup>
                                        </Col>
                                    </FormGroup>
                                    {
                                        this.familydata.livestatus == "Decreased" && (
                                            <FormGroup row>
                                                <Label lg={4}>COD</Label>
                                                <Col>
                                                    <Typeahead
                                                        id="id"
                                                        options={this.problems}
                                                        key="label"
                                                        multiple
                                                        onChange={(selected)=>this.handleSelect("causeofdeath",selected)}
                                                        selected={this.familydata.causeofdeath?this.familydata.causedeath:[]}
                                                        ></Typeahead>
                                                </Col>
                                            </FormGroup>
                                        )
                                    }
                                    <FormGroup row>
                                        <Label lg={4}>Other illness</Label>
                                        <Col>
                                            <Typeahead
                                            id="id"
                                            options={this.problems}
                                            key="label"
                                            multiple
                                            onChange={(selected)=>this.handleSelect("illness",selected)}
                                            defaultSelected={this.familydata.illness?this.familydata.illness:[]}
                                            ></Typeahead>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={4}>DOB</Label>
                                        <Col lg={4}>
                                            <DatePicker
                                             value={this.familydata.DOB?new Date(this.familydata.DOB):null}
                                             onChange={(date)=>{this.familydata.DOB = Moment(new Date(date)).format('YYYY-MM-DD')}}
                                             ></DatePicker>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label lg={4}>Notes</Label>
                                        <Col>
                                            <textarea className="form-control" defaultValue={this.familydata.note} onChange={(e)=>this.handleChange("note",e.target.value)}></textarea>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.showmodal}>Cancel</Button>
                        {
                            !this.familydata.id && (
                                <Button color={this.state.addanother?"success":"default"} onClick={() => {this.setState({addanother:!this.state.addanother})}} style={{marginLeft:15}}>Add Another</Button>
                            )
                        }
                        <Button color="primary" onClick={this.save}>Add</Button>
                    </ModalFooter>
                </Modal>
            </Card>
        )
    }
}

export default FH;