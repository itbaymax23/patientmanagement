import React from 'react';
import * as Macro from '../../action/Macro';
import {Modal,ModalHeader,ModalBody,Row,Col} from 'reactstrap';
class TemplateEditList extends React.Component
{
    sectionid = false;
    constructor(props)
    {
        super(props);
        this.state = {
            data:[]
        }
    }

    searchinput = (value) => {
        let self = this;
        Macro.getproblemlist(value).then(function(data){
            self.setState({
                data:data.data
            })
        })
    }

    componentWillReceiveProps(props)
    {
        if(this.sectionid != props.sectionid)
        {
            this.sectionid = props.sectionid;
            if(this.sectionid == '1')
            {
                let self = this;
                Macro.getproblemlist('').then(function(data){
                    self.setState({
                        data:data.data
                    })
                })
            }
        }
    }

    render()
    {
        return (
            <Modal isOpen={this.props.open}>
                <ModalHeader toggle={this.props.close}>Add {this.props.title}</ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <input className="form-control" onChange={(e)=>this.searchinput(e.target.value)}></input>
                        </Col>
                    </Row>
                    <ul className="dialog">
                        {
                            this.state.data.map((row,index)=>{
                                return (
                                    <li className="dialogitem">{row.CommonName?row.CommonName:row.ShortDesc}</li>
                                )
                            })
                        }
                    </ul>
                </ModalBody>
            </Modal>
        )
    }
}

export default TemplateEditList;