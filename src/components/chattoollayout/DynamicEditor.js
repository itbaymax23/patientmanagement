import React from 'react';
import DynamicAdd from './DynamicAdd';
import DynamicContent from './DynamicContent';
import $ from 'jquery'; 
import ContentEditable from 'react-contenteditable'
import {Row,Col,ModalFooter,Modal,ModalHeader,ModalBody} from 'reactstrap';

class DynamicEditor extends React.Component
{  
    selected = -1;
    caption = "";
    updated = false;
    constructor(props)
    {
        super(props);
        this.state = {
            dynamicelement:false
        }
    }

    componentDidMount()
    {
        let self = this;
        $(document).on('click','.dynamicelementedit',function(){
            self.selected = $(this).attr('key');
            self.selectedid = $(this).attr('id');
            self.selectedcomponent = $(this);
            self.setState({
                dynamicelement:true
            })
       })
    }

    rendertextdata = (value) => 
    {
        var myReg = /\{\{DynamicElement data=\'(.*?)\'\}\}/g;

        if(!value)
        {
            return '';
        }
        var arraysplit = value.split(myReg);
        console.log('array',arraysplit);

        for(let item in arraysplit)
        {
            if(item % 2 == 1)
            {
                arraysplit[item] = '<span class="dynamicelementedit" id="' + JSON.parse(arraysplit[item]).id + '" contenteditable="false">' + JSON.parse(arraysplit[item]).caption + '</span>';
            }
        }

        value = arraysplit.join("");
        //value = value.replace(myReg,ReactDomServer.renderToString(<DynamicElement id="$1"></DynamicElement>));
        let array = value.split(/\[(.*?)\]/g);
        if(array.length > 1 && !this.updated)
        {
            this.caption = array[1];
            this.updated = true;
            this.setState({
                dynamicelement:true
            })
        }
        return value;
    }

    handleDiagnosticElement = (value) => {
        value = value.replace(/\<span class=\"dynamicelementedit\" id=\"(.*?)\"(.*?)\>(.*?)\<\/span\>/g,"{{DynamicElement data='{\"id\":$1,\"caption\":\"$3\"}'}}");
        this.props.save(value);
    }

    adddynamic = (data) => {

        let textData = this.props.textdata;
        
        if(!data)
        {
            textData = textData.replace(/\[(.*?)\]/g,"");
        }
        else
        {
            textData = textData.replace(/\[(.*)\]/g,"{{DynamicElement data='" + JSON.stringify(data) + "'}}");
        }

        if(this.selectedcomponent && data)
        {
            this.selectedcomponent.innerHTML = data.caption;
        }
        this.props.save(textData);
        this.selected = -1;
        this.selectedid = false;
        this.updated = false;
        this.selectedcomponent = false;
        this.setState({
            dynamicelement:false
        })
    }

    render()
    {
        return (
            <Row>
                <Col><ContentEditable className="contenteditable" onChange={(e)=>this.handleDiagnosticElement(e.target.value)} html={this.rendertextdata(this.props.textdata)}></ContentEditable></Col>
                <DynamicAdd caption={this.caption} showdynamic={this.state.dynamicelement} adddynamic={this.adddynamic} id={this.selectedid} close={()=>this.setState({dynamicelement:false})}></DynamicAdd>
            </Row>
        )
    }
}

export default DynamicEditor;