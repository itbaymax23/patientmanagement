import React from 'react';

import {Modal,ModalBody,ModalHeader,ModalFooter,Button} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSave} from '@fortawesome/free-solid-svg-icons';

import {Editor} from 'react-draft-wysiwyg';
import {convertToRaw,EditorState,ContentState,convertFromHTML} from 'draft-js';
import draftToHTML from 'draftjs-to-html';
import {RadioGroup,Radio} from 'react-ui-icheck';
class FreeText extends React.Component
{
    editdata = "" // edit data which typed in free text
    text = "";
    data = {};
    constructor(props)
    {
        super(props);
        this.state = {
            selected:'CC',
            editorstate:EditorState.createEmpty()
        }
    }

    //edit data retrieved in free text
    onEditorStateChange = (state) => {           
        let text = state.getCurrentContent().getPlainText();
        this.data[this.state.selected].freetextdata = text?draftToHTML(convertToRaw(state.getCurrentContent())):'';
        this.setState({
            editorstate:state
        })
    }
    
    // initialization
    componentWillReceiveProps(props)
    {
        this.data = JSON.parse(JSON.stringify(this.props.data));
    }
    
    initdata = (value) => {
        let state = EditorState.createEmpty(); 
        console.log(value);
        if(value)
        {
            state = EditorState.createWithContent(
                        ContentState.createFromBlockArray(
                        convertFromHTML(value)
                        )
                    );
        }

        this.setState({
            editorstate:state
        })
    }

    selectsection = (value) => {
        if(!this.data[value])
        {
            this.data[value] = {};
        }
        this.initdata(this.data[value].freetextdata);
        this.setState({selected:value})
    }

    render()
    {
        return (<Modal isOpen={this.props.isopen} size="lg">
            <ModalHeader toggle={this.props.close}>Edit Free Text</ModalHeader>
            <ModalBody>
                <RadioGroup
                className="d-flex align-items-center"
                name="section"
                onChange={(event, value) => this.selectsection(value)}
                radioWrapClassName="form-check form-check-inline"
                radioWrapTag="div"
                value={this.state.selected}
                >
                    {
                        this.props.sections.map((row,index)=>{
                            return (
                                <Radio label={row} value={row} radioClass="iradio_square-blue"></Radio>
                            )
                        })
                    }
                </RadioGroup>
                {/* Free Text Edit */}
                <Editor 
                    onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                    editorStyle={{minHeight:300}}
                    editorState={this.state.editorstate}
                    >
                </Editor>

            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={this.props.close}>
                    Cancel
                </Button>
                <Button color="success" onClick={()=>this.props.save(this.data,this.props.sections)}>
                    <FontAwesomeIcon icon={faSave} style={{marginRight:10}}></FontAwesomeIcon>
                    Save
                </Button>
            </ModalFooter>
        </Modal>)        
    }
}

export default FreeText;