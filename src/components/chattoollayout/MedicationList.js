import React from 'react';
import {Row,Col,Pagination,PaginationItem,PaginationLink,InputGroup,InputGroupAddon,Input,InputGroupText} from 'reactstrap';
import * as Problem from '../../action/Problem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';
class MedicationList extends React.Component
{
    searchvalue = "";
    constructor(props)
    {
        super(props);
        this.state = {
            meds:[],
            currentpage:1,
            totalpage:1
        }
    }

    componentDidMount(){
        let self = this;
        Problem.getmeds({search:'',currentpage:1}).then(function(data){
            self.setState({
                totalpage:data.data.totalpage,
                meds:data.data.list                
            })
        })
    }

    handleChange = (value) => {
        let self = this;
        this.searchvalue = value;
        Problem.getmeds({search:value,currentpage:1}).then(function(data){
            self.setState({
                totalpage:data.data.totalpage,
                meds:data.data.list,
                currentpage:1
            })
        })
    }

    page = (page) => {
        if(page > 0 && page <= this.state.totalpage)
        {
            let self = this;
            Problem.getmeds({search:this.searchvalue,currentpage:page}).then(function(data){
                self.setState({
                    currentpage:page,
                    totalpage:data.data.totalpage,
                    meds:data.data.list
                })
            })
        }
        
    }

    render()
    {
        let pagelist = [];

        var firstpage = Math.max(this.state.currentpage - 2,1);
        
        if(firstpage > this.state.totalpage - 4)
        {
            firstpage = Math.max(this.state.currentpage - 4,1)
        }

        for(let index = firstpage;index<=Math.min(firstpage + 4,this.state.totalpage);index++)
        {
            pagelist.push(index);
        }

        return (
            <Row>
                <Col lg={12}>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText id="basic-addon1"><FontAwesomeIcon icon={faSearch}></FontAwesomeIcon></InputGroupText>
                        </InputGroupAddon>
                        <Input placeholder="Type here to Search the List"  onChange={(e)=>this.handleChange(e.target.value)} defaultValue={this.searchvalue}></Input>
                    </InputGroup>
                </Col>
                <Col lg={12} style={{marginTop:10}}>
                    <ul className="medslist">
                    {
                        this.state.meds.map((row,index)=>{
                            return (
                                <li key={index} onClick={()=>{if(this.props.select){this.props.select(row)}}}>{row.name}</li>
                            )
                        })
                    }
                    </ul>
                </Col>
                <Col lg={12}>
                    <Row>
                        <Col lg={4}>
                            <InputGroup>
                                <Input value={this.state.currentpage} onChange={(e)=>this.page(e.target.value)}></Input>
                                <InputGroupAddon addonType="append">
                                    <InputGroupText>/{this.state.totalpage}</InputGroupText>
                                </InputGroupAddon>
                            </InputGroup>
                        </Col>
                        <Col>
                        {
                        this.state.meds.length > 0 && (
                            <Pagination>
                                <PaginationItem>
                                    <PaginationLink previous onClick={() => this.page(1)}/>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink first onClick={() => this.page(this.state.currentpage - 1)}>
                                        <FontAwesomeIcon icon={faAngleLeft}></FontAwesomeIcon>
                                    </PaginationLink>
                                </PaginationItem>
                                {
                                    pagelist.map((row,index)=>{
                                        return (
                                            <PaginationItem key={index} onClick={() => this.page(row)} active={this.state.currentpage == row}>
                                                <PaginationLink>{row}</PaginationLink>
                                            </PaginationItem>
                                        )
                                    })
                                }
                                <PaginationItem>
                                    <PaginationLink last onClick={()=>this.page(this.state.currentpage + 1)}>
                                    <FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon>
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink next onClick={()=>this.page(this.state.totalpage)}></PaginationLink>
                                </PaginationItem>
                            </Pagination>
                        )
                    }
                        </Col>
                    </Row>
                    
                    
                </Col>
            </Row>
        )
    }
}

export default MedicationList;