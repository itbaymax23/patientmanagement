import React from 'react';
import {Row,Col} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
class FamilyNode extends React.Component
{
    constructor(props)
    {
        super(props);
        console.log("familydata",props);
    }

    render()
    {
        return (
           <div className="root" style={this.props.style}>
               <div className={this.props.node.relation?this.props.node.livestatus == 'Decreased'?"inner femail":"inner male":"inner patient"} onClick={()=>this.props.node.relation && this.props.Select()}>
                    <div className="relation">{this.props.node.relation }</div>
                    <div className="name">{this.props.node.name} {this.props.node.DOB?'(' + this.props.node.DOB + ')':''}</div>
                    <div className="illness">
                        {this.props.node.illness && (<p>PMH : { this.props.node.illness}</p>)}
                        {/* {
                            this.props.node.illness && this.props.node.illness.split(',').map((row,index)=>{
                                return (<p>PMH : {row}</p>)
                            })
                        } */}
                    </div>
                    <div className="deadcause">
                        {this.props.node.causeofdeath && (<p>COD : {this.props.node.causeofdeath}</p>)}
                        {/* {
                            this.props.node.causeofdeath && this.props.node.causeofdeath.split(',').map((row,index)=>{
                                return (<p>COD : {row}</p>)
                            })
                        }                         */}
                    </div>
                </div>
                <FontAwesomeIcon icon={faTimes} style={{marginLeft:-15,zIndex:50,color:"red"}} onClick={()=>this.props.node.relation && this.props.delete()}></FontAwesomeIcon>
           </div>
        )   
    }
}

export default FamilyNode;