import React from 'react';
import * as Problem from '../../action/Problem';
import {Row,Col,Button,Modal,ModalHeader,ModalBody,ModalFooter,Popover,PopoverBody} from 'reactstrap';
import Card from '../Card';
import ReviewSide from '../chartboxcomponent/action/ReviewSide';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimes,faPlus} from '@fortawesome/free-solid-svg-icons';
import {Editor} from 'react-draft-wysiwyg';
import {convertToRaw} from 'draft-js';
import draftToHTML from 'draftjs-to-html';
import $ from 'jquery';
import Tree from 'react-animated-tree';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
                        
class Review extends React.Component
{
    selectedreview = false;
    buttonpresstimer = null;
    editdata = "";
    editrosdata = [];
    problems = [];
    timeline = false;
    description = "";
    selecteditem = "";
    selectedindex = "";
    constructor(props)
    {
        super(props);
        this.state = {
            edit:false,
            selectedpositive:false,
            selectedenable:false,
            selectedpositiveenable:{},
            editros:false,
            editrosdata:[],
            update:false,
            details:false
        }
        this.updateinfo();
    }

    //initialization
    updateinfo = () => {
        let self = this;
        Problem.getmasterfile(9).then(function(data){
            let editdata = JSON.parse(JSON.stringify(data.data)); 
            let statedata = data.data;
            for(let item in statedata)
            {   
               statedata[item].positive = [];
               statedata[item].list = []
            }

            console.log(editdata);
            self.setState({editrosdata:editdata});

            self.props.save("ROS",{data:statedata,freetextenable:false,freetextdata:""},9);
        })
        Problem.getproblems().then(function(data){
            for(let item in data.data)
            {
                data.data[item].label = data.data[item].CommonName?data.data[item].CommonName:data.data[item].ShortDesc;
                data.data[item].id = data.data[item].Code;
            }
            self.problems = data.data;
        })
    }

    //add free text
    addfreetext = () => {
        this.props.save("ROS",{freetextdata:this.editdata,freetextenable:true})
        this.setState({
            edit:false
       })
    } 

    //when props is accepted
    componentWillReceiveProps(props)
    {
        let self = this;
        if(props.initdata)
        {
            this.updateinfo(); //initializati   on
        }

        //get the content from macro
        if(props.updated && props.selectedsection == 9 && props.macro.SectionId == 9)
        {
            Problem.getmasterfilefrommacro(props.macro.Id).then(function(res){
                let data = self.props.ROS.data;
                for(let item in res.data)
                {
                    if(!data[item])
                    {
                        data[item] = {};
                        data[item].Name = res.data[item].Name;
                    }
                    data[item].list = res.data[item].list;
                    data[item].initdata = res.data[item].Content;
                    data[item].positive = [];
                }
                self.props.save("ROS",{data:data,freetextdata:"",freetextenable:false});
            })
        }
    }

    
    componentDidMount()
    {
        let self = this;

        //long click event listener
        window.addEventListener("click",function(e){
            if($(e.target).parents('#popoverselect').length == 0)
            {   
                if(!self.selectedreview)
                {
                    self.setState({
                        selectedenable:false
                    })
                }
                else
                {
                    self.selectedreview = false;
                }
            }
        })
    }
    
    //show description dialog
    showdetail = (name) => {
        var namearray = name.split('_');
        this.selecteditem = namearray[0].split('section')[1];
               
        var data = this.props.ROS.data;
        this.description = data[this.selecteditem].positive[this.selectedindex].description?data[this.selecteditem].positive[this.selectedindex].description:'';
        this.setState({details:true});
    }

    //longclick listener
    handleClick = (sectionid,data,index) => {
        let self = this;
        this.buttonpresstimer = setTimeout(()=>{
            console.log("element");
            self.selectedreview = true;
            self.selectedindex = index;
            self.setState({
                selectedpositive:"section" + sectionid + "_" + data.Id,
                selectedenable:true,
                selectedpositiveenable:data
            })
        },1500);
    }

    //positive symptoms get
    gettitle = (positive,sectionId) => {
        let titlelist = [];
        let self = this;
        console.log(positive);
        positive.map((list,index)=>{
            if(index > 0)
            {
                titlelist.push(' , ');
            }
            titlelist.push(<span id={"section" + sectionId + "_" + list.Id} onClick={()=>this.removepositive(sectionId,index)} onMouseDown={()=>this.handleClick(sectionId,list,index)} onMouseUp={()=>this.handlebuttonpressed()}>{self.getelementtitle(list)} {list.description?'(' + list.description + ')':''}</span>)
        })

        return titlelist;
    }

    //edit ros data
    edit = () => {
        this.setState({
            edit:!this.state.edit
        })
    }
    
    //remove positive
    removepositive = (sectionid,index) => {
        if(this.selectedreview)
        {
            return true;
        }

        let data = this.props.ROS.data;
        data[sectionid].positive[index].description = "";
        data[sectionid].list.push(data[sectionid].positive[index]);
        data[sectionid].positive.splice(index,1);

        this.props.save("ROS",{data:data},9);
    }

    //positive add
    addpositive = (id,index) => {
        let data = this.props.ROS.data;
        data[id].positive.push(data[id].list[index]);
        data[id].list.splice(index,1);

        this.props.save("ROS",{data:data},9);
        
    }

    //get the data from negative
    getinitdata = (initdata,list,subsectionId) => {
        var listelement = [];
        let self = this;

        var listdata = ['Negative for ',''];
        listelement = [listdata[0]];
        if(initdata)
        {
            var listdata = initdata.split('{{ItemList}}');
            listelement = [listdata[0]];
        }
        
        list.map((listrow,index)=>{
            if(index > 0)
            {
                listelement.push(' , ');
            }
            listelement.push(<span onClick={()=>this.addpositive(subsectionId,index)}>{self.getelementtitle(listrow)}</span>)
        }) 

        listelement.push(listdata[1]);
        return listelement;
    }

    //get element title
    getelementtitle = (icdelement) => {
        return icdelement.CommonName ? icdelement.CommonName:icdelement.ShortDesc;
    }

    //free text wisywig editor
    onEditorStateChange = (state) => {
        this.editdata = draftToHTML(convertToRaw(state.getCurrentContent()));
    }

    //long click listner delete
    handlebuttonpressed = (value) => {
        clearTimeout(this.buttonpresstimer);
    }
    
    //view mode set paragraph,line   default line
    setparam = (item,value) =>
    {
        let data = this.props.ROS.data;
        data[item][value] = data[item][value]?false:true;
        this.props.save("ROS",{data:data},9);
    }

    //delete data
    removereview = (removeitem) => {
        var data = this.props.ROS.data;
        data[removeitem].list = [];
        data[removeitem].positive = [];
        data[removeitem].perHPI = false;

        this.props.save("ROS",{data:data},9);
    }
    
    //edit ros data
    editROS = () => {
        this.editrosdata = this.state.editrosdata;
        this.setState({
            editros:!this.state.editros
        })
    }

    //toggle edit ros item
    toggle = () => {
        this.setState({
            editros:false
        })
    }

    //add symptom to name
    addenrosdata = (name) => {
        this.editrosdata[name].list.push({editable:true,options:this.problems});
        this.setState({
            update:!this.state.update
        })
    }

    deleteenrosdata = (name,index) => {
        this.editrosdata[name].list.splice(index,1);
        this.setState({
            update:!this.state.update
        })
    }
    
    selectproblem = (name,index,value) => {
        if(value.length > 0)
        {
            this.editrosdata[name].list[index] = value[0];
            this.setState({
                update:!this.state.update
            })
        }
    }

    saveeditros = () => {
        var elementlist = [];
        for(let item in this.editrosdata)
        {
            for(let itemlist in this.editrosdata[item].list)
            {
                elementlist.push({
                    MasterFileSubsectionId:this.editrosdata[item].Id,
                    ReferenceEntity:"ICD10",
                    ReferenceKeyChar:this.editrosdata[item].list[itemlist].Code,
                    ReferenceKeyInt:this.editrosdata[item].list[itemlist].Id,
                    IsIncluded:true})
            }
        }

        let self = this;
        Problem.saveRosdata(elementlist).then(function(data){
            if(data.data.success)
            {
                self.setState({
                    editros:false,
                    editrosdata:self.editrosdata
                })
            }
        })
    }

    setdefault = (name) => {
        var data = this.props.ROS.data;
        data[name].list = JSON.parse(JSON.stringify(this.state.editrosdata[name].list));
        data[name].positive = [];
        data[name].perHPI = false;
        console.log(data);
        this.props.save("ROS",{data:data},10);
    }

    setviewparam = (param) => {
        this.props.save("ROS",{param:param},10);
    }
    
    savedescription = () => {
        var data = this.props.ROS.data;
        data[this.selecteditem].positive[this.selectedindex].description = this.description;
        this.props.save('ROS',{data:data},10);
        this.selecteditem = false;
        this.setState({
            details:false
        })
    }

    renderparagraph = ()=>{
        var list = [];
        for(let item in this.props.ROS.data)
        {
            if(this.props.ROS.data[item].positive.length > 0 || this.props.ROS.data[item].list.length > 0)
            {
                list.push(<span>In {this.props.ROS.data[item].Name} Patient reports </span>);
            }
            else
            {
                continue;
            }
            var positive = this.gettitle(this.props.ROS.data[item].positive,item);
            
            if(positive.length > 0)
            {
                list.push(<span> As Positive </span>)
                list.push(<span className="reviewproblem positive">{positive}</span>)
                if(this.props.ROS.data[item].list.length > 0)
                {
                    list.push(<span> but reports As negative </span>);
                }
            }

            for(let itemlist in this.props.ROS.data[item].list)
            {
                list.push(<span> no <span className="reviewproblem specific"><span><span onClick={()=>this.addpositive(item,itemlist)}>{this.getelementtitle(this.props.ROS.data[item].list[itemlist])}</span></span></span></span>)                    
            }
        }

        return list;
    }


    render()
    {
        let self = this;
        console.log(this.state.data);
        let config = open => (
            {
                from:{height:0,opacity:0,transform:"none",willChange:"auto"},
                to:{
                    height:open?'auto':0,
                    opacity:open?1:0,
                    transform:"none",
                    willChange:"auto"
                }
            }
        )
        
        let Tag = 'p';

        return (
            <Card title="Review Of Systems" tag={ReviewSide} showmacro={this.props.showmacro} undo={this.updateinfo} sectionId={9} editable={this.edit} edit={this.editROS} setviewparam = {this.setviewparam}>
                <Col>
                {
                    (this.props.ROS.param != 'paragraph' && !this.props.ROS.freetextenable) && Object.keys(this.props.ROS.data).map((keyName,i)=>{
                        return (
                            <Row className="reviewcontent">
                                <Col lg={2} md={2} sm={3} xs={4}>
                                    {self.props.ROS.data[keyName].Name}
                                </Col>
                                <Col>
                                    {!self.props.ROS.data[keyName].perHPI && self.props.ROS.data[keyName].positive.length > 0 && (
                                        <Tag className="reviewproblem positive">Positive for {self.gettitle(self.props.ROS.data[keyName].positive,keyName)}</Tag>
                                    )}
                                    {
                                        !self.props.ROS.data[keyName].perHPI && self.props.ROS.data[keyName].list.length > 0 && (
                                            <Tag className="reviewproblem specific">{self.getinitdata(self.props.ROS.data[keyName].initdata,self.props.ROS.data[keyName].list,keyName)}</Tag>
                                        )
                                    }
                                    {
                                        self.props.ROS.data[keyName].perHPI && (<p>* Per HPI</p>)
                                    }
                                    
                                </Col>
                                <Col lg={2} md={2} sm={3} xs={4}>
                                    <div style={{marginLeft:"auto"}}>
                                        <span className="default" onClick={()=>self.setdefault(keyName)}>Default</span>
                                        <span className="default" style={{marginLeft:20}} onClick={()=>this.setparam(keyName,"perHPI")}>Per HPI</span>
                                        <Button color="secondary" style={{marginLeft:20,padding:"3px 6px",fontSize:10}} onClick={()=>this.removereview(keyName)}><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></Button>
                                    </div>
                                </Col>
                            </Row>
                        )
                    })
                }
                {
                    (!this.props.ROS.freetextenable && this.props.ROS.param == 'paragraph') && (
                        <div className="reviewcontent">
                            {this.renderparagraph()}
                        </div>
                    )
                }
                {
                    this.props.ROS.freetextenable && (
                    <p dangerouslySetInnerHTML={{__html:this.props.ROS.freetextdata}}></p>
                    )
                }
                </Col>
                <Modal isOpen={this.state.edit} size="lg">
                    <ModalHeader toggle={this.edit}>Add Free Text</ModalHeader>
                    <ModalBody>
                        <Editor 
                            onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                            editorStyle={{minHeight:300}}
                            >
                        </Editor>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.edit}>Cancel</Button>
                        <Button color="success" style={{marginLeft:10}} onClick={this.addfreetext}>Save</Button>
                    </ModalFooter>
                </Modal>
                {
                    this.state.selectedpositive && (
                        <Popover target={this.state.selectedpositive} isOpen={this.state.selectedenable} placement="bottom">
                            <PopoverBody>
                                <ul className="assessment_add">
                                    <li onClick={()=> this.showdetail(this.state.selectedpositive)}>Details</li>
                                    <li onClick={()=>this.props.addassessments(this.state.selectedpositiveenable,"primary")}>Add Primary Diagnosis</li>
                                
                                    <li onClick={() => this.props.addassessments(this.state.selectedpositiveenable,"secondary")}>Add Secondary Diagnosis</li>
                                </ul>
                            </PopoverBody>
                        </Popover>
                    )
                }

                <Modal isOpen={this.state.editros} size="lg">
                    <ModalHeader toggle={this.toggle}>Edit Review System</ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col>
                                {
                                    Object.keys(this.editrosdata).map((keyName,index)=>{
                                        return (
                                            <Tree 
                                            id={keyName}
                                                content={<span>{self.editrosdata[keyName].Name} <FontAwesomeIcon icon={faPlus} style={{color:"#337ab7",cursor:"pointer"}} onClick={()=>this.addenrosdata(keyName)}></FontAwesomeIcon></span>} springConfig={config}>
                                                    {
                                                        self.editrosdata[keyName].list.map((prolist,listindex)=>{
                                                            console.log(prolist.options);
                                                            return (
                                                                <Row className="leaf">
                                                                    <Tree 
                                                                    id={keyName + "_" + listindex}
                                                                    content={(<div style={{display:"flex"}}>
                                                                        {!prolist.editable && (<span>
                                                                            {prolist.CommonName?prolist.CommonName:prolist.ShortDesc} 
                                                                        </span>)
                                                                        }
                                                                        { 
                                                                            prolist.editable && (
                                                                                <Col lg={7} style={{paddingLeft:0}}>
                                                                                    <Typeahead
                                                                                     id={listindex}
                                                                                     isLoading={prolist.loading?true:false}
                                                                                     labelKey="label"
                                                                                     id="id"
                                                                                     onInputChange={query=>{
                                                                                         clearTimeout(self.timeline);
                                                                                         self.timeline = setTimeout(()=>{
                                                                                            self.editrosdata[keyName].list[listindex].loading = true;
                                                                                            self.setState({
                                                                                                update:!self.state.update
                                                                                            })
                                                                                            Problem.getproblems(query).then(function(doc){
                                                                                                doc = doc.data;
                                                                                                for(let item in doc)
                                                                                                {
                                                                                                    doc[item].label = doc[item].CommonName?doc[item].CommonName:doc[item].ShortDesc;
                                                                                                    doc[item].id = doc[item].Code;
                                                                                                }

                                                                                                self.editrosdata[keyName].list[listindex].options = doc;
                                                                                                self.editrosdata[keyName].list[listindex].loading = false;
                                                                                                self.setState({
                                                                                                    update:!self.state.update
                                                                                                })
                                                                                            })
                                                                                         },1000)
                                                                                        
                                                                                     }}
                                                                                     options={prolist.options?prolist.options:[]}
                                                                                     onChange={(value)=>{this.selectproblem(keyName,listindex,value)}}
                                                                                     >
                                                                                    </Typeahead>
                                                                                </Col>
                                                                                )
                                                                        }
                                                                        <FontAwesomeIcon icon={faTimes} style={{margin:"0px 15px",color:"red",cursor:"pointer"}} onClick={()=>this.deleteenrosdata(keyName,listindex)}></FontAwesomeIcon>
                                                                    </div>)} springConfig={config}></Tree>
                                                                   </Row>
                                                            )
                                                        })
                                                    }
                                            </Tree>
                                        )
                                    })
                                }
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter style={{zIndex:9999}}>
                        <Button color="success" onClick={this.saveeditros}>Save</Button>
                        <Button color="secondary" style={{marginRight:10}} onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.details}>
                    <ModalHeader toggle={()=>this.setState({details:!this.state.details})}>
                        Details for {(this.selecteditem && this.props.ROS.data[this.selecteditem].positive[this.selectedindex].CommonName)?this.props.ROS.data[this.selecteditem].positive[this.selectedindex].CommonName:''}
                    </ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col>
                                <textarea className="form-control" rows="5" onChange={(e)=>{this.description = e.target.value}}>{this.description}</textarea>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={()=>this.setState({details:false})}>Cancel</Button>
                        <Button color="success" onClick={()=>this.savedescription()} style={{marginLeft:10}}>Save</Button>
                    </ModalFooter>
                </Modal>
            </Card>
        )
    }
}

export default Review;