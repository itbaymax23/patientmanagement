import React from 'react';
import {Radio,RadioGroup} from 'react-ui-icheck';
import {Row,Col,Modal,ModalHeader,ModalBody,Button,ModalFooter} from 'reactstrap';
import Tree from 'react-animated-tree';
import ContentEditable from 'react-contenteditable';
import * as Macro from '../../action/Macro';
import * as Problem from '../../action/Problem';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faBook,faFolder, faPlusCircle, faFolderPlus, faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import EditContent from './EditContent';
import DynamicEditor from './DynamicEditor';
// import {ConfirmAlert} from 'react-confirm-alert';

class TemplateEditor extends React.Component
{
    folder = {};
    macro = {};


    constructor(props)
    {
        super(props);
        this.state = {
            selected:'ME',
            macroelement:[],
            sections:[{label:'CC',sectionId:'1'},{label:'HPI',sectionId:'2'},{label:'PMH',sectionId:'3'},{label:'PSH',sectionId:'4'},{label:'ADR',sectionId:'6'},{label:'ROS',sectionId:'9'},{label:"PE",sectionId:'10'},{label:'Plan/Orders',sectionId:'13'}],
            selectedsectionId:'1',
            selectmacro:-1,
            type:1,
            folder:false,
            edited:false,
            updated:false
        }
    }

    select = (select) => {
        this.setState({
            selected:select,
            selectmacro:-1,
            type:1,
            edited:false
        })
    }
    
    selectmacro = (id,type) => {

        if(!type)
        {
            type = 1;
        }

        this.setState({
            selectmacro:id,
            type:type
        })
    }

    componentDidMount()
    {
        let self = this;
        Problem.getmacros().then(function(result){
            self.setState({
                macroelement:result.data
            })
        })
    }

    display = (macrosectionid) => {
        let macros = {};
        let macroelement = this.state.macroelement;
        for(let item in macroelement)
        {
            if(macroelement[item].SectionId == Number(macrosectionid))
            {
                macros = macroelement[item];
                break;
            }
        }
        if(macros.Name)
        {
            return (
                <Tree content={<span className={this.state.selectmacro == -1?'selected':'select'} onClick={()=>this.selectmacro(-1)}><FontAwesomeIcon icon={faFolder} style={{color:'#fc8c14',marginRight:10}}></FontAwesomeIcon><span className={this.state.selectmacro == -1?'selected':'select'}>{macros.Name}</span></span>} open>
                    {
                        this.displaytree(macros.macros)
                    }
                </Tree>
            )    
        }
        return this.displaytree(macros);
    }

    displaytree = (macros,parentid) => {
        let elementlist = [];
        let macroelement = [];
        for(let item in macros)
        {
            if((!parentid && !macros[item].ParentMacroId) || macros[item].ParentMacroId == parentid)
            {
                macroelement.push(macros[item]);
            }
        }

        console.log('macro_tree',parentid);
        for(let item in macroelement)
        {
            elementlist.push(<Tree content={<span>{
                macroelement[item].MacroType == 1 && (
                    <span onClick={()=>this.selectmacro(macroelement[item].Id,macroelement[item].MacroType)} className={this.state.selectmacro == macroelement[item].Id?'selected':'select'}><FontAwesomeIcon icon={faFolder} style={{color:'#fc8c14',marginRight:10}}></FontAwesomeIcon>{macroelement[item].Name}</span>
                )}
                {macroelement[item].MacroType == 2 && (
                    <span onClick={()=>this.selectmacro(macroelement[item].Id,macroelement[item].MacroType)} className={this.state.selectmacro == macroelement[item].Id?'selected':'select'}><FontAwesomeIcon icon={faBook} style={{marginRight:10}}></FontAwesomeIcon>{macroelement[item].Name}</span>
                )}</span>
            }>
                {
                    macroelement[item].MacroType == 1 && this.displaytree(macros,macroelement[item].Id)
                }
            </Tree>)
        }

        return elementlist;
    }
    

    selectsection = (value) => {
        this.setState({
            selectedsectionId:value,
            selectmacro:-1,
            type:1
        })
    }

    addnewfolder = () => {
        let macroelement = this.state.macroelement;
        for(let item in macroelement)
        {
            if(macroelement[item].SectionId == Number(this.state.selectedsectionId))
            {
                this.folder.MacroCategoryId = macroelement[item].Id;
                this.folder.SectionId = macroelement[item].SectionId;
                if(this.state.selectmacro > -1)
                {
                    this.folder.ParentMacroId = this.state.selectmacro;
                }
                this.folder.MacroType = 1;
                this.setState({
                    folder:true
                })
                break;
            }
        }
    }

    createnewmacro = () => {
        let macroelement = this.state.macroelement;
        for(let item in macroelement)
        {
            if(macroelement[item].SectionId == Number(this.state.selectedsectionId))
            {
                this.macro.MacroCategoryId = macroelement[item].Id;
                this.macro.SectionId = macroelement[item].SectionId;
                if(this.state.selectmacro > -1)
                {
                    this.macro.ParentMacroId = this.state.selectmacro;
                }

                this.macro.MacroType = 2;
                this.macro.Content = "";
                this.setState({
                    edited:true
                });
                break;
            }
        }
    }

    getlabel = (sectionid) => {
        let label = this.state.sections;
        for(let item in label)
        {
            if(label[item].sectionId == sectionid)
            {
                return label[item].label;
            }
        }
    }   

    renamefolder = () => {
        let macroelement = this.state.macroelement;
        for(let item in macroelement)
        {
            if(macroelement[item].SectionId == Number(this.state.selectedsectionId))
            {
                for(let itemmacro in macroelement[item].macros)
                {
                    if(macroelement[item].macros[itemmacro].Id == this.state.selectmacro)
                    {
                        this.folder = macroelement[item].macros[itemmacro];
                        this.setState({
                            folder:true
                        })
                        break;
                    }
                }
                
            }
        }
    }

    deletemacroitem = () => {
        let macroelement = this.state.macroelement;
        for(let item in macroelement)
        {
            if(macroelement[item].SectionId == Number(this.state.selectedsectionId))
            {
                for(let itemmacro in macroelement[item].macros)
                {
                    if(macroelement[item].macros[itemmacro].Id == this.state.selectmacro)
                    {
                       let macroarray = [macroelement[item].macros[itemmacro]];
                       macroarray = macroarray.concat(this.getchildren(macroelement[item].macros,macroelement[item].macros[itemmacro].Id));
                       
                       let idarray = [];
                       for(let index in macroarray)
                       {
                            idarray.push(macroarray[index].Id);
                       }
                       
                       let self = this;
                       Macro.deletemacro(idarray).then(function(data){
                           if(data.data.success)
                           {
                                for(let item in macroelement)
                                {
                                    if(macroelement[item].SectionId == Number(self.state.selectedsectionId))
                                    {
                                        let itemmacro = 0;
                                        while(macroelement[item].macros[itemmacro])
                                        {
                                            if(idarray.indexOf(macroelement[item].macros[itemmacro].Id) > -1)
                                            {
                                                macroelement[item].macros.splice(itemmacro,1);
                                            }
                                            else
                                            {
                                                itemmacro ++;
                                            }
                                        }
                                    }
                                }

                                self.setState({
                                    macroelement:macroelement
                                })
                           }
                       })
                    }
                }
                
            }
        }
    }

    deletefolder = () => {
        if(window.confirm('Are you sure to delete this folder'))
        {
            this.deletemacroitem();
        }
        
    }

    getchildren = (macros,parentid) =>
    {
        let itemarray = [];
        for(let item in macros)
        {
            if(macros[item].ParentMacroId == parentid)
            {
                itemarray.push(macros[item]);
                itemarray = itemarray.concat(this.getchildren(macros,macros[item].Id));
            }
        }

        return itemarray;
    }

    editmacro = () => {

    }

    deletemacro = () => {
        if(window.confirm('Are you sure to delete this macro'))
        {
            this.deletemacroitem();
        }
    }

    savefolder = () => {
        let self = this;
        Macro.addmacro(this.folder).then(function(res){
            if(res.data.id)
            {
                self.folder.Id = res.data.id;
                let macroelement = self.state.macroelement;

                for(let item in macroelement)
                {
                    if(macroelement[item].SectionId == Number(self.state.selectedsectionId))
                    {
                        macroelement[item].macros.push(JSON.parse(JSON.stringify(self.folder)));
                        self.setState({
                            macroelement:macroelement,
                            folder:false
                        });
                        break;
                    }
                }
            }
            else
            {
                let macroelement = self.state.macroelement;

                for(let item in macroelement)
                {
                    if(macroelement[item].SectionId == Number(self.state.selectedsectionId))
                    {
                        for(let itemmacro in macroelement[item].macros)
                        {
                            if(macroelement[item].macros[itemmacro].Id == self.folder.Id)
                            {
                                macroelement[item].macros[itemmacro] = JSON.parse(JSON.stringify(self.folder));
                            }
                            
                        }

                        self.setState({
                            macroelement:macroelement,
                            folder:false
                        })
                        break;
                    }
                }
            }
        })
    }

    render()
    {
        return(
            <Modal isOpen={this.props.open} size="lg" className="custom-modal template">
                <ModalHeader toggle={this.props.close}>Edit Template</ModalHeader>
                <ModalBody>
                    <Row>
                        <Col lg={2} className="sidebartemplate">    
                            <Row>
                                <Col><Button color={this.state.selected == 'ME'?'success':"default"} onClick={()=>this.select('ME')}>Macro Editor</Button></Col>
                            </Row>
                            <Row style={{marginTop:10}}>
                                <Col><Button color={this.state.selected == 'CC'?'success':"default"} onClick={()=>this.select('CC')}>CC Default Content</Button></Col>
                            </Row>
                        </Col>
                        {
                            !this.state.edited && (
                                <Col>
                                    <Row>
                                        <RadioGroup
                                        className="d-flex align-items-center"
                                        name="section_select"
                                        onChange={(event, value) => this.selectsection(value)}
                                        radioWrapClassName="form-check form-check-inline"
                                        radioWrapTag="div"
                                        value={this.state.selectedsectionId}
                                        >
                                            {
                                                this.state.sections.map((row,index)=>{
                                                    return (
                                                        <Radio key={index} value={row.sectionId} label={row.label} radioClass="iradio_square-blue"></Radio>
                                                    )
                                                })
                                            }
                                        </RadioGroup>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Row style={{marginTop:10}}>
                                            {
                                                this.state.type == 1 && (
                                                    <Col lg={12}>
                                                        <Button color="success" onClick={this.createnewmacro}><FontAwesomeIcon icon={faPlusCircle} style={{marginRight:10}}></FontAwesomeIcon>Create New Macro</Button>
                                                        <Button color="primary" style={{marginLeft:10}} onClick={this.addnewfolder}>
                                                            <FontAwesomeIcon icon={faFolderPlus} style={{marginRight:10}}></FontAwesomeIcon> Add New Folder
                                                        </Button>
                                                        {
                                                            this.state.selectmacro != -1 && (
                                                                <Button color="warning" style={{marginLeft:10}} onClick={this.renamefolder}>
                                                                    <FontAwesomeIcon icon={faPencilAlt} style={{marginRight:10}}></FontAwesomeIcon>
                                                                    Rename Folder
                                                                </Button>
                                                            )
                                                        }
                                                        {
                                                            this.state.selectmacro != -1 && (
                                                                <Button color="secondary" style={{marginLeft:10}} onClick={this.deletefolder}>
                                                                    <FontAwesomeIcon icon={faTrash} style={{marginRight:10}}></FontAwesomeIcon>
                                                                    Delete Folder
                                                                </Button>
                                                            )
                                                        }
                                                    </Col>
                                                )
                                            }

                                            {
                                                this.state.type == 2 && (
                                                    <Col>
                                                        <Button color="success" onClick={this.editmacro}><FontAwesomeIcon icon={faPencilAlt} style={{marginRight:10}}></FontAwesomeIcon>Edit Macro</Button>
                                                        <Button color="secondary" style={{marginLeft:10}} onClick={this.deletemacro}><FontAwesomeIcon icon={faTrash} style={{marginRight:10}}></FontAwesomeIcon>Delete Macro</Button>
                                                    </Col>
                                                )
                                            }
                                            </Row>
                                            <Row>
                                                <Col>
                                                {
                                                    this.display(this.state.selectedsectionId)
                                                }
                                                </Col>
                                            </Row>
                                        </Col>    
                                    </Row>
                                    
                                </Col>
                            )
                        }

                        {
                            this.state.edited && (
                                <Col>
                                    <EditContent data={this.macro} save={(item,data)=>{this.macro[item]= data; this.setState({updated:!this.state.updated})}} sectionid={this.state.selectedsectionId} label={this.getlabel(this.state.selectedsectionId)}></EditContent>
                                </Col>
                            )
                        }
                    </Row>
                </ModalBody>

                <Modal isOpen={this.props.open && this.state.folder}>
                    <ModalHeader>Macro Folder</ModalHeader>
                    <ModalBody>
                        <textarea className="form-control" defaultValue={this.folder.Name} rows={5} onChange={(e)=>{this.folder.Name = e.target.value;this.setState({updated:!this.state.updated})}}></textarea>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={()=>this.setState({folder:false})}>Cancel</Button>
                        <Button color="success" onClick={this.savefolder}>Save</Button>
                    </ModalFooter>
                </Modal>
            </Modal>
        )
    }
}

export default TemplateEditor;