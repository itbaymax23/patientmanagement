import React from 'react';
import Tree from 'react-animated-tree';
import * as Problem from '../../action/Problem';
import {Modal,ModalBody,ModalFooter,ModalHeader,Row,Col} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBook} from '@fortawesome/free-solid-svg-icons';

class Macro extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            macros:[]
        }

        this.updateinfo();
    }
    
    updateinfo = () => {
        let self = this;
        Problem.getmacros().then(function(data){
            self.setState({
                macros:data.data
            })
        })
    }

    close = () => {
        this.setState({
            open:!this.state.open
        })
    }
    
    select_macro = (macrolist,macro) => {
        macro.list = [];
        let itemlist = [macro.Id];

        for(let item in macrolist)
        {
            if(macrolist[item].Id == macro.Id)
            {
                continue;
            }

            if(itemlist.indexOf(macrolist[item].ParentMacroId) > -1)
            {
                itemlist.push(macrolist[item].ParentMacroId);
                macro.list.push(macrolist[item]);
            }
        }

        this.props.select_macro(macro);
    }

    render()
    {
        return (
            <Modal isOpen={this.props.open}>
                <ModalHeader toggle={this.props.toggle}>Insert Macro</ModalHeader>
                <ModalBody>
                    <Row>
                        <Col className="macro">
                        {
                            this.state.macros.map((row,index)=>{
                                return (
                                    <Tree content={<span><FontAwesomeIcon icon={faBook} style={{marginRight:10}}></FontAwesomeIcon>{row.Name}</span>} open={this.props.sectionId == row.SectionId}>
                                        {
                                            row.macros.map((item,item_index)=>{
                                                return (
                                                    <Tree content={<span className="macroitem" onClick={(e)=>this.select_macro(row.macros,item)}>{item.Name}</span>}></Tree>
                                                )
                                            })
                                        }
                                    </Tree>
                                )
                            })
                        }
                        </Col>
                    </Row>
                    
                </ModalBody>
            </Modal>
        )
    }
}

export default Macro;