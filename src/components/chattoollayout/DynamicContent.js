import React from 'react';
import {UncontrolledPopover,PopoverBody} from 'reactstrap';
//import {Popover,OverlayTrigger} from 'react-bootstrap';
import * as Problem from '../../action/Problem';

import momentlocalizer from 'react-widgets-moment';
import {DatePicker} from 'react-widgets';
import Moment from 'moment';

class DynamicContent extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            open:false,
            data:{},
            popoverdisabled:['PN','HS','AG','MF','OW']
        };

        //this.updateinfo(props.id);
    }

    updateinfo = (id) => {
        let self = this;
        Problem.getdynamicelement(id).then(function(res){
            self.setState({
                data:res.data
            })
        })
    }

    componentDidMount()
    {
        this.updateinfo(this.props.id);
    }

    componentWillReceiveProps(props)
    {
        if(props.id && this.state.data.id != props.id)
        {
            this.updateinfo(props.id);
        }
    }

    getvalue = () => {
        if(this.props.patient && this.state.data.controltype == "HS")
        {
            return this.props.patient.Sex == "M"?"He":"She";
        }
        else if(this.props.patient && this.state.data.controltype == 'PN')
        {
            return this.props.patient.FirstName + " " + this.props.patient.LastName;
        }
        else if(this.props.patient && this.state.data.controltype == 'AG')
        {
            return new Date().getFullYear() - new Date(this.props.patient.DateOfBirth).getFullYear();
        }
        else if(this.props.patient && this.state.data.controltype == 'MF')
        {
            return this.props.patient.Sex == 'M'?"male":"female";
        }
        else if(this.props.patient && this.state.data.controltype == 'OW')
        {
            return this.props.patient.Sex == 'M'?"His":"Her";
        }
        else
        {
            return this.props.value?this.props.value:this.state.data.defaultvalue?this.state.data.defaultvalue:this.state.data.caption;
        }
    }

    handleValue = (value_dynamic,checked)=>{
        let value = this.props.value;
        let array = value?value.split(','):[];
        if(checked)
        {
            if(this.state.data.controltype == 'ACL' || this.state.data.controltype == 'MSL')
            {
                array.push(value_dynamic);
            }
            else
            {
                array = [value_dynamic];

                let self = this;
                window.setTimeout(()=>{
                    self.refs.trigger.setState({
                        isOpen:false
                    })
                },100)
                
            }
        }
        else
        {
            if(array.indexOf(value_dynamic) > -1)
            {
                array.splice(array.indexOf(value_dynamic),1);
            }
        }

        this.props.save(array.join(','));
    }

    getstring = (date) => {
        return date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
    }

    

    render()
    {
        Moment.locale('en');
        momentlocalizer();
        let valuearray = this.props.value?this.props.value.split(','):[];
        return (
            <span id={this.props.dataid} className={this.state.popoverdisabled.indexOf(this.state.data.controltype) == -1?"dynamiccontent":""}>
                {this.getvalue()}
                {
                    (this.props.id && this.state.popoverdisabled.indexOf(this.state.data.controltype) == -1) && (
                        <UncontrolledPopover ref="trigger" target={this.props.dataid} trigger="legacy" placement="bottom">
                            <PopoverBody>
                                {
                                    (this.state.data.controltype == 'ACL' || this.state.data.controltype == 'MSL' ||  this.state.data.controltype == 'SSL') && this.state.data.listoption && this.state.data.listoption.split(',').map((row,index)=>{
                                        return (
                                            <div><p className={valuearray.indexOf(row) > -1?"dynamicitem checkeditem":"dynamicitem uncheckeditem"} onClick={(e)=>this.handleValue(row,valuearray.indexOf(row)>-1?false:true,this.state.data.controltype)}>{row}</p></div>
                                        )
                                    })
                                }
                                {
                                   this.state.data.controltype == 'FT' && (
                                       <input type="text" className="form-control" defaultValue={this.props.value} onChange={(e)=>this.props.save(e.target.value)}></input>
                                   )
                                }

                                {
                                    this.state.data.controltype == 'DF' && (
                                        <DatePicker 
                                        defaultValue={this.props.value?new Date(this.props.value):new Date()}
                                        onChange={(date)=>{this.props.save(this.getstring(date))}}
                                        ></DatePicker>
                                    )
                                }
                            </PopoverBody>
                        </UncontrolledPopover>
                    )
                }
            </span>
        )
    }
}

export default DynamicContent;