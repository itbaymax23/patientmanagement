import React from 'react';
import {Row,Col,Modal} from 'reactstrap';
import * as Problem from '../../action/Problem';

class DynamicElement extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            controltype:[{label:"Single Select List",value:'SSL'},{label:"Admits/denies combo list",value:'ACL'},{label:'Free text',value:'FT'},{label:'He/She',value:'HS'},{label:'Patient Name',value:'Patient Name'}],
            datasource:[{label:'Free Text',value:'freetext'}],
            data:{
                caption:props.caption?props.caption:''
            }
        }

        this.updateinfo();
    }

    updateinfo = () => {
        let self = this;
        Problem.getdynamicelement(this.props.id).then(function(result){
            self.setState({
                data:result.data
            })
        })
    }

        
    gettitle = (caption) => {
        if(this.props.edit)
        {
            return caption;
        }
        else{
            return '[' + caption + ']';
        }
    }

    clickedelement = () => {
    }

    render()
    {
        return (
            <span className="dynamicelement" onClick={this.clickedelement} contentEditable={false}>
                {
                    this.state.data.caption
                }
            </span>
        )
    }
}


export default DynamicElement;