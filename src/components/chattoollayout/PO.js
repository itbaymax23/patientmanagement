import React from 'react';
import {Row,Col,Progress,Button,Table,Modal,ModalBody,ModalHeader,ModalFooter,Form,FormGroup,Label} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCheckCircle, faPlus} from '@fortawesome/free-solid-svg-icons';
import {Typeahead} from 'react-bootstrap-typeahead';
import {NumberPicker} from 'react-widgets';
import simpleNumberLocalizer from 'react-widgets-simple-number';

import Card from '../Card';
import PlanOrder from '../chartboxcomponent/action/PlanOrder';
import PlanOrderRow from '../chartboxcomponent/PlanOrderRow';
import AssessmentTable from '../chartboxcomponent/AssessmentTable';

import * as User from '../../action/user';
import * as Macro from '../../action/Macro';
import _ from 'lodash';
import {Editor} from 'react-draft-wysiwyg';
import {convertToRaw} from 'draft-js';
import draftToHTML from 'draftjs-to-html';

class PO extends React.Component
{
    dispose = {};
    editable = "";
    constructor(props)
    {
        super(props);
        this.state =  {
            action:["None","Admit","Discharge","Transfer"],
            location:["Home","Hospital","Nursing Home","Personal Care Home"],
            condition:["stable","Improving","Unstable","Guarded"],
            diet:[],
            other:[],
            cardio:["Attempt Resuscitation(CTR)","Allow Natural Death(AND) - Do Not Attempt Resuscitation"],
            medical:["Comfort Measures","Limited Additional inventions","Full Treatment"],
            anti:["No Antibiotics","Determine use or limitation of antibiotics when infection occurs","Use antibiotics if life can be prolonged"],
            affinity:["Yes","No Artificial nutrition by tube","No IV Fluids","Trial Period of artifical nutrition by tube","Trial Period of IV Fluids","Long Term Artifical Nutrition By Tube","Long Term IV Fluids"],
            misc:[],
            providers:[],
            adddispose:false,
            month:["January","Febrary","March","April","May","June","July","August","September","October","November","December"],
            freetext:false
        }
    }

    componentDidMount()
    {
        let self = this;
        User.getproviders().then(function(data){
            let providers = data.data;
            let providerlist = [];

            for(let item in providers)
            {
                providerlist.push({label:providers[item].fName + " " + providers[item].lName,id:providers[item].providerId})
            }

            self.setState({
                providers:providerlist
            })
        })

        Macro.getdataforplan().then(function(res){
            self.setState({
                misc:res.data.misc,
                diet:res.data.diets,
                other:res.data.other
            })
        })
    }

    onEditorStateChange = (state) => {
        this.editdata = draftToHTML(convertToRaw(state.getCurrentContent()));
    }

    componentWillReceiveProps(props)
    {
        
    }

    save = (param,value) => {
        let data = this.props.data;
        if(data[param] == value)
        {
            data[param] = null;
        }
        else
        {
            data[param] = value;
        }        
        this.props.save('PO',data,14);
    }

    closedispose = () => {
        this.setState({
            adddispose:false
        })
    }

    adddispose = () => {
        this.dispose = {};
        this.setState({
            adddispose:true
        })
    }

    handleChangedispose = (name,value) => {
        this.dispose[name] = value;
    }

    savedispose = () => {
        let data = this.props.data;
        if(!data.dispose)
        {
            data.dispose = [];
        }

        data.dispose.push(this.dispose);

        this.props.save("PO",data,14);
        this.setState({adddispose:false})
    }

    addfreetext = () => {
        this.setState({
            freetext:true
        })
    }

    addfreetextdata = () => {
        let data = this.props.data;
        data.freetextenable = true;
        data.freetextdata = this.editdata;
        this.props.save('PO',data,13);
        this.setState({freetext:false})
    }

    undo = () => {
        let data = this.props.data;
        for(let item in data)
        {
            data[item] = false;
        }

        let assessments = this.props.assessments;
        if(assessments.labs)
        {
            assessments.labs = false;
        }
        if(assessments.meds)
        {
            assessments.meds = false;
        }
        if(assessments.imaging)
        {
            assessments.imaging = false;
        }
        this.props.save('PO',data,14);
    }

    hasorder = (row) => {
        return row.labs.length > 0 || row.imaging.length > 0 || row.meds.length > 0;
    }

    closefreetext = () => {
        this.setState({freetext:false})
    }
    render()
    {
        let self = this;
        return (
            <Card title="Plan/Orders" tag={PlanOrder} previewsection = {()=>this.props.previewsection(13)} freetext={this.addfreetext} undo={this.undo} showmacro={()=>this.props.showmacro(13)}>
                {
                    !this.props.data.freetextenable && (
                        <Col className="planorder">
                            <Row>
                                <Col lg={10} md={9} sm={12} xs={12}>
                                    <Row className="planorderrow">
                                        <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                                            <Row className="title">
                                                <Col>
                                                    <p>Disposition :</p>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <div className="planordercell" style={{borderWidth:1,borderRightStyle:'solid',borderColor:'rgb(226,226,226)'}}>
                                                    <Col className="planordertitle" lg={12}>Action : </Col>
                                                    <Col lg={12}>
                                                        {
                                                            this.state.action.map((row,index) => {
                                                                return (
                                                                    <span key={index} className={(self.props.data.action == row)?"specific planordercontent":"planordercontent"} onClick={()=>this.save('action',row)}>{row}</span>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                </div>
                                                <div className="planordercell">
                                                    <Col className="planordertitle" lg={12}>Location : </Col>
                                                    <Col lg={12}>
                                                        {
                                                            this.state.location.map((row,index) => {
                                                                return (
                                                                    <span className={self.props.data.location == row?"specific planordercontent":"planordercontent"}  onClick={()=>this.save('location',row)}>{row}</span>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                </div>
                                                <Col>
                                                    <select className="form-control" onChange={(e)=>this.save('specificlocation',e.target.value)}>
                                                        <option value="">Specific Location</option>
                                                        {
                                                            this.props.location && this.props.location.map((row,index)=>{
                                                                return (
                                                                    <option key={index} value={row.label}>{row.label}</option>
                                                                )
                                                            })
                                                        }
                                                    </select>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    
                                    <Row className="planorderrow">
                                        <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                                            <Row className="title">
                                                <Col>
                                                    <p>Diet :</p>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <div className="planordercell" style={{alignItems:'center'}}>
                                                    <Col lg={12}>
                                                        {
                                                            this.state.diet.map((row,index) => {
                                                                return (
                                                                    <span className={self.props.data.diet == row?"specific planordercontent":"planordercontent"}  key={index} onClick={()=>this.save('diet',row)}>{row}</span>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                </div>
                                            </Row>
                                        </Col>
                                        <Col lg={3}>
                                            <Progress style={{height:40,borderWidth:1,marginTop:'10%'}} value={50}/>
                                        </Col>
                                    </Row>

                                    <Row className="planorderrow">
                                        <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                                            <Row className="sub">
                                                <Col lg={12}>
                                                    <p className="subtitle">Other : </p>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <div className="planordercell" style={{paddingTop:10}}>
                                                    <Col lg={12}>
                                                        {
                                                            this.state.other.map((row,index) => {
                                                                return (
                                                                    <span className={self.props.data.other == row?"specific planordercontent":"planordercontent"} key={index} onClick={()=>this.save('other',row)}>{row}</span>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                </div>
                                            </Row>
                                        </Col>
                                    </Row>
                                    
                                    <Row className="planorderrow">
                                        <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                                            <Row className="title"><Col><p>Advanced Directives/POLST : </p></Col></Row>
                                            <Row className="sub">
                                                <Col lg={12}>
                                                    <Row className="directive">
                                                        <Col>
                                                            <p>(A)</p>
                                                            <p>Cardiopulmonary</p>
                                                            <p>Resuscitation</p>
                                                        </Col>
                                                        <Button color="default">
                                                            <FontAwesomeIcon icon={faCheckCircle}></FontAwesomeIcon>
                                                        </Button>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <div className="planordercell" style={{paddingTop:80}}>
                                                    <Col lg={12}>
                                                        {
                                                            this.state.cardio.map((row,index) => {
                                                                return (
                                                                    <span key={index} className={(self.props.data.cardio == row || (index == 0 && !self.props.data.cardio))?"specific planordercontent":"planordercontent"} onClick={()=>this.save('cardio',row)}>{row}</span>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                </div>
                                            </Row>
                                        </Col>
                                    </Row>

                                    <Row className="planorderrow">
                                        <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                                            <Row className="sub">
                                                <Col lg={12}>
                                                    <Row className="directive">
                                                        <Col>
                                                            <p>(B)</p>
                                                            <p>Medical</p>
                                                            <p>Interventions</p>
                                                        </Col>
                                                        <Button color="default">
                                                            <FontAwesomeIcon icon={faCheckCircle}></FontAwesomeIcon>
                                                        </Button>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <div className="planordercell" style={{paddingTop:10}}>
                                                    <Col lg={12}>
                                                        {
                                                            this.state.medical.map((row,index) => {
                                                                return (
                                                                    <span key={index} className="planordercontent" className={(self.props.data.medical == row || (index == 0 && !self.props.data.medical))?"specific planordercontent":"planordercontent"} onClick={()=>this.save('medical',row)}>{row}</span>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                </div>
                                            </Row>
                                        </Col>
                                    </Row>

                                    <Row className="planorderrow">
                                        <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                                            <Row className="sub">
                                                <Col lg={12}>
                                                    <Row className="directive">
                                                        <Col>
                                                            <p>(C)</p>
                                                            <p>Antibiotics</p>
                                                        </Col>
                                                        <Button color="default">
                                                            <FontAwesomeIcon icon={faCheckCircle}></FontAwesomeIcon>
                                                        </Button>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <div className="planordercell" style={{paddingTop:10}}>
                                                    <Col lg={12}>
                                                        {
                                                            this.state.anti.map((row,index) => {
                                                                return (
                                                                    <span key={index} className={(self.props.data.anti == row || (index == 0 && !self.props.data.anti))?"specific planordercontent":"planordercontent"} onClick={()=>this.save('anti',row)}>{row}</span>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                </div>
                                            </Row>
                                        </Col>
                                    </Row>
                                    
                                    <Row className="planorderrow">
                                        <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                                            <Row className="sub">
                                                <Col lg={12}>
                                                    <Row className="directive">
                                                        <Col>
                                                            <p>(D)</p>
                                                            <p>Affinitically</p>
                                                            <p>Administratered</p>
                                                            <p>Nutrition / Fluid</p>
                                                        </Col>
                                                        <Button color="default">
                                                            <FontAwesomeIcon icon={faCheckCircle}></FontAwesomeIcon>
                                                        </Button>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <div className="planordercell" style={{paddingTop:10}}>
                                                    <Col lg={12}>
                                                        {
                                                            this.state.affinity.map((row,index) => {
                                                                return (
                                                                    <span key={index} className="planordercontent" className={(self.props.data.affinity == row || (index == 0 && !self.props.data.affinity))?"specific planordercontent":"planordercontent"} onClick={()=>this.save('affinity',row)}>{row}</span>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                </div>
                                            </Row>
                                        </Col>
                                    </Row>
                                    
                                    <Row className="planorderrow">
                                        <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                                            <Row className="title">
                                                <Col lg={12}>
                                                    <p>MISC : </p>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <div className="planordercell" style={{paddingTop:10}}>
                                                    <Col lg={12}>
                                                        {
                                                            this.state.misc.map((row,index) => {
                                                                return (
                                                                    <span key={index} className={self.props.data.misc == row?"specific planordercontent":"planordercontent"} onClick={()=>this.save('misc',row)}>{row}</span>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                </div>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row className="planorderrow">
                                        <Col lg={3} md={3} sm={4} xs={6} className="planorderheader">
                                            <Row className="title">
                                                <Col lg={12}>
                                                    <p>Deposite : <FontAwesomeIcon icon={faPlus} style={{color:'#4168f7',cursor:'pointer'}} onClick={this.adddispose}></FontAwesomeIcon></p>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <div className="planordercell" style={{paddingTop:10,width:'100%'}}>
                                                    <Col lg={12}>
                                                        <Table responsive bordered style={{marginBottom:0}}>
                                                            <thead style={{backgroundColor:"#ececec"}}>
                                                                <th>Provider</th>
                                                                <th>Location</th>
                                                                <th>Select Form</th>
                                                            </thead>
                                                            <tbody>
                                                                {
                                                                    this.props.data.dispose && this.props.data.dispose.map((row,index)=>{
                                                                        return (
                                                                            <tr key={index}>
                                                                                <td>{row.provider}</td>
                                                                                <td>{row.location}</td>
                                                                                <td>{row.day?'Day:' + row.day + "/":""}{row.week?"Week:" + row.week + "/":""}{row.month?"Month:" + row.month:""}</td>
                                                                            </tr>
                                                                        )
                                                                    })
                                                                }
                                                            </tbody>
                                                        </Table>
                                                    </Col>
                                                </div>
                                            </Row>
                                        </Col>
                                    </Row>
                                    
                                    <AssessmentTable assessments={this.props.assessments} other={this.props.data.other} showproblem={this.props.showproblem} save={this.props.save}/>
                                </Col>
                                <Col className="ordercontainer" lg={2} md={3} sm={12} xs={12} style={{paddingTop:10,paddingBottom:10}}>
                                    <div className="neworder" style={{height:"50%"}}>
                                        <div className="orderheader">
                                            Current Order(s)
                                        </div>
                                        <div className="orderbody"></div>
                                    </div>
                                    <div className="neworder" style={{height:"50%"}}>
                                        <div className="orderheader">
                                            New Order(s)
                                        </div>
                                        <div className="orderbody">
                                            {
                                                this.props.assessments.primary.map((row,index) => {
                                                    return (
                                                        <Row className="orderitems">
                                                            <Col lg={12} className="itemtitle" lg={12}>{row.CommonName?row.CommonName:row.ShortDesc}</Col>
                                                            <Col lg={12} className="itemcontent">
                                                                {
                                                                    row.labs && row.labs.map((content,index)=>{
                                                                        return (
                                                                            <Row key={index}>
                                                                                <Col>{content.CommonName?content.CommonName:content.BillDesc} (Laboratory)</Col>
                                                                            </Row>
                                                                        )
                                                                    })
                                                                }
                                                                {
                                                                    row.imaging && row.imaging.map((content,index)=>{
                                                                        return (
                                                                            <Row key={index}>
                                                                                <Col>{content.CommonName?content.CommonName:content.BillDesc} (Imaging)</Col>
                                                                            </Row>
                                                                        )
                                                                    })
                                                                }

                                                                {
                                                                    row.meds && row.meds.map((content,index)=>{
                                                                        return (
                                                                            <Row key={index}>
                                                                                <Col>{content.name} ({content.dosage}) (Medicine)</Col>
                                                                            </Row>
                                                                        )
                                                                    })
                                                                }
                                                            </Col>
                                                        </Row>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                    
                                </Col>
                            </Row>
                        </Col>
                    )
                }
                
                {
                    this.props.data.freetextenable && (
                        <Col className="planorder" dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></Col>
                    )
                }
                <Modal isOpen={this.state.adddispose}>
                    <ModalHeader toggle={this.closedispose}>Add Deposite</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup row>
                                <Label lg={4}>Provider</Label>
                                <Col>
                                    <Typeahead 
                                    labelKey="label" 
                                    id="id" 
                                    options={this.state.providers} 
                                    placeholder="Type in Provider"
                                    onInputChange={(value)=>this.handleChangedispose('provider',value)}
                                    onChange={(selected)=>this.handleChangedispose('provider',selected[0].label)}
                                    defaultInputValue={this.dispose.provider}
                                    ></Typeahead>
                                </Col>                                
                            </FormGroup>
                            <FormGroup row>
                                <Label lg={4}>Location</Label>
                                <Col>
                                    <select className="form-control" onChange={(e)=>this.handleChangedispose('location',e.target.value)} defaultValue={this.dispose.location}>
                                        {
                                            this.props.location.map((row,index)=>{
                                                return (
                                                    <option value={row.label}>{row.label}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Col>
                                   <Row>
                                       <Label lg={4}>Day</Label>
                                        <Col>
                                            <select className="form-control" onChange={(e)=>this.handleChangedispose('day',e.target.value)} defaultValue={this.dispose.day}>
                                                <option value=""></option>
                                                {
                                                    _.range(1,30).map(value=>
                                                        <option value={value}>{value}</option>
                                                    )
                                                }
                                            </select>
                                       </Col>
                                   </Row>
                                </Col>
                                <Col>
                                   <Row>
                                       <Label lg={4}>Week</Label>
                                        <Col>
                                            <select className="form-control" onChange={(e)=>this.handleChangedispose('week',e.target.value)} defaultValue={this.dispose.week}>
                                                <option value=""></option>
                                                {
                                                    _.range(1,5).map(value=>
                                                        <option value={value}>{value}</option>
                                                    )
                                                }
                                            </select>
                                       </Col>
                                   </Row>
                                </Col>
                                <Col>
                                    <Row>
                                       <Label lg={4}>Month</Label>
                                        <Col>
                                            <select className="form-control" onChange={(e)=>this.handleChangedispose('month',e.target.value)} defaultValue={this.dispose.month}>
                                                {
                                                   this.state.month.map(value=>
                                                        <option value={value}>{value}</option>
                                                    )
                                                }
                                            </select>
                                       </Col>
                                   </Row>
                                </Col>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.closedispose}>Cancel</Button>
                        <Button color="success" onClick={this.savedispose}>Save</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.freetext} size="lg">
                    <ModalHeader toggle={this.closefreetext}>Add Free Text</ModalHeader>
                    <ModalBody>
                        <Editor 
                            onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                            editorStyle={{minHeight:300}}
                            >
                        </Editor>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.closefreetext}>Cancel</Button>
                        <Button color="success" style={{marginLeft:10}} onClick={this.addfreetextdata}>Save</Button>
                    </ModalFooter>
                </Modal>
            </Card>
        )
    }
}

export default PO;