import React from 'react';
import Card from '../Card';
import {ReviewSide,EditExam} from '../chartboxcomponent/action'
import {Row,Col,Button,Modal,ModalHeader,ModalBody,ModalFooter} from 'reactstrap';
import * as Problem from '../../action/Problem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo,faTimes,faFolder} from '@fortawesome/free-solid-svg-icons';
import {Editor} from 'react-draft-wysiwyg';
import {convertToRaw} from 'draft-js';
import draftToHTML from 'draftjs-to-html';
import $ from 'jquery';
class PE extends React.Component
{
    editdata = "";
    editmasterfile = {};
    ids = [];
    freetext = "";
    constructor(props)
    {
        super(props);
        this.state = {
            edit:{},
            masteredit:false,
            updated:false,
            freetextview:false
        }
        this.updateinfo();
    }

    componentWillReceiveProps(props)
    {
        if(props.updated && props.selectedsection == 10 && props.macro.SectionId == 10)
        {
            console.log('macro',props.macro);
            let self = this;
            window.setTimeout(()=>{
                let Subsectionlist = []; let nodeidlist = [];
                
                

                if(props.macro.SubsectionId)
                {
                    Subsectionlist.push(props.macro.SubsectionId);
                    var data = {};
                    data[props.macro.SubsectionNodeId] = props.macro.Content;
                    nodeidlist.push(data);
                }
                
                for(let item in props.macro.list)
                {
                    Subsectionlist.push(props.macro.list[item].SubsectionId);
                }

                let totaldata = this.props.PE.data;
                for(let item in totaldata)
                {
                    if(Subsectionlist.indexOf(totaldata[item].Id) > -1)
                    {
                        for(let itemlist in totaldata[item].list)
                        {
                            if(!totaldata[item].list[itemlist].ParentNodeId)
                            {
                                totaldata = this.setnormalformacro(totaldata,item,totaldata[item].list[itemlist].Id,true);
                            }
                        }
                    }
                }

                // for(let itemsection in props.macro.list)
                // {
                //     if(Subsectionlist.indexOf(props.macro.list[itemsection].SubsectionId) > -1)
                //     {
                //         if(props.macro.list[itemsection].SubsectionNodeId)
                //         {
                //             nodeidlist[Subsectionlist.indexOf(props.macro.list[itemsection].SubsectionId)][props.macro.list[itemsection].SubsectionNodeId] = props.macro.list[itemsection].Content;
                //         }
                //     }
                //     else
                //     {
                //         Subsectionlist.push(props.macro.list[itemsection].SubsectionId);
                //         var data = {};
                //         data[props.macro.list[itemsection].SubsectionNodeId] = props.macro.list[itemsection].Content;
                //         nodeidlist.push(data);
                //     }
                // }
                // console.log('subsectionlist',Subsectionlist);
                // console.log('penodelist',nodeidlist);
                // let dataPE = props.PE;
                // for(let item in dataPE.data)
                // {
                //     if(Subsectionlist.indexOf(dataPE.data[item].Id) > -1)
                //     {
                //         var nodelist = nodeidlist[Subsectionlist.indexOf(dataPE.data[item].Id)];
                //         for(let itemlist in dataPE.data[item].list)
                //         {
                //             if(nodelist[dataPE.data[item].list[itemlist].Id])
                //             {
                //                 dataPE.data[item].list[itemlist].text = nodelist[dataPE.data[item].list[itemlist].Id];
                //             }    
                //         }
                //     }           
                // }

                self.props.save('PE',{data:totaldata,freetextview:false},10);
            },100)
        }
    }

    updateinfo = () => {
        let self = this;
        Problem.getmasternote(10).then(function(data){
            let edit = self.state.edit;
            data = data.data;
            for(let item in data)
            {
                data[item].text = "";
                edit[item] = false;
            }

            self.props.save("PE",{data:data,freetextview:false,freetext:"",freetextenable:false,freetextdata:""},10);
            
            self.setState({
                edit:edit
            })
        })
    }

    viewtree = (key) => {
        let edit = this.state.edit;
        edit[key] = !edit[key];
        this.setState({edit:edit})
    }

    onEditorStateChange = (state) => {
        this.freetext = draftToHTML(convertToRaw(state.getCurrentContent()));
    }

    setnormalformacro = (data,name,Id,normal) => {
        var parentid = [Id];
        var dataparent = {};
        for(let item in data[name].list)
        {
            if(!Id || data[name].list[item].Id == Id || parentid.indexOf(data[name].list[item].ParentNodeId) > -1)
            {
                data[name].list[item].normal = normal;

                if(!data[name].list[item].normal)
                {
                    data[name].list[item].text = "";
                }

                parentid.push(data[name].list[item].Id);
                if(!dataparent[data[name].list[item].Id])
                {
                    dataparent[data[name].list[item].Id] = [];
                }

                if(data[name].list[item].ParentNodeId && parentid.indexOf(data[name].list[item].ParentNodeId) > -1)
                {
                    dataparent[parentid[parentid.indexOf(data[name].list[item].ParentNodeId)]].push(data[name].list[item].DefaultContent);
                }
                
                dataparent[data[name].list[item].Id] = [data[name].list[item].DefaultContent];
                
            }
        }
        console.log(dataparent);
        for(let item in data[name].list)
        {
            if(parentid.indexOf(data[name].list[item].Id) > -1)
            {
                if(!data[name].list[item].normal)
                {
                    continue;
                }

                if(data[name].list[item].DefaultContent)
                {
                    data[name].list[item].text = data[name].list[item].DefaultContent;
                }
                else
                {
                    data[name].list[item].text = this.gettitle(dataparent[data[name].list[item].Id]);
                }
            }
        }

        return data;
    }

    setnormal = (name,Id,normal) => {
        var data = this.props.PE.data;
        var parentid = [Id];
        var dataparent = {};
        for(let item in data[name].list)
        {
            if(!Id || data[name].list[item].Id == Id || parentid.indexOf(data[name].list[item].ParentNodeId) > -1)
            {
                data[name].list[item].normal = normal;

                if(!data[name].list[item].normal)
                {
                    data[name].list[item].text = "";
                }

                parentid.push(data[name].list[item].Id);
                if(!dataparent[data[name].list[item].Id])
                {
                    dataparent[data[name].list[item].Id] = [];
                }

                if(data[name].list[item].ParentNodeId && parentid.indexOf(data[name].list[item].ParentNodeId) > -1)
                {
                    dataparent[parentid[parentid.indexOf(data[name].list[item].ParentNodeId)]].push(data[name].list[item].DefaultContent);
                }
                
                dataparent[data[name].list[item].Id] = [data[name].list[item].DefaultContent];
                
            }
        }
        console.log(dataparent);
        for(let item in data[name].list)
        {
            if(parentid.indexOf(data[name].list[item].Id) > -1)
            {
                if(!data[name].list[item].normal)
                {
                    continue;
                }

                if(data[name].list[item].DefaultContent)
                {
                    data[name].list[item].text = data[name].list[item].DefaultContent;
                }
                else
                {
                    data[name].list[item].text = this.gettitle(dataparent[data[name].list[item].Id]);
                }
            }
        }

        console.log('PE',data);
        this.props.save('PE',{data:data},10);
        this.setState({
            updated:!this.state.updated
        })
    }

    gettitle = (data) => {
        let titlearray = [];
        for(let item in data)
        {
            if(data[item])
            {
                titlearray.push(data[item]);
            }
        }

        return titlearray.join(',');
    }

    toggle = (e) => {
        $(e.target).parents('.tree-node').eq(0).find('.inner-note-tree').eq(0).toggle();
    }
    
    showmasteredit = () => {
        if(!this.state.masteredit)
        {
            this.editmasterfile = JSON.parse(JSON.stringify(this.props.PE.data));
            this.ids = [];
        }

        this.setState({
            masteredit:!this.state.masteredit
        })
    }
    
    getdatalistforedit(name,parent)
    {
        var datalist = [];
        for(let item in this.editmasterfile[name].list)
        {
            if(!parent)
            {
                if(!this.editmasterfile[name].list[item].ParentNodeId)
                {
                    datalist.push(this.editmasterfile[name].list[item]);
                }
            }

            else if(this.editmasterfile[name].list[item].ParentNodeId == parent)
            {
                datalist.push(this.editmasterfile[name].list[item]);
            }
        }
        
        return datalist;
    }
    getdatalist(name,parent)
    {
        var datalist = [];
        for(let item in this.props.PE.data[name].list)
        {
            if(!parent)
            {
                if(!this.props.PE.data[name].list[item].ParentNodeId)
                {
                    datalist.push(this.props.PE.data[name].list[item]);
                }
            }

            else if(this.props.PE.data[name].list[item].ParentNodeId == parent)
            {
                datalist.push(this.props.PE.data[name].list[item]);
            }
        }
        
        return datalist;
    }

    getnormal = (name,parentid) => 
    {
        var enable = true;
        for(let item in this.props.PE.data[name].list)
        {
            if(!parentid)
            {
                if(!this.props.PE.data[name].list[item].normal)
                {
                    enable = false;
                    break;
                }
            }
            else
            {
                if(this.props.PE.data[name].list[item].Id == parentid)
                {
                    if(!this.props.PE.data[name].list[item].normal)
                    {
                        enable = false;
                    }
                }
            }
        }

        return enable;
    }

    handleChange = (value) => {
        this.editdata = value;
    }

    savechange = (name,id) => {
        var data = this.props.PE.data;
        for(let item in data[name].list)
        {
            if(data[name].list[item].Id == id)
            {
                data[name].list[item].text = this.editdata;
            }
        }

        this.props.save('PE',{data:data},10);
    }

    addsubfolder = (name,id,subfolder) => {
        var data = {};
        
        var maxid = this.getmaxid();

        data = {Name:"",ParentNodeId:id,ParentSubsectionId:this.editmasterfile[name].Id,IsSubfolder:subfolder,DefaultContent:"",Id:maxid + 1,IsBullet:true,NodeOrder:1};
        this.editmasterfile[name].list.push(data);

        this.setState({
            updated:!this.state.updated
        })
    }

    handleEditChange = (name,id,value,argument) => 
    {
        console.log(value);
        for(let item in this.editmasterfile[name].list)
        {
            if(this.editmasterfile[name].list[item].Id == id)
            {
                this.editmasterfile[name].list[item][argument] = value;
                break;
            }
        }
    }

    getmaxid = () => {
        let max = 0;
        for(let item in this.editmasterfile)
        {
            for(let itemlist in this.editmasterfile[item].list)
            {
                if(this.editmasterfile[item].list[itemlist].Id > max)
                {
                    max = this.editmasterfile[item].list[itemlist].Id;
                }
            }
        }

        return max;
    }

    delete = (name,id) => {
        let idarray = [id];
        let index = 0;
        while(this.editmasterfile[name].list[index])
        {   
            if(idarray.indexOf(this.editmasterfile[name].list[index].Id) > -1 || idarray.indexOf(this.editmasterfile[name].list[index].ParentNodeId) > -1)
            {   
                idarray.push(this.editmasterfile[name].list[index].Id);
                this.editmasterfile[name].list.splice(index,1);
            }
            else
            {
                index++;
            }
        }

        this.setState({
            updated:!this.state.updated
        })
    }

    savemasterfile = () => {
        let self = this;
        
        let list = [];

        var enable = true;
        console.log(this.editmasterfile);
        for(let item in this.editmasterfile)
        {
            for(let listitem in this.editmasterfile[item].list)
            {
                if(this.editmasterfile[item].list[listitem].Name)
                {
                    list.push(this.editmasterfile[item].list[listitem]);
                }
                else
                {
                    enable = false;
                }
            }
        }

        if(!enable)
        {
            alert('Please type in all name field in master file');
            return;
        }

        Problem.savemasterfile(list).then(function(res){
            self.props.save('PE',{data:self.editmasterfile});
            self.setState({
                masteredit:false
            })
        })
    }   

    displaytreeforedit = (name,data,parentid) => {
        let self = this;
        console.log(name);
        return (
            <ul className="inner-note-tree">
                {
                    data.map((row,index)=>{
                        let itemlist = self.getdatalistforedit(name,row.Id);                        
                        return (
                            <li className="tree-node pe-item-node pe-subfolder-node">
                                <Row className={itemlist.length > 0?"title":""} onClick={(e)=>self.toggle(e)}>
                                    <Col lg={4}>
                                        <Row>
                                            <Col lg={1}>
                                            {
                                                row.IsSubfolder == 1 && (
                                                    <FontAwesomeIcon icon={faFolder} className="folder"></FontAwesomeIcon>    
                                                )
                                            }
                                            </Col>
                                            <Col lg={10}>
                                                {/* <div contentEditable={true} className="editcontent" onChange={(e)=>this.handleEditChange(name,row.Id,e.target.value,"Name")}></div> */}
                                                <input className="form-control" defaultValue={row.Name} onChange = {(e)=>this.handleEditChange(name,row.Id,e.target.value,"Name")}></input>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col lg={5}>
                                        <input className="form-control" defaultValue={row.DefaultContent} onChange = {(e)=>this.handleEditChange(name,row.Id,e.target.value,"DefaultContent")}></input>
                                    </Col>
                                    <Col>
                                        
                                    </Col>
                                    <Col>
                                        <span className="addfinding" onClick={()=>this.delete(name,row.Id)}>Delete</span>
                                    </Col>
                                </Row>
                                {
                                    row.IsSubfolder == 1 && self.displaytreeforedit(name,itemlist,row.Id)
                                }
                            </li>
                        )
                    })
                }
                <li className="tree-node pe-item-node pe-subfolder-node">
                    <span className="addfinding" onClick={()=>this.addsubfolder(name,parentid,0)}>Add New Finding</span>
                </li>
                <li className="tree-node pe-item-node pe-subfolder-node">
                    <span className="addfinding" onClick={()=>this.addsubfolder(name,parentid,1)}>Add Subfolder</span>
                </li>
            </ul>
        )
    }

    displaytree = (name,data,parentid) => {   
        let self = this;
        return (
            <ul className={!parentid?'note-tree':'inner-note-tree'}>
                {
                    data.length > 0 && 
                    (<li className="first-node tree-node">
                        <Row>
                            <Col lg={4}><button className={this.getnormal(name,parentid)?"btn btn-xs btn-set-normal btn-info":"btn btn-xs btn-set-normal btn-default"} onClick={()=>this.setnormal(name,parentid,!this.getnormal(name,parentid))}>N</button><span>Set All Normal</span></Col>
                        </Row>
                    </li>)
                }
                {
                   data.map((row,index)=>{
                       let rowlist = self.getdatalist(name,row.Id);
                        return (
                            <li className="tree-node pe-item-node pe-subfolder-node">
                                <Row>
                                    <Col lg={4}>
                                        <button className={row.normal?"btn btn-xs btn-set-normal btn-info":"btn btn-xs btn-set-normal btn-default"} onClick={()=>self.setnormal(name,row.Id,!row.normal)}>N</button>
                                        <span>{row.Name}</span>
                                        {
                                            rowlist.length > 0 && (<span style={{marginLeft:10,cursor:'pointer'}} onClick={(e)=>self.toggle(e)}>>></span>)}
                                    </Col>
                                    <Col>
                                        <input className="form-control" defaultValue={row.text} onChange={(e)=>this.handleChange(e.target.value)} onBlur={(e)=>this.savechange(name,row.Id)}></input>
                                    </Col>
                                </Row>

                                {
                                    rowlist.length > 0 && self.displaytree(name,rowlist,row.Id)
                                }
                            </li>
                        )
                   })
                }
            </ul>
        )
    }

    addfreetext = () => {
        if(this.freetext == '<p></p>' || !this.freetext)
        {
            this.props.save('PE',{
                freetext:this.freetext,freetextview:false
            },10)
        }
        else
        {
            this.props.save('PE',{
                freetext:this.freetext,freetextview:true
            },10)
        }
        this.setState({freetextview:!this.state.freetextview})
    }
    
    editable = ()=>{
        this.setState({
            freetextview:!this.state.freetextview
        })
    }

    default = (name,index) => {
        this.setnormal(name,this.props.PE.data[name].list[index].Id,true);
    }

    deletedata = (name,index) => {
        let data = this.props.PE.data;
        data[name].list[index].text = "";

        this.props.save("PE",{data:data},10);
    }

    checkparam = (data) => {
        let enable = false;
        for(let item in data.list)
        {
            if(!data.list[item].ParentNodeId)
            {
                if(data.list[item].text)
                {
                    enable = true;
                }
            }
        }

        return enable;
    }

    render()
    {
        let self = this;
        return (
        <Card title="Physical Exam" tag={ReviewSide} edit={this.showmasteredit} undo={()=>this.updateinfo()} editable={()=>this.editable()} showmacro={this.props.showmacro} sectionId={10} setviewparam = {(param)=>{
            let pe = this.props.PE; pe.param = param; this.props.save('PE',pe,10);
        }}>
            <Row>
                {
                    this.props.vitalsign.freetextenable && (
                        <Col dangerouslySetInnerHTML={{__html:this.props.vitalsign.freetextdata}}></Col>        
                    )
                }
                {
                    !this.props.vitalsign.freetextenable && (
                        <Col>
                            <EditExam data={this.props.vitalsign.data} save={(data)=>this.props.save("VitalSign",{data:data},10)} diagnose={this.props.diagnose} savediagnosis = {(data)=>this.props.save('Assessments',data,8)}/>
                        </Col>
                    )
                }
               
            </Row>
            <Row>
                <Col lg={12}>
                {
                   (!this.props.PE.freetextenable && !this.props.PE.freetextview && this.props.PE.param != 'paragraph') && Object.keys(this.props.PE.data).map((keyName,index)=>{
                        return(
                            <Row className="signentry">
                                <Col lg={2} onClick={()=>self.viewtree(keyName)} className="description">{this.props.PE.data[keyName].Name}</Col>
                                <Col>
                                {
                                    !this.state.edit[keyName] && this.props.PE.data[keyName].list.map((item,indexitem)=>{
                                        if(!item.ParentNodeId)
                                        {
                                            return (
                                                <Row className="description">
                                                    <Col>
                                                        <div className="description-container"  onClick={()=>self.viewtree(keyName)}>
                                                            <span className="param" style={{textDecoration:'underline'}}>{item.Name}</span>
                                                            <span className="param">{item.text}</span>
                                                        </div>
                                                    </Col>
                                                    <Button color="default"  onClick={()=>self.viewtree(keyName)}>Expand</Button>
                                                    <Col lg={2}>
                                                        <Row>
                                                            <Col><span className="default" onClick={()=>this.default(keyName,indexitem)} style={{cursor:'pointer',textDecoration:'underline'}}>Default</span></Col>
                                                            <Col><Button color="secondary" onClick={()=>this.deletedata(keyName,indexitem)}><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></Button></Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            )
                                        }
                                    })
                                }
                                {
                                    this.state.edit[keyName] && (
                                        <Row className="pe_tree">
                                            {this.displaytree(keyName,this.getdatalist(keyName))}
                                        </Row>)
                                }
                                </Col>
                            </Row>
                        )
                    })
                }
                {
                    (!this.props.PE.freetextenable && !this.props.PE.freetextview && this.props.PE.param == 'paragraph') && (
                        Object.keys(this.props.PE.data).map((keyName,index)=>{
                            if(this.checkparam(this.props.PE.data[keyName]))
                            {
                                return (
                                    <span key={index}>
                                        <span className="title">{this.props.PE.data[keyName].Name}</span>
                                        {
                                            this.props.PE.data[keyName].list.map((item,indexitem)=>{
                                                if(!item.ParentNodeId && item.text)
                                                {
                                                    return (
                                                        <span key={indexitem}>
                                                            <span className="itemname">{item.Name}</span> - 
                                                            <span>{item.text}</span>
                                                        </span>
                                                    )
                                                }
                                            })
                                        }
                                    </span>
                                )
                            }
                        })
                    )
                }
                </Col>
                {(!this.props.PE.freetextenable && this.props.PE.freetextview) && (
                    <Col lg={12} dangerouslySetInnerHTML={{__html:this.props.PE.freetext}}></Col>
                )}
                {
                    this.props.PE.freetextenable && (
                        <Col lg={12} dangerouslySetInnerHTML={{__html:this.props.PE.freetextdata}}></Col>
                    )
                }
            </Row>
            <Modal isOpen={this.state.masteredit} size="lg">
                <ModalHeader toggle={this.showmasteredit}>PE Master File</ModalHeader>
                <ModalBody>
                    <Row className="pe_tree" style={{marginTop:50,overflowY:"auto"}}>
                        <ul className="note-tree">
                            {
                                Object.keys(this.editmasterfile).map((row,index)=>{
                                    return (
                                        <li className="tree-node" key={index}>
                                            <Row className="title" onClick={(e)=>self.toggle(e)}>
                                                <Col lg={4}>
                                                    <FontAwesomeIcon icon={faFolder} className="folder"></FontAwesomeIcon>
                                                    {this.editmasterfile[row].Name}
                                                </Col>
                                            </Row>
                                            {
                                                self.displaytreeforedit(row,self.getdatalistforedit(row))
                                            }
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={this.showmasteredit}>Cancel</Button>
                    <Button color="success" style={{marginLeft:10}} onClick={this.savemasterfile}>Save</Button>
                </ModalFooter>
            </Modal>
            <Modal isOpen={this.state.freetextview} size="lg">
                <ModalHeader toggle={this.editable}>Add Free Text</ModalHeader>
                <ModalBody>
                    <Editor 
                        onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                        editorStyle={{minHeight:300}}
                        >
                    </Editor>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={this.editable}>Cancel</Button>
                    <Button color="success" style={{marginLeft:10}} onClick={this.addfreetext}>Save</Button>
                </ModalFooter>
            </Modal>

        </Card>)
    }
}


export default PE;