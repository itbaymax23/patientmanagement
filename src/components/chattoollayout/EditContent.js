import React from 'react';
import {Row,Col,Button,Modal,ModalBody,ModalHeader} from 'reactstrap'
import DynamicEditor from './DynamicEditor';
import TemplateList from './TemplateEditList';
import * as Macro from '../../action/Macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus,faTimes } from '@fortawesome/free-solid-svg-icons';
class EditContent extends React.Component
{
    addtitle = "";
    data = "";
    indexid = "";
    content = "";
    list = "";
    problemid = '';
    listarray = [];
    constructor(props)
    {
        super(props);
        this.state = {
            newline:['1','2'],
            complain:false,
            selected:-1,
            opendialog:false,
            listopen:false
        }
    }
    
    componentWillReceiveProps(props)
    {
        let self = this;
        if(props.sectionid == 1)
        {
            this.problemid = 'ICD10Items';
            let list = props.data.ICD10Items?props.data.ICD10Items:"";
            if(this.list != list)
            {
                this.list = list + "";
                if(this.list)
                {
                    Macro.getproblemlist(this.list).then(function(data){
                        this.listarray = data.data;
                    })
                }
            }
        }
    }
    

    save = (data,index) => {
        let array = this.props.data.Content.split(' . ');
        array[index] = data;
        this.props.save('Content',array.join(' . '));
    }

    newline = () => {
        let array = this.props.data.Content.split(' . ');
        if(this.state.selected > -1)
        {
            array.splice(this.state.selected,0,"");
        }
        else
        {
            array.push("");
        }
        this.props.save('Content',array.join(' . '));
    }

    select = (index) => {
        this.setState({
            selected:index
        })
    }

    moveup = () =>{
        let array = this.props.data.Content.split(' . ');
        if(this.state.selected > 0)
        {
            let data = array[this.state.selected - 1];
            array[this.state.selected - 1] = array[this.state.selected];
            array[this.state.selected] = data;
            this.setState({
                selected:this.state.selected - 1
            })

            this.props.save('Content',array.join(' . '));
        }
    }

    deleteline = () => {
        let array = this.props.data.Content.split(' . ');
        if(this.state.selected > -1)
        {
            array.splice(this.state.selected,1);
            this.props.save('Content',array.join(' . '));
        }
    }

    movedown = () => {
        let array = this.props.data.Content.split(' . ');
        if(this.state.selected > -1 && this.state.selected < array.length - 1)
        {
            let data = array[this.state.selected + 1];
            array[this.state.selected + 1] = array[this.state.selected];
            array[this.state.selected] = data;
            this.setState({
                selected:this.state.selected + 1
            })

            this.props.save('Content',array.join(' . '));
        }
    }

    complain_edit = () => {
        if(this.props.sectionid == '1')
        {
            this.title = "Problems";
            this.setState({
                listopen:true
            })
        }
    }

    select = (data) => {
        
    }
    render()
    {
        let self = this;
        return (
            <Row>
                <Col lg={12} className="editor_title">
                    Macro Editor ({this.props.label})
                </Col>
                <Col lg={12} className="edittool">
                    {
                        this.props.sectionid == '1' && (
                            <Button color="primary" onClick={this.complain_edit}>Complain Edit</Button>
                        )
                    }
                    {
                        this.props.sectionid == '2' && (
                            <Button color="primary">Associated Diagnoses</Button>
                        )
                    }
                    {
                        this.state.newline.indexOf(this.props.sectionid) > -1 && (
                            <Button color="primary" onClick={this.newline}>New Line</Button>
                        )
                    }
                    {
                        this.state.newline.indexOf(this.props.sectionid) > -1 && (
                            <Button color="primary" onClick={this.moveup}>Move Up</Button>
                        )
                    }
                    {
                        this.state.newline.indexOf(this.props.sectionid) > -1 && (
                            <Button color="primary" onClick={this.movedown}>Move down</Button>
                        )
                    } 
                    {
                        this.state.newline.indexOf(this.props.sectionid) > -1 && (
                            <Button color="primary" onClick={this.deleteline}>Delete Line</Button>
                        )
                    }
                    <Button color="primary">Save</Button> 
                </Col>
                <Col className="contenteditor">
                {
                    this.props.data.Content.split(' . ').map((row,index)=>{
                        return (
                            <Row>
                                <Col className={this.state.selected == index?"editoritem selecteditem":"editoritem"} onClick={()=>this.select(index)}>
                                    {
                                        self.state.newline.indexOf(self.props.sectionid) > -1 && (
                                            <DynamicEditor key={index} textdata={row} save={(data)=>this.save(data,index)}></DynamicEditor>
                                        )
                                    }
                                    
                                </Col>
                            </Row>
                        )
                    })
                }
                </Col>
                
                <Modal isOpen={this.state.listopen}>
                    <ModalHeader toggle={()=>this.setState({listopen:false})}>Edit List</ModalHeader>
                    <ModalBody>
                        <Row className="editdialog">
                            <Col lg={12}>{this.title} List</Col>
                            <Col lg={12}>
                                <Row>
                                    <Col lg={9} className="editlist"></Col>
                                    <Col lg={2} className="edittool">
                                        <Row>
                                            <Col lg={12}>
                                                <Button color="default">
                                                    <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                                                </Button>
                                            </Col>
                                            <Col lg={12}>
                                                <Button color="default">
                                                    <FontAwesomeIcon icon={faTimes} style={{color:"red"}}></FontAwesomeIcon>
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal>

               <TemplateList 
               open={this.state.opendialog} 
               sectionid={this.props.sectionid} 
               title={this.title} 
               close={()=>this.setState({opendialog:false})} 
               selected={this.list} 
               indexid={this.indexid}
               select={(data)=>this.select(data)}
               ></TemplateList> 
            </Row>
        )
    }
}

export default EditContent;