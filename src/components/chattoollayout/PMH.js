import React from 'react';
import Card from '../../components/Card';
import {Row,Col,Modal,ModalBody,ModalHeader,ModalFooter,Button,UncontrolledPopover,PopoverBody} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimesCircle,faPlus, faSortDown,faEdit,faBook,faTimes} from '@fortawesome/free-solid-svg-icons';
import HTMLParser from 'html-to-react';
import { DetailPage } from '../chartboxcomponent/action';
import BasicAction from '../chartboxcomponent/action/BasicAction';
import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {convertToRaw,EditorState,ContentState,convertFromHTML} from 'draft-js';
import draftToHTML from 'draftjs-to-html';

import * as Problem from '../../action/Problem';
import * as Macro from '../../action/Macro';
import $ from 'jquery';

class PMH extends React.Component
{
    inputdata = "";
    editdata = "";
    editdatatext = "";
    initdata = [];
    constructor(props)
    {
        super(props);
        this.state = {
            basichx:false,
            completehx:false,
            edit:false,
            edititem:false,
            param:{},
            commonproblem:[],
            common:false,
            updated:false,
            freetextstate:EditorState.createEmpty()
        }
    }

    componentDidMount()
    {
        let self = this;
        Problem.getproblemcommon().then(function(data){
            self.setState({
                commonproblem:data.data
            })
        })
    }

    
    onEditorStateChange = (state) => {
        this.editdata = draftToHTML(convertToRaw(state.getCurrentContent()));
        this.editdatatext = state.getCurrentContent().getPlainText();
    }

    edit = () => {
        let data = this.props.data;

        let state = EditorState.createEmpty();

        if(data.freetext)
        {
            state =  EditorState.createWithContent(
                ContentState.createFromBlockArray(
                convertFromHTML(data.freetext)
                )
            );
        }

        this.setState({
            edit:!this.state.edit,
            freetextstate:state
        })
    }

    toggle = () => {
        this.setState({
            edit:false,
            edititem:false
        })
    }
    addfreetext = () => {
        var data = this.props.data;
        if(this.state.edit)
        {
            data.freetext = this.editdata && this.editdatatext?this.editdata:"";
        }
        else if(this.state.edititem)
        {
            if(!data.textdata)
            {
                data.textdata = {};
            }
            data.textdata[this.state.selectedindex] = this.editdata && this.editdatatext?this.editdata:"";
        }

        this.props.save("PMH",data);
        this.setState({
            edit:false,
            edititem:false
        })
    }
    
    edititem = (id) => {
        let state = EditorState.createEmpty();
        let data = this.props.data;

        if(!data.textdata)
        {
            data.textdata = {};
        }

        if(data.textdata[id])
        {
            state =  EditorState.createWithContent(
                ContentState.createFromBlockArray(
                convertFromHTML(data.textdata[id])
                )
            );
        }

        this.setState({
            edititem:true,
            selectedindex:id,
            freetextstate:state
        })
    }

    delete = (index) => {
        let data = this.props.data;
        data.problem.splice(index,1);

        this.props.save("PMH",data)
    }

    setviewParam = (id,param) => {
        let paramarray = this.props.data.param;
        if(!paramarray)
        {
            paramarray = {};
        }
        paramarray[id] = param;
        this.props.save("PMH",{param:paramarray});
    }

    handleChange = (value) => {
        this.inputdata = value;
        let self = this;
        if(this.inputdata)
        {
            Problem.getproblems(this.inputdata).then(function(data){
                self.initdata = data.data;
                self.setState({
                    updated:!self.state.updated
                })
            })
        }
        else
        {
            this.initdata = JSON.parse(JSON.stringify(this.state.commonproblem));
            this.setState({
                updated:!this.state.updated
            })
        }
    }

    add = () => {
    //    this.inputdata = "";
    //    this.initdata = JSON.parse(JSON.stringify(this.state.commonproblem));
    //    this.setState({
    //        updated:!this.state.updated
    //    })
        this.props.showproblem('PMH',undefined,2);
    }

    isselected = (item) => {
        let problem = this.props.data.problem;
        for(let problemitem in problem)
        {
            if(problem[problemitem].Id == item.Id)
            {
                return problemitem;
            }
        }

        return -1;
    }

    addproblem = (item) => {
        let index = this.isselected(item);

        let problem = this.props.data.problem;
        if(index > -1)
        {
            problem.splice(index,1);
        }
        else
        {
            problem.push(item);
        }

        this.props.save('PMI',{problem:problem},3);
    } 

    componentWillReceiveProps(props)
    {
        if(props.updatemacro && props.selectedSectionId == 3 && props.macro.SectionId == 3)
        {
            if(props.macro.Content)
            {
                let data = props.data;
                let self = this;
                Macro.getproblemsformacro(props.macro.Content).then(function(result){
                    data.problem = result.data;
                    data.freetext = "";
                    data.textdata = {};
                    self.props.save("PMH",data,3);
                })
            }
        }
    }

    togglecomponent = (e) => {
        $(e.target).parents('.main_content').eq(0).find('.freetext').eq(0).toggle("slow");
    }
    
    basichx = (e) => {
        $(e.target).parents('.cardentry').eq(0).find('.freetext').each(function(){
            if($(this).css('display') != 'none')
            {
                $(this).toggle('slow');
            }
        })
    }

    completehx = (e) => {
        $(e.target).parents('.cardentry').eq(0).find('.freetext').each(function(){
            if($(this).css('display') == 'none')
            {
                $(this).toggle('slow');
            }
        })
    }

    undo = () => {
        let data = this.props.data;
        for(let item in data)
        {
            data[item] = false;
        }
        
        data.problem = [];
        this.props.save('PMH',data,3);
    }

    render()
    {
        let self = this;
        return (
        <Card title="Past Medical History" basichx={this.basichx} undo={this.undo} completehx={this.completehx} tag={BasicAction} add={this.add} freetext = {()=>this.edit()} edit={this.edit} macro={()=>this.props.showmacro(3)} id="addproblem">
            {
                !this.props.data.freetextenable && this.props.data.problem.map((row,index)=>{
                    return(
                        <Row className="default_container">
                            <div className="main_content">
                                <span className="title">{row.CommonName?row.CommonName:row.ShortDesc} ({row.Code})</span>
                                <span className="righticon" style={{marginLeft:5}} onClick={(e)=>this.togglecomponent(e)}>
                                    <FontAwesomeIcon icon={faSortDown}></FontAwesomeIcon>
                                </span>
                                <p className="freetext" dangerouslySetInnerHTML={{__html:this.props.data.textdata?this.props.data.textdata[row.Id]:''}}></p>
                                {/* {(!this.props.data.param || this.props.data.param[row.Id] != "paragraph") && (<p className="freetext" dangerouslySetInnerHTML={{__html:this.props.data.textdata?this.props.data.textdata[row.Id]:''}}></p>)}
                                <div className="freetext" style={{display:"inline-block"}} dangerouslySetInnerHTML={{__html:this.props.data.textdata?this.props.data.textdata[row.Id]:''}}></div>)} */}
                            </div>
                            <Col lg={5} md={5} sm={6} xs={6} style={{marginLeft:"auto"}}>
                                <Row>
                                    <div style={{marginLeft:"auto"}}>
                                        <Button color="primary" style={{marginLeft:10}} onClick={()=>this.edititem(row.Id)}>T</Button>
                                        <Button color="secondary" style={{marginLeft:10,fontSize:10,padding:"2px 3px"}} onClick={()=>this.delete(index)}><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></Button>
                                    </div>
                                </Row>
                            </Col>
                        </Row>
                    )
                })
            }
            {
                (!this.props.data.freetextenable && this.props.data.freetext) && (
                    <p dangerouslySetInnerHTML={{__html:this.props.data.freetext}}></p>
                )
            }
            {
                this.props.data.freetextenable && (
                    <p dangerouslySetInnerHTML={{__html:this.props.data.freetextdata}}></p>
                )
            }
            <Modal isOpen={this.state.edititem || this.state.edit} size="lg">
                <ModalHeader toggle={this.toggle}>Add Free Text</ModalHeader>
                <ModalBody>
                    <Editor 
                        onEditorStateChange={(state)=>this.onEditorStateChange(state)}
                        editorStyle={{minHeight:300}}
                        defaultEditorState={this.state.freetextstate}
                        >
                    </Editor>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    <Button color="success" style={{marginLeft:10}} onClick={this.addfreetext}>Save</Button>
                </ModalFooter>
            </Modal>
            {/* <UncontrolledPopover target="addproblem" trigger="legacy" placement="bottom" ref="addproblem">
                <PopoverBody>
                    <ul className="problemlist">
                        {
                            this.initdata.map((row,index)=>{
                                return (
                                    <li key={index} className={self.isselected(row) > -1?'checkeditem':''} onClick={()=>this.addproblem(row)}>{row.CommonName?row.CommonName:row.ShortDesc} ({row.Code})</li>
                                )
                            })
                        }
                    </ul>
                    <Row>
                        <Col>
                            <input className="form-control" placeholder="Search Problems Here" onChange={(e)=>this.handleChange(e.target.value)} defaultValue={this.inputdata}></input>
                        </Col>
                    </Row>
                </PopoverBody>
            </UncontrolledPopover> */}
            
        </Card>)
    }
}

export default PMH;