import React from 'react';
import { FaAlignJustify } from 'react-icons/fa';
import {Button} from 'reactstrap';
import {
  // UncontrolledTooltip,
  Nav,
  Navbar,
  NavItem,
  NavLink as BSNavLink,
} from 'reactstrap';
import bn from '../../utils/bemnames';
import $ from 'jquery';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import {Sticky,StickyContainer} from 'react-sticky';

const bem = bn.create('sidebar');

class Sidebar extends React.Component {
  state = {
    isOpenComponents: true,
    isOpenContents: true,
    isOpenPages: true,
    nav1:false,
    nav2:false,
    sticky:false,
    top:0
  };

  navcomponents = [
    { to: '/save_complete',name:'Save Complete',exact:false,action:this.props.savecomplete},
    { to: '/save_incomplete',name:'Save Incomplete',exact:false,action:this.props.saveincomplete},
    { to: '/preview',name:'Preview',exact:false,action:this.props.preview},
    { to: '/new_note',name:'New Note',exact:false,action:this.props.newnote},
    { to:"/loadnotes",name:"Load Notes",exact:false,action:this.props.loadnotes},
    { to: '/free_text',name:'Free Text',exact:false,action:this.props.freetext},
    { to: '/send',name:'Send',exact:false,action:this.props.preview},
    { to: '/template',name:'Template',exact:false,action:this.props.edittemplate}];
  
  navItems = [
    { to: '/', name: 'SOMR', exact: true,action:this.props.showpsg},
    { to: '/pomr', name: 'POMR', exact: false,action:this.props.showpsg},
    { to: '/summary', name: 'SUMMARY', exact: false,action:this.props.summary},
    { to: '/psg', name: 'PSG', exact: false,action:this.props.showpsg},
  ];
  
  navitem_second = [
    { to: '#CC',  name:'CC',  exact:false},
    { to: '#HPI', name:'HPI', exact:false},
    { to: '#PMH', name:'PMH', exact:false},
    { to: '#PSH', name:'PSH', exact:false},
    { to: '#MEDS',name:'Meds',exact:false},
    { to: '#ADR', name:'Adr', exact:false},
    { to: '#SH',  name:'SH',  exact:false},
    { to: "#FH" , name:"FH",  exact:false},
    { to: '#ROS', name:'ROS', exact:false},
    { to: '#PE',  name:'PE',  exact:false},
    { to: '#DD',  name:'DATA',exact:false},
    { to: '#ASSESSMENT',name:'Assessment',exact:false},
    { to: '#PO',name:'Plan / Orders',exact:false},
    { to: '#EMCODE',name:'E & M',exact:false}
  ]

  handleClick = name => () => {
    this.setState(prevState => {
      const isOpen = prevState[`isOpen${name}`];
      return {
        [`isOpen${name}`]: !isOpen,
      };
    });
  };

  componentDidMount()
  {
    console.log($('#nav2')[0].getBoundingClientRect());
    let documenttop = document.body.getBoundingClientRect();
    let top = $('#nav2')[0].getClientRects()[0].top;
    top = top - documenttop.top;
    this.setState(prevState=>{
      return {
        top:top
      }
    })
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillMount()
  {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = ()=> {
    if (window.scrollY > this.state.top) {
      this.setState(prevState => {
          return {
            scrollingLock: true
          }
        }
      );
    } else if (window.scrollY < this.state.top) {
      this.setState(prevState => {
        return {
          scrollingLock: false
        }
      }
    );
    }
  
  }

  navslide = (e,nav) => {
    this.setState(prevState => {
      if(!prevState[nav])
      {
        $('.navcontent' + nav).find('.navcontentcontainer').eq(0).slideUp();
      }
      else
      {
        $('.navcontent' + nav).find('.navcontentcontainer').eq(0).slideDown();
      }

      prevState[nav] = !prevState[nav];
      return prevState;
    })
  }

  render() {
    let style = {marginTop:40};
    if(this.state.scrollingLock)
    {
      style.position = "fixed";
      style.top = 0;
      style.marginTop = 10;
    }

    return (
      <aside className={bem.b()} style={{width:"100%"}}>
        <div className={bem.e('content') + " navcontentnav1"}>
          <Navbar>
              <span className="text-white" style={{marginTop:"auto"}}>
                Note
              </span>
              <Button color="outline-default" style={{marginLeft:"auto"}} onClick={(e)=>{this.props.saveposition()}}><FaAlignJustify/></Button>
          </Navbar>
          <div className="navcontentcontainer">
            <Nav className="navbar_container" vertical>
              {this.navItems.map(({ to, name, exact,action}, index) => (
                <NavItem key={index} className={bem.e('nav-item')} onClick={action}>
                    <span className="text-uppercase" style={{cursor:"pointer"}}>{name}</span>
                </NavItem>
              ))}
            </Nav>
            <Nav className="nav_content" vertical>
              {this.navcomponents.map(({ to, name, exact,action}, index) => (
                <NavItem key={index} className={bem.e('nav-item')} onClick={action}>
                    <span className="text-uppercase" style={{cursor:'pointer'}}>{name}</span>
                </NavItem>
              ))}
            </Nav>
          </div>
          
        </div>
        
        <div id="nav2" className={bem.e('content') + " navcontentnav2"} style={style}>
          <Navbar>
              <span className="text-white" style={{marginTop:"auto"}}>
                Navigation
              </span>
              <Button color="outline-default" style={{marginLeft:"auto"}} onClick={(e)=>{this.props.saveposition()}}><FaAlignJustify/></Button>
          </Navbar>
          <Nav className="nav_content navcontentcontainer" vertical>
            {this.navitem_second.map(({ to, name, exact}, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <AnchorLink href={to} className="nav-link">{name}</AnchorLink>
                </NavItem>
              ))}
          </Nav>
        </div>
         
      </aside>
      
    );
  }
}

export default Sidebar;
