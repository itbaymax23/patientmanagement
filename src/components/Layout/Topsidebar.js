import React from 'react';
import {
    Nav,
    NavLink as BSNavLink,
    Navbar,
    NavItem} from 'reactstrap';
import bn from '../../utils/bemnames';
import { NavLink } from 'react-router-dom';
const topsidebarcontent = [{
    to:"/",
    name:"Dashboard",
    exact:true,
    Icon:null
},{
    to:"/charting",
    name:"Charting",
    exact:false,
    Icon:null
},{
    to:"/reports",
    name:"Reports",
    exact:false,
    Icon:null
},{
    to:"/admin",
    name:"Administration",
    exact:false,
    Icon:null
}]

const bem = bn.create('sidebar');
class TopSidebar extends React.Component
{
   
    handleClick = name => () => {
        this.setState(prevState => {
            const isOpen = prevState[`isOpen${name}`];

            return {
            [`isOpen${name}`]: !isOpen,
            };
        });
    };

    render(){
        return (
            <div className="topsidebar">
                <Nav horizontal="horizontal">
                    {topsidebarcontent.map(({to,name,exact},index)=>(
                        <NavItem key={index} className={bem.e('nav-item')}>
                            <BSNavLink
                                id={`nav-item-${name}-${index}`}
                                tag={NavLink}
                                activeClassName="active"
                                exact={exact}
                                to={to}
                            >
                                <span>{name}</span>
                            </BSNavLink>
                        </NavItem>
                    ))}
                </Nav>
            </div>
        )
    }
}

export default TopSidebar;