import { Content,  Header} from '../../components/Layout';
import {Row,Col} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChevronUp} from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import TopSidebar from './Topsidebar';
import 'icheck/skins/all.css';
import {
  MdImportantDevices,
  // MdCardGiftcard,
  MdLoyalty,
} from 'react-icons/md';
import NotificationSystem from 'react-notification-system';
import { NOTIFICATION_SYSTEM_STYLE } from '../../utils/constants';
import {AppContainer} from 'react-hot-loader';


class MainLayout extends React.Component {
  state = {
    breadcrumbs:[{ name: 'Dashboard', active: true }]
  }

  static isSidebarOpen() {
    return document
      .querySelector('.cr-sidebar')
      .classList.contains('cr-sidebar--open');
  }

  componentWillReceiveProps({ breakpoint }) {
    
  }

  componentDidMount() {
    // setTimeout(() => {
    //   if (!this.notificationSystem) {
    //     return;
    //   }

    //   this.notificationSystem.addNotification({
    //     title: <MdImportantDevices />,
    //     message: 'Welome to Reduction Admin!',
    //     level: 'info',
    //   });
    // }, 1500);

    // setTimeout(() => {
    //   if (!this.notificationSystem) {
    //     return;
    //   }

    // //   this.notificationSystem.addNotification({
    // //     title: <MdLoyalty />,
    // //     message:
    // //       'Reduction is carefully designed template powered by React and Bootstrap4!',
    // //     level: 'info',
    // //   });
    // // }, 2500);
  }

 

  render() {
    const { children } = this.props;
    return (
      <AppContainer>
        <main className="cr-app bg-light">
          <Row style={{width:"100%",margin:0}}>
            <Col lg={12} style={{backgroundColor:"black"}}>
              <Row style={{paddingTop:10,paddingBottom:10}}>
                <Col lg={3} md={3} sm={6} xs={6} style={{paddingTop:8,paddingBottom:8}}>
                  <span style={{color:"white",marginLeft:12}}><FontAwesomeIcon icon={faChevronUp}></FontAwesomeIcon></span>
                  <span style={{color:"white",marginLeft:20}}>Welcome Admin</span>
                </Col>
                <Col lg={6} md={6} sm={4} xs={4} style={{textAlign:"center",paddingTop:8,paddingBottom:8}}>
                  <span style={{color:"white"}}>Washington International Medicine</span>
                </Col>
                <Col lg={3} md={3} sm={2} xs={2} style={{textAlign:"center"}}>
                  <Header/>
                </Col>
              </Row>
            </Col>
            <Col lg={12}>
              <Row className="topsidebarcontainer">
                <Col lg={3} md={3} sm={6} xs={6}></Col>
                <Col lg={6} md={6} sm={4} xs={4}>
                  <Row>
                    <TopSidebar/>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col lg={12} style={{paddingTop:20,backgroundColor:"white"}}>
              <Content fluid onClick={this.handleContentClick}>
                {children}
              </Content>
              <NotificationSystem
                  dismissible={false}
                  ref={notificationSystem =>
                    (this.notificationSystem = notificationSystem)
                  }
                  style={NOTIFICATION_SYSTEM_STYLE}
                />
            </Col>
          </Row>
          
        </main>
      </AppContainer>
    );
  }
}

export default MainLayout;
