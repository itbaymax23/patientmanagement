import React from 'react';
import Select from 'react-select';
import {Row,Col,InputGroup,Button,Input,InputGroupAddon} from 'reactstrap';
import * as User from '../../action/user';
class Searchbar extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            location:[],
            patient_name:""
        }
    }
    componentDidMount()
    {
        
        
    }

    handleChange = (e) =>{
        this.setState({
            patient_name:e.target.value
        })
    } 

    search = () => {
        let self = this;
        User.getpatients(this.state.patient_name).then(function(data){
            self.props.setPatient(data.data);
        })
    }
    
    render(){
        return(
            <Row className="searchbar" style={{marginTop:10}}>
                <Col lg={6} md={6} sm={6} xs={12} style={{marginLeft:10,zIndex:500}}>
                  <Select options={this.props.location} placeholder="Location" styles={{zIndex:300}}></Select>  
                </Col>
                <Col>
                    <Row>
                        <Col>
                            <InputGroup>
                                <Input placeholder="Patient Search ..." onChange={(e)=>this.handleChange(e)} value={this.state.patient_name}></Input>
                                <InputGroupAddon addonType="append"><Button color="primary" onClick={this.search}>Search</Button></InputGroupAddon>
                            </InputGroup>
                        </Col>
                        {/* <Col lg={7} md={7} sm={7} xs={12}>
                            <Button color="primary"><FontAwesomeIcon icon={faEnvelope} style={{marginRight:10}}></FontAwesomeIcon>New message</Button>
                            <Button color="primary" style={{marginLeft:10}}><img src={alerticon} style={{marginRight:10}}></img>Alert/Task</Button>
                            <Button color="primary" style={{marginLeft:10}}><img src={docicon}  style={{marginRight:10}}></img> Plan/Orders</Button>
                            <Button color="primary" style={{marginLeft:10}}><img src={foldericon}  style={{marginRight:10}}></img> Documents</Button>
                        </Col> */}
                    </Row>
                </Col>
            </Row>
        );
    }
}

export default Searchbar;