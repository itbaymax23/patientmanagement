import React from 'react';

class FileComponent extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            data:{
                pdf:"pdf.png",
                doc:"word.png"
            }
        }
    }

    filetype(file)
    {
        let filetype = file.split('.');
        filetype = filetype[filetype.length - 1];

        let docs = ["doc","docx","xls"];
        if(filetype == 'pdf' || docs.indexOf(filetype) > -1)
        {
            if(docs.indexOf(filetype) > -1)
            {
                filetype = "doc";
            }
            return this.state.data[filetype];
        }
        else
        {
            return file;
        }
    }

    render(){
        console.log(this.filetype(this.props.filename));
        return(
            <div className="file_container" onClick={this.props.clickcomponent}>
                <div className="filetitle">{this.props.name}</div>
                <div className="filebody" style={{backgroundImage:"url('" + this.filetype(this.props.filename) + "')",backgroundSize:"cover"}}></div>                                    
             </div>
        )
    }
}

export default FileComponent;