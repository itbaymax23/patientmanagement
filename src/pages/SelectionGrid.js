import React from 'react';
import {Row,Col,Button,Modal,ModalBody,ModalFooter,ModalHeader} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimesCircle} from '@fortawesome/free-solid-svg-icons';
class SelectionGrid extends React.Component
{
    data = {};
    constructor(props)
    {
        super(props);
        this.state = {
            updated:false
        }
    }

    componentWillReceiveProps(props)
    {
        console.log("grid",props);
        this.data = props.data;
    }

    getproblemlist = (data) => {
       
       let itemlist = [];
       let valid_item = [];
       for(let item in data)
       {
            if(item == 'Assessments')
            {
                for(let element in data[item].primary)
                {
                    if(valid_item.indexOf(data[item].primary[element].Id) == -1)
                    {
                        itemlist.push(data[item].primary[element]);
                        valid_item.push(data[item].primary[element].Id);
                    }
                }

                for(let element in data[item].secondary)
                {
                    if(valid_item.indexOf(data[item].secondary[element].Id) == -1)
                    {
                        itemlist.push(data[item].secondary[element]);
                        valid_item.push(data[item].secondary[element].Id);
                    }
                }
            }
            else if(item == 'CC')
            {
                for(let element in data[item].problem)
                {
                    for(let element_sub in data[item].problem[element])
                    {
                        if(valid_item.indexOf(data[item].problem[element][element_sub].Id) == -1)
                        {
                            itemlist.push(data[item].problem[element][element_sub]);
                            valid_item.push(data[item].problem[element][element_sub].Id);
                        }
                    }
                }
            }
            else
            {
                for(let element in data[item].problem)
                {
                    if(valid_item.indexOf(data[item].problem[element].Id) == -1)
                    {
                        itemlist.push(data[item].problem[element]);
                        valid_item.push(data[item].problem[element].Id);
                    }
                }
            }

       }
       console.log("problemlist",itemlist);
       return itemlist;
    }

    getproblemdata = (id,data) => {
        if(!data)
        {
            return false;
        }
        var enable = false;
        for(let element in data)
        {
            if(data[element].Id == id)
            {
                enable = true;
                break;
            }
        }

        return enable;
    }

    addCC = (e,item,value) =>
    {
        console.log(this.data.CC);
        if(e.target.checked)
        {
            if(!this.data.CC.problem[item])
            {
                this.data.CC.problem[item] = [];
            }

            this.data.CC.problem[item].push(value);
        }
        else
        {
            for(let element in this.data.CC.problem[item])
            {
                if(this.data.CC.problem[item][element].Id == value.Id)
                {
                    console.log(element);
                    this.data.CC.problem[item].splice(element,1);
                }
            }
        }

        console.log(this.data.CC);
    }

    add = (e,item,value) =>
    {
        if(e.target.checked)
        {
            if(!this.data[item].problem)
            {
                this.data.CC.problem = [];
            }

            this.data[item].problem.push(value);
        }
        else
        {
            for(let element in this.data[item].problem)
            {
                if(this.data[item].problem[element].Id == value.Id)
                {
                    this.data[item].problem.splice(element,1);
                }
            }
        }
    }

    addAssessment = (e,item,value) =>
    {
        if(e.target.checked)
        {
            if(!this.data.Assessments[item])
            {
                this.data.Assessments[item] = [];
            }

            for(let item_assessment in this.data.Assessments)
            {
                if(item_assessment != item)
                {
                    for(let item_element in this.data.Assessments[item_assessment])
                    {
                        if(this.data.Assessments[item_assessment][item_element].Id == value.Id)
                        {
                            this.data.Assessments[item_assessment].splice(item_assessment,1);
                            break;
                        }
                    }
                }
            }

            this.data.Assessments[item].push(value);
        }
        else
        {
            for(let element in this.data.Assessments[item])
            {
                if(this.data.Assessments[item][element].Id == value.Id)
                {
                    this.data.Assessments[item].splice(element,1);
                }
            }
        }
        this.setState({
            updated:!this.state.updated
        })
    }

    render(){
        let problemlist = this.getproblemlist(this.props.data);
        return (
            <Modal isOpen={this.props.problemgrid} size="lg" className="problemgridcontainer">
                <ModalHeader className="problemgrid">
                    <Row>
                        <Col>Problem Selection Grid</Col>
                        <FontAwesomeIcon icon={faTimesCircle} onClick={()=>{this.props.close()}} style={{cursor:"pointer"}}></FontAwesomeIcon>
                    </Row>
                </ModalHeader>
                <ModalBody className="problemgridbody">
                    <Row className="bodycontainer">
                        <Col lg={12} className="bodyheader">
                            <Row>
                            <Col lg={3} className="problem">Problem</Col>
                            <Col lg={3}>
                                <Row><Col>CC</Col></Row>
                                <Row><Col>Chronic</Col><Col>Acute</Col></Row>
                            </Col>
                            <Col>HPI</Col>
                            <Col>PMH</Col>
                            <Col lg={3}>Assessment</Col>
                            </Row>
                        </Col>
                        {
                            problemlist.map((row,index)=>{
                                return (
                                    <Col lg={12} className="problemcontainer">
                                        <Row>
                                            <Col lg={3} className="problem">{row.CommonName?row.CommonName:row.ShortDesc}</Col>
                                            <Col lg={3}>
                                                <Row><Col>1<input type="checkbox" style={{marginLeft:5}} defaultChecked={this.getproblemdata(row.Id,this.props.data.CC.problem[0])} onChange={(e)=>this.addCC(e,0,row)}></input></Col><Col>2<input type="checkbox" style={{marginLeft:5}}  defaultChecked={this.getproblemdata(row.Id,this.props.data.CC.problem[1])} onChange={(e)=>this.addCC(e,1,row)}></input></Col></Row>
                                            </Col>
                                            <Col><input type="checkbox"  defaultChecked={this.getproblemdata(row.Id,this.props.data.HPI.problem)} onChange={(e)=>this.add(e,"HPI",row)}></input></Col>
                                            <Col><input type="checkbox"  defaultChecked={this.getproblemdata(row.Id,this.props.data.PMH.problem)} onChange={(e)=>this.add(e,"PMH",row)}></input></Col>
                                            <Col lg={3}>P<input type="checkbox" style={{marginLeft:5,marginRight:10}}  defaultChecked={this.getproblemdata(row.Id,this.props.data.Assessments.primary)} checked={this.getproblemdata(row.Id,this.data.Assessments.primary)} onChange={(e)=>this.addAssessment(e,"primary",row)}></input>S<input type="checkbox" style={{marginLeft:5}} defaultChecked={this.getproblemdata(row.Id,this.props.data.Assessments.secondary)}  checked={this.getproblemdata(row.Id,this.data.Assessments.secondary)} onChange={(e)=>this.addAssessment(e,"secondary",row)}></input></Col>
                                        </Row>
                                    </Col>
                                )
                            })
                        }
                        
                    </Row>
                </ModalBody>
                <ModalFooter className="problemgridfooter">
                    <div style={{margin:"auto"}}>
                        <Button color="success" onClick={()=>this.props.save(this.data)}>Save</Button>
                        <Button color="secondary" onClick={()=>this.props.close()} style={{marginLeft:20}}>Cancel</Button>
                    </div>
                    
                </ModalFooter>
        </Modal>
        )
        
    }
}

export default SelectionGrid;