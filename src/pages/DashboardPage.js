import React from 'react';
import {Row,Col,Button,InputGroup,InputGroupAddon,Input} from 'reactstrap';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.base.css';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFolder, faExclamationTriangle, faFileArchive, faEnvelope} from '@fortawesome/free-solid-svg-icons';
import FileManager from '../components/FileManager';

// import medium from '../assets/img/icon/medium.png';
// import small from '../assets/img/icon/small.png';
// import detail from '../assets/img/icon/detail.png';
// import content from '../assets/img/icon/content.png';
// import note from '../assets/img/icon/note.png';
import Scheduler from '../components/chartboxcomponent/Scheduler';
class DashboardPage extends React.Component {
  constructor(props)
  {
    super(props);
    this.state = {      
      tabledata:[{
        name:"Jhon Smith(4.17.1955)",Demo:"25-y/0 WF",info:(<FontAwesomeIcon icon={faFolder} style={{color:"#f6cc1e"}}></FontAwesomeIcon>),dole:"25/4/2019"
        ,subject:"Lorem ipsum dolor dummy test",'alert':(<FontAwesomeIcon icon={faExclamationTriangle}></FontAwesomeIcon>),doc:(<FontAwesomeIcon icon={faFileArchive}></FontAwesomeIcon>),
        message:(<FontAwesomeIcon icon={faEnvelope}></FontAwesomeIcon>)
      },{
        name:"Jhon Smith(4.17.1955)",Demo:"25-y/0 WF",info:(<FontAwesomeIcon icon={faFolder} style={{color:"#f6cc1e"}}></FontAwesomeIcon>),dole:"25/4/2019"
        ,subject:"Lorem ipsum dolor dummy test",'alert':(<FontAwesomeIcon icon={faExclamationTriangle}></FontAwesomeIcon>),doc:(<FontAwesomeIcon icon={faFileArchive}></FontAwesomeIcon>),
        message:(<FontAwesomeIcon icon={faEnvelope}></FontAwesomeIcon>)
      },{
        name:"Jhon Smith(4.17.1955)",Demo:"25-y/0 WF",info:(<FontAwesomeIcon icon={faFolder} style={{color:"#f6cc1e"}}></FontAwesomeIcon>),dole:"25/4/2019"
        ,subject:"Lorem ipsum dolor dummy test",'alert':<FontAwesomeIcon icon={faExclamationTriangle}></FontAwesomeIcon>,doc:(<FontAwesomeIcon icon={faFileArchive}></FontAwesomeIcon>),
        message:(<FontAwesomeIcon icon={faEnvelope}></FontAwesomeIcon>)
      },{
        name:"Jhon Smith(4.17.1955)",Demo:"25-y/0 WF",info:(<FontAwesomeIcon icon={faFolder} style={{color:"#f6cc1e"}}></FontAwesomeIcon>),dole:"25/4/2019"
        ,subject:"Lorem ipsum dolor dummy test",'alert':(<FontAwesomeIcon icon={faExclamationTriangle}></FontAwesomeIcon>),doc:(<FontAwesomeIcon icon={faFileArchive}></FontAwesomeIcon>),
        message:(<FontAwesomeIcon icon={faEnvelope}></FontAwesomeIcon>)
      },{
        name:"Jhon Smith(4.17.1955)",Demo:"25-y/0 WF",info:(<FontAwesomeIcon icon={faFolder} style={{color:"#f6cc1e"}}></FontAwesomeIcon>),dole:"25/4/2019"
        ,subject:"Lorem ipsum dolor dummy test",'alert':(<FontAwesomeIcon icon={faExclamationTriangle}></FontAwesomeIcon>),doc:(<FontAwesomeIcon icon={faFileArchive}></FontAwesomeIcon>),
        message:(<FontAwesomeIcon icon={faEnvelope}></FontAwesomeIcon>)
      },{
        name:"Jhon Smith(4.17.1955)",Demo:"25-y/0 WF",info:(<FontAwesomeIcon icon={faFolder} style={{color:"#f6cc1e"}}></FontAwesomeIcon>),dole:"25/4/2019"
        ,subject:"Lorem ipsum dolor dummy test",'alert':(<FontAwesomeIcon icon={faExclamationTriangle}></FontAwesomeIcon>),doc:(<FontAwesomeIcon icon={faFileArchive}></FontAwesomeIcon>),
        message:(<FontAwesomeIcon icon={faEnvelope}></FontAwesomeIcon>)
      },{
        name:"Jhon Smith(4.17.1955)",Demo:"25-y/0 WF",info:(<FontAwesomeIcon icon={faFolder} style={{color:"#f6cc1e"}}></FontAwesomeIcon>),dole:"25/4/2019"
        ,subject:"Lorem ipsum dolor dummy test",'alert':(<FontAwesomeIcon icon={faExclamationTriangle}></FontAwesomeIcon>),doc:(<FontAwesomeIcon icon={faFileArchive}></FontAwesomeIcon>),
        message:(<FontAwesomeIcon icon={faEnvelope}></FontAwesomeIcon>)
      }]
    }
  }

  componentDidMount() {
    // this is needed, because InfiniteCalendar forces window scroll
    window.scrollTo(0, 0);
  }
  
  

  format = (cell,row) => {
    return cell;
  }
  
  render() {
   
   
    return (
      <Row>
        <Col lg={12}>
          <Row>
            <Col lg={5} md={5} sm={5} xs={12}>
              <Scheduler></Scheduler>
            </Col>
            <Col className="task_manager">
              <Row>
                <div style={{marginLeft:25}}>
                  <Button color="default">Recent Charts</Button>
                </div>
                <Col>
                    
                </Col>
                <Col>
                  <Row>
                    <Col>
                      <InputGroup>
                        <Input placeholder="Search Here ..."></Input>
                        <InputGroupAddon>
                          <Button color="primary">GO</Button>
                        </InputGroupAddon>
                      </InputGroup>
                    </Col>
                    <div style={{marginRight:30}}><Button color="default">Advanced</Button></div>
                  </Row>
                  
                </Col>
                
              </Row>
              <Row style={{marginTop:20}}>
                <Col>
                  <BootstrapTable data={this.state.tabledata} responsible bordered={false}>
                    <TableHeaderColumn dataField="name" isKey={true} width={200}>Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="Demo" dataFormat={this.format}>Demo</TableHeaderColumn>
                    <TableHeaderColumn dataField="info" dataFormat={this.format}>Info</TableHeaderColumn>
                    <TableHeaderColumn dataField="dole">DOLE</TableHeaderColumn>
                    <TableHeaderColumn dataField="subject" width={200}>SUBJECT</TableHeaderColumn>
                    <TableHeaderColumn dataField="alert" dataFormat={this.format}>Alets/Tasks</TableHeaderColumn>
                    <TableHeaderColumn dataField="doc" dataFormat={this.format}>Document</TableHeaderColumn>
                    <TableHeaderColumn dataField="message" dataFormat={this.format}>Messaging</TableHeaderColumn>
                  </BootstrapTable>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col className="file_manager" lg={12} style={{marginTop:20}}>
            <FileManager tabledata = {this.state.tabledata}/>
        </Col>
      </Row>
      
    );
  }
}

// const styles = {
//   sortbuttonstyle:{
//     backgroundColor:"#3d82f2",
//     color:"white",
//     borderRadius:5,
//     padding:5
//   }
// }

export default DashboardPage;
