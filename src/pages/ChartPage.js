//library
import React from 'react';
import {Row,Col,Button} from 'reactstrap';
import Filter from '../components/chartboxcomponent/Filter';
import Card from '../components/Card';
import $ from 'jquery';
import UndoRedo from 'react-undo';
import Moment from 'moment';

//icons
import {faAngleRight,faAngleLeft} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


//others
import {Sidebar} from '../components/Layout';
import Searchbar from '../components/Layout/Searchbar';
import Personinfo from '../components/chartboxcomponent/Personinfo/PersonInfo';
import ActivePersoninfo from '../components/chartboxcomponent/Personinfo/ActivePersoninfo';
import PlanOrderComponent from '../components/chartboxcomponent/PlanOrderComponent';
import Coding from '../components/chartboxcomponent/EMCoding';
import ListViewAlpha from '../components/chartboxcomponent/ListViewAlphabatic';
import ActionButton from '../components/chartboxcomponent/action/Buttons';

//action buttons
import {PlanOrder} from '../components/chartboxcomponent/action';


//css
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import 'react-toggle-switch/dist/css/switch.min.css';
import 'react-widgets/dist/css/react-widgets.css';


//Section Component
import {CC,HPI,PMH,PSH,SH,Macro,Review,PE,DD,MEDS,ADR,FH,Assessment,PO,TemplateEditor} from '../components/chattoollayout';



//Dialog
import SelectionGrid from './SelectionGrid';
import Notelist from '../components/chartboxcomponent/Notelist';
import FreeText from '../components/chattoollayout/Freetext';
import Summary from '../components/chartboxcomponent/Summary';
import Preview from '../components/chartboxcomponent/Preview';

//API
import * as User from "../action/user";
import * as Problem from '../action/Problem';

class ChartPage extends React.Component {

  selectedproblemitem = 'CC'; //Present Selected Item

  updatemacro = false; // macro update action

  updated = false; //update flag if the component is loaded

  updatedSectionId = 1; // the section id updated

  updatedpatientinfo = false; // update flag for patient update
  
  selectedproblems = []; //selected problems in CC,HPI,PMH

  initdata = false; //init flag
  
  sessionupdate = false;
  updated_session = false;

  //section array for the all section
  sectionidarray = {CC:1,HPI:2,PMH:3,Assessments:4,PSH:5,MEDS:6,ADR:7,SHI:8,ROS:9,ROS:10,PE:11,DD:12,PO:13,EM:14,VitalSign:15};

  //constructor for init all state and data
  constructor(props)
  {
    super(props);
    this.state = {
      patientdata:[], //patient list
      selectednote:false,
      notelist:[], //note list for patient
      location:[], //location list
      selected:0, //selected patient index
      showtoolbar:false, //toolbar show flag
      problemlist:[], //problem list for CC,HPI ... section
      problemtitle:"", // toolbar title
      problemgrid:false, // grid show flag
      races:[], //race list for patient
      selectedSectionId:1, //section id selected
      macroopen:false, // macro open flag
      locationdata:"", // location data that selected
      title:"", // title for notes 
      date:new Date(), // date for notes
      notetype:1, // notetype for charting tool
      isopenpreload:false,
      freetextopen:false,//freetext dialog display flag
      freetext:false,//freetext enable
      freetextdata:"", //freetext data
      issummaryopen:false, //summary dialog open flag
      preview:false, //preview dialog open flag,
      templateopen:false,//template dialog open flag,
      noteposition:false,
      previewonly:false,
      previewsectionid:false,
      //data for charting tool
      data:{
        CC:{
          textData:[],
          problem:[]
        },

        HPI:{
          problem:[]
        },

        PMH:{
          problem:[]
        },

        Assessments:{
          primary:[],
          secondary:[]
        },

        PSH:{
          problem:[]
        },

        MEDS:{
          
        },

        ADR:{
          adversereaction:[]
        },

        SHI:{
          substance:[],
          social:[],
          data:{
            substance:{}
          }
        },

        ROS:{
          data:{}
        },
        PE:{
          data:{}
        },

        DD:{

        },

        PO:{
          
        },

        EM:{},
        VitalSign:{}
      },

      //macro array that shows
      macros:{
      }
    }
  }

  //component update
  componentDidMount() {
    
    //identifier for searchtoolbar sticky
    let documenttop = document.body.getBoundingClientRect();
    let top = $('.searchtoolbar')[0].getClientRects()[0].top;
    top -= documenttop.top;
    let self = this;

    this.setState(prevState=>{
      return {
        top:top
      }
    })

    //get location of hospital
    User.getlocation().then(function(data){
        let locationarray = [];
        
        for(let item in data.data)
        {
            locationarray.push({label:data.data[item].Name,value:data.data[item].Id,type:data.data[item].typename});
        }

        self.setState({
            location:locationarray,
            locationdata:locationarray[0].value
        })
    });
    
    //cpt info for symptoms
    Problem.getcpt().then(function(data){
      self.setState({
        cpt:data.data
      })
    })

    //get patient list
    User.getpatients().then(function(data){
      self.setState({
        patientdata:data.data
      })

      if(data.data[self.state.selected].Id)
      {
        self.getnotelist(data.data[self.state.selected].Id);
      }
    })
    
    //get race list for user info
    User.getraces().then(function(data){
      self.setState({
        races:data.data
      })
    })

    //icd list for problems
    Problem.getproblems().then(function(data){
      self.setState({
        problemlist:data.data
      })
    })

    window.addEventListener('scroll',this.handleScrolltoollbar);

    if(window.sessionStorage.getItem('session'))
    {
      let data = JSON.parse(window.sessionStorage.getItem('session'));
      console.log('sessionstorage',data);   
      this.sessionupdate = true;
      this.setState({data:data}); 
    }
    else
    {
      this.initdata = true;
    }
    this.updated_session = true;  
  } 


  //get note list from selected patientid

  getnotelist = (patientid) => {
    let self = this;
    User.getnotelist(patientid).then(function(data){
        self.setState({
          notelist:data.data
        });
    })
  }

  //component update listener
  componentDidUpdate()
  {
    //update flag initialize
    this.historyadd = false;
    this.updatemacro = false;
    this.updated = false;
    this.updatedpatientinfo = false;
    this.initdata = false;
    this.underable = false;
    this.sessionupdate = false;
  }
  
  //searchtoolbar sticky 
  componentWillMount()
  {
    window.removeEventListener('scroll',this.handleScrolltoollbar);
  }

  //macro show
  showmacro = (sectionId) => {
    this.setState({
      selectedSectionId:sectionId,
      macroopen:!this.state.macroopen
    })
  }

  //searchtoolbar control
  handleScrolltoollbar = () => {
    if(this.state.top < window.scrollY)
    {
      $('.searchtoolbar').css("position","fixed");
      $('.searchtoolbar').css('top','20px');
    }
    else
    {
      $('.searchtoolbar').css("position","absolute");
      $('.searchtoolbar').css('top','auto');
    }
  }

  //patient info deselect
  close = (index) => {
    let patientdata = this.state.patientdata;
    patientdata.splice(index,1);
    this.setState({
      patientdata:patientdata
    })
  }

  //patient search tool
  setPatient = (data) => {
    this.setState({
      patientdata:data
    })
  }

  //patient activate tool
  selectpatient = (index) => {
    this.updatedpatientinfo = true;
    
    this.getnotelist(this.state.patientdata[this.state.selected].Id);
    
    this.initdata = true;

    this.setState({
      selected:index
    });
  }

  //toolbar show
  showtoolbar = () => {
    $('.toolbarcontainer').slideToggle("slow","linear");
    this.setState({
      showtoolbar:!this.state.showtoolbar
    })
  }

  //problem selection grid show
  showproblem = (item,index,sectionid) => {
    if(!this.state.showtoolbar)
    {
      this.showtoolbar();
    }

    this.selectedproblemindex = index;
    this.selectedproblemitem = item;
    this.selectedSectionId = sectionid;

    //title confiture
    if(item == 'CC')
    {
      this.setState({
        problemtitle:"Chief Complaint"
      })
     
    }
    else if(item == 'HIS')
    {
      this.setState({
        problemtitle:"History Of Patient Illness"
      })

      
    }
    else if(item == 'PMH')
    {
      this.setState({
        problemtitle:"Past Medical History"
      })
      
    }
    else if(item == 'PSH')
    {
      this.setState({
        problemtitle:"Past Surgical History"
      })
    }
    else if(item == 'Assessments')
    {
      this.setState({
        problemtitle:"Assessments"
      })
    }

    this.addproblemlist();
  }

  //problem selected flag
  addproblemlist = () => {
      if(this.selectedproblemitem == 'CC')
      {
        this.selectedproblems = this.state.data.CC.problem[this.selectedproblemindex];
      }
      else if(this.selectedproblemitem == 'Assessments')
      {
        this.selectedproblems = this.state.data.Assessments[this.selectedproblemindex];
      }
      else
      {
        this.selectedproblems = this.state.data[this.selectedproblemitem].problem;
      }
  }

  //problem insert function
  insertproblem = (problemdata,data) => {
    var enable = false;
    for(let item in data)
    {
      if(data[item].Id == problemdata.Id)
      {
        enable = true;
        break;
      }
    }

    if(!enable)
    {
      data.push(problemdata);
    }

    return data;
  } 

  //problem add from search tool
  addproblem = (problemdata) => {
    if(this.selectedproblemitem)
    {
      let data = this.state.data;
      if(this.selectedproblemindex != undefined)
      {
        if(this.selectedproblemitem == 'CC')
        {
          if(!data[this.selectedproblemitem].problem[this.selectedproblemindex])
          {
            for(let item = 0;item<this.selectedproblemindex;item++)
            {
              if(!data[this.selectedproblemitem].problem[item])
              {
                data[this.selectedproblemitem].problem[item] = [];
              }
            }
            
            data[this.selectedproblemitem].problem[this.selectedproblemindex] = [];
          }
          
          data[this.selectedproblemitem].problem[this.selectedproblemindex].push(problemdata);

          data.HPI.problem = this.insertproblem(problemdata,data.HPI.problem);
          let savedindex;
          if(this.selectedproblemindex < 2)
          {
            data.Assessments.primary = this.insertproblem(problemdata,data.Assessments.primary);
            savedindex = 'secondary';
          }
          else
          {
            data.Assessments.secondary = this.insertproblem(problemdata,data.Assessments.secondary);
            savedindex = 'primary';
          }

          for(let item in data.Assessments[savedindex])
          {
              if(data.Assessments[savedindex][item].Id == problemdata.Id)
              {
                data.Assessments[savedindex].splice(item,1);
              }
          }
        }
        else if(this.selectedproblemitem == "Assessments"){
          data.Assessments[this.selectedproblemindex] = this.insertproblem(problemdata,data.Assessments[this.selectedproblemindex]);
          
          for(let item_assess in data.Assessments)
          {
            if(item_assess == this.selectedproblemindex)
            {
              continue;
            }

            for(let itemproblem in data.Assessments[item_assess])
            {
              if(data.Assessments[item_assess][itemproblem].Id == problemdata.Id)
              {
                data.Assessments[item_assess].splice(itemproblem,1);
                break;
              }
            }
          }
        }
        else
        {
          if(this.selectedproblemitem == 'HPI')
          {
            if(!data[this.selectedproblemitem].problem[this.selectedproblemindex].relateddiagnosis)
            {
              data[this.selectedproblemitem].problem[this.selectedproblemindex].relateddiagnosis = [];
            }

            var enable = true;
            for(let itemdiagnosis in data[this.selectedproblemitem].problem[this.selectedproblemindex].relateddiagnosis)
            {
              if(problemdata.Code == data[this.selectedproblemitem].problem[this.selectedproblemindex].relateddiagnosis[itemdiagnosis].Code)
              {
                enable = false;
              }
            }

            if(enable)
            {
              data[this.selectedproblemitem].problem[this.selectedproblemindex].relateddiagnosis.push(problemdata);
            }

            Problem.addrelateddiagnosis(data[this.selectedproblemitem].problem[this.selectedproblemindex].Id,problemdata.Code).then(function(data){

            })
          }
          else
          {
            data[this.selectedproblemitem].problem[this.selectedproblemindex] = problemdata;
          }
        }
        
        this.historyadd = true;
      }
      else
      {
        data[this.selectedproblemitem].problem.push(problemdata);
      }

      if(this.updated_session)
      {
        window.sessionStorage.setItem("session",JSON.stringify(data));
        this.sessionupdate = true;
      }
      this.setState({data:data});
      this.addproblemlist();
      this.updated = true;
    }
  }

  //item save from child component
  save = (item,newdata,sectionid) => {
    let data = this.state.data;
    if(!data[item])
    {
      data[item] = {};
    }
    for(let newitem in newdata)
    {
      data[item][newitem] = newdata[newitem];
    }
    this.underable = false;
    this.updated = true;
    this.updatedSectionId = sectionid;
    
    this.setState(data);
    if(this.updated_session)
    {
      console.log("sessiondata",data);
      window.sessionStorage.setItem("session",JSON.stringify(data));
      this.sessionupdate = true;
    }
    
  }

  //symptom search function from cpt or icd10 from search tool bar
  search = (value) => {
    let self = this;
    if(this.selectedproblemitem == "PSH")
    {
      Problem.getcpt(value).then(function(data){
        self.setState({
          cpt:data.data
        })
      })
    }
    else
    {
      Problem.getproblems(value).then(function(data){
        self.setState({
          problemlist:data.data
        })
      })
    }
  }

  //macro select
  selectmacro = (macro) => {
    let macros = this.state.macros;
    macros[this.state.selectedSectionId] = macro;
    this.setState({
      macro:macros,
      macroopen:false
    })
    this.updatemacro = true;
  }

  //note save
  savenote = (savetype) => {
    var data = {};
    data.patientid = this.state.patientdata[this.state.selected].Id;
    data.NoteType = this.state.notetype;
    data.SaveType = savetype;
    data.LocationId = this.state.locationdata;
    data.NoteDate = Moment(this.state.date).format("YYYY-MM-DD");
    data.Name = this.state.title;

    data.data = this.state.data;
    let dataarray = [];
    for(let item in data.data)
    {
      dataarray.push({SectionId:this.sectionidarray[item],data:data.data[item]})
    }
    let self = this;
    data.data = dataarray;
    User.savenote(data).then(function(data){
      if(data.data.success)
      {
        self.setState({noteid:data.data.noteid});
        self.getnotelist(self.state.patientdata[self.state.selected].Id);
      }
    })
  }

  previewsection = (sectionid) => {
    this.setState({
      preview:true,
      previewonly:true,
      previewsectionid:sectionid
    })
  }
  //get section name from id
  getsectionname = (sectionid) => {
    for(let item in this.sectionidarray)
    {
      if(this.sectionidarray[item] == sectionid)
      {
        return item;
      }
    }
  }

  //loading view from note
  loadviewnote = (id) => {
    let self = this;
    User.getnotedata(id).then(function(data){
      var realdata  = {};
      for(let item in data.data.data)
      {
        realdata[self.getsectionname(data.data.data[item].SectionId)] = data.data.data[item].data;
      }

      
      let state = self.state.data;
      for(let item in state)
      {
        state[item] = realdata[item];
      }
      self.setState({
        data:state,
        noteid:id,
        isopenpreload:false,
        title:data.data.Name,
        notetype:data.data.NoteTypeId,
        locationdata:data.data.LocationId,
        date:new Date(data.data.NoteDate)});
    })
  }

  //the function  to load previous note
  previous = () => {
    for(let item in this.state.notelist)
    {
      if(this.state.notelist[item].Id == this.state.noteid)
      {
        if(item > 0)
        {
          this.loadviewnote(this.state.notelist[item - 1].Id);
          break;
        }
      }
    }
  }

  //initalize note
  initnote = () =>{
    let data = {
      CC:{
        textData:[],
        problem:[]
      },

      HPI:{
        problem:[]
      },

      PMH:{
        problem:[]
      },

      Assessments:{
        primary:[],
        secondary:[]
      },

      PSH:{
        problem:[]
      },

      MEDS:{
        
      },

      ADR:{
        adversereaction:[]
      },

      SHI:{
        substance:[],
        social:[],
        data:{
          substance:{}
        }
      },

      ROS:{
        data:{}
      },
      PE:{
        data:{}
      },

      DD:{

      },

      PO:{},

      EM:{},
      VitalSign:{}
    }

    this.setState({
      data:data,noteid:false
    })
    this.initdata = true;
  }
  //the function to load next note
  next = () => {
    console.log(this.state.notelist);
    for(let item in this.state.notelist)
    {
      if(this.state.notelist[item].Id == this.state.noteid)
      {
        if(item < this.state.notelist.length - 1)
        {
          this.loadviewnote(this.state.notelist[Number(item) + 1].Id);
          break;
        }
      }
    }
  }

  //the function to check if you can load the previous or next note
  canpage = (type) => {
    
    for(let item in this.state.notelist)
    {
      if(this.state.notelist[item].Id == this.state.noteid)
      {
        if(type == 'previous')
        {
          return item > 0;
        }
        else{
          return item < this.state.notelist.length - 1;
        }
      }
    }

  }

  changeposition = () => {
    
    this.setState({noteposition:!this.state.noteposition});
  }

  render() {
    let toolbarstyle = {
      left:150,zIndex:200,display:"flex",position:"absolute"
    }
    console.log("selectedproblem",this.selectedproblemitem);

    let sliderdata = [];
    var slideritem = '';
    if(this.selectedproblemitem == 'CC' || this.selectedproblemitem == 'HPI' || this.selectedproblemitem == 'PMH' || this.selectedproblemitem == 'Assessments')
    {
      sliderdata = this.state.problemlist;
      slideritem = 'ShortDesc';
    }
    else
    {
      sliderdata = this.state.cpt;
      slideritem = 'BillDesc';
    }


    //hospital info

    let locationinfo = null;
    let locationtype = null;
    for(let item in this.state.location)
    {
      if(this.state.location[item].value == this.state.locationdata)
      {
        locationinfo = this.state.location[item].label;
        locationtype = this.state.location[item].type;
      }
    }

   
    let stylecontent = {maxWidth:'Calc(100% - 200px)'};
    if(this.state.noteposition.length > 0 && this.state.noteposition.length < 2)
    {
      stylecontent.maxWidth = 'Calc(100% - 400px)';
    }

    return (
      <Row>
        <Col className="charttoolheader" lg={12}>
          <Row>
             <Col className="personinfoactive">
              {this.state.patientdata[this.state.selected] && (
                <ActivePersoninfo 
                  patientid={this.state.patientdata[this.state.selected].Id}
                  name={this.state.patientdata[this.state.selected].FirstName + " " + this.state.patientdata[this.state.selected].LastName}
                  close = {(index)=>this.close(index)}
                  index={this.state.selected}
                  location={this.state.location}
                  races={this.state.races}
                  updated={this.updatedpatientinfo}
                ></ActivePersoninfo>
              )}
            </Col>
            <Col>
              <Row>
                <Col lg={8} className="personinfo">
                  <Personinfo patient={this.state.patientdata} close={this.close} selectpatient={this.selectpatient} location={this.state.location} races={this.state.races}/>
                </Col>
                <Col lg={4} className="search">
                  <Searchbar setPatient={this.setPatient} location={this.state.location}></Searchbar>
                </Col>
              </Row>
              <Row>
                <ActionButton/>
                {
                  this.state.noteid && this.state.notelist.length > 1 && (
                    <div style={{marginLeft:"auto",marginRight:15}}>
                      {
                        this.canpage("previous") && (
                          <Button color="default" onClick={this.previous}>
                            <FontAwesomeIcon icon={faAngleLeft} style={{marginRight:5}}></FontAwesomeIcon>Previous
                          </Button>
                        )
                      }
                      {
                        this.canpage("next") && (
                          <Button color="default" style={{marginLeft:10}} onClick={this.next}>
                            <FontAwesomeIcon icon={faAngleRight} style={{marginRight:5}}></FontAwesomeIcon> Next
                          </Button>
                        )
                      }
                    </div>
                  )
                }
              </Row>
            </Col>
          </Row>
        </Col>
        
        <Col style={{marginTop:30}}>
          <Row>
            {
              !this.state.noteposition && (
                <div style={{marginLeft:10,marginRight:20,zIndex:300,width:160}}>
                  <Sidebar 
                    showpsg = {()=>{this.setState({problemgrid:true})}}
                    savecomplete={()=>this.savenote(1)}
                    saveincomplete={()=>this.savenote(0)}
                    newnote={()=>this.initnote()}
                    loadnotes = {()=>this.setState({isopenpreload:true})}
                    freetext = {()=>this.setState({freetextopen:true})}
                    summary = {()=>this.setState({issummaryopen:true})}
                    preview={()=>this.setState({preview:true})}
                    edittemplate = {()=>this.setState({templateopen:true})}
                    left="left"
                    position={this.state.noteposition}
                    saveposition = {this.changeposition}
                    />
              </div>
              )
            }
            
            <div  className="searchtoolbar" style={toolbarstyle}>
                <ListViewAlpha
                   data={sliderdata} 
                   title={this.state.problemtitle} 
                   add={this.addproblem} 
                   showtoolbar={this.showtoolbar} 
                   itemName={slideritem}
                   selectedproblem={this.selectedproblems?this.selectedproblems:[]}
                   searchproblem={(value)=>{this.search(value)}}
                   selectedid = {this.selectedproblemitem == 'PSH'?'symptom':'problem'}
                   ></ListViewAlpha>
            </div> 
            <Col className="contentchart" style={stylecontent}>
              <Row>
                <Col lg={12}>
                  <Filter 
                    location={this.state.location} 
                    locationinfo = {this.state.locationdata}
                    date={this.state.date}
                    title={this.state.title}
                    notetype={this.state.notetype}
                    save={(data,item)=>{let state = this.state; state[item] = data; this.setState(state);}}/>
                </Col>
                {
                  !this.state.freetext && (
                    <Col lg={12}>
                      <Row>
                          <Col id="CC" lg={12} style={{marginTop:20}}>
                            <UndoRedo
                            as={CC}
                            props={{
                              data:this.state.data.CC,
                              patient:this.state.patientdata[this.state.selected],
                              showproblem:this.showproblem,
                              historyadd:this.historyadd,
                              save:this.save,
                              problemgrid:function(){this.setState({problemgrid:true})},
                              showmacro:this.showmacro,
                              updated:this.updatedSectionId == 1?this.updated:false,
                              underale:this.updatedSectionId == 1?this.underable:false,
                              initdata:this.initdata,
                              sessionupdate:this.sessionupdate && this.updated_session,
                              HPI:this.state.data.HPI,
                              PMH:this.state.data.PMH,
                              Assessments:this.state.data.Assessments,
                              macro:this.state.macros[1],
                              selectedsection:this.state.selectedSectionId,
                              updatemacro:this.updatemacro
                            }}
                            trackProps={['data']}
                            onChange={(props)=>{console.log("undo",props);this.save("CC",props.data,1)}}
                            >
                            </UndoRedo>
                        </Col>
                        
                        <Col id="HPI" lg={12} style={{marginTop:20}}>
                          <HPI 
                            data={this.state.data.HPI} 
                            showproblem={this.showproblem} 
                            updated={this.updatedSectionId == 2?this.updated:false} 
                            save={(item,data)=>this.save(item,data)}
                            problemgrid={()=>this.setState({problemgrid:true})} 
                            showmacro={this.showmacro}
                            updatemacro={this.updatemacro}
                            selectedsection={this.state.selectedSectionId}
                            macro={this.state.macros[2]}
                            patient={this.state.patientdata[this.state.selected]}
                            ></HPI>
                        </Col>
                      
                        <Col id="PMH" lg={12} style={{marginTop:20}}>
                          <PMH data={this.state.data.PMH}
                              showproblem={this.showproblem} 
                              updated={this.updatedSectionId == 2?this.updated:false}
                              save={(item,data)=>this.save(item,data)}
                              showmacro={this.showmacro}
                              macro={this.state.macros[3]}
                              updatemacro={this.updatemacro}
                              selectedSectionId={this.state.selectedSectionId}
                              ></PMH>
                        </Col>
                      
                        <Col id="PSH" lg={12} style={{marginTop:20}}>
                          <PSH 
                          data={this.state.data.PSH} 
                          showproblem={this.showproblem} 
                          save={(data)=>this.save(data)} 
                          showmacro={this.showmacro}
                          updatemacro={this.updatemacro}
                          selectedSectionId={this.state.selectedSectionId}
                          macro={this.state.macros[4]}
                          />
                        </Col>
                      
                        <Col id="MEDS" lg={12} style={{marginTop:20}}>
                          <MEDS 
                          save={this.save} 
                          meds={this.state.data.MEDS.meds?this.state.data.MEDS.meds:[]} 
                          freetextdata={this.state.data.MEDS.freetextdata} 
                          PMH={this.state.data.PMH}
                          ></MEDS>
                        </Col>
                      
                      
                        <Col id="ADR" lg={12} style={{marginTop:20}}>
                          <ADR 
                          showmacro={this.showmacro} 
                          save={this.save} 
                          ADR={this.state.data.ADR}
                          macro={this.state.macros[6]}
                          updatemacro={this.updatemacro}
                          selectedSectionId={this.state.selectedSectionId}
                          initdata={this.initdata}
                          />
                        </Col>
                      
                        <Col id='SH' lg={12} style={{marginTop:20}}>
                          <SH save={this.save} SHI={this.state.data.SHI} initdata={this.initdata}/>
                        </Col>
                        
                        
                        <Col id="FH" lg={12} style={{marginTop:20}}>
                          {this.state.patientdata[this.state.selected] && (
                            <FH patient={this.state.patientdata[this.state.selected]} showmacro={this.showmacro} initdata={this.initdata} save={this.save} data={this.state.data.FH?this.state.data.FH:{}}/>
                          )}
                          </Col>
                        
                        <Col id="ROS" lg={12} style={{marginTop:20}}>
                          <Review
                          macro={this.state.macros[9]}
                          updated={this.updatemacro} 
                          showmacro={this.showmacro} 
                          selectedsection={this.state.selectedSectionId}
                          save={this.save}
                          ROS={this.state.data.ROS}
                          initdata={this.initdata}
                          addassessments = {(value,item)=>{this.selectedproblemitem = "Assessments"; this.selectedproblemindex = item;this.addproblem(value);}}
                          ></Review>
                        </Col>
                        <Col id="PE" lg={12} style={{marginTop:20}}>
                          <PE 
                          showmacro={this.showmacro} 
                          PE={this.state.data.PE} 
                          diagnose={this.state.data.Assessments} 
                          save={this.save} 
                          selectedsection={this.state.selectedSectionId} 
                          macro={this.state.macros[10]} 
                          updated={this.updatemacro}
                          vitalsign={this.state.data.VitalSign}
                          ></PE>
                        </Col>
                        <Col id="DD" lg={12} style={{marginTop:20}}>
                          <DD data={this.state.data.DD}/>
                        </Col>
                      
                        <Col id='ASSESSMENT' lg={12} style={{marginTop:20}}>
                          <Assessment
                            data={this.state.data.Assessments}
                            showproblem={this.showproblem}
                            save = {this.save}
                            showmacro={this.showmacro}
                             />
                        </Col>
                      
                        <Col id="PO" lg={12} style={{marginTop:20}}>
                          <PO save={this.save} data={this.state.data.PO} vitalsign={this.state.data.VitalSign} location={this.state.location} assessments={this.state.data.Assessments} showproblem={this.showproblem} previewsection={this.previewsection} showmacro={this.showmacro}/>
                          {/* <Card title="Plan/Orders" tag={PlanOrder}>
                            <PlanOrderComponent/>
                          </Card> */}
                        </Col>
                      
                        <Col id="EMCODE" lg={12} style={{marginTop:20}}>
                          <Card title="E & M Coding">
                            <Coding data={this.state.data} location={locationinfo} locationtype={locationtype} EM={this.state.data.EM} save={this.save}/>
                          </Card>
                        </Col>
                      </Row>
                  </Col>
                  )
                }

                {
                  this.state.freetext && (
                    <Col lg={12} dangerouslySetInnerHTML={{__html:this.state.freetextdata}}>
                    </Col>
                  )
                }
                
              </Row>
            </Col>
            {
              this.state.noteposition && (
                <div style={{marginLeft:10,marginRight:20,zIndex:300,width:160}}>
                  <Sidebar 
                    showpsg = {()=>{this.setState({problemgrid:true})}}
                    savecomplete={()=>this.savenote(1)}
                    saveincomplete={()=>this.savenote(0)}
                    newnote={()=>this.initnote()}
                    loadnotes = {()=>this.setState({isopenpreload:true})}
                    freetext = {()=>this.setState({freetextopen:true})}
                    summary = {()=>this.setState({issummaryopen:true})}
                    preview={()=>this.setState({preview:true})}
                    edittemplate = {()=>this.setState({templateopen:true})}
                    left="right"
                    position={this.state.noteposition}
                    saveposition={this.changeposition}
                    />
              </div>
              )
            }   
          </Row>
        </Col>
        
        {/* Macro Dialog */}
        <Macro 
        sectionId={this.state.selectedSectionId} 
        open={this.state.macroopen} toggle={()=>this.setState({macroopen:false})} 
        select_macro={this.selectmacro}></Macro>
        
        {/* Note Presentation Part */}
        <Notelist
         notelist={this.state.notelist}
         isopen={this.state.isopenpreload} 
         toggle={()=>this.setState({isopenpreload:!this.state.isopenpreload})}
         viewnotes = {(id)=>this.loadviewnote(id)}
         >
         </Notelist>

         {/* Free Text */}
         <FreeText 
          isopen={this.state.freetextopen}
          sections={['CC','HPI','PMH','PSH','MEDS','ADR','SHI','FH','ROS','VitalSign','PE','DD','PO','Assessments']}
          save={(data,sections)=>{
            for(let item in sections)
            {
              if(!data[sections[item]])
              {
                data[sections[item]] = {};
              }
              data[sections[item]].freetextenable = true;
            }
            if(this.updated_session)
            {
              window.sessionStorage.setItem("session",JSON.stringify(data));
              this.sessionupdate = true;
            }
            this.setState({data:data,freetextopen:false});
          }}
          initdata = {this.state.freetextdata}
          close={()=>this.setState({freetextopen:false})}
          data={this.state.data}
         >
         </FreeText>
         
         {/* Selection Grid */}
         <SelectionGrid 
          problemgrid={this.state.problemgrid} 
          data={{CC:this.state.data.CC,PMH:this.state.data.PMH,HPI:this.state.data.HPI,Assessments:this.state.data.Assessments}}
          close={()=>{this.setState({problemgrid:false})}}
          save={(data)=>{
            let state = this.state.data; state.CC = data.CC; state.HPI = data.HPI;state.Assessments = data.Assessments;state.PMH = data.PMH;
            this.setState({data:state,problemgrid:false})
            this.updated = true;
          }}
          ></SelectionGrid>
          
         {/* Summary */}
         <Summary
          isopen={this.state.issummaryopen}
          close={()=>this.setState({issummaryopen:false})}
          data={this.state.data}
          save={(item,data,sectionid)=>this.save(item,data,sectionid)}
          ></Summary>

          {/* Preview Dialog */}
          <Preview 
          preview={this.state.preview}
          close={()=>this.setState({preview:false,previewonly:false,previewsectionid:false})}
          data={this.state.data}
          location={locationinfo}
          notetype={this.state.notetype}
          patientdata = {this.state.patientdata[this.state.selected]}
          date = {this.state.date}
          previewonly={this.state.previewonly}
          previewsectionid={this.state.previewsectionid}
          >

          </Preview>

          <TemplateEditor
          open={this.state.templateopen}
          close={()=>this.setState({templateopen:false})}
          ></TemplateEditor>
      </Row>
      
    );
  }
}

const styles = {
  sortbuttonstyle:{
    backgroundColor:"#3d82f2",
    color:"white",
    borderRadius:5,
    padding:5
  }
}

export default ChartPage;
