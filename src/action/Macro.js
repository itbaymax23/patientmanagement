import * as API from '../api';
export async function getmacroccproblem(idarray)
{
    const result = API.getmacroccproblem(idarray);
    return result;
}

export async function gethpiproblems(idarray)
{
    const result = API.gethpiproblems(idarray);
    return result;
}

export async function getproblemsformacro(code)
{
    const result = API.getproblemsformacro(code);
    return result;
}

export async function getsymptomformacro(code)
{
    const result = API.getsymptomformacro(code);
    return result;
}

export async function getallergy(content)
{
    const result = API.getallergy(content);
    return result;
}

export async function addmacro(data)
{
    const result = API.addmacro(data);
    return result;
}

export async function deletemacro(data)
{
    const result = API.deletemacro(data);
    return result;
}

export async function getproblemlist(data)
{
    const result = API.getproblems(data);
    return result;
}

export async function getcptlist(data)
{
    const result = API.getcpt(data);
    return result;
}

export async function getproblemlistwithcode(data)
{
    const result = API.getproblemlistwithcode(data);
    return result;
}

export async function getdataforplan()
{
    const result = API.getdataforplan();
    return result;
}

export async function searchplan(data)
{
    const result = API.searchplan(data);
    return result;
}