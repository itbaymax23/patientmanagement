import * as API from '../api';

//api to get location
export async function getlocation()
{
    const result = await API.getlocation();
    return result;
}

//api to get patient list
export async function getpatients(username)
{
    const result = await API.getpatients({username});
    return result;
}

//api to active the patient
export async function serActive(id)
{
    const result = await API.setActive({id});
    return result;
}

//api to get the user info from patientid
export async function getuserinfo(id)
{
    const result = await API.getuserinfo(id);
    return result;
}

//api to save the patient info to database
export async function saveuserinfo(item,data)
{
    const result = await API.saveuserinfo(item,data);
    return result;
}

//api to delete info from patient 
export async function deleteitem(item,id)
{
    const result  = await API.deleteitem(item,id);
    return result;
}

//api to save emergency contact for patient
export async function saveemergency(id,data)
{
    const result = await API.saveemergency(id,data);
    return result;
}

//api to get the races
export async function getraces()
{
    const result = await API.getraces();
    return result;
}

//api to update democratic info for patient
export async function updatedemocratics(data,id)
{
    const result = await API.updatedemocratics(data,id);
    return result;
}

//get social history list 
export async function getsocialhistory()
{
    const result = await API.getsocialhistory();
    return result;
}

//api to save the notes
export async function savenote(data)
{
    const result = await API.savenotes(data);
    return result;
}

//api to get notelist
export async function getnotelist(patientid)
{   
    const result = await API.getnotelist(patientid);
    return result;
}

//api to get the notedata
export async function getnotedata(id) 
{
    const result = await API.getnotedata(id);
    return result;
}

//api to get the provider
export async function getproviders()
{
    const result = await API.getproviders();
    return result;
}