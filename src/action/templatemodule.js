import DynamicElement from '../components/chattoollayout/DynamicElement';
import ReactDom from 'react-dom';
export function converttemp(title,patientdata)
{
    var myReg = /\[(.*?)\]/g;
    var array = title.match(myReg);
    for(let item in array)
    {
        let value_array = array[item].split('.');
        let value = value_array[0].split('[')[1] + value_array[1].split(']')[0];
        if(value_array[0] == '[Patient' && patientdata)
        {
            var index = value_array[1].split(']')[0];
            switch(index.toUpperCase())
            {
                case 'FULLNAME':
                    value = patientdata.FirstName + " " + patientdata.LastName;
                    break;
                case 'AGE':
                    value = new Date().getFullYear() - new Date(patientdata.DateOfBirth).getFullYear();
                    break;
                case 'SEX':
                    value = patientdata.Sex == 'M'?'male':'female';
                    break;
                case 'PRONOUN':
                    value = patientdata.Sex == 'M'?"He":"She";
                    break;
            }
        }

        title = title.replace(array[item],value);
    }

    return title;
}

export function getdynamicelement(data)
{
    
}

export function getdynamicelementedit(data)
{
    var Reg = /\[(.*?)\]/g;
    var array = data.split(Reg);
    
    for(let item in array)
    {
        if(item % 2 == 1)
        {
            array[item] = '<DynamicElement caption="' + array[item] + '"></DynamicElement>';
        }
    }
    console.log('reg_example',array.join(''));
    return array.join('');
}