import * as API from '../api';

export async function getfamilyhistory(id)
{
    const result = await API.getfamilyhistory(id);
    return result;
}

export async function savefamilyhistory(data)
{
    const result = await API.savefamilyhistory(data);
    return result;
}

export async function deletefamilyhistory(id)
{
    const result = await API.deletefamilyhistory(id);
    return result;
}

export async function getRelation()
{
    const result = await API.getrelation();
    return result;
}