import * as API from '../api';

export async function getproblems(value)
{
    const result = await API.getproblems(value);
    return result;
}

export async function getcpt(value)
{
    const result = await API.getcpt(value);
    return result;
}

export async function getmacros()
{
    const result = await API.getmacros();
    return result;
}

export async function getmasterfile(sectionId)
{
    const result = await API.getmasterfile(sectionId);
    return result;
}

export async function getmasterfilefrommacro(macro)
{
    const result = await API.getmasterfilefrommacro(macro);
    return result;
}

export async function getmasternote(id)
{
    const result = await API.getmasternote(id);
    return result;
}

export async function getdiagnostic()
{
    const result = await API.getdiagnostic();
    return result;
}

export async function getmeds(data)
{
    const result = await API.getmeds(data);
    return result;
}

export async function searchmeds(value)
{
    const result = await API.searchmeds(value);
    return result;
}

export async function getalergyinfo()
{
    const result = await API.getalergyinfo();
    return result;
}

//save Ros Data function
export async function saveRosdata(data)
{
    const result = await API.saveRosdata(data);
    return result;
}

//retrieve common information in problem list#
export async function getproblemcommon()
{
    const result = await API.getcommonproblem();
    return result;
}

//retriever common information in cpt list
export async function getcptcommon()
{
    const result = await API.getcommoncpt();
    return result;
}

//save master files in PE section
export async function savemasterfile(data){
    const result = await API.savemasterfile(data);
    return result;
}

export async function getmedsinfo(data)
{
    const result = await API.getmedsinfo(data);
    return result;
}

export async function getmedsdetail(id)
{
    const result = await API.getmedsdetail(id);
    return result;
}

export async function addindication(id,type,content)
{
    const result = await API.addindication(id,type,content);
    return result;
}

export async function savedynamicelement(data)
{
    const result = await API.savedynamicelement(data);
    return result;
}

export async function getdynamicelement(id)
{
    const result = await API.getdynamicelement(id);
    return result;
}

export async function getrelateddiagnosis(id)
{
    const result = await API.getrelateddiagnosis(id);
    return result;
}

export async function addrelateddiagnosis(id,code)
{
    const result = await API.addrelateddiagnosis(id,code);
    return result;
}
