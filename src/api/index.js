import axios from 'axios';
const APIUrl = 'http://localhost:4000/api/';
export const getlocation = () => 
    axios({
        url:APIUrl + "location/get",
        method:"get",
        CORS:true,
        header:{
            "Access-Control-Allow-Origin":"*"
        } 
    })

export const getpatients = ({username})=>
    axios({
        url:APIUrl + "patient/get",
        method:"get",
        CORS:true,
        params:{
            username
        }
    })

export const setActive = ({id})=>{
    axios({
        url:APIUrl + "patient/active",
        method:"PUT",
        CORS:true,
        params:{
            id
        }
    })
}

export const getuserinfo = (id) => 
    axios({
        url:APIUrl + "patient/get/" + id,
        method:"GET",
        CORS:true,
        params:{}
    })

export const saveuserinfo = (item,data) => 

    axios({
        url:APIUrl + "patient/" + item,
        method:"POST",
        CORS:true,
        data:data
    })

export const deleteitem = (item,id) => 
    axios({
        url:APIUrl + "patient/delete/" + item + "/" + id,
        method:"DELETE",
        CORS:true,
    }) 

export const saveemergency = (id,data) => 
    axios({
        url:APIUrl + "patient/emergency/" + id,
        method:"POST",
        CORS:true,
        data:data
    })

export const getproblems = (value) => 
    axios({
        url:APIUrl + "note/getproblem",
        method:"GET",
        CORS:true,
        params:{search:value}
    })

export const gettemplate = (type) => 
    axios({
        url:APIUrl + 'note/gettemplate/' + type,
        method:"GET",
        CORS:true
    })

export const getraces = () => 
axios({
    url:APIUrl + "patient/getraces",
    method:"GET",
    CORS:true
})


export const updatedemocratics = (data,id) => 
axios({
    url:APIUrl + "patient/updateinfo/" + id,
    method:"POST",
    CORS:true,
    data:data
})

export const getcpt = (value) => axios({
    url:APIUrl + "cpt/get",
    method:"GET",
    CORS:true,
    params:{search:value}
})

export const getsocialhistory = () => axios({
    url:APIUrl + "social/get",
    method:"GET",
    CORS:true   
})

export const getmacros = () => axios({
    url:APIUrl + "problem/macro",
    method:"GET",
    CORS:true
})

export const getmasterfile = (sectionid) => axios({
    url:APIUrl + "masterfile/get/" + sectionid,
    method:"GET",
    CORS:true
})

export const getmasterfilefrommacro =  (macro) => axios({
    url:APIUrl + "masterfile/macro/" + macro,
    method:"GET",
    CORS:true
})

export const getmasternote = (id) => axios({
    url:APIUrl + "masternode/" + id,
    method:"GET",
    CORS:true
})

export const getdiagnostic = () => axios({
    url:APIUrl + "diagnostic",
    method:"GET",
    CORS:true
})

export const getmeds = (data) => axios({
    url:APIUrl + "getmeds",
    method:"GET",
    CORS:true,
    params:data
})

export const searchmeds = (value) => axios({
    url:APIUrl + "search/drugs",
    method:"GET",
    CORS:true,
    params:{search:value}
})

export const getalergyinfo = () => axios({
    url:APIUrl + "alergyinfo/get",
    method:"GET",
    CORS:true
})

export const getfamilyhistory = (id) => axios({
    url:APIUrl + "family/get/" + id,
    method:"GET",
    CORS:true
})

export const savefamilyhistory = (data) => axios({
    url:APIUrl + "family/save",
    method:"POST",
    CORS:true,
    data:data
})

export const deletefamilyhistory = (id) => axios({
    url:APIUrl + "family/delete/" + id,
    method:"DELETE",
    CORS:true
})

export const getrelation = () => axios({
    url:APIUrl + "relation/get",
    method:"GET",
    CORS:true
})

export const saveRosdata = (data) => axios({
    url:APIUrl + "master/save",
    method:"POST",
    CORS:true,
    data:data
})

export const savenotes = (data) => axios({
    url:APIUrl + "notes/save",
    method:"POST",
    CORS:true,
    data:data
})

//api to get notelist

export const getnotelist = (patientid) => axios({
    url:APIUrl + "notes/get/" + patientid,
    method:"GET",
    CORS:true
})

//api to get notedata
export const getnotedata = (noteid) => axios({
    url:APIUrl + "notes/data/" + noteid,
    method:"GET",
    CORS:true
})

//api to get common problems
export const getcommonproblem = () => axios({
    url:APIUrl + "problem/common",
    method:"GET",
    CORS:true
})

//api to get common cpts
export const getcommoncpt = () => axios({
    url:APIUrl + "cpt/common",
    method:"GET",
    CORS:true
})

//api to save master files in PE section
export const savemasterfile = (data) => axios({
    url:APIUrl + 'problem/savemasterfile',
    method:'POST',
    CORS:true,
    data:data
})

export const getmedsinfo = (data) => axios({
    url:APIUrl + 'meds/info',
    method:"GET",
    CORS:true,
    params:data
})

export const getmedsdetail = (id) => axios({
    url:APIUrl + "meds/detail/" + id,
    method:"GET",
    CORS:true
})

export const addindication = (id,type,content) => axios({
    url:APIUrl + "meds/addindication/" + type + "/" + id,
    method:'POST',
    data:{content:content},
    CORS:true
})

export const savedynamicelement = (data) => axios({
    url:APIUrl + "dynamic/add",
    method:'POST',
    data:data,
    CORS:true
})

export const getdynamicelement = (id) => axios({
    url:APIUrl + "dynamic/get/" + id,
    method:"GET",
    CORS:true
})

export const getrelateddiagnosis = (id) => axios({
    url:APIUrl + "problem/getrelated/" + id,
    method:"GET",
    CORS:true
});

export const addrelateddiagnosis = (id,code) => axios({
    url:APIUrl + "problem/addrelated/" + id,
    method:"POST",
    data:{
        code:code
    },
    CORS:true
})

export const getproviders = () => axios({
    url:APIUrl + "providers",
    method:"GET",
    CORS:true
})

export const getmacroccproblem = (idarray) => axios({
    url:APIUrl + "macro/cc/problems",
    method:"POST",
    data:{id:idarray},
    CORS:true
})

export const gethpiproblems = (idarray) => axios({
    url:APIUrl + "macro/hpi/problems",
    method:"POST",
    data:{id:idarray},
    CORS:true
})

export const getproblemsformacro = (code) => axios({
    url:APIUrl + "macro/problems",
    method:"GET",
    params:{query:code},
    CORS:true
})

export const getsymptomformacro = (code) => axios({
    url:APIUrl + "macro/symptoms",
    method:"GET",
    params:{query:code},
    CORS:true
})

export const getallergy = (content) => axios({
    url:APIUrl + "macro/allergy",
    method:"GET",
    params:{query:content},
    CORS:true
})

export const addmacro = (data) => axios({
    url:APIUrl + "macro/add",
    method:'POST',
    data:{data:data},
    CORS:true
})


export const deletemacro = (idarray) => axios({
    url:APIUrl + "macro/delete",
    method:'POST',
    data:{id:idarray},
    CORS:true
})

export const getproblemlistwithcode = (Code) => axios({
    url:APIUrl + "problem/search/Code",
    method:'GET',
    params:{data:Code},
    CORS:true
})

export const getdataforplan = () => axios({
    url:APIUrl + "macro/getdataplan",
    method:"GET",
    CORS:true
})

export const searchplan = (data) => axios({
    url:APIUrl + "plan/search",
    method:"GET",
    CORS:true,
    params:data
})